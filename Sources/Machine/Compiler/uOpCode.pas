unit uOpCode;

interface

type
  TOID = Byte;
  TFID = Byte;

const
  OID_BINARY = $01;
  OID_CALL = $02;
  OID_CLEAN = $03;
  OID_DUP = $04;
  OID_EMPTY = $05;
  OID_FIELD = $06;
  OID_INJECT = $07;
  OID_ITEM = $08;
  OID_JIT = $09;
  OID_JMP = $0A;
  OID_LOAD = $0B;
  OID_LOG = $0C;
  OID_MOVE = $0D;
  OID_NOP = $0E;
  OID_POP = $0F;
  OID_PUSH = $10;
  OID_RANGE = $11;
  OID_RETURN = $12;
  OID_RUN = $13;
  OID_SPLIT = $14;
  OID_STACK = $15;
  OID_SYNC = $16;
  OID_TAIL = $17;
  OID_THROW = $18;
  OID_UNARY = $19;

  DID_DATA = $10;
  DID_DATA_ANY = DID_DATA + $01;
  DID_DATA_ARRAY = DID_DATA + $02;
  DID_DATA_BOOL = DID_DATA + $03;
  DID_DATA_FUNC = DID_DATA + $04;
  DID_DATA_NUMERIC = DID_DATA + $05;
  DID_DATA_PIPE = DID_DATA + $06;
  DID_DATA_SET = DID_DATA + $07;
  DID_DATA_STRING = DID_DATA + $08;
  DID_DATA_STUB = DID_DATA + $09;

  DID_DATA_NUMERIC_INT = $01;
  DID_DATA_NUMERIC_BIGINT = $02;
  DID_DATA_NUMERIC_BIGFLT = $03;

  DID_DATA_ARRAY_VAR = $01;
  DID_DATA_ARRAY_EMPTY = $02;
  DID_DATA_ARRAY_STUB = $03;
  DID_DATA_ARRAY_FIXED = $04;

  DID_FUNCTION = $20;

  FID_CUSTOM = $00;

  FID_BIT = $10;
  FID_BIT_AND = FID_BIT + $1;
  FID_BIT_NOT = FID_BIT + $2;
  FID_BIT_OR = FID_BIT + $3;
  FID_BIT_SHL = FID_BIT + $4;
  FID_BIT_SHR = FID_BIT + $5;
  FID_BIT_XOR = FID_BIT + $6;

  FID_DATE = $20;
  FID_DATE_DECODE = FID_DATE + $1;
  FID_DATE_ENCODE = FID_DATE + $2;
  FID_DATE_FMT = FID_DATE + $3;
  FID_DATE_PARSE = FID_DATE + $4;

  FID_GC = $30;
  FID_GC_CLEAN = FID_GC + $1;
  FID_GC_DISABLE = FID_GC + $2;
  FID_GC_ENABLE = FID_GC + $3;

  FID_FILE = $40;
  FID_FILE_COPY = FID_FILE + $1;
  FID_FILE_GETPOS = FID_FILE + $2;
  FID_FILE_PACK = FID_FILE + $3;
  FID_FILE_READFLOAT = FID_FILE + $4;
  FID_FILE_READINT = FID_FILE + $5;
  FID_FILE_READSTRING = FID_FILE + $6;
  FID_FILE_SETPOS = FID_FILE + $7;
  FID_FILE_SIZE = FID_FILE + $8;
  FID_FILE_TRUNC = FID_FILE + $9;
  FID_FILE_UNPACK = FID_FILE + $A;
  FID_FILE_WRITEFLOAT = FID_FILE + $B;
  FID_FILE_WRITEINT = FID_FILE + $C;
  FID_FILE_WRITESTRING = FID_FILE + $D;

  FID_FS = $50;
  FID_FS_ABSPATH = FID_FS + $1;
  FID_FS_CHDIR = FID_FS + $2;
  FID_FS_CLOSE = FID_FS + $3;
  FID_FS_DELETE = FID_FS + $4;
  FID_FS_MKDIR = FID_FS + $5;
  FID_FS_OPEN = FID_FS + $6;
  FID_FS_PWD = FID_FS + $7;
  FID_FS_RMDIR = FID_FS + $8;
  FID_FS_SCAN = FID_FS + $9;
  FID_FS_STAT = FID_FS + $A;

  FID_IO = $60;
  FID_IO_CLEAR = FID_IO + $1;
  FID_IO_COLOR = FID_IO + $2;
  FID_IO_CURPOS = FID_IO + $3;
  FID_IO_GET = FID_IO + $4;
  FID_IO_GOTO = FID_IO + $5;
  FID_IO_KEY = FID_IO + $6;
  FID_IO_PAUSE = FID_IO + $7;
  FID_IO_PUT = FID_IO + $8;
  FID_IO_SIZE = FID_IO + $9;

  FID_MATH = $70;
  FID_MATH_ABS = FID_MATH + $1;
  FID_MATH_ARCCOS = FID_MATH + $2;
  FID_MATH_ARCSIN = FID_MATH + $3;
  FID_MATH_ARCTAN = FID_MATH + $4;
  FID_MATH_COS = FID_MATH + $5;
  FID_MATH_FLOOR = FID_MATH + $6;
  FID_MATH_FRAC = FID_MATH + $7;
  FID_MATH_LOG = FID_MATH + $8;
  FID_MATH_RAND = FID_MATH + $9;
  FID_MATH_SIN = FID_MATH + $A;
  FID_MATH_TAN = FID_MATH + $B;

  FID_OS = $80;
  FID_OS_ENVLIST = FID_OS + $1;
  FID_OS_EXEC = FID_OS + $2;
  FID_OS_GETENV = FID_OS + $3;
  FID_OS_LSOF = FID_OS + $4;
  FID_OS_LSOS = FID_OS + $5;
  FID_OS_PID = FID_OS + $6;
  FID_OS_SETENV = FID_OS + $7;
  FID_OS_SLEEP = FID_OS + $8;
  FID_OS_THREADID = FID_OS + $9;
  FID_OS_TIME = FID_OS + $A;

  FID_PROC = $90;
  FID_PROC_CLOSE = FID_PROC + $1;
  FID_PROC_OPEN = FID_PROC + $2;
  FID_PROC_READ = FID_PROC + $3;
  FID_PROC_START = FID_PROC + $4;
  FID_PROC_WAIT = FID_PROC + $5;
  FID_PROC_WRITE = FID_PROC + $6;

  FID_REX = $A0;
  FID_REX_FIND = FID_REX + $1;
  FID_REX_MATCH = FID_REX + $2;
  FID_REX_REPLACE = FID_REX + $3;
  FID_REX_SPLIT = FID_REX + $4;

  FID_STR = $B0;
  FID_STR_ASCII = FID_STR + $1;
  FID_STR_FMT = FID_STR + $2;
  FID_STR_LEN = FID_STR + $3;
  FID_STR_LOWER = FID_STR + $4;
  FID_STR_PARSE = FID_STR + $5;
  FID_STR_POS = FID_STR + $6;
  FID_STR_SUB = FID_STR + $7;
  FID_STR_TEXT = FID_STR + $8;
  FID_STR_UPPER = FID_STR + $9;

  FID_TCP = $C0;
  FID_TCP_ACCEPT = FID_TCP + $1;
  FID_TCP_CLOSE = FID_TCP + $2;
  FID_TCP_CONNECT = FID_TCP + $3;
  FID_TCP_LISTEN = FID_TCP + $4;
  FID_TCP_RECEIVE = FID_TCP + $5;
  FID_TCP_REMOTE = FID_TCP + $6;
  FID_TCP_SEND = FID_TCP + $7;

  FID_UDP = $D0;
  FID_UDP_BIND = FID_UDP + $1;
  FID_UDP_BROADCAST = FID_UDP + $2;
  FID_UDP_CLOSE = FID_UDP + $3;
  FID_UDP_RECEIVE = FID_UDP + $4;
  FID_UDP_SEND = FID_UDP + $5;

function FidToName(Fid: TFID): string;

implementation

function FidToName(Fid: TFID): string;
begin
  case Fid of
    FID_BIT_AND:
      Result := 'bit::and';
    FID_BIT_NOT:
      Result := 'bit::not';
    FID_BIT_OR:
      Result := 'bit::or';
    FID_BIT_SHL:
      Result := 'bit::shl';
    FID_BIT_SHR:
      Result := 'bit::shr';
    FID_BIT_XOR:
      Result := 'bit::xor';

    FID_DATE_DECODE:
      Result := 'date::decode';
    FID_DATE_ENCODE:
      Result := 'date::encode';
    FID_DATE_FMT:
      Result := 'date::fmt';
    FID_DATE_PARSE:
      Result := 'date::parse';

    FID_FILE_COPY:
      Result := 'file::copy';
    FID_FILE_GETPOS:
      Result := 'file::getpos';
    FID_FILE_READFLOAT:
      Result := 'file::readfloat';
    FID_FILE_READINT:
      Result := 'file::readint';
    FID_FILE_READSTRING:
      Result := 'file::readstring';
    FID_FILE_SETPOS:
      Result := 'file::setpos';
    FID_FILE_SIZE:
      Result := 'file::size';
    FID_FILE_TRUNC:
      Result := 'file::trunc';
    FID_FILE_WRITEFLOAT:
      Result := 'file::writefloat';
    FID_FILE_WRITEINT:
      Result := 'file::writeint';
    FID_FILE_WRITESTRING:
      Result := 'file::writestring';

    FID_FS_ABSPATH:
      Result := 'fs::abspath';
    FID_FS_CHDIR:
      Result := 'fs::chdir';
    FID_FS_CLOSE:
      Result := 'fs::close';
    FID_FS_DELETE:
      Result := 'fs::delete';
    FID_FS_MKDIR:
      Result := 'fs::mkdir';
    FID_FS_OPEN:
      Result := 'fs::open';
    FID_FS_PWD:
      Result := 'fs::pwd';
    FID_FS_RMDIR:
      Result := 'fs::rmdir';
    FID_FS_SCAN:
      Result := 'fs::scan';
    FID_FS_STAT:
      Result := 'fs::stat';

    FID_IO_CLEAR:
      Result := 'io::clear';
    FID_IO_COLOR:
      Result := 'io::color';
    FID_IO_CURPOS:
      Result := 'io::curpos';
    FID_IO_GET:
      Result := 'io::get';
    FID_IO_GOTO:
      Result := 'io::goto';
    FID_IO_KEY:
      Result := 'io::key';
    FID_IO_PAUSE:
      Result := 'io::pause';
    FID_IO_PUT:
      Result := 'io::put';
    FID_IO_SIZE:
      Result := 'io::size';

    FID_MATH_ABS:
      Result := 'math::abs';
    FID_MATH_COS:
      Result := 'math::cos';
    FID_MATH_FLOOR:
      Result := 'math::floor';
    FID_MATH_FRAC:
      Result := 'math::frac';
    FID_MATH_LOG:
      Result := 'math::log';
    FID_MATH_RAND:
      Result := 'math::rand';
    FID_MATH_SIN:
      Result := 'math::sin';

    FID_OS_EXEC:
      Result := 'os::exec';
    FID_OS_GETENV:
      Result := 'os::getenv';
    FID_OS_LSOF:
      Result := 'os::lsof';
    FID_OS_LSOS:
      Result := 'os::lsos';
    FID_OS_SETENV:
      Result := 'os::setenv';
    FID_OS_SLEEP:
      Result := 'os::sleep';
    FID_OS_TIME:
      Result := 'os::time';

    FID_REX_FIND:
      Result := 'rex::find';
    FID_REX_MATCH:
      Result := 'rex::match';
    FID_REX_REPLACE:
      Result := 'rex::replace';
    FID_REX_SPLIT:
      Result := 'rex::split';

    FID_STR_ASCII:
      Result := 'str::ascii';
    FID_STR_FMT:
      Result := 'str::fmt';
    FID_STR_LEN:
      Result := 'str::len';
    FID_STR_LOWER:
      Result := 'str::lower';
    FID_STR_PARSE:
      Result := 'str::parse';
    FID_STR_POS:
      Result := 'str::pos';
    FID_STR_SUB:
      Result := 'str::sub';
    FID_STR_TEXT:
      Result := 'str::text';
    FID_STR_UPPER:
      Result := 'str::upper';

    FID_TCP_ACCEPT:
      Result := 'tcp::accept';
    FID_TCP_CLOSE:
      Result := 'tcp::close';
    FID_TCP_CONNECT:
      Result := 'tcp::connect';
    FID_TCP_LISTEN:
      Result := 'tcp::listen';
    FID_TCP_RECEIVE:
      Result := 'tcp::receive';
    FID_TCP_REMOTE:
      Result := 'tcp::remote';
    FID_TCP_SEND:
      Result := 'tcp::send';

    FID_UDP_BIND:
      Result := 'udp::bind';
    FID_UDP_BROADCAST:
      Result := 'udp::broadcast';
    FID_UDP_CLOSE:
      Result := 'udp::close';
    FID_UDP_RECEIVE:
      Result := 'udp::receive';
    FID_UDP_SEND:
      Result := 'udp::send';
  else
    Result := '<undefined>';
  end;
end;

end.
