unit uStreamConverter;

interface

uses
  uArray, uData, uTypedStream, uNumeric;

type
  TStreamConverter = class(TConverter)
  public
    constructor Create(Stream: TTypedStream);
  private
    FStream: TTypedStream;
  public
    procedure ConvertAny(Value: TData); override;
    procedure ConvertArray(Items: array of TData; Size: Integer); override;
    procedure ConvertBool(Value: Boolean); override;
    procedure ConvertFunc(Func: TObject; Closures: TArray<TData>); override;
    procedure ConvertNumeric(Value: TNumeric); override;
    procedure ConvertPipe(Data: TData); override;
    procedure ConvertSet(Items: TArray<UInt64>); override;
    procedure ConvertString(Value: string); override;
    procedure ConvertStub; override;
  end;

implementation

uses uExceptions, uOpCode, mp_base;

{ TStreamConverter }

constructor TStreamConverter.Create(Stream: TTypedStream);
begin
  FStream := Stream;
end;

procedure TStreamConverter.ConvertAny(Value: TData);
begin
  Value.Convert(Self);
end;

procedure TStreamConverter.ConvertArray(Items: array of TData; Size: Integer);
var
  I: Integer;
begin
  with FStream do
  begin
    WriteByte(DID_DATA_ARRAY);
    if Size = 0 then
    begin
      if Items[0] <> nil then
      begin
        WriteByte(DID_DATA_ARRAY_EMPTY);
        Items[0].Convert(Self);
      end
      else
        WriteByte(DID_DATA_ARRAY_STUB);
    end else begin
      if Size < 0 then
        WriteByte(DID_DATA_ARRAY_FIXED)
      else
        WriteByte(DID_DATA_ARRAY_VAR);
      WriteInteger(Length(Items));
      for I := 0 to High(Items) do
        Items[I].Convert(Self);
    end;
  end;
end;

procedure TStreamConverter.ConvertBool(Value: Boolean);
begin
  FStream.WriteByte(DID_DATA_BOOL);
  FStream.WriteByte(Byte(Value));
end;

procedure TStreamConverter.ConvertFunc(Func: TObject; Closures: TArray<TData>);
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

procedure TStreamConverter.ConvertNumeric(Value: TNumeric);
begin
  FStream.WriteByte(DID_DATA_NUMERIC);
  Value.Serialize(FStream);
end;

procedure TStreamConverter.ConvertPipe(Data: TData);
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

procedure TStreamConverter.ConvertSet(Items: TArray<UInt64>);
var
  I: Integer;
begin
  with FStream do
  begin
    WriteByte(DID_DATA_SET);
    WriteInteger(Length(Items));
    for I := 0 to High(Items) do
      Write(Items[I], 8);
  end;
end;

procedure TStreamConverter.ConvertString(Value: string);
begin
  FStream.WriteByte(DID_DATA_STRING);
  FStream.WriteString(Value);
end;

procedure TStreamConverter.ConvertStub;
begin
  FStream.WriteByte(DID_DATA_STUB);
end;

end.
