unit uUnitGC;

interface

uses
  uUnit;

type
  TUnitGC = class(TUnit)
  public
    constructor Create;
  end;

  { *
    @descr Garbage Collector information and controll routines.
    @types _memstat: (base: int, peak: int)
    * }

implementation

uses uFuncGCDisable, uFuncGCEnable, uFuncGCClean;

{ TUnitGC }

constructor TUnitGC.Create;
begin
  inherited Create('<gc>');

  AddValue('clean', MakeFunc(TFuncGCClean.Create, [], []));
  AddValue('disable', MakeFunc(TFuncGCDisable.Create, [], []));
  AddValue('enable', MakeFunc(TFuncGCEnable.Create, [], []));
end;

end.
