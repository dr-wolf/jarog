unit uStringSet;

interface

uses
  uArray;

type
  TStringSet = class(TObject)
  public
    constructor Create; overload;
    constructor Create(Items: array of string); overload;
    destructor Destroy; override;
  private
    FItems: TArray<string>;
    function GetItem(I: Integer): string;
  public
    property Item[I: Integer]: string read GetItem; default;
    function AsArray: TArray<string>;
    function IndexOf(const Item: string): Integer;
    function Count: Integer;
    function Add(const Item: string): Integer;
    procedure Delete(const Item: string);
    procedure Clear;
  end;

implementation

{ TStringSet }

constructor TStringSet.Create;
begin
  SetLength(FItems, 0);
end;

constructor TStringSet.Create(Items: array of string);
var
  I: Integer;
begin
  SetLength(FItems, 0);
  for I := 0 to High(Items) do
    Add(Items[I]);
end;

destructor TStringSet.Destroy;
begin
  FItems := nil;
  inherited;
end;

function TStringSet.Add(const Item: string): Integer;
begin
  Result := IndexOf(Item);
  if Result < 0 then
  begin
    SetLength(FItems, Length(FItems) + 1);
    FItems[High(FItems)] := Item;
    Result := High(FItems);
  end;
end;

function TStringSet.AsArray: TArray<string>;
begin
  Result := Copy(FItems, 0);
end;

procedure TStringSet.Clear;
begin
  SetLength(FItems, 0);
end;

function TStringSet.Count: Integer;
begin
  Result := Length(FItems);
end;

procedure TStringSet.Delete(const Item: string);
var
  I, n: Integer;
begin
  n := IndexOf(Item);
  if n >= 0 then
  begin
    for I := n to High(FItems) - 1 do
      FItems[I] := FItems[I + 1];
    SetLength(FItems, Length(FItems) - 1);
  end;
end;

function TStringSet.GetItem(I: Integer): string;
begin
  Result := FItems[I];
end;

function TStringSet.IndexOf(const Item: string): Integer;
begin
  Result := High(FItems);
  while (Result >= 0) and (FItems[Result] <> Item) do
    Dec(Result);
end;

end.
