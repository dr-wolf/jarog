unit uFuncDateParse;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncDateParse = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_TEXT = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Parses date and time string into unix timestamp.
    @type func(text: string -> int)
    @in text string # Date and time string in YYYY/MM/DD HH:MM:SS format
    @out - int # Unix timestamp
    * }

implementation

uses SysUtils, uDataNumeric, uDataString, uOpCode;

{ TFuncDateParse }

constructor TFuncDateParse.Create;
begin
  inherited Create([MakeString], MakeInt);
  FFuncUid := FID_DATE_PARSE;
end;

procedure TFuncDateParse.Execute(Context: TFuncContext);
begin
  Return(TDataNumeric.Create(Round((StrToDateTime(AsString(Context.Data[P_TEXT])) - 25569) * 86400)));
end;

end.
