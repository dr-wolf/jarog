unit uExpressionTest;

interface

uses
  uArray, uExpression, uData, uType, uValue, uOp, uDataSection;

type
  TExpressionTest = class(TExpression)
  public
    constructor Create(Data: TDataSection; Expression: TExpression; TypeInfo: TType);
    destructor Destroy; override;
  private
    FDefault: TData;
    FExpression: TExpression;
  public
    function Compile: TArray<TOp>; override;
    function Eval: IValue; override;
  end;

implementation

uses uExceptions, uTypeFactory, uTypeVoid, uDataBool, uOpMove, uOpLoad;

{ TExpressionTest }

constructor TExpressionTest.Create(Data: TDataSection; Expression: TExpression; TypeInfo: TType);
begin
  if Expression.ResultType is TTypeVoid then
    raise ECodeException.Create(E_NO_RESULT);
  inherited Create(Data);
  FExpression := Expression;
  if FExpression.Group = egStatic then
    FGroup := egStatic
  else
    FGroup := egMixed;
  FResultType := TypeFactory.GetBool;
  FDefault := TypeInfo.Default;
end;

destructor TExpressionTest.Destroy;
begin
  FDefault.Dispose;
  FExpression.Free;
  inherited;
end;

function TExpressionTest.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FExpression.Compile);
  AppendOp(Result, [TOpLoad.Create(FData.Keep(FDefault), False), TOpMove.Create(True, True)]);
end;

function TExpressionTest.Eval: IValue;
begin
  try
    FDefault.Assign(FExpression.Eval.Data);
    Result := TValue.Create(FResultType, TDataBool.Create(True));
  except
    Result := TValue.Create(FResultType, TDataBool.Create(False));
  end;
end;

end.
