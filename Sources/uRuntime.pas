unit uRuntime;

interface

uses
  Classes, uConfig, uApp, uCoreContext, uIOProvider;

type
  TRuntime = class(TObject)
  public
    constructor Create(Config: TConfig; IOProvider: TIOProvider);
    destructor Destroy; override;
  private
    FCoreContext: TCoreContext;
    FApp: TApp;
  public
    procedure LoadFromStream(Stream: TStream);
    procedure Run;
  end;

implementation

uses uExecFile, uMachine, mp_base;

{ TRuntime }

constructor TRuntime.Create(Config: TConfig; IOProvider: TIOProvider);
begin
  FCoreContext := TCoreContext.Create(Config, IOProvider);
  FApp := TApp.Create;
end;

destructor TRuntime.Destroy;
begin
  FApp.Free;
  FCoreContext.Free;
  inherited;
end;

procedure TRuntime.LoadFromStream(Stream: TStream);
var
  ExecFile: TExecFile;
begin
  ExecFile := TExecFile.Create(FCoreContext);
  try
    ExecFile.LoadFromStream(Stream);
    FApp.Load(ExecFile);
  finally
    ExecFile.Free;
  end;
end;

procedure TRuntime.Run;
begin
  with TMachine.Create(FApp, FCoreContext) do
    try
      Run;
    finally
      Free;
    end;
end;

end.
