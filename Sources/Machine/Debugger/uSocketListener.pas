unit uSocketListener;

interface

uses
  Classes, SynSock, BlckSock;

type
  TSocketListener = class(TObject)
  public
    constructor Create(Port: Integer);
    destructor Destroy; override;
  private
    FListeningSocket: TTCPBlockSocket;
    FClientSocket: TTCPBlockSocket;
  public
    function ReadCommand: Byte;
    function ReadIndex: Integer;
    procedure Send(Stream: TStream);
    procedure WaitForConnection;
    procedure DropConnection;
  end;

implementation

uses SysUtils;

{ TSocketListener }

constructor TSocketListener.Create(Port: Integer);
begin
  FListeningSocket := TTCPBlockSocket.Create;
  with FListeningSocket do
  begin
    CreateSocket;
    SetLinger(True, 10000);
    Bind('0.0.0.0', IntToStr(Port));
    Listen;
  end;
  FClientSocket := TTCPBlockSocket.Create;
end;

destructor TSocketListener.Destroy;
begin
  FListeningSocket.CloseSocket;
  FListeningSocket.Free;
  FClientSocket.CloseSocket;
  FClientSocket.Free;
  inherited;
end;

procedure TSocketListener.DropConnection;
begin
  FClientSocket.CloseSocket;
end;

function TSocketListener.ReadCommand: Byte;
begin
  Result := FClientSocket.RecvByte(-1);
end;

function TSocketListener.ReadIndex: Integer;
begin
  Result := FClientSocket.RecvInteger(-1);
end;

procedure TSocketListener.Send(Stream: TStream);
begin
  Stream.Seek(0, soFromBeginning);
  FClientSocket.SendStream(Stream);
end;

procedure TSocketListener.WaitForConnection;
begin
  if FClientSocket.Socket = INVALID_SOCKET then
    while True do
      if FListeningSocket.CanRead(1000) then
      begin
        FClientSocket.Socket := FListeningSocket.Accept;
        FClientSocket.GetSins;
        Break;
      end;
end;

end.
