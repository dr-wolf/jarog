unit uFuncStrText;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncStrText = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_BYTES = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Builds a string from a list of int ascii codes. Opposite function to str::ascii.
    @type func(bytes: list(int) -> string)
    @in bytes list(int) # List of ascii codes
    @out - string # String
    * }

implementation

uses uDataArray, uDataNumeric, uDataString, uOpCode;

{ TFuncStrText }

constructor TFuncStrText.Create;
begin
  inherited Create([MakeList(MakeInt)], MakeString);
  FFuncUid := FID_STR_TEXT;
end;

procedure TFuncStrText.Execute(Context: TFuncContext);
var
  a: TDataArray;
  i: Integer;
  s: string;
begin
  s := '';
  a := Context.Data[P_BYTES] as TDataArray;
  for i := 0 to a.Count - 1 do
    s := s + Chr(AsInt(a.Item(i)));
  Return(TDataString.Create(s));
end;

end.
