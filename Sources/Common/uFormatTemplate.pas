unit uFormatTemplate;

interface

type
  TOutputFormat = (ofDecimal, ofScientific, ofFixed, ofString, ofHexadecimal);

type
  TFormatTemplate = record
    ItemIndex: Integer;
    Width: Word;
    Precision: Word;
    RightPadded: Boolean;
    Format: TOutputFormat;
  end;

const
  FORMAT_SYMBOLS: array [TOutputFormat] of Char = ('d', 'e', 'f', 's', 'x');
  FORMAT_SYMBOLS_SET: set of AnsiChar = ['d', 'e', 'f', 's', 'x'];


function DetectFormat(Symbol: Char): TOutputFormat;
function GenericFormat(FormatTemplate: TFormatTemplate): string;

implementation

uses SysUtils;

function DetectFormat(Symbol: Char): TOutputFormat;
begin
  case Symbol of
    'd':
      Result := ofDecimal;
    'e':
      Result := ofScientific;
    'f':
      Result := ofFixed;
    's':
      Result := ofString;
    'x':
      Result := ofHexadecimal;
  else
    Result := ofDecimal;
  end;
end;

function GenericFormat(FormatTemplate: TFormatTemplate): string;
begin
  Result := '%';
  if FormatTemplate.RightPadded then
    Result := Result + '-';
  Result := Result + IntToStr(FormatTemplate.Width) + FORMAT_SYMBOLS[FormatTemplate.Format];
end;

end.
