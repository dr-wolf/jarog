unit uFuncMathArcSin;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncMathArcSin = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_VALUE = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Retruns angle of the given sinus.
    @type func(value: float -> float)
    @in value float # Sinus of the angle
    @out - float # Angle in radians
    * }

implementation

uses uDataNumeric, uOpCode, uBigFloat, mp_types, mp_real;

{ TFuncMathArcSin }

constructor TFuncMathArcSin.Create;
begin
  inherited Create([MakeFloat], MakeFloat);
  FFuncUid := FID_MATH_ARCSIN;
end;

procedure TFuncMathArcSin.Execute(Context: TFuncContext);
var
  v, r: mp_float;
begin
  mpf_init(r);
  v := AsNumeric(Context.Data[P_VALUE]).ToFloat;
  mpf_arcsin(v, r);
  mpf_clear(v);
  Return(TDataNumeric.Create(TBigFloat.Create(r)));
end;

end.
