unit uDataPool;

interface

uses
  uArray;

type
  TGCLevel = (lNonDisposable, lPreDisposable, lDisposable);

type
  TPooledObject = class(TObject)
  protected
    FGCLevel: TGCLevel;
  public
    class function NewInstance: TObject; override;
    procedure FreeInstance; override;
    procedure SetGCLevel(Level: TGCLevel; Recursive: Boolean; Forced: Boolean = False); virtual;
  end;

type
  TDataPool = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private type
    TInstance = record
      ThreadId: Cardinal;
      Instance: TDataPool;
    end;
  private
    class var FInstances: TArray<TInstance>;
    class var FFreezed: TArray<TDataPool>;
  private
    FEnabled: Boolean;
    FRegistry: TArray<TPooledObject>;
    FRegistrySize: Integer;
    FPosition: Integer;
    FCleaned: Integer;
    procedure Store(NewObject: TPooledObject);
    class function InstanceId(ThreadId: Cardinal): Integer;
  public
    property Enabled: Boolean read FEnabled write FEnabled;
    procedure CleanUp(Forced: Boolean);
    procedure Reset;
    class function GetInstance: TDataPool; static;
    class procedure Initialize; static;
    class procedure FreezeInstance; static;
    class procedure Finalize; static;
  end;

implementation

uses Classes;

const BLOCK_SIZE = 1024;

{ TPoolObject }

procedure TPooledObject.FreeInstance;
begin
  inherited;
end;

class function TPooledObject.NewInstance: TObject;
begin
  Result := inherited NewInstance;
  TDataPool.GetInstance.Store(Result as TPooledObject);
end;

procedure TPooledObject.SetGCLevel(Level: TGCLevel; Recursive: Boolean; Forced: Boolean);
begin
  if Forced or (FGCLevel <> lNonDisposable) then
    FGCLevel := Level;
end;

{ TDataPool }

constructor TDataPool.Create;
begin
  FRegistrySize := BLOCK_SIZE div 2;
  SetLength(FRegistry, FRegistrySize);
  FEnabled := True;
end;

destructor TDataPool.Destroy;
var
  I: Integer;
begin
  for I := FPosition - 1 downto 0 do
    FRegistry[I].Free;
  FRegistry := nil;
  inherited;
end;

procedure TDataPool.CleanUp(Forced: Boolean);
var
  I: Integer;
begin
  if Forced or (FEnabled and (Random(10000) = 0)) then
    FCleaned := 0;

  I := FCleaned;
  while I < FPosition do
  begin
    if FRegistry[I].FGCLevel = lDisposable then
    begin
      FRegistry[I].Free;
      FRegistry[I] := FRegistry[FPosition - 1];
      Dec(FPosition);
    end
    else
      Inc(I);
  end;

  FCleaned := FPosition;
end;

procedure TDataPool.Reset;
begin
  if FEnabled then
    FCleaned := 0;
end;

procedure TDataPool.Store(NewObject: TPooledObject);
begin
  FRegistry[FPosition] := NewObject;
  Inc(FPosition);
  if FPosition >= FRegistrySize then
  begin
    Inc(FRegistrySize, BLOCK_SIZE);
    SetLength(FRegistry, FRegistrySize);
  end;
end;

class function TDataPool.GetInstance: TDataPool;
var
  ThreadId: Cardinal;
  I: Integer;
begin
  ThreadId := TThread.CurrentThread.ThreadId;
  I := InstanceId(ThreadId);
  if I > High(FInstances) then
  begin
    SetLength(FInstances, I + 1);
    FInstances[I].ThreadId := ThreadId;
    FInstances[I].Instance := TDataPool.Create;
  end;
  Result := FInstances[I].Instance;
end;

class procedure TDataPool.Initialize;
begin
  SetLength(FInstances, 0);
end;

class function TDataPool.InstanceId(ThreadId: Cardinal): Integer;
begin
  Result := 0;
  while (Result <= High(FInstances)) and (FInstances[Result].ThreadId <> ThreadId) do
    Inc(Result);
end;

class procedure TDataPool.Finalize;
var
  I: Integer;
begin
  for I := 0 to High(FInstances) do
    FInstances[I].Instance.Free;
  for I := 0 to High(FFreezed) do
    FFreezed[I].Free;
end;

class procedure TDataPool.FreezeInstance;
var
  I: Integer;
begin
  I := InstanceId(TThread.CurrentThread.ThreadId);
  if I <= High(FInstances) then
  begin
    SetLength(FFreezed, Length(FFreezed) + 1);
    FFreezed[High(FFreezed)] := FInstances[I].Instance;
    if I < High(FInstances) then
      FInstances[I] := FInstances[High(FInstances)];
    SetLength(FInstances, Length(FInstances) - 1);
  end;
end;

end.
