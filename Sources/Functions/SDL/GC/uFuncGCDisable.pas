unit uFuncGCDisable;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncGCDisable = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Disables GC for current thread.
    @type func()
    * }

implementation

uses uDataPool, uOpCode;

{ TFuncGCDisable }

constructor TFuncGCDisable.Create;
begin
  inherited Create([]);
  FFuncUid := FID_GC_ENABLE;
end;

procedure TFuncGCDisable.Execute(Context: TFuncContext);
begin
  TDataPool.GetInstance.Enabled := False;
end;

end.
