unit ProcessPipes;

interface

uses
  Types, SysUtils, Classes;

type
  EPipeError = class(EStreamError);
  EPipeSeek = class(EPipeError);
  EPipeCreation = class(EPipeError);

  TOutputPipeStream = class(THandleStream)
  private
    function GetNumBytesAvailable: DWord;
  public
    property NumBytesAvailable: DWord read GetNumBytesAvailable;
    function Write(const Buffer; Count: Longint): Longint; override;
    function Seek(const Offset: Int64; Origin: TSeekOrigin): Int64; override;
  end;

  TInputPipeStream = Class(THandleStream)
  public
    function Seek(const Offset: Int64; Origin: TSeekOrigin): Int64; override;
    function Read(var Buffer; Count: Longint): Longint; Override;
  end;

implementation

{$IFNDEF UNIX}

uses
  Windows;

{$ENDIF}

{ TOutputPipeStream }

function TOutputPipeStream.GetNumBytesAvailable: DWord;
begin
{$IFNDEF UNIX}
  if not PeekNamedPipe(Handle, nil, 0, nil, @Result, nil) then
    Result := 0;
{$ENDIF}
end;

function TOutputPipeStream.Write(Const Buffer; Count: Longint): Longint;
begin
  raise EStreamError.Create('Cannot write to this stream, not implemented');
end;

function TOutputPipeStream.Seek(const Offset: Int64; Origin: TSeekOrigin): Int64;
begin
  raise EStreamError.Create('Cannot seek stdout stream');
end;

{ TInputPipeStream }

function TInputPipeStream.Read(Var Buffer; Count: Longint): Longint;
begin
  raise EStreamError.Create('Cannot read from stdin stream');
end;

function TInputPipeStream.Seek(const Offset: Int64; Origin: TSeekOrigin): Int64;
begin
  raise EStreamError.Create('Cannot seek stdin stream');
end;

end.
