unit uFuncMathFloor;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncMathFloor = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_VALUE = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Returns the largest integer less than or equal to a given number.
    @type func(value: float -> int)
    @in value float # Initial number
    @out - int # Result
    * }

implementation

uses uDataNumeric, uOpCode;

{ TFuncMathFloor }

constructor TFuncMathFloor.Create;
begin
  inherited Create([MakeFloat], MakeInt);
  FFuncUid := FID_MATH_FLOOR;
end;

procedure TFuncMathFloor.Execute(Context: TFuncContext);
begin
  Return(TDataNumeric.Create(AsNumeric(Context.Data[P_VALUE]).Floor));
end;

end.
