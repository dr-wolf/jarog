unit uUnitRex;

interface

uses
  uUnit;

type
  TUnitRex = class(TUnit)
  public
    constructor Create;
  end;

  { *
    @descr Regular expressions.
    @types _match: (match: string, pos: int, length: int)
    @const case_insensitive # 1
    @const cyrillic # 2
    @const newline # 4
    @const greed # 8
    @const multiline # 16
    @const extended # 32
    * }

implementation

uses uTypeFactory, uValue, uDataNumeric, uFuncRex, uFuncRexMatch, uFuncRexFind, uFuncRexReplace,
  uFuncRexSplit;

{ TUnitRex }

constructor TUnitRex.Create;
begin
  inherited Create('<rex>');

  AddType('_match', TypeFactory.GetPredefined(pdtRex));

  AddValue('case_insensitive', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(REX_CASEINSENSITIVE)));
  AddValue('cyrillic', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(REX_CYRILLIC)));
  AddValue('newline', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(REX_NEWLINE)));
  AddValue('greed', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(REX_GREED)));
  AddValue('multiline', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(REX_MULTILINE)));
  AddValue('extended', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(REX_EXTENDED)));

  AddValue('match', MakeFunc(TFuncRexMatch.Create, [TypeFactory.GetString, TypeFactory.GetString, TypeFactory.GetInt],
    ['regex', 'text', 'modifiers'], TypeFactory.GetBool));
  AddValue('find', MakeFunc(TFuncRexFind.Create, [TypeFactory.GetString, TypeFactory.GetString, TypeFactory.GetInt],
    ['regex', 'text', 'modifiers'], TypeFactory.GetList(TypeFactory.GetPredefined(pdtRex))));
  AddValue('replace', MakeFunc(TFuncRexReplace.Create, [TypeFactory.GetString, TypeFactory.GetString,
    TypeFactory.GetString, TypeFactory.GetInt], ['regex', 'text', 'replacement', 'modifiers'], TypeFactory.GetString));
  AddValue('split', MakeFunc(TFuncRexSplit.Create, [TypeFactory.GetString, TypeFactory.GetString, TypeFactory.GetInt],
    ['regex', 'text', 'modifiers'], TypeFactory.GetList(TypeFactory.GetString)));
end;

end.
