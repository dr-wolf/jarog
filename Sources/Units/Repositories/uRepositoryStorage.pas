unit uRepositoryStorage;

interface

uses
  uSocketRepository, uFileRepository, uProcessRepository, BlckSock;

type
  TRepositoryStorage = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FTCPSocketRepository: TSocketRepository<TTCPBlockSocket>;
    FUDPSocketRepository: TSocketRepository<TUDPBlockSocket>;
    FFileRepository: TFileRepository;
    FProcessRepository: TProcessRepository;
    function GetTCPSocketRepository: TSocketRepository<TTCPBlockSocket>;
    function GetUDPSocketRepository: TSocketRepository<TUDPBlockSocket>;
    function GetFileRepository: TFileRepository;
    function GetProcessRepository: TProcessRepository;
  public
    property TCPSocks: TSocketRepository<TTCPBlockSocket> read GetTCPSocketRepository;
    property UDPSocks: TSocketRepository<TUDPBlockSocket> read GetUDPSocketRepository;
    property Files: TFileRepository read GetFileRepository;
    property Processes: TProcessRepository read GetProcessRepository;
  end;

implementation

{ TRepositoryStorage }

constructor TRepositoryStorage.Create;
begin
  FTCPSocketRepository := nil;
  FUDPSocketRepository := nil;
  FFileRepository := nil;
  FProcessRepository := nil;
end;

destructor TRepositoryStorage.Destroy;
begin
  if FTCPSocketRepository <> nil then
    FTCPSocketRepository.Free;
  if FUDPSocketRepository <> nil then
    FUDPSocketRepository.Free;
  if FFileRepository <> nil then
    FFileRepository.Free;
  if FProcessRepository <> nil then
    FProcessRepository.Free;
  inherited;
end;

function TRepositoryStorage.GetProcessRepository: TProcessRepository;
begin
  if FProcessRepository = nil then
    FProcessRepository := TProcessRepository.Create;
  Result := FProcessRepository;
end;

function TRepositoryStorage.GetFileRepository: TFileRepository;
begin
  if FFileRepository = nil then
    FFileRepository := TFileRepository.Create;
  Result := FFileRepository;
end;

function TRepositoryStorage.GetTCPSocketRepository: TSocketRepository<TTCPBlockSocket>;
begin
  if FTCPSocketRepository = nil then
    FTCPSocketRepository := TSocketRepository<TTCPBlockSocket>.Create;
  Result := FTCPSocketRepository;
end;

function TRepositoryStorage.GetUDPSocketRepository: TSocketRepository<TUDPBlockSocket>;
begin
  if FUDPSocketRepository = nil then
    FUDPSocketRepository := TSocketRepository<TUDPBlockSocket>.Create;
  Result := FUDPSocketRepository;
end;

end.
