unit uFuncStrLen;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncStrLen = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_TEXT = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Retruns length of a string.
    @type func(text: string -> int)
    @in text string # String value
    @out - int # String length
    * }

implementation

uses uDataNumeric, uDataString, uOpCode;

{ TFuncStrLen }

constructor TFuncStrLen.Create;
begin
  inherited Create([MakeString], MakeInt);
  FFuncUid := FID_STR_LEN;
end;

procedure TFuncStrLen.Execute(Context: TFuncContext);
begin
  Return(TDataNumeric.Create(Length(AsString(Context.Data[P_TEXT]))));
end;

end.
