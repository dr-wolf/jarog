unit uFuncOSEnvList;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncOSEnvList = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Returns list of all environment variable names.
    @type func(-> list(string))
    @out - list(string) # List of environment variable names
    * }

implementation

uses uArray, uData, uDataString, uDataArray, uOpCode;

{ TFuncOSEnvList }

constructor TFuncOSEnvList.Create;
begin
  inherited Create([], MakeList(MakeString));
  FFuncUid := FID_OS_ENVLIST;
end;

procedure TFuncOSEnvList.Execute(Context: TFuncContext);
var
  d: TArray<TData>;
  k: TArray<string>;
  i: Integer;
begin
  k := FCoreContext.Env.Variables.Keys;
  SetLength(d, Length(k));
  for i := 0 to High(k) do
    d[i] := TDataString.Create(k[i]);
  Return(TDataArray.Create(d, False));
end;

end.
