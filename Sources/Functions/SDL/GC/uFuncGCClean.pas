unit uFuncGCClean;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncGCClean = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Runs GS immediately.
    @type func()
    * }

implementation

uses uDataPool, uOpCode;

{ TFuncGCClean }

constructor TFuncGCClean.Create;
begin
  inherited Create([]);
  FFuncUid := FID_GC_CLEAN;
end;

procedure TFuncGCClean.Execute(Context: TFuncContext);
begin
  TDataPool.GetInstance.CleanUp(True);
end;

end.
