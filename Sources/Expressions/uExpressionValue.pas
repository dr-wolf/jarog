unit uExpressionValue;

interface

uses
  uArray, uExpression, uValue, uOp, uDataSection;

type
  TExpressionValue = class(TExpression)
  public
    constructor Create(Data: TDataSection; Value: IValue);
  private
    FValue: IValue;
  public
    function Eval: IValue; override;
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uOpLoad;

{ TExpressionValue }

constructor TExpressionValue.Create(Data: TDataSection; Value: IValue);
begin
  inherited Create(Data);
  FValue := Value;
  FResultType := FValue.TypeInfo;
  FGroup := egStatic;
end;

function TExpressionValue.Compile: TArray<TOp>;
begin
  SetLength(Result, 1);
  Result[0] := TOpLoad.Create(FData.Keep(FValue.Data), False);
end;

function TExpressionValue.Eval: IValue;
begin
  Result := FValue;
end;

end.
