unit uFuncContext;

interface

uses
  uThreadSafe, uArray, uData, uExecFile, uTypedStream;

type
  TFuncContext = class(TThreadObject)
  public
    constructor Create;
    destructor Destroy; override;
  private type
    TPersistencyLevel = (plLocal, plClosure, plGlobal);

    TDataItem = record
      Persistency: TPersistencyLevel;
      Data: TData;
    end;
  private
    FThreadId: Cardinal;
    FSelfId: Integer;
    FVariables: TArray<TDataItem>;
    function GetData(Id: Integer): TData;
  public
    property ThreadId: Cardinal read FThreadId;
    property Data[Id: Integer]: TData read GetData;
    function Keep(Data: TData; Persist: Boolean; IsSelf: Boolean = False): Integer;
    function Recreate(Caller: TData; ThreadId: Cardinal = 0): TFuncContext;
    procedure DisposeData;
    procedure Replace(Id: Integer; Data: TData);
    procedure Save(Stream: TTypedStream; ExecFile: TExecFile);
    procedure Load(Stream: TTypedStream; ExecFile: TExecFile);
  end;

implementation

uses SyncObjs, uStringConverter;

{ TFuncContext }

constructor TFuncContext.Create;
begin
  inherited Create;
  SetLength(FVariables, 0);
  FSelfId := -1;
end;

destructor TFuncContext.Destroy;
begin
  FVariables := nil;
  inherited;
end;

function TFuncContext.GetData(Id: Integer): TData;
begin
  Result := FVariables[Id].Data;
end;

function TFuncContext.Recreate(Caller: TData; ThreadId: Cardinal = 0): TFuncContext;
var
  I: Integer;
begin
  Lock;
  Result := TFuncContext.Create;
  if ThreadId <> 0 then
    Result.FThreadId := ThreadId;
  SetLength(Result.FVariables, Length(FVariables));
  for I := 0 to High(FVariables) do
    if FVariables[I].Persistency = plGlobal then
      Result.FVariables[I] := FVariables[I]
    else
    begin
      Result.FVariables[I].Data := FVariables[I].Data.Dup;
      Result.FVariables[I].Persistency := FVariables[I].Persistency;
    end;
  if FSelfId > -1 then
  begin
    Result.FSelfId := FSelfId;
    Result.Replace(FSelfId, Caller);
  end;
  Unlock;
end;

function TFuncContext.Keep(Data: TData; Persist: Boolean; IsSelf: Boolean = False): Integer;
begin
  Result := 0;
  while (Result < Length(FVariables)) and (FVariables[Result].Data <> Data) do
    Inc(Result);
  if Result = Length(FVariables) then
  begin
    SetLength(FVariables, Result + 1);
    FVariables[Result].Data := Data;
    if Persist then
      FVariables[Result].Persistency := plGlobal
    else
      FVariables[Result].Persistency := plLocal;
  end else begin
    if Persist and (FVariables[Result].Persistency = plLocal) then
      FVariables[Result].Persistency := plClosure;
  end;
  if IsSelf then
    FSelfId := Result;
end;

procedure TFuncContext.DisposeData;
var
  I: Integer;
begin
  for I := 0 to High(FVariables) do
    if FVariables[I].Persistency = plLocal then
      FVariables[I].Data.Dispose;
end;

procedure TFuncContext.Load(Stream: TTypedStream; ExecFile: TExecFile);
var
  I: Integer;
begin
  with Stream do
  begin
    FSelfId := ReadInteger;
    SetLength(FVariables, ReadInteger);
    for I := 0 to High(FVariables) do
    begin
      FVariables[I].Persistency := TPersistencyLevel(ReadByte);
      FVariables[I].Data := ExecFile.Load(ReadInteger) as TData;
    end;
  end;
end;

procedure TFuncContext.Save(Stream: TTypedStream; ExecFile: TExecFile);
var
  I: Integer;
begin
  with Stream do
  begin           //TODO: no nil ckeck
    if ExecFile <> nil then
      WriteInteger(FSelfId);
    WriteInteger(Length(FVariables));
    for I := 0 to High(FVariables) do
      if ExecFile <> nil then
      begin
        WriteByte(Byte(FVariables[I].Persistency));
        WriteInteger(ExecFile.Save(FVariables[I].Data as TData, (FVariables[I].Data as TData).Serialize));
      end
      else
        WriteString(LogData(FVariables[I].Data));
  end;
end;

procedure TFuncContext.Replace(Id: Integer; Data: TData);
begin
  FVariables[Id].Data := Data;
end;

end.
