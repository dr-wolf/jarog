unit uOpMove;

interface

uses
  uOp, uBytecode;

type
  TOpMove = class(TOp)
  public
    constructor Create(Test, Report: Boolean);
  private
    FTest: Boolean;
    FReport: Boolean;
  public
    procedure Serialize(var Code: TBytecode); override;
  end;

implementation

uses uOpCode;

{ TOpMove }

constructor TOpMove.Create(Test, Report: Boolean);
begin
  inherited Create(OID_MOVE);
  FTest := Test;
  FReport := Report;
end;

procedure TOpMove.Serialize(var Code: TBytecode);
begin
  Code.Op := CompileOp(FTest, FReport);
end;

end.
