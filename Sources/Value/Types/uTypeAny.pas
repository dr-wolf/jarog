unit uTypeAny;

interface

uses
  uOperations, uType, uData;

type
  TTypeAny = class(TType)
  public
    function Apply(Op: TBinaryOp; T: TType): TType; override;
    function Compatible(T: TType; StrictCompat: Boolean = False): Boolean; override;
    function Pipeable: Boolean; override;
    function Default: TData; override;
  end;

implementation

uses uExceptions, uTypeFactory, uTypeVoid, uDataAny;

{ TTypeAny }

function TTypeAny.Apply(Op: TBinaryOp; T: TType): TType;
begin
  inherited;
  if not(Op in [boEqual, boNotEqual]) then
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, FHash);
  Result := TypeFactory.GetBool;
end;

function TTypeAny.Compatible(T: TType; StrictCompat: Boolean = False): Boolean;
begin
  if StrictCompat then
    Result := T is TTypeAny
  else
    Result := not(T is TTypeVoid);
end;

function TTypeAny.Default: TData;
begin
  Result := TDataAny.Create;
end;

function TTypeAny.Pipeable: Boolean;
begin
  Result := False;
end;

end.
