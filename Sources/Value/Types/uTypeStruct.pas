unit uTypeStruct;

interface

uses
  uArray, uDict, uOperations, uType, uData;

type
  TTypeStruct = class(TType)
  public
    constructor Create(const Hash: string; Fields: array of TType; Names: array of string);
    destructor Destroy; override;
  private
    FFieldIndices: TDict<Integer>;
    FFields: TArray<TType>;
    FNameless: Boolean;
  public
    property Nameless: Boolean read FNameless;
    function Apply(Op: TBinaryOp; T: TType): TType; override;
    function Pipeable: Boolean; override;
    function Compatible(T: TType; StrictCompat: Boolean = False): Boolean; override;
    function Default: TData; override;
    function Fields: TArray<string>;
    function Size: Integer;
    function IndexOf(const Field: string): Integer;
    function NestedType(Field: string = ''): TType; override;
    function FieldIndex(const Field: string): Integer;
  end;

implementation

uses SysUtils, uExceptions, uTypeFactory, uDataArray;

{ TTypeStruct }

constructor TTypeStruct.Create(const Hash: string; Fields: array of TType; Names: array of string);
var
  i: Integer;
begin
  inherited Create(Hash);
  if (Length(Fields) <> Length(Names)) and (Length(Names) > 0) then
    raise ECodeException.Create(E_STRUCT_MIXED);
  FNameless := (Length(Names) = 0);
  SetLength(FFields, Length(Fields));
  for i := 0 to High(FFields) do
    FFields[i] := Fields[i];
  FFieldIndices := TDict<Integer>.Create;
  for i := 0 to High(Fields) do
    if FNameless then
      FFieldIndices.Put('_' + IntToStr(i), i)
    else
      FFieldIndices.Put(Names[i], i);
end;

destructor TTypeStruct.Destroy;
begin
  FFields := nil;
  FFieldIndices.Free;
  inherited;
end;

function TTypeStruct.Apply(Op: TBinaryOp; T: TType): TType;
begin
  inherited;
  if not(Op in [boEqual, boNotEqual]) then
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, FHash);
  Result := TypeFactory.GetBool;
end;

function TTypeStruct.Default: TData;
var
  A: TArray<TData>;
  i: Integer;
begin
  SetLength(A, Length(FFields));
  for i := 0 to High(FFields) do
    A[i] := FFields[i].Default;
  Result := TDataArray.Create(A, True);
end;

function TTypeStruct.Compatible(T: TType; StrictCompat: Boolean = False): Boolean;
var
  i: Integer;
begin
  Result := (T is TTypeStruct) and (Length(FFields) = Length(TTypeStruct(T).FFields));
  if Result then
    for i := 0 to High(FFields) do
    begin
      Result := Result and FFields[i].Compatible(TTypeStruct(T).FFields[i], StrictCompat);
      if not Result then
        Break;
    end;
end;

function TTypeStruct.FieldIndex(const Field: string): Integer;
begin
  if not FFieldIndices.Contains(Field) then
    raise ECodeException.Create(E_FIELD_UNDECLARED, Field);
  Result := IndexOf(Field);
end;

function TTypeStruct.Fields: TArray<string>;
var
  keys: TArray<string>;
  i: Integer;
begin
  keys := FFieldIndices.keys;
  SetLength(Result, FFieldIndices.Count);
  for i := 0 to High(Result) do
    Result[FFieldIndices[keys[i]]] := keys[i];
end;

function TTypeStruct.IndexOf(const Field: string): Integer;
begin
  Result := FFieldIndices[Field];
end;

function TTypeStruct.NestedType(Field: string): TType;
begin
  if Field = '' then
    raise ECodeException.Create(E_SYNTAX_ERROR, '');
  if not FFieldIndices.Contains(Field) then
    raise ECodeException.Create(E_FIELD_UNDECLARED, Field);
  Result := FFields[FFieldIndices[Field]];
end;

function TTypeStruct.Pipeable: Boolean;
var
  i: Integer;
begin
  Result := True;
  for i := 0 to High(FFields) do
  begin
    Result := Result and FFields[i].Pipeable;
    if not Result then
      Exit;
  end;
end;

function TTypeStruct.Size: Integer;
begin
  Result := Length(FFields);
end;

end.
