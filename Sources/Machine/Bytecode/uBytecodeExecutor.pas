unit uBytecodeExecutor;

interface

uses
  SyncObjs, SysUtils, uByteCode, uState, uCoreContext;

procedure ExecuteCode(const Code: TBytecode; State: TState; CoreContext: TCoreContext); inline;

implementation

uses uArray, uOpCode, uExceptions, uDataPool, uDataStack, uData, uDataArray, uDataBool, uDataNumeric, uDataFunc,
  uDataPipe, uDataSet, uOperations, uStringConverter, uMessageQueue, uIOProvider;

procedure OpBinary(const Code: TBytecode; State: TState); inline;
var
  d: TData;
begin
  d := State.Stack.Pop;
  State.Stack.Push(State.Stack.Pop.Apply(TBinaryOp(Code.SParam), d).Dispose);
  State.Jump();
end;

procedure OpCall(const Code: TBytecode; State: TState); inline;
begin
  State.Jump();
  (State.Stack.Pop as TDataFunc).Call(State);
end;

procedure OpClean(const Code: TBytecode; State: TState); inline;
begin
  TDataPool.GetInstance.CleanUp(False);
  State.Jump();
end;

procedure OpDup(const Code: TBytecode; State: TState); inline;
var
  d: TData;
begin
  d := State.Stack.Pop;
  State.Stack.Push(d);
  State.Stack.Push(d.Dup.Dispose);
  State.Jump();
end;

procedure OpEmpty(const Code: TBytecode; State: TState); inline;
var
  f: Boolean;
begin
  f := State.Stack.IsEmpty;
  State.Stack.Push(TDataBool.Create(f).Dispose);
  State.Jump();
end;

procedure OpField(const Code: TBytecode; State: TState); inline;
begin
  State.Stack.Push(State.Stack.Pop.Item(Code.LParam));
  State.Jump();
end;

procedure OpInject(const Code: TBytecode; State: TState); inline;
var
  d, f: TData;
  c: TArray<TData>;
  i: Integer;
begin
  d := State.Stack.Pop;
  SetLength(c, d.Count);
  for i := 0 to d.Count - 1 do
    c[i] := d.Item(d.Count - i - 1);

  f := State.Stack.Pop.Dup;
  (f as TDataFunc).AddClosures(c);
  State.Stack.Push(f);
  State.Jump();
end;

procedure OpItem(const Code: TBytecode; State: TState); inline;
var
  Index, Data: TData;
begin
  Index := State.Stack.Pop;
  Data := State.Stack.Pop;
  State.Stack.Push(Data.Item(TranslateIndex(AsInt(Index), Data.Count)));
  State.Jump();
end;

procedure OpJit(const Code: TBytecode; State: TState); inline;
var
  d: TData;
begin
  d := State.Stack.Pop;
  if AsBool(d) xor Code.FlagA then
    State.Jump(Code.LParam)
  else
    State.Jump();
end;

procedure OpJump(const Code: TBytecode; State: TState); inline;
begin
  State.Jump(Code.LParam);
end;

procedure OpLoad(const Code: TBytecode; State: TState); inline;
begin
  if Code.FlagA then
    State.Stack.Push(State.Context.Data[Code.LParam])
  else
    State.Stack.Push(State.Data.Data[Code.LParam]);
  State.Jump();
end;

procedure OpLog(const Code: TBytecode; State: TState; IOProvider: TIOProvider); inline;
var
  s: string;
begin
  s := Format('[Thread-%d %s]: %s = ', [State.ThreadId, FormatDateTime('hh:mm:ss', Now), State.Data.Text[Code.LParam]]);
  IOProvider.Log(s + LogData(State.Stack.Pop));
  State.Jump();
end;

procedure OpMove(const Code: TBytecode; State: TState); inline;
var
  dst, src: TData;
begin
  dst := State.Stack.Pop;
  src := State.Stack.Pop;

  if Code.FlagA then
    try
      dst.Assign(src.Unwrap);
      if Code.FlagB then
        State.Stack.Push(TDataBool.Create(True).Dispose);
    except
      if Code.FlagB then
        State.Stack.Push(TDataBool.Create(False).Dispose);
    end
  else begin
    dst.Assign(src.Unwrap);
    if Code.FlagB then
      State.Stack.Push(dst.Dup.Dispose);
  end;
  State.Jump();
end;

procedure OpNop(const Code: TBytecode; State: TState); inline;
begin
  State.Jump();
end;

procedure OpPop(const Code: TBytecode; State: TState); inline;
begin
  if State.Stack.Frame = TFrameType(Code.SParam) then
    State.Stack.DropFrame;
  State.Jump();
end;

procedure OpPush(const Code: TBytecode; State: TState); inline;
begin
  State.Stack.PushFrame(TFrameType(Code.SParam), Code.LParam);
  State.Jump();
end;

procedure OpRange(const Code: TBytecode; State: TState); inline;
var
  Index, Source: TData;
  Items: TArray<TData>;
  i: Integer;
begin
  Index := State.Stack.Pop;
  Source := State.Stack.Pop;
  SetLength(Items, Index.Count);
  for i := 0 to High(Items) do
    Items[i] := Source.Item(TranslateIndex(AsInt(Index.Item(i)), Source.Count));
  State.Stack.Push(TDataArray.Create(Items, False));
  State.Jump();
end;

procedure OpReturn(const Code: TBytecode; State: TState); inline;
var
  d: TData;
begin
  if Code.FlagB then
    d := State.Stack.Pop
  else
    d := nil;
  if Code.FlagA then
  begin
    while State.Stack.Frame <> ftFunction do
      State.Stack.DropFrame;
    State.Return;
  end else begin
    if State.Stack.Frame = ftFinallyReturn then
      State.Return
    else
      State.Jump;
  end;
  if d <> nil then
    State.Stack.Push(d);
end;

procedure OpRun(const Code: TBytecode; State: TState; MessageQueue: TMessageQueue); inline;
var
  f, a: TData;
begin
  f := State.Stack.Pop;
  a := State.Stack.Pop;
  MessageQueue.PostMessage(msCreatreThread, [f, a, State.Event]);
  State.Event.WaitFor(INF);
  State.Jump();
end;

procedure OpSplit(const Code: TBytecode; State: TState); inline;
var
  i: Integer;
  Data: TData;
begin
  Data := State.Stack.Pop;
  for i := Data.Count - 1 downto 0 do
    State.Stack.Push(Data.Item(i).Dup.Dispose);
  State.Jump();
end;

procedure OpStack(const Code: TBytecode; State: TState); inline;
var
  i: Integer;
  a: TArray<TData>;
  s: TDataSet;
begin
  case TArrayType(Code.SParam) of
    atSet:
      begin
        s := TDataSet.Create;
        for i := 0 to Code.LParam - 1 do
          s.AddItem(State.Stack.Pop);
        State.Stack.Push(s.Dispose);
      end;
    atStruct, atArray:
      begin
        SetLength(a, Code.LParam);
        for i := 0 to Code.LParam - 1 do
          a[i] := State.Stack.Pop;
        State.Stack.Push(TDataArray.Create(a, TArrayType(Code.SParam) = atStruct).Dispose(False));
      end;
  end;
  State.Jump();
end;

procedure OpSync(const Code: TBytecode; State: TState); inline;
var
  s: TCriticalSection;
begin
  s := State.Data.Section[Code.LParam];
  if Code.FlagA then
    s.Enter
  else
    s.Leave;
  State.Jump();
end;

procedure OpTail(const Code: TBytecode; State: TState); inline;
var
  d: TData;
begin
  d := State.Stack.Pop;
  State.Stack.Push((d as TDataPipe).Tail);
  State.Jump();
end;

procedure OpThrow(const Code: TBytecode; State: TState); inline;
var
  d: TData;
begin
  if State.Stack.Frame = ftException then
  begin
    d := State.Stack.Pop;
    State.Stack.DropFrame;
    raise ERuntimeException.Create(E_RUNTIME_ERROR, d);
  end;
  State.Jump();
end;

procedure OpUnary(const Code: TBytecode; State: TState); inline;
begin
  State.Stack.Push(State.Stack.Pop.Apply(TUnaryOp(Code.SParam)));
  State.Jump();
end;

procedure ExecuteCode(const Code: TBytecode; State: TState; CoreContext: TCoreContext); inline;
begin
  case Code.Op of
    OID_BINARY:
      OpBinary(Code, State);
    OID_CALL:
      OpCall(Code, State);
    OID_CLEAN:
      OpClean(Code, State);
    OID_DUP:
      OpDup(Code, State);
    OID_EMPTY:
      OpEmpty(Code, State);
    OID_FIELD:
      OpField(Code, State);
    OID_INJECT:
      OpInject(Code, State);
    OID_ITEM:
      OpItem(Code, State);
    OID_JIT:
      OpJit(Code, State);
    OID_JMP:
      OpJump(Code, State);
    OID_LOAD:
      OpLoad(Code, State);
    OID_LOG:
      OpLog(Code, State, CoreContext.IOProvider);
    OID_MOVE:
      OpMove(Code, State);
    OID_NOP:
      OpNop(Code, State);
    OID_POP:
      OpPop(Code, State);
    OID_PUSH:
      OpPush(Code, State);
    OID_RANGE:
      OpRange(Code, State);
    OID_RETURN:
      OpReturn(Code, State);
    OID_RUN:
      OpRun(Code, State, CoreContext.MessageQueue);
    OID_SPLIT:
      OpSplit(Code, State);
    OID_STACK:
      OpStack(Code, State);
    OID_SYNC:
      OpSync(Code, State);
    OID_TAIL:
      OpTail(Code, State);
    OID_THROW:
      OpThrow(Code, State);
    OID_UNARY:
      OpUnary(Code, State);
  else
    raise ERuntimeException.Create('Undefined opcode');
  end;
end;

end.
