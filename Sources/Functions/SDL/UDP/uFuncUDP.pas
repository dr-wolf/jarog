unit uFuncUDP;

interface

uses
  uStandardFunc, uFuncContext, BlckSock, Classes;

type
  TFuncUDP = class(TStandardFunc)
  protected
    function RetrieveSocket(const VarId: Integer; Context: TFuncContext): TUDPBlockSocket;
    function RetrieveStream(const VarId: Integer; Context: TFuncContext): TStream;
    function RetrieveIP(const VarId: Integer; Context: TFuncContext): string;
    function RetrievePort(const VarId: Integer; Context: TFuncContext): Integer;
  end;

implementation

uses SynAIP, uDataString, uDataNumeric, uExceptions;

{ TFuncUDP }

function TFuncUDP.RetrieveIP(const VarId: Integer; Context: TFuncContext): string;
begin
  Result := AsString(Context.Data[VarId]);
  if not IsIP(Result) and not IsIP6(Result) then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Invalid IP address!'));
end;

function TFuncUDP.RetrievePort(const VarId: Integer; Context: TFuncContext): Integer;
begin
  Result := AsInt(Context.Data[VarId]);
  if (Result < 0) or (Result > 65535) then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Invalid port number!'));
end;

function TFuncUDP.RetrieveSocket(const VarId: Integer; Context: TFuncContext): TUDPBlockSocket;
begin
  Result := FCoreContext.Repos.UDPSocks.Sock[AsInt(Context.Data[VarId])];
  if Result = nil then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Invalid socket handle!'));
end;

function TFuncUDP.RetrieveStream(const VarId: Integer; Context: TFuncContext): TStream;
begin
  Result := FCoreContext.Repos.Files.Stream[AsInt(Context.Data[VarId])];
  if Result = nil then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Invalid file handle!'));
end;

end.
