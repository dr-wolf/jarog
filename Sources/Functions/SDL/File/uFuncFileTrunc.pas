unit uFuncFileTrunc;

interface

uses
  uFuncFile, uFuncContext;

type
  TFuncFileTrunc = class(TFuncFile)
  public
    constructor Create;
  private const
    P_HANDLE = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Truncates file on current position.
    @type func(handle: int)
    @in handle int # Handle of a stream
    * }

implementation

uses Classes, uOpCode;

{ TFuncFileTrunc }

constructor TFuncFileTrunc.Create;
begin
  inherited Create([MakeInt]);
  FFuncUid := FID_FILE_TRUNC;
end;

procedure TFuncFileTrunc.Execute(Context: TFuncContext);
var
  s: TStream;
begin
  s := RetrieveStream(P_HANDLE, Context);
  s.Size := s.Position;
end;

end.
