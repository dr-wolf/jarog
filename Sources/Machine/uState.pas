unit uState;

interface

uses
  SyncObjs, uStack, uDataSection, uDataStack, uFuncContext, uTypedStream;

type
  TLayerType = (ltFunc, ltSafe, ltException, ltLoop);

type
  TState = class(TObject)
  public
    constructor Create(Address: Integer; Data: TDataSection);
    destructor Destroy; override;
  private
    class var FThreadMax: Integer;
  private
    FThreadId: Cardinal;
    FCompleted: Boolean;
    FEvent: TEvent;
    FAddress: Integer;
    FStack: TDataStack;
    FData: TDataSection;
    FContextStack: TStack<TFuncContext>;
    function GetContext: TFuncContext;
  public
    property ThreadId: Cardinal read FThreadId;
    property Event: TEvent read FEvent;
    property Completed: Boolean read FCompleted;
    property Context: TFuncContext read GetContext;
    property Address: Integer read FAddress;
    property Stack: TDataStack read FStack;
    property Data: TDataSection read FData;
    procedure Jump(Delta: Integer = 1);
    procedure Call(Address: Integer; Context: TFuncContext);
    procedure Return;
    procedure Serialize(Stream: TTypedStream);
  end;

implementation

uses SysUtils, uExceptions, uThreadSafe;

{ TState }

constructor TState.Create(Address: Integer; Data: TDataSection);
begin
  SafeInc(FThreadMax);
  FThreadId := FThreadMax;
  FAddress := Address;
  FCompleted := False;
  FEvent := TEvent.Create(nil, False, False, 'Thread-' + IntToStr(FThreadId));
  FStack := TDataStack.Create;
  FData := Data;
  FContextStack := TStack<TFuncContext>.Create;
end;

destructor TState.Destroy;
begin
  while FContextStack.Depth > 0 do
    FContextStack.Pop.Free;
  FContextStack.Free;

  FStack.Free;
  FEvent.Free;
  inherited;
end;

function TState.GetContext: TFuncContext;
begin
  if FContextStack.Depth > 0 then
    Result := FContextStack.Peek
  else
    Result := nil;
end;

procedure TState.Call(Address: Integer; Context: TFuncContext);
begin
  if Context <> nil then
  begin
    FStack.PushFrame(ftFunction, FAddress);
    FContextStack.Push(Context);
  end;
  FAddress := Address;
end;

procedure TState.Jump(Delta: Integer);
begin
  FAddress := FAddress + Delta;
end;

procedure TState.Return;
var
  f: TFrameType;
begin
  f := FStack.Frame;
  FAddress := FStack.Address;
  if FAddress < 0 then
    raise ERuntimeException.Create('Undefined return address');
  FStack.DropFrame;
  if f = ftFunction then
  begin
    with FContextStack.Pop do
    begin
      DisposeData;
      Free;
    end;
    if FStack.CallDepth = 0 then
      FCompleted := True;
  end;
end;

procedure TState.Serialize(Stream: TTypedStream);
begin
  Stream.WriteInteger(FAddress);
  FStack.Dump(Stream);
  if GetContext <> nil then
    GetContext.Save(Stream, nil)
  else
    Stream.WriteInteger(0);
end;

end.
