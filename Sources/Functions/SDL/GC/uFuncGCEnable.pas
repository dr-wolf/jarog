unit uFuncGCEnable;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncGCEnable = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Enables GC for current thread.
    @type func()
    * }

implementation

uses uDataPool, uOpCode;

{ TFuncGCEnable }

constructor TFuncGCEnable.Create;
begin
  inherited Create([]);
  FFuncUid := FID_GC_ENABLE;
end;

procedure TFuncGCEnable.Execute(Context: TFuncContext);
begin
  TDataPool.GetInstance.Enabled := True;
end;


end.
