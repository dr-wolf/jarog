unit uCompiler;

interface

uses
  Classes, uAppMold, uUnitLibrary, uConfig;

type
  TCompiler = class(TObject)
  public
    constructor Create(Config: TConfig);
    destructor Destroy; override;
  private
    FUnitLibrary: TUnitLibrary;
    FAppMold: TAppMold;
  public
    procedure LoadFromFile(const FileName: string; var LinesProcessed: Integer);
    procedure LogToFile(const FileName: string);
    procedure SaveToStream(Stream: TStream; var CodeSize, DataSize: Int64);
  end;

implementation

uses uExecFile;

{ TCompiler }

constructor TCompiler.Create(Config: TConfig);
begin
  FUnitLibrary := TUnitLibrary.Create(Config);
  FAppMold := TAppMold.Create;
end;

destructor TCompiler.Destroy;
begin
  FAppMold.Free;
  FUnitLibrary.Free;
  inherited;
end;

procedure TCompiler.LoadFromFile(const FileName: string; var LinesProcessed: Integer);
begin
  FUnitLibrary.Load(FileName, FAppMold).Initialize(FAppMold);
  LinesProcessed := FUnitLibrary.LinesProcessed;
end;

procedure TCompiler.LogToFile(const FileName: string);
begin
  FAppMold.Log(FileName);
end;

procedure TCompiler.SaveToStream(Stream: TStream; var CodeSize, DataSize: Int64);
var
  ExecFile: TExecFile;
begin
  ExecFile := TExecFile.Create(nil);
  try
    FAppMold.Enumerate;
    FAppMold.Serialize(ExecFile);
    ExecFile.SaveToStream(Stream);
    CodeSize := ExecFile.OpStream.Size;
    DataSize := ExecFile.DataSize;
  finally
    ExecFile.Free;
  end;
end;

end.
