unit uDataBool;

interface

uses
  uOperations, uData, uExecFile, uTypedStream;

type
  TDataBool = class(TData)
  public
    constructor Create; overload;
    constructor Create(const Value: Boolean); overload;
    constructor Create(const Value: string); overload;
  private
    FValue: Boolean;
  public
    function Apply(Op: TBinaryOp; const Data: TData): TData; override;
    function Apply(Op: TUnaryOp): TData; override;
    function Dup: TData; override;
    function IsEqual(const Data: TData): Boolean; override;
    function Serialize(const ExecFile: TExecFile): TTypedStream; override;
    procedure Assign(const Data: TData); override;
    procedure Convert(C: TConverter); override;
  end;

function AsBool(const Data: TData): Boolean;

implementation

uses uExceptions, uIdentifierRegistry, uOpCode;

function AsBool(const Data: TData): Boolean;
begin
  if Data.Unwrap is TDataBool then
    Result := (Data.Unwrap as TDataBool).FValue
  else
    raise ERuntimeException.Create(E_EXPECTED_BOOL, nil);
end;

{ TDataBool }

constructor TDataBool.Create;
begin
  inherited Create();
  FValue := False;
end;

constructor TDataBool.Create(const Value: Boolean);
begin
  inherited Create;
  FValue := Value;
end;

constructor TDataBool.Create(const Value: string);
begin
  inherited Create;
  FValue := Value = RI_TRUE;
end;

procedure TDataBool.Assign(const Data: TData);
begin
  FValue := AsBool(Data);
end;

function TDataBool.Apply(Op: TBinaryOp; const Data: TData): TData;
begin
  case Op of
    boAnd:
      Result := TDataBool.Create(FValue and AsBool(Data));
    boOr:
      Result := TDataBool.Create(FValue or AsBool(Data));
    boEqual:
      Result := TDataBool.Create(IsEqual(Data));
    boNotEqual:
      Result := TDataBool.Create(not IsEqual(Data));
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  end
end;

function TDataBool.Apply(Op: TUnaryOp): TData;
begin
  if Op = uoNot then
    Result := TDataBool.Create(not FValue)
  else
    Result := Self;
end;

function TDataBool.Dup: TData;
begin
  Result := TDataBool.Create(FValue);
end;

procedure TDataBool.Convert(C: TConverter);
begin
  C.ConvertBool(FValue);
end;

function TDataBool.IsEqual(const Data: TData): Boolean;
begin
  Result := FValue = AsBool(Data);
end;

function TDataBool.Serialize(const ExecFile: TExecFile): TTypedStream;
begin
  Result := TTypedStream.Create;
  with Result do
  begin
    WriteByte(DID_DATA_BOOL);
    WriteByte(Byte(FValue));
  end;
end;

end.
