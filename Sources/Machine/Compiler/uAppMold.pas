unit uAppMold;

interface

uses
  uArray, uStringSet, uOp, uDataSection, uExecFile;

type
  TAppMold = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FDataSection: TDataSection;
    FOps: TArray<TOp>;
    FSectionNames: TStringSet;
    function GetSize: Integer;
    function GetOp(I: Integer): TOp;
  public
    property DataSection: TDataSection read FDataSection;
    property Op[I: Integer]: TOp read GetOp; default;
    property Size: Integer read GetSize;
    function RegisterCriticalSection(Name: string): Integer;
    procedure AddAll(Ops: array of TOp);
    procedure ClearBreaks;
    procedure Enumerate;
    procedure Log(const Path: string);
    procedure Serialize(ExecFile: TExecFile);
    procedure SetStartOp(Op: TOp);
    procedure ToggleBreak(I: Integer);
  end;

implementation

uses SysUtils, Classes, uOpCall, uTypedStream, uBytecode, uBytecodeLogger;

{ TAppMold }

constructor TAppMold.Create;
begin
  FDataSection := TDataSection.Create;
  SetLength(FOps, 2);
  FOps[1] := TOpCall.Create();
  FSectionNames := TStringSet.Create;
end;

destructor TAppMold.Destroy;
var
  I: Integer;
begin
  FDataSection.Free;
  for I := 0 to High(FOps) do
    FOps[I].Free;
  FOps := nil;
  FSectionNames.Free;
  inherited;
end;

procedure TAppMold.AddAll(Ops: array of TOp);
begin
  AppendOp(FOps, Ops);
end;

procedure TAppMold.ClearBreaks;
var
  I: Integer;
begin
  for I := 0 to High(FOps) do
    FOps[I].Break := False;
end;

procedure TAppMold.Enumerate;
var
  I: Integer;
begin
  for I := 0 to High(FOps) do
    FOps[I].SetIndex(I);
end;

function TAppMold.GetOp(I: Integer): TOp;
begin
  Result := FOps[I];
end;

function TAppMold.GetSize: Integer;
begin
  Result := Length(FOps);
end;

procedure TAppMold.Log(const Path: string);
var
  I: Integer;
  c: TBytecode;
begin
  with TStringList.Create do
  begin
    for I := 0 to High(FOps) do
    begin
      ZeroCode(c);
      FOps[I].Serialize(c);
      Add('0x' + IntToHex(I, 8) + ': ' + LogBytecode(c, FDataSection));
    end;
    SaveToFile(Path + '.log');
    Free;
  end;
end;

function TAppMold.RegisterCriticalSection(Name: string): Integer;
begin
  REsult := FSectionNames.Add(Name);
end;

procedure TAppMold.Serialize(ExecFile: TExecFile);
var
  I: Integer;
  c: TBytecode;
begin
  FDataSection.Save(ExecFile);
  ExecFile.OpStream.WriteInteger(FSectionNames.Count);

  ExecFile.OpStream.WriteInteger(Length(FOps));
  for I := 0 to High(FOps) do
  begin
    ZeroCode(c);
    FOps[I].Serialize(c);
    WriteCodeToStream(c, ExecFile.OpStream);
  end;
end;

procedure TAppMold.SetStartOp(Op: TOp);
begin
  FOps[0] := Op;
end;

procedure TAppMold.ToggleBreak(I: Integer);
begin
  FOps[I].Break := not FOps[I].Break;
end;

end.
