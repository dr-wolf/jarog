unit uClassStream;

interface

uses
  SysUtils, Classes;

type
  TClassStream = class(TObject)
  public
    constructor Create; overload;
    constructor Create(Stream: TStream; Size: Int64); overload;
    destructor Destroy; override;
  private
    FStream: TStream;
    function GetSize: Int64;
  public
    property Size: Int64 read GetSize;
    function ReadByte: Byte;
    function ReadInteger: Int64;
    function ReadDouble: Double;
    function ReadString: string;
    procedure WriteByte(B: Byte);
    procedure WriteInteger(I: Int64);
    procedure WriteDouble(E: Double);
    procedure WriteString(S: string);
    procedure SaveTo(Stream: TStream);
  end;

implementation

{ TClassStream }

constructor TClassStream.Create;
begin
  FStream := TMemoryStream.Create;
end;

constructor TClassStream.Create(Stream: TStream; Size: Int64);
begin
  FStream := TMemoryStream.Create;
  FStream.CopyFrom(Stream, Size);
  FStream.Position := 0;
end;

destructor TClassStream.Destroy;
begin
  FStream.Free;
  inherited;
end;

function TClassStream.GetSize: Int64;
begin
  Result := FStream.Size;
end;

function TClassStream.ReadByte: Byte;
begin
  FStream.ReadBuffer(Result, SizeOf(Result));
end;

function TClassStream.ReadDouble: Double;
begin
  FStream.ReadBuffer(Result, SizeOf(Result));
end;

function TClassStream.ReadInteger: Int64;
var
  h, l: Byte;
begin
  FStream.ReadBuffer(h, SizeOf(h));
  l := h and $F0 shr 4;
  Result := 0;
  FStream.ReadBuffer(Result, l);
  if h and $0F <> 0 then
    Result := -Result;
end;

function TClassStream.ReadString: string;
var
  bytes: TBytes;
  l: Integer;
begin
  l := ReadInteger;
  SetLength(bytes, l);
  FStream.ReadBuffer(Pointer(bytes)^, l);
  Result := StringOf(bytes);
end;

procedure TClassStream.SaveTo(Stream: TStream);
begin
  Stream.CopyFrom(FStream, 0);
end;

procedure TClassStream.WriteByte(B: Byte);
begin
  FStream.WriteBuffer(B, SizeOf(B));
end;

procedure TClassStream.WriteDouble(E: Double);
begin
  FStream.WriteBuffer(E, SizeOf(E));
end;

procedure TClassStream.WriteInteger(I: Int64);

  function SigBytes(V: Int64): Byte;
  begin
    if V > 0 then
    begin
      Result := 8;
      while V and $FF00000000000000 = 0 do
      begin
        V := V shl 8;
        Dec(Result);
      end;
    end
    else
      Result := 0;
  end;

var
  h, l: Byte;
begin
  l := SigBytes(Abs(I));
  h := (Byte(I < 0) and $0F) or (l and $0F shl 4);
  I := Abs(I);
  FStream.WriteBuffer(h, SizeOf(h));
  FStream.WriteBuffer(I, l);
end;

procedure TClassStream.WriteString(S: string);
var
  bytes: TBytes;
begin
  bytes := BytesOf(S);
  WriteInteger(Length(bytes));
  FStream.WriteBuffer(Pointer(bytes)^, Length(bytes));
end;

end.
