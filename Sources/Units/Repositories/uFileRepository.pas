unit uFileRepository;

interface

uses
  Classes, uArray, uRepository;

type
  TFileRepository = class(TRepository)
  public
    constructor Create;
  private type
    TFile = record
      FileName: string;
      Handle: Integer;
      Stream: TStream;
    end;
  protected
    FFiles: TArray<TFile>;
    function GetHandle(Id: Integer): Integer; override;
    function Size: Integer; override;
    procedure Release(Id: Integer); override;
    function GetStream(Handle: Integer): TStream;
  public
    property Stream[Handle: Integer]: TStream read GetStream;
    function Add(Stream: TStream; const FileName: string): Integer;
    function StreamFilename(Handle: Integer): string;
  end;

implementation

{ TFileRepository }

constructor TFileRepository.Create;
begin
  SetLength(FFiles, 0);
end;

function TFileRepository.GetHandle(Id: Integer): Integer;
begin
  Result := FFiles[Id].Handle;
end;

function TFileRepository.GetStream(Handle: Integer): TStream;
var
  I: Integer;
begin
  I := Find(Handle);
  if I > -1 then
    Result := FFiles[I].Stream
  else
    Result := nil;
end;

procedure TFileRepository.Release(Id: Integer);
begin
  FFiles[Id].Stream.Free;
  if Id < High(FFiles) then
    FFiles[Id] := FFiles[High(FFiles)];
  SetLength(FFiles, Length(FFiles) - 1);
end;

function TFileRepository.Size: Integer;
begin
  Result := Length(FFiles);
end;

function TFileRepository.StreamFilename(Handle: Integer): string;
var
  I: Integer;
begin
  I := Find(Handle);
  if I > -1 then
    Result := FFiles[I].FileName
  else
    Result := '';
end;

function TFileRepository.Add(Stream: TStream; const FileName: string): Integer;
var
  f: TFile;
begin
  f.Handle := Random(MaxInt);
  f.Stream := Stream;
  f.FileName := FileName;
  SetLength(FFiles, Length(FFiles) + 1);
  FFiles[High(FFiles)] := f;
  Result := f.Handle;
end;

end.
