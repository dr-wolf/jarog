unit uFuncFileWriteInt;

interface

uses
  uFuncFile, uFuncContext;

type
  TFuncFileWriteInt = class(TFuncFile)
  public
    constructor Create;
  private const
    P_HANDLE = 0;
    P_VALUE = 1;
    P_SIZE = 2;
    P_SIGNED = 3;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Writes integer value to file stream.
    @type func(handle: int, value: int, size: int, signed: bool)
    @in handle int # Handle of a stream
    @in value int # Value to be written
    @in size int # Size in bytes, can be 1, 2, 4 or 8, see sz_byte, sz_word, sz_int and sz_long constants, other bytes are truncated
    @in signed bool # If true value is written as signed otherwise it is written as unsigned
    * }

implementation

uses Classes, uDataNumeric, uDataBool, uNumeric, uOpCode;

{ TFuncFileWriteInt }

constructor TFuncFileWriteInt.Create;
begin
  inherited Create([MakeInt, MakeInt, MakeInt, MakeBool]);
  FFuncUid := FID_FILE_WRITEINT;
end;

procedure TFuncFileWriteInt.Execute(Context: TFuncContext);

  procedure WriteInt8(s: TStream; n: TNumeric);
  var
    b: ShortInt;
  begin
    b := ShortInt(n.ToInt64);
    s.WriteBuffer(b, SizeOf(b));
  end;

  procedure WriteUInt8(s: TStream; n: TNumeric);
  var
    b: Byte;
  begin
    b := Byte(n.ToUInt64);
    s.WriteBuffer(b, SizeOf(b));
  end;

  procedure WriteInt16(s: TStream; n: TNumeric);
  var
    b: SmallInt;
  begin
    b := SmallInt(n.ToInt64);
    s.WriteBuffer(b, SizeOf(b));
  end;

  procedure WriteUInt16(s: TStream; n: TNumeric);
  var
    b: Word;
  begin
    b := Word(n.ToUInt64);
    s.WriteBuffer(b, SizeOf(b));
  end;

  procedure WriteInt32(s: TStream; n: TNumeric);
  var
    b: Integer;
  begin
    b := Integer(n.ToInt64);
    s.WriteBuffer(b, SizeOf(b));
  end;

  procedure WriteUInt32(s: TStream; n: TNumeric);
  var
    b: Cardinal;
  begin
    b := Cardinal(n.ToUInt64);
    s.WriteBuffer(b, SizeOf(b));
  end;

  procedure WriteInt64(s: TStream; n: TNumeric);
  var
    b: Int64;
  begin
    b := n.ToInt64;
    s.WriteBuffer(b, SizeOf(b));
  end;

  procedure WriteUInt64(s: TStream; n: TNumeric);
  var
    b: UInt64;
  begin
    b := n.ToUInt64;
    s.WriteBuffer(b, SizeOf(b));
  end;

var
  s: TStream;
  l: Byte;
  b: Boolean;
begin
  s := RetrieveStream(P_HANDLE, Context);
  l := RetrievePackSize(P_SIZE, [1, 2, 4, 8], Context);
  b := AsBool(Context.Data[P_SIGNED]);
  case l of
    1:
      if b then
        WriteInt8(s, AsNumeric(Context.Data[P_VALUE]))
      else
        WriteUInt8(s, AsNumeric(Context.Data[P_VALUE]));
    2:
      if b then
        WriteInt16(s, AsNumeric(Context.Data[P_VALUE]))
      else
        WriteUInt16(s, AsNumeric(Context.Data[P_VALUE]));
    4:
      if b then
        WriteInt32(s, AsNumeric(Context.Data[P_VALUE]))
      else
        WriteUInt32(s, AsNumeric(Context.Data[P_VALUE]));
    8:
      if b then
        WriteInt64(s, AsNumeric(Context.Data[P_VALUE]))
      else
        WriteUInt64(s, AsNumeric(Context.Data[P_VALUE]));
  end;
end;

end.
