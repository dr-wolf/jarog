unit uFuncFileReadInt;

interface

uses
  uFuncFile, uFuncContext;

type
  TFuncFileReadInt = class(TFuncFile)
  public
    constructor Create;
  private const
    P_HANDLE = 0;
    P_SIZE = 1;
    P_SIGNED = 2;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Reads integer value from file stream.
    @type func(handle: int, size: int, signed: bool -> int)
    @in handle int # Handle of a stream
    @in size int # Size in bytes, can be 1, 2, 4 or 8, see sz_byte, sz_word, sz_int and sz_long constants
    @in signed bool # If true value is threated as signed otherwise it is read as unsigned
    @out - int # Returned value
    * }

implementation

uses Classes, uStandardFunc, uDataBool, uDataNumeric, uBigInt, uOpCode;

{ TFuncFileReadInt }

constructor TFuncFileReadInt.Create;
begin
  inherited Create([MakeInt, MakeInt, MakeBool], MakeInt);
  FFuncUid := FID_FILE_READINT;
end;

procedure TFuncFileReadInt.Execute(Context: TFuncContext);

  function ReadInt8(s: TStream): TDataNumeric;
  var
    b: ShortInt;
  begin
    s.ReadBuffer(b, SizeOf(b));
    Result := TDataNumeric.Create(b);
  end;

  function ReadUInt8(s: TStream): TDataNumeric;
  var
    b: Byte;
  begin
    s.ReadBuffer(b, SizeOf(b));
    Result := TDataNumeric.Create(b);
  end;

  function ReadInt16(s: TStream): TDataNumeric;
  var
    b: SmallInt;
  begin
    s.ReadBuffer(b, SizeOf(b));
    Result := TDataNumeric.Create(b);
  end;

  function ReadUInt16(s: TStream): TDataNumeric;
  var
    b: Word;
  begin
    s.ReadBuffer(b, SizeOf(b));
    Result := TDataNumeric.Create(b);
  end;

  function ReadInt32(s: TStream): TDataNumeric;
  var
    b: Integer;
  begin
    s.ReadBuffer(b, SizeOf(b));
    Result := TDataNumeric.Create(b);
  end;

  function ReadUInt32(s: TStream): TDataNumeric;
  var
    b: Cardinal;
  begin
    s.ReadBuffer(b, SizeOf(b));
    Result := TDataNumeric.Create(b);
  end;

  function ReadInt64(s: TStream): TDataNumeric;
  var
    b: Int64;
  begin
    s.ReadBuffer(b, SizeOf(b));
    Result := TDataNumeric.Create(TBigInt.Create(b));
  end;

  function ReadUInt64(s: TStream): TDataNumeric;
  var
    b: UInt64;
  begin
    s.ReadBuffer(b, SizeOf(b));
    Result := TDataNumeric.Create(TBigInt.Create(b));
  end;

var
  s: TStream;
  l: Byte;
  b: Boolean;
begin
  s := RetrieveStream(P_HANDLE, Context);
  l := RetrievePackSize(P_SIZE, [1, 2, 4, 8], Context);
  b := AsBool(Context.Data[P_SIGNED]);
  case l of
    1:
      if b then
        Return(ReadInt8(s))
      else
        Return(ReadUInt8(s));
    2:
      if b then
        Return(ReadInt16(s))
      else
        Return(ReadUInt16(s));
    4:
      if b then
        Return(ReadInt32(s))
      else
        Return(ReadUInt32(s));
    8:
      if b then
        Return(ReadInt64(s))
      else
        Return(ReadUInt64(s));
  end;
end;

end.
