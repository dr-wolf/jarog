unit uStandardFunc;

interface

uses
  uArray, uFunction, uFuncContext, uData, uCoreContext, uClosure, uState;

type
  TStandardFunc = class(TFunction)
  public
    constructor Create(Params: array of TData; Result: TData = nil);
    destructor Destroy; override;
  private
    FResultId: Integer;
  protected
    FCoreContext: TCoreContext;
    function MakeAny: TData;
    function MakeBool(Default: Boolean = False): TData;
    function MakeFloat(Default: Double = 0): TData;
    function MakeInt(Default: Integer = 0): TData;
    function MakeList(Item: TData): TData;
    function MakeString(Default: string = ''): TData;
    function MakeStruct(Fields: array of TData): TData;
    procedure Execute(Context: TFuncContext); virtual; abstract;
    procedure Return(Data: TData);
  public
    function Log: string; override;
    procedure Call(State: TState; Caller: TData; Closures: TArray<IClosure>); override;
    procedure AttachContext(CoreContext: TCoreContext); override;
  end;

implementation

uses uOpCode, uDataAny, uDataArray, uDataBool, uDataNumeric,
  uBigFloat, uDataString;

{ TStandardFunc }

constructor TStandardFunc.Create(Params: array of TData; Result: TData = nil);
var
  i: Integer;
begin
  FContext := TFuncContext.Create;
  SetLength(FClosures, 0);
  SetLength(FParameters, Length(Params));
  for i := 0 to High(FParameters) do
    FParameters[i] := FContext.Keep(Params[i], False);
  if Result <> nil then
    FResultId := FContext.Keep(Result, True)
  else
    FResultId := -1;
end;

destructor TStandardFunc.Destroy;
begin
  inherited;
end;

procedure TStandardFunc.AttachContext(CoreContext: TCoreContext);
begin
  FCoreContext := CoreContext;
end;

procedure TStandardFunc.Call(State: TState; Caller: TData; Closures: TArray<IClosure>);
var
  c: TFuncContext;
begin
  c := FContext.Recreate(Caller, State.ThreadId);
  try
    LoadParams(c, State);
    Execute(c);
    if FResultId > -1 then
      State.Stack.Push(c.Data[FResultId].Dup.Dispose);
  finally
    c.Free;
  end;
end;

function TStandardFunc.Log: string;
begin
  Result := FidToName(FFuncUid);
end;

function TStandardFunc.MakeAny: TData;
begin
  Result := TDataAny.Create;
end;

function TStandardFunc.MakeBool(Default: Boolean): TData;
begin
  Result := TDataBool.Create(Default);
end;

function TStandardFunc.MakeFloat(Default: Double = 0): TData;
begin
  Result := TDataNumeric.Create(TBigFloat.Create(Default));
end;

function TStandardFunc.MakeInt(Default: Integer = 0): TData;
begin
  Result := TDataNumeric.Create(Default);
end;

function TStandardFunc.MakeList(Item: TData): TData;
begin
  Result := TDataArray.Create(Item);
end;

function TStandardFunc.MakeString(Default: string): TData;
begin
  Result := TDataString.Create(Default);
end;

function TStandardFunc.MakeStruct(Fields: array of TData): TData;
begin
  Result := TDataArray.Create(Fields);
end;

procedure TStandardFunc.Return(Data: TData);
begin
  FContext.Data[FResultId].Assign(Data.Dispose);
end;

end.
