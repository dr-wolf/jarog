unit uFuncMathAbs;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncMathAbs = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_VALUE = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Retruns absolute value of a number.
    @type func(value: float -> float)
    @in value float # Initial number
    @out - float # It's absolute value
    * }

implementation

uses uDataNumeric, uOpCode;

{ TFuncMathAbs }

constructor TFuncMathAbs.Create;
begin
  inherited Create([MakeFloat], MakeFloat);
  FFuncUid := FID_MATH_ABS;
end;

procedure TFuncMathAbs.Execute(Context: TFuncContext);
begin
  Return(TDataNumeric.Create(AsNumeric(Context.Data[P_VALUE]).Module));
end;

end.
