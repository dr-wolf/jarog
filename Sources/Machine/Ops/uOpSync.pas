unit uOpSync;

interface

uses
  uOp, uBytecode;

type
  TOpSync = class(TOp)
  public
    constructor Create(SectionId: Integer; Enter: Boolean);
    destructor Destroy; override;
  private
    FSectionId: Integer;
    FEnter: Boolean;
  public
    procedure Serialize(var Code: TBytecode); override;
  end;

implementation

uses uOpCode;

{ TOpSync }

constructor TOpSync.Create(SectionId: Integer; Enter: Boolean);
begin
  inherited Create(OID_SYNC);
  FEnter := Enter;
  FSectionId := SectionId;
end;

destructor TOpSync.Destroy;
begin
  inherited;
end;

procedure TOpSync.Serialize(var Code: TBytecode);
begin
  Code.Op := CompileOp(FEnter);
  Code.LParam := FSectionId;
end;

end.
