unit uFuncRexMatch;

interface

uses
  uFuncRex, uFuncContext;

type
  TFuncRexMatch = class(TFuncRex)
  public
    constructor Create;
  private const
    P_REGEX = 0;
    P_TEXT = 1;
    P_MODIFIERS = 2;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Tests a string for matching given regular expression.
    @type func(regex: string, text: string -> bool)
    @in regex string # Regular expression to test
    @in text string # Text to be matched with regular expression
    @in modifiers int # Reqular expression modifiers
    @out - bool # Match result
    * }

implementation

uses uDataBool, uDataNumeric, uDataString, uOpCode;

{ TFuncRexMatch }

constructor TFuncRexMatch.Create;
begin
  inherited Create([MakeString, MakeString, MakeInt], MakeBool);
  FFuncUid := FID_REX_MATCH;
end;

procedure TFuncRexMatch.Execute(Context: TFuncContext);
begin
  with InitRex(AsInt(Context.Data[P_MODIFIERS])) do
  begin
    Expression := AsString(Context.Data[P_REGEX]);
    Return(TDataBool.Create(Exec(AsString(Context.Data[P_TEXT]))));
    Free;
  end;
end;

end.
