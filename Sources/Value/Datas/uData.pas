unit uData;

interface

uses
  uArray, uOperations, uExecFile, uTypedStream, uDataPool, uNumeric;

type
  TConverter = class;

  TData = class(TPooledObject)
  public
    constructor Create;
    destructor Destroy; override;
  public
    function Apply(Op: TBinaryOp; const Data: TData): TData; overload; virtual;
    function Apply(Op: TUnaryOp): TData; overload; virtual;
    function Count: Integer; virtual;
    function Dispose(Recursive: Boolean = True): TData;
    function Dup: TData; virtual; abstract;
    function Unwrap: TData; virtual;
    function IsEqual(const Data: TData): Boolean; virtual; abstract;
    function Item(I: Integer): TData; virtual;
    function Serialize(const ExecFile: TExecFile): TTypedStream; virtual; abstract;
    procedure Assign(const Data: TData); virtual; abstract;
    procedure Convert(C: TConverter); virtual; abstract;
  end;

  TConverter = class(TObject)
  public
    procedure ConvertAny(Value: TData); virtual; abstract;
    procedure ConvertArray(Items: array of TData; Size: Integer); virtual; abstract;
    procedure ConvertBool(Value: Boolean); virtual; abstract;
    procedure ConvertFunc(Func: TObject; Closures: TArray<TData>); virtual; abstract;
    procedure ConvertNumeric(Value: TNumeric); virtual; abstract;
    procedure ConvertPipe(DefaultData: TData); virtual; abstract;
    procedure ConvertSet(Items: TArray<UInt64>); virtual; abstract;
    procedure ConvertString(Value: string); virtual; abstract;
    procedure ConvertStub(); virtual; abstract;
  end;

implementation

uses uExceptions;

function AsIntegerArray(Items: array of Integer): TArray<Integer>;
var
  I: Integer;
begin
  if Length(Items) > 0 then
  begin
    SetLength(Result, Length(Items));
    for I := 0 to High(Items) do
      Result[I] := Items[I];
  end
  else
    Result := nil;
end;

{ TData }

constructor TData.Create;
begin
  FGCLevel := lNonDisposable;
end;

destructor TData.Destroy;
begin
  inherited;
end;

function TData.Count: Integer;
begin
  raise ERuntimeException.Create(E_TYPE_ATOMIC, nil);
end;

function TData.Dispose(Recursive: Boolean = True): TData;
begin
  SetGCLevel(lDisposable, Recursive, True);
  Result := Self;
end;

function TData.Unwrap: TData;
begin
  Result := Self;
end;

function TData.Item(I: Integer): TData;
begin
  raise ERuntimeException.Create(E_TYPE_ATOMIC, nil);
end;

function TData.Apply(Op: TBinaryOp; const Data: TData): TData;
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

function TData.Apply(Op: TUnaryOp): TData;
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

end.
