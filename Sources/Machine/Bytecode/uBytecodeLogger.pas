unit uBytecodeLogger;

interface

uses
  uBytecode, uDataSection;

function LogBytecode(Code: TBytecode; Data: TDataSection): string;

implementation

uses uOp, uOpCode, uOperations, uDataStack, uStringConverter;

function LogBytecode(Code: TBytecode; Data: TDataSection): string;
begin
  case Code.Op of
    OID_BINARY:
      Result := BOP_NAMES[TBinaryOp(Code.SParam)];
    OID_CALL:
      Result := 'call';
    OID_CLEAN:
      Result := 'clean';
    OID_DUP:
      Result := 'dup';
    OID_EMPTY:
      Result := 'empty';
    OID_FIELD:
      Result := 'field ' + Int(Code.LParam);
    OID_INJECT:
      Result := 'inject';
    OID_ITEM:
      Result := 'item';
    OID_JIT:
      if Code.FlagA then
        Result := 'jnt ' + SignedInt(Code.LParam)
      else
        Result := 'jit ' + SignedInt(Code.LParam);
    OID_JMP:
      Result := 'jmp ' + SignedInt(Code.LParam);
    OID_LOAD:
      if Code.FlagA then
        Result := 'load ' + DataAddress(Code.LParam)
      else
        Result := 'load ' + LogData(Data.Data[Code.LParam]);
    OID_LOG:
      Result := 'log';
    OID_MOVE:
      begin
        Result := 'mov';
        if Code.FlagA or Code.FlagB then
          Result := Result + ' ';
        if Code.FlagA then
          Result := Result + 't';
        if Code.FlagB then
          Result := Result + 'r';
      end;
    OID_NOP:
      Result := 'nop';
    OID_POP:
      Result := 'pop ' + FrameName(TFrameType(Code.SParam));
    OID_PUSH:
      begin
        Result := 'push ' + FrameName(TFrameType(Code.SParam));
        if Code.LParam > 0 then
          Result := Result + ' ' + CodeAddress(Code.LParam);
      end;
    OID_RANGE:
      Result := 'range';
    OID_RETURN:
      begin
        Result := 'ret';
        if Code.FlagA or Code.FlagB then
        begin
          Result := Result + ' ';
          if Code.FlagA then
            Result := Result + 'f';
          if Code.FlagB then
            Result := Result + 'r';
        end;
      end;
    OID_RUN:
      Result := 'run';
    OID_SPLIT:
      Result := 'split';
    OID_STACK:
      Result := 'stack ' + Int(Code.LParam);
    OID_SYNC:
      begin
        Result := 'sync ' + DataAddress(Code.LParam);
        if Code.FlagA then
          Result := Result + ' in'
        else
          Result := Result + ' out';
      end;
    OID_TAIL:
      Result := 'tail';
    OID_THROW:
      Result := 'throw';
    OID_UNARY:
      Result := UOP_NAMES[TUnaryOp(Code.SParam)];
  else
    Result := '???';
  end;
end;

end.
