unit uDebugger;

interface

uses
  Classes, uSocketListener, uIOProvider, uApp, uWorkerList;

type
  TDebugger = class(TThread)
  public
    constructor Create(Port: Integer; IOProvider: TIOProvider; App: TApp; Workers: TWorkerList);
    destructor Destroy; override;
  private
    FSocketListener: TSocketListener;
    FResponseStream: TMemoryStream;
    FIOProvider: TIOProvider;
    FApp: TApp;
    FWorkers: TWorkerList;
    FDebuggedWorker: Integer;
    function NextSuspendedWorker(WorkerId: Integer): Integer;
  protected
    procedure Execute; override;
  public

  end;

implementation

uses SyncObjs, uDebuggerConst, uWorker, uTypedStream;

{ TDebugger }

constructor TDebugger.Create(Port: Integer; IOProvider: TIOProvider; App: TApp; Workers: TWorkerList);
begin
  inherited Create(False);
  FSocketListener := TSocketListener.Create(Port);
  FResponseStream := TMemoryStream.Create;

  FIOProvider := IOProvider;
  FApp := App;
  FWorkers := Workers;
end;

destructor TDebugger.Destroy;
begin
  FResponseStream.Free;
  FSocketListener.Free;
  inherited;
end;

procedure TDebugger.Execute;
var
  c: Byte;
begin
  while True do
  begin
    FSocketListener.WaitForConnection;
    FResponseStream.Clear;
    try
      c := FSocketListener.ReadCommand;
      if c = DBG_GET_CODE then
        FApp.Log(FResponseStream)
      else if c = DBG_GET_LOGS then
        FIOProvider.FlushLogs(FResponseStream)
      else
      begin
        if FDebuggedWorker < 0 then
          FDebuggedWorker := NextSuspendedWorker(FDebuggedWorker);
        if FDebuggedWorker >= 0 then
          case c of
            DBG_GET_STATE:
              FWorkers[FDebuggedWorker].SerializeState(FResponseStream);
            DBG_TOGGLE_BP:
              FApp.ToggleBreak(FSocketListener.ReadIndex);
            DBG_RUN:
              begin
                FWorkers[FDebuggedWorker].Run(RM_RUN);
                Sleep(10);
                FDebuggedWorker := NextSuspendedWorker(FDebuggedWorker);
              end;
            DBG_STEP:
              FWorkers[FDebuggedWorker].Run(RM_STEP);
            DBG_STEPOVER:
              FWorkers[FDebuggedWorker].Run(RM_STEPOVER);
            DBG_DETACH:
              begin
                FSocketListener.DropConnection;
                Continue;
              end;
          end
        else
          with TTypedStream.Create do
          begin
            WriteInteger(-1);
            SaveTo(FResponseStream);
            Free;
          end;
      end;
    finally
      FSocketListener.Send(FResponseStream);
    end;
    if FWorkers.AllWorksStopped then
      Break;
  end;
end;

function TDebugger.NextSuspendedWorker(WorkerId: Integer): Integer;
begin
  Result := WorkerId;
  if WorkerId < 0 then
    WorkerId := 0;
  FWorkers.Lock;
  repeat
    Inc(Result);
    if Result >= FWorkers.Count then
      Result := 0;
    if (Result = WorkerId) and not FWorkers[Result].CheckMode(RM_SUSPEND) then
    begin
      Result := -1;
      Break;
    end;
  until FWorkers[Result].CheckMode(RM_SUSPEND);
  FWorkers.Unlock;
end;

end.
