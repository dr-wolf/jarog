unit uFuncOSGetEnv;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncOSGetEnv = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_NAME = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Returns value of environment variable.
    @type func(name: string -> string)
    @in name string # Name of variable
    @out - string # Returned value of variable
    * }

implementation

uses SysUtils, uDataString, uOpCode;

{ TFuncOSGetEnv }

constructor TFuncOSGetEnv.Create;
begin
  inherited Create([MakeString], MakeString);
  FFuncUid := FID_OS_GETENV;
end;

procedure TFuncOSGetEnv.Execute(Context: TFuncContext);
var
  v, s: string;
begin
  v := AsString(Context.Data[P_NAME]);
  if FCoreContext.Env.Variables.Contains(v) then
    s := FCoreContext.Env.GetValue(v)
  else
    s := GetEnvironmentVariable(v);
  Return(TDataString.Create(s));
end;

end.
