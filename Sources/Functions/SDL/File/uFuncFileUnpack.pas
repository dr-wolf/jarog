unit uFuncFileUnpack;

interface

uses
  uFuncFile, uFuncContext;

type
  TFuncFileUnpack = class(TFuncFile)
  public
    constructor Create;
  private const
    P_HANDLE = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Unpacks any from file stream.
    @type func(handle: int -> any)
    @in handle int # Handle of a stream
    @out - any # Unpacked value
    * }

implementation

uses uData, uOpCode, uTypedStream, uDataLoader;

{ TFuncFileUnpack }

constructor TFuncFileUnpack.Create;
begin
  inherited Create([MakeInt], MakeAny);
  FFuncUid := FID_FILE_UNPACK;
end;

procedure TFuncFileUnpack.Execute(Context: TFuncContext);
var
  ts: TTypedStream;
  d: TData;
begin
  ts := TTypedStream.Create(RetrieveStream(P_HANDLE, Context), False);
  try
    d := UnpackData(ts) as TData;
  finally
    ts.Free;
  end;
  Return(d);
end;

end.
