unit uThreadSafe;

interface

uses SyncObjs;

type
  TThreadObject = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FCriticalSection: TCriticalSection;
  public
    procedure Lock; inline;
    procedure Unlock; inline;
  end;

  TThreadInterfacedObject = class(TInterfacedObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FCriticalSection: TCriticalSection;
  public
    procedure Lock; inline;
    procedure Unlock; inline;
  end;

procedure SafeInc(var Value: Integer); inline;
procedure SafeDec(var Value: Integer); inline;
function SafeCmp(var Target: Integer; Value: Integer): Boolean; inline;
procedure SafeSet(var Target: Integer; Value: Integer); inline;

implementation

procedure SafeInc(var Value: Integer); inline;
begin
{$IFDEF FPC}
  InterlockedIncrement(Value);
{$ELSE}
  TInterlocked.Increment(Value);
{$ENDIF}
end;

procedure SafeDec(var Value: Integer); inline;
begin
{$IFDEF FPC}
  InterlockedDecrement(Value);
{$ELSE}
  TInterlocked.Decrement(Value);
{$ENDIF}
end;

function SafeCmp(var Target: Integer; Value: Integer): Boolean; inline;
begin
{$IFDEF FPC}
  Result := InterlockedCompareExchange(Target, Value, Value) = Value;
{$ELSE}
  Result := TInterlocked.CompareExchange(Target, Value, Value) = Value;
{$ENDIF}
end;

procedure SafeSet(var Target: Integer; Value: Integer); inline;
begin
{$IFDEF FPC}
  InterlockedExchange(Target, Value);
{$ELSE}
  TInterlocked.Exchange(Target, Value);
{$ENDIF}
end;

{ TThreadObject }

constructor TThreadObject.Create;
begin
  FCriticalSection := TCriticalSection.Create;
end;

destructor TThreadObject.Destroy;
begin
  FCriticalSection.Free;
  inherited;
end;

procedure TThreadObject.Lock;
begin
  FCriticalSection.Enter;
end;

procedure TThreadObject.Unlock;
begin
  FCriticalSection.Leave;
end;

{ TThreadInterfacedObject }

constructor TThreadInterfacedObject.Create;
begin
  FCriticalSection := TCriticalSection.Create;
end;

destructor TThreadInterfacedObject.Destroy;
begin
  FCriticalSection.Free;
  inherited;
end;

procedure TThreadInterfacedObject.Lock;
begin
  FCriticalSection.Enter;
end;

procedure TThreadInterfacedObject.Unlock;
begin
  FCriticalSection.Leave;
end;

end.
