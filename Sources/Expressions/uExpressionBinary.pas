unit uExpressionBinary;

interface

uses
  uArray, uOperations, uExpression, uValue, uOp, uDataSection;

type
  TExpressionBinary = class(TExpression)
  public
    constructor Create(Data: TDataSection; Left, Right: TExpression; Op: TBinaryOp);
    destructor Destroy; override;
  private
    FOperation: TBinaryOp;
    FLeft: TExpression;
    FRight: TExpression;
  public
    function Eval: IValue; override;
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uExceptions, uTypeVoid, uDataBool, uOpBinary, uOpJump, uOpJit, uOpLoad;

{ TExpressionBinary }

constructor TExpressionBinary.Create(Data: TDataSection; Left, Right: TExpression; Op: TBinaryOp);
begin
  if (Left.ResultType is TTypeVoid) or (Right.ResultType is TTypeVoid) then
    raise ECodeException.Create(E_NO_RESULT);
  inherited Create(Data);
  FLeft := Left;
  FRight := Right;
  FOperation := Op;
  if (FLeft.Group = egStatic) and (FRight.Group = egStatic) then
    FGroup := egStatic
  else
    FGroup := egMixed;
  FResultType := FLeft.ResultType.Apply(FOperation, FRight.ResultType);
end;

destructor TExpressionBinary.Destroy;
begin
  FLeft.Free;
  FRight.Free;
  inherited;
end;

function TExpressionBinary.Compile: TArray<TOp>;
var
  a: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FLeft.Compile);
  if FOperation in [boAnd, boOr] then
  begin
    if FOperation = boAnd then
      AppendOp(Result, [TOpJit.Create(False, 3), TOpLoad.Create(FData.Keep(TDataBool.Create(False)), False)])
    else
      AppendOp(Result, [TOpJit.Create(True, 3), TOpLoad.Create(FData.Keep(TDataBool.Create(True)), False)]);
    a := FRight.Compile;
    AppendOp(Result, [TOpJump.Create(Length(a) + 1)]);
    AppendOp(Result, a);
  end else begin
    AppendOp(Result, FRight.Compile);
    AppendOp(Result, [TOpBinary.Create(FOperation)]);
  end;

end;

function TExpressionBinary.Eval: IValue;
begin
  Result := FLeft.Eval.Apply(FOperation, FRight.Eval);
end;

end.
