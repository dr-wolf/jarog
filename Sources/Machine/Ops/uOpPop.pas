unit uOpPop;

interface

uses
  uDataStack, uOp, uBytecode;

type
  TOpPop = class(TOp)
  public
    constructor Create(Frame: TFrameType);
  private
    FFrame: TFrameType;
  public
    procedure Serialize(var Code: TBytecode); override;
  end;

implementation

uses uOpCode;

{ TOpPop }

constructor TOpPop.Create(Frame: TFrameType);
begin
  inherited Create(OID_POP);
  FFrame := Frame;
end;

procedure TOpPop.Serialize(var Code: TBytecode);
begin
  Code.Op := CompileOp();
  Code.SParam := Byte(FFrame);
end;

end.
