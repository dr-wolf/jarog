unit uExpressionStruct;

interface

uses
  uExpression, uArray, uValue, uOp, uDataSection;

type
  TExpressionStruct = class(TExpression)
  public
    constructor Create(Data: TDataSection; Fields: array of TExpression; Names: array of string);
    destructor Destroy; override;
  private
    FFields: TArray<TExpression>;
    FNameless: Boolean;
  public
    property Nameless: Boolean read FNameless;
    function Compile: TArray<TOp>; override;
    function Eval: IValue; override;
    function Fields: TArray<string>;
  end;

implementation

uses uExceptions, uTypeFactory, uType, uTypeVoid, uTypeStruct, uData, uDataArray, uOpStack;

{ TExpressionStruct }

constructor TExpressionStruct.Create(Data: TDataSection; Fields: array of TExpression; Names: array of string);
var
  Types: TArray<TType>;
  i: Integer;
begin
  inherited Create(Data);
  SetLength(FFields, Length(Fields));
  SetLength(Types, Length(Fields));
  FNameless := Length(Names) = 0;
  if Length(Fields) > 0 then
  begin
    FGroup := Fields[0].Group;
    for i := 0 to High(Types) do
    begin
      if Fields[i].ResultType is TTypeVoid then
        raise ECodeException.Create(E_NO_RESULT);
      FFields[i] := Fields[i];
      Types[i] := Fields[i].ResultType;
      if FGroup <> Fields[i].Group then
        FGroup := egMixed;
    end;
  end
  else
    FGroup := egStatic;
  if (FGroup = egVariable) and not FNameless then
    FGroup := egMixed;
  FResultType := TypeFactory.GetStruct(Types, Names);
end;

destructor TExpressionStruct.Destroy;
var
  i: Integer;
begin
  for i := 0 to High(FFields) do
    FFields[i].Free;
  inherited;
end;

function TExpressionStruct.Fields: TArray<string>;
begin
  Result := (FResultType as TTypeStruct).Fields;
end;

function TExpressionStruct.Compile: TArray<TOp>;
var
  i: Integer;
begin
  SetLength(Result, 0);
  for i := High(FFields) downto 0 do
    AppendOp(Result, FFields[i].Compile);
  AppendOp(Result, [TOpStack.Create(atStruct, Length(FFields))]);
end;

function TExpressionStruct.Eval: IValue;
var
  i: Integer;
  d: TArray<TData>;
begin
  SetLength(d, Length(FFields));
  for i := 0 to High(FFields) do
    d[i] := FFields[i].Eval.Data.Dup;
  Result := TValue.Create(FResultType, TDataArray.Create(d));
end;

end.
