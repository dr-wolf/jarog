unit uFuncFSOpen;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncFSOpen = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_PATH = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Opens file. If file does not exist it will be created. If no path specified in-memory file will be created.
    @type func(path: string -> int)
    @in path string # Path to the file, pass empty string to create in-memory file
    @out - int # File handle
    * }

implementation

uses SysUtils, {$IFDEF UNIX} IOStream, {$ELSE} Windows, {$ENDIF} Classes, uDataNumeric, uDataString,
  uOpCode;

function CreateStdInStream: TStream;
begin
{$IFDEF UNIX}
  Result := TIOStream.Create(iosInput);
{$ELSE}
  Result := THandleStream.Create(GetStdHandle(STD_INPUT_HANDLE));
{$ENDIF}
end;

function CreateStdOutStream: TStream;
begin
{$IFDEF UNIX}
  Result := TIOStream.Create(iosOutPut);
{$ELSE}
  Result := THandleStream.Create(GetStdHandle(STD_OUTPUT_HANDLE));
{$ENDIF}
end;

function CreateStdErrStream: TStream;
begin
{$IFDEF UNIX}
  Result := TIOStream.Create(iosError);
{$ELSE}
  Result := THandleStream.Create(GetStdHandle(STD_ERROR_HANDLE));
{$ENDIF}
end;

{ TFuncFSOpen }

constructor TFuncFSOpen.Create;
begin
  inherited Create([MakeString], MakeInt);
  FFuncUid := FID_FS_OPEN;
end;

procedure TFuncFSOpen.Execute(Context: TFuncContext);
type
  TStreamType = (stStdIn, stStdOut, stStdErr, stMem, stFile);

  function DetectStreamType(FileName: string): TStreamType;
  const
    NAMES: array [TStreamType] of string = ('<stdin>', '<stdout>', '<stderr>', '', '*');
  begin
    Result := Low(TStreamType);
    while Result <> High(TStreamType) do
      if NAMES[Result] = FileName then
        Exit
      else
        Result := Succ(Result);
  end;

var
  fname: string;
  stream: TStream;
begin
  fname := AsString(Context.Data[P_PATH]);
  case DetectStreamType(fname) of
    stStdIn:
      stream := CreateStdInStream;
    stStdOut:
      stream := CreateStdOutStream;
    stStdErr:
      stream := CreateStdErrStream;
    stMem:
      stream := TMemoryStream.Create;
  else
    fname := ExpandFileName(fname);
    if FileExists(fname) then
      stream := TFileStream.Create(fname, fmOpenReadWrite)
    else
      stream := TFileStream.Create(fname, fmCreate or fmOpenReadWrite);
  end;
  Return(TDataNumeric.Create(FCoreContext.Repos.Files.Add(stream, fname)));
end;

end.
