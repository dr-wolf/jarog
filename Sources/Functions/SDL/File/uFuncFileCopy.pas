unit uFuncFileCopy;

interface

uses
  uFuncFile, uFuncContext;

type
  TFuncFileCopy = class(TFuncFile)
  public
    constructor Create;
  private const
    P_SOURCE = 0;
    P_DESTINATION = 1;
    P_SIZE = 2;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Copies bytes from one file stream to another.
    @type func(source: int, destination: int, size: int -> int)
    @in source int # Handle of source stream
    @in destination int # Handle of destignation stream
    @in size int # Amount of bytes to be copied
    @out - int # How many bytes were actually copied
    * }

implementation

uses Classes, uStandardFunc, uDataNumeric, uOpCode;

{ TFuncFileCopy }

constructor TFuncFileCopy.Create;
begin
  inherited Create([MakeInt, MakeInt, MakeInt], MakeInt);
  FFuncUid := FID_FILE_COPY;
end;

procedure TFuncFileCopy.Execute(Context: TFuncContext);
var
  src, dst: TStream;
begin
  src := RetrieveStream(P_SOURCE, Context);
  dst := RetrieveStream(P_DESTINATION, Context);
  Return(TDataNumeric.Create(dst.CopyFrom(src, AsInt(Context.Data[P_SIZE]))));
end;

end.
