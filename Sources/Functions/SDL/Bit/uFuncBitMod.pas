unit uFuncBitMod;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncBitMod = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Finds the remainder after division of one number by another.
    @type func(value: int, divider: int -> int)
    @in value int # The dividend
    @in divider int # Th divider
    @out - int # The remainder
    * }

implementation

uses uTypeFactory, uDataNumeric, uOpCode;

{ TFuncBitMod }

constructor TFuncBitMod.Create;
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt], ['value', 'divider'], TypeFactory.GetInt);
  FFuncUid := FID_BIT_MOD;
end;

procedure TFuncBitMod.Execute(Context: TFuncContext);
begin
  Return(TDataNumeric.Create(AsInt(Context.Data[P_value]) mod AsInt(Context.Data[P_divider])));
end;

end.
