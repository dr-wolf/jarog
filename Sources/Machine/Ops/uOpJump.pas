unit uOpJump;

interface

uses
  uTag, uOp, uBytecode;

type
  TOpJump = class(TOp)
  public
    constructor Create(Delta: Integer; Tag: ITag = nil);
  private
    FDelta: Integer;
    FJumpTag: ITag;
  public
    procedure Serialize(var Code: TBytecode); override;
  end;

implementation

uses uOpCode;

{ TOpJump }

constructor TOpJump.Create(Delta: Integer; Tag: ITag = nil);
begin
  inherited Create(OID_JMP);
  FDelta := Delta;
  FJumpTag := Tag;
end;

procedure TOpJump.Serialize(var Code: TBytecode);
begin
  Code.Op := CompileOp();
  if FJumpTag <> nil then
    Code.LParam := FJumpTag.GetAddress - FIndex + FDelta
  else
    Code.LParam := FDelta;
end;

end.
