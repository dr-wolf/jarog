unit uUnitStr;

interface

uses
  uUnit;

type
  TUnitStr = class(TUnit)
  public
    constructor Create;
  end;

  { *
    @descr String utilities.
    * }

implementation

uses uTypeFactory, uFuncStrFmt, uFuncStrLen, uFuncStrSub, uFuncStrPos, uFuncStrParse, uFuncStrAscii, uFuncStrText,
  uFuncStrLower, uFuncStrUpper;

{ TUnitStr }

constructor TUnitStr.Create;
begin
  inherited Create('<str>');

  AddValue('fmt', MakeFunc(TFuncStrFmt.Create, [TypeFactory.GetString, TypeFactory.GetList(TypeFactory.GetAny)],
    ['format', 'values'], TypeFactory.GetString));
  AddValue('parse', MakeFunc(TFuncStrParse.Create, [TypeFactory.GetString], ['text'], TypeFactory.GetFloat));
  AddValue('len', MakeFunc(TFuncStrLen.Create, [TypeFactory.GetString], ['text'], TypeFactory.GetInt));
  AddValue('sub', MakeFunc(TFuncStrSub.Create, [TypeFactory.GetString, TypeFactory.GetInt, TypeFactory.GetInt],
    ['text', 'pos', 'len'], TypeFactory.GetString));
  AddValue('pos', MakeFunc(TFuncStrPos.Create, [TypeFactory.GetString, TypeFactory.GetString], ['subtext', 'text'],
    TypeFactory.GetInt));
  AddValue('ascii', MakeFunc(TFuncStrAscii.Create, [TypeFactory.GetString], ['text'],
    TypeFactory.GetList(TypeFactory.GetInt)));
  AddValue('text', MakeFunc(TFuncStrText.Create, [TypeFactory.GetList(TypeFactory.GetInt)], ['bytes'],
    TypeFactory.GetString));
  AddValue('lower', MakeFunc(TFuncStrLower.Create, [TypeFactory.GetString], ['text'], TypeFactory.GetString));
  AddValue('upper', MakeFunc(TFuncStrUpper.Create, [TypeFactory.GetString], ['text'], TypeFactory.GetString));
end;

end.
