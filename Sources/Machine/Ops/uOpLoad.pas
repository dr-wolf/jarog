unit uOpLoad;

interface

uses
  uOp, uBytecode;

type
  TOpLoad = class(TOp)
  public
    constructor Create(DataId: Integer; Variable: Boolean);
  private
    FVariable: Boolean;
    FDataId: Integer;
  public
    procedure Serialize(var Code: TBytecode); override;
  end;

implementation

uses uOpCode;

{ TOpLoad }

constructor TOpLoad.Create(DataId: Integer; Variable: Boolean);
begin
  inherited Create(OID_LOAD);
  FDataId := DataId;
  FVariable := Variable;
end;

procedure TOpLoad.Serialize(var Code: TBytecode);
begin
  Code.Op := CompileOp(FVariable);
  Code.LParam := FDataId;
end;

end.
