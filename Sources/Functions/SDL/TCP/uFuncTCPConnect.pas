unit uFuncTCPConnect;

interface

uses
  uFuncTCP, uFuncContext, BlckSock;

type
  TFuncTCPConnect = class(TFuncTCP)
  public
    constructor Create;
  private const
    P_IP = 0;
    P_PORT = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Connects to remote IP abd port.
    @type func(ip: string, port: int -> int)
    @in ip string # Remote IP
    @in port int # Remote port
    @out - int # Handle of a socket
    * }

implementation

uses SysUtils, uDataNumeric, uDataString, uExceptions, uOpCode;

{ TFuncTCPConnect }

constructor TFuncTCPConnect.Create;
begin
  inherited Create([MakeString, MakeInt], MakeInt);
  FFuncUid := FID_TCP_CONNECT;
end;

procedure TFuncTCPConnect.Execute(Context: TFuncContext);
var
  ip: string;
  port: Integer;
  sock: TTCPBlockSocket;
begin
  ip := RetrieveIP(P_IP, Context);
  port := RetrievePort(P_PORT, Context);
  sock := TTCPBlockSocket.Create;
  sock.Connect(ip, IntToStr(port));
  if sock.LastError <> 0 then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create(sock.LastErrorDesc));
  Return(TDataNumeric.Create(FCoreContext.Repos.TCPSocks.Add(sock)));
end;

end.
