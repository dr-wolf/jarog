unit uFuncIOCurPos;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncIOCurPos = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Returns cursor position.
    @type func(-> struct(x: int, y: int))
    @out - struct(x: int, y: int) # Cursor coords
    * }

implementation

uses uDataNumeric, uDataArray, uOpCode;

{ TFuncIOCurPos }

constructor TFuncIOCurPos.Create;
begin
  inherited Create([], MakeStruct([MakeInt, MakeInt]));
  FFuncUid := FID_IO_CURPOS;
end;

procedure TFuncIOCurPos.Execute(Context: TFuncContext);
var
  X, Y: Integer;
begin
  FCoreContext.IOProvider.GetCursorPos(X, Y);
  Return(TDataArray.Create([TDataNumeric.Create(X), TDataNumeric.Create(Y)]))
end;

end.
