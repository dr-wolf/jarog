unit uFuncIOClear;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncIOClear = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Clears console.
    @type func()
    * }

implementation

uses uOpCode;

{ TFuncIOClear }

constructor TFuncIOClear.Create;
begin
  inherited Create([]);
  FFuncUid := FID_IO_CLEAR;
end;

procedure TFuncIOClear.Execute(Context: TFuncContext);
begin
  FCoreContext.IOProvider.ClearScreen;
end;

end.
