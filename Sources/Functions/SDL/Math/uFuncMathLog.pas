unit uFuncMathLog;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncMathLog = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_BASE = 0;
    P_VALUE = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Returns the logarithm of a number.
    @type func(base: float, value: float -> float)
    @in value float # Logarithm base
    @in value float # Initial number
    @out - float # Result value
    * }

implementation

uses uDataNumeric, uOpCode, uBigFloat, mp_types, mp_real;

{ TFuncMathLog }

constructor TFuncMathLog.Create;
begin
  inherited Create([MakeFloat, MakeFloat], MakeFloat);
  FFuncUid := FID_MATH_LOG;
end;

procedure TFuncMathLog.Execute(Context: TFuncContext);
var
  a, b, r: mp_float;
begin
  mpf_init(r);
  a := AsNumeric(Context.Data[P_BASE]).ToFloat;
  b := AsNumeric(Context.Data[P_VALUE]).ToFloat;
  mpf_logbase(a, b, r);
  mpf_clear2(a, b);
  Return(TDataNumeric.Create(TBigFloat.Create(r)));
end;

end.
