unit uFuncOSLsos;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncOSLsos = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_TCP = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Returns list of handles of network TCP or UDP sockets that are opened by program.
    @type func(tcp: bool -> list(int))
    @in tcp bool # if true returns TCP sockets othervise UDP sockets are returned
    @out - list(int) # List of handles of open sockets
    * }

implementation

uses uArray, uData, uDataBool, uDataNumeric, uDataArray, uOpCode;

{ TFuncOSLsos }

constructor TFuncOSLsos.Create;
begin
  inherited Create([MakeBool], MakeList(MakeInt));
  FFuncUid := FID_OS_LSOS;
end;

procedure TFuncOSLsos.Execute(Context: TFuncContext);
var
  a: TArray<TData>;
  h: TArray<Integer>;
  I: Integer;
begin
  if AsBool(Context.Data[P_TCP]) then
    h := FCoreContext.Repos.TCPSocks.GetAllHandles
  else
    h := FCoreContext.Repos.UDPSocks.GetAllHandles;
  SetLength(a, Length(h));
  for I := 0 to High(h) do
    a[I] := TDataNumeric.Create(h[I]);
  Return(TDataArray.Create(a, False));
end;

end.
