unit uExpressionCast;

interface

uses
  uArray, uExpression, uType, uValue, uOp, uDataSection;

type
  TExpressionCast = class(TExpression)
  public
    constructor Create(Data: TDataSection; Expression: TExpression; TypeInfo: TType);
    destructor Destroy; override;
  private
    FExpression: TExpression;
  public
    function Compile: TArray<TOp>; override;
    function Eval: IValue; override;
  end;

implementation

uses uExceptions, uTypeVoid, uOpMove, uOpLoad;

{ TExpressionCast }

constructor TExpressionCast.Create(Data: TDataSection; Expression: TExpression; TypeInfo: TType);
begin
  if Expression.ResultType is TTypeVoid then
    raise ECodeException.Create(E_NO_RESULT);
  inherited Create(Data);
  FExpression := Expression;
  if Expression.Group = egStatic then
    FGroup := egStatic
  else
    FGroup := egMixed;
  FResultType := TypeInfo;
end;

destructor TExpressionCast.Destroy;
begin
  FExpression.Free;
  inherited;
end;

function TExpressionCast.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FExpression.Compile);
  AppendOp(Result, [TOpLoad.Create(FData.Keep(FResultType.Default), False), TOpMove.Create(False, True)]);
end;

function TExpressionCast.Eval: IValue;
var
  r: IValue;
begin
  r := FExpression.Eval;
  Result := TValue.Create(FResultType);
  Result.Data.Assign(r.Data);
end;

end.
