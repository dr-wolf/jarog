unit uFuncFSChDir;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncFSChDir = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_PATH = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Changes current directory.
    @type func(path: string)
    @in path string # New path
    * }

implementation

uses SysUtils, uDataString, uOpCode;

{ TFuncFSChDir }

constructor TFuncFSChDir.Create;
begin
  inherited Create([MakeString]);
  FFuncUid := FID_FS_CHDIR;
end;

procedure TFuncFSChDir.Execute(Context: TFuncContext);
begin
  if not SetCurrentDir(AsString(Context.Data[P_PATH])) then
    RaiseLastOSError;
end;

end.
