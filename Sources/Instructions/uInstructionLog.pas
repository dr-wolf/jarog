unit uInstructionLog;

interface

uses
  uArray, uInstruction, uExpression, uOp;

type
  TInstructionLog = class(TInstruction)
  public
    constructor Create(Expression: TExpression; TypeHintId: Integer);
    destructor Destroy; override;
  private
    FExpression: TExpression;
    FTypeHintId: Integer;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uExceptions, uOpLog, uTypeVoid, uOpClean;

{ TInstructionLog }

constructor TInstructionLog.Create(Expression: TExpression; TypeHintId: Integer);
begin
  if Expression.ResultType is TTypeVoid then
    raise ECodeException.Create(E_NO_RESULT);
  inherited Create;
  FExpression := Expression;
  FTypeHintId := TypeHintId;
end;

destructor TInstructionLog.Destroy;
begin
  FExpression.Free;
  inherited;
end;

function TInstructionLog.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FExpression.Compile);
  AppendOp(Result, [TOpLog.Create(FTypeHintId), TOpClean.Create]);
end;

end.
