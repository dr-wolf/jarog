unit uFuncMathArcCos;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncMathArcCos = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_VALUE = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Retruns angle of the given cosinus.
    @type func(value: float -> float)
    @in value float # Cosinus of the angle
    @out - float # Angle in radians
    * }

implementation

uses uDataNumeric, uOpCode, uBigFloat, mp_types, mp_real;

{ TFuncMathArcCos }

constructor TFuncMathArcCos.Create;
begin
  inherited Create([MakeFloat], MakeFloat);
  FFuncUid := FID_MATH_ARCCOS;
end;

procedure TFuncMathArcCos.Execute(Context: TFuncContext);
var
  v, r: mp_float;
begin
  mpf_init(r);
  v := AsNumeric(Context.Data[P_VALUE]).ToFloat;
  mpf_arccos(v, r);
  mpf_clear(v);
  Return(TDataNumeric.Create(TBigFloat.Create(r)));
end;

end.
