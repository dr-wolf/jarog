program jarog;

{$IFNDEF FPC}
{$APPTYPE CONSOLE}
{$ENDIF}
{$R *.res}
(*
  {$IFDEF UNIX}
  CThreads,
  CMem,
  {$ENDIF}
*)

uses
  {$IFDEF UNIX}
  CThreads,
  CMem,
  {$ENDIF}
  SysUtils,
  Classes,
  blcksock in '..\..\Libs\Synapse\blcksock.pas',
  synacode in '..\..\Libs\Synapse\synacode.pas',
  synafpc in '..\..\Libs\Synapse\synafpc.pas',
  synaip in '..\..\Libs\Synapse\synaip.pas',
  synautil in '..\..\Libs\Synapse\synautil.pas',
  synsock in '..\..\Libs\Synapse\synsock.pas',
  mp_base in '..\..\Libs\MPArith\mp_base.pas',
  btypes in '..\..\Libs\MPArith\btypes.pas',
  mp_types in '..\..\Libs\MPArith\mp_types.pas',
  mp_prng in '..\..\Libs\MPArith\mp_prng.pas',
  isaac in '..\..\Libs\MPArith\isaac.pas',
  mp_real in '..\..\Libs\MPArith\mp_real.pas',
  mp_rc32 in '..\..\Libs\MPArith\mp_rc32.pas',
  RegExpr in '..\..\Libs\RegExpr.pas',
  MD5 in '..\..\Libs\MD5.pas',
  ChildProcess in '..\..\Libs\DProcess\ChildProcess.pas',
  ProcessPipes in '..\..\Libs\DProcess\ProcessPipes.pas',
  uApp in '..\..\Sources\Machine\uApp.pas',
  uAppMold in '..\..\Sources\Machine\Compiler\uAppMold.pas',
  uArray in '..\..\Sources\Common\uArray.pas',
  uBigFloat in '..\..\Sources\Value\Datas\Numeric\uBigFloat.pas',
  uBigInt in '..\..\Sources\Value\Datas\Numeric\uBigInt.pas',
  uBytecode in '..\..\Sources\Machine\Bytecode\uBytecode.pas',
  uBytecodeExecutor in '..\..\Sources\Machine\Bytecode\uBytecodeExecutor.pas',
  uBytecodeLogger in '..\..\Sources\Machine\Bytecode\uBytecodeLogger.pas',
  uClosure in '..\..\Sources\Value\uClosure.pas',
  uConfig in '..\..\Sources\uConfig.pas',
  uConsole in '..\uConsole.pas',
  uConsoleInterface in '..\..\Sources\Machine\uConsoleInterface.pas',
  uCoreContext in '..\..\Sources\Machine\uCoreContext.pas',
  uCustomFunc in '..\..\Sources\Functions\uCustomFunc.pas',
  uData in '..\..\Sources\Value\Datas\uData.pas',
  uDataAny in '..\..\Sources\Value\Datas\uDataAny.pas',
  uDataArray in '..\..\Sources\Value\Datas\uDataArray.pas',
  uDataBool in '..\..\Sources\Value\Datas\uDataBool.pas',
  uDataFunc in '..\..\Sources\Value\Datas\uDataFunc.pas',
  uDataLoader in '..\..\Sources\Machine\Compiler\uDataLoader.pas',
  uDataNumeric in '..\..\Sources\Value\Datas\uDataNumeric.pas',
  uDataPipe in '..\..\Sources\Value\Datas\uDataPipe.pas',
  uDataPool in '..\..\Sources\Value\uDataPool.pas',
  uDataSection in '..\..\Sources\Value\uDataSection.pas',
  uDataSet in '..\..\Sources\Value\Datas\uDataSet.pas',
  uDataStack in '..\..\Sources\Value\uDataStack.pas',
  uDataString in '..\..\Sources\Value\Datas\uDataString.pas',
  uDataStub in '..\..\Sources\Value\Datas\uDataStub.pas',
  uDebugger in '..\..\Sources\Machine\Debugger\uDebugger.pas',
  uDebuggerConst in '..\..\Sources\Machine\Debugger\uDebuggerConst.pas',
  uDict in '..\..\Sources\Common\uDict.pas',
  uEnvironment in '..\..\Sources\uEnvironment.pas',
  uExceptions in '..\..\Sources\uExceptions.pas',
  uExecFile in '..\..\Sources\Machine\Compiler\uExecFile.pas',
  uFileRepository in '..\..\Sources\Units\Repositories\uFileRepository.pas',
  uFormatTemplate in '..\..\Sources\Common\uFormatTemplate.pas',
  uFuncBitAnd in '..\..\Sources\Functions\SDL\Bit\uFuncBitAnd.pas',
  uFuncBitNot in '..\..\Sources\Functions\SDL\Bit\uFuncBitNot.pas',
  uFuncBitOr in '..\..\Sources\Functions\SDL\Bit\uFuncBitOr.pas',
  uFuncBitShl in '..\..\Sources\Functions\SDL\Bit\uFuncBitShl.pas',
  uFuncBitShr in '..\..\Sources\Functions\SDL\Bit\uFuncBitShr.pas',
  uFuncBitXor in '..\..\Sources\Functions\SDL\Bit\uFuncBitXor.pas',
  uFuncContext in '..\..\Sources\Functions\uFuncContext.pas',
  uFuncDateDecode in '..\..\Sources\Functions\SDL\Date\uFuncDateDecode.pas',
  uFuncDateEncode in '..\..\Sources\Functions\SDL\Date\uFuncDateEncode.pas',
  uFuncDateFmt in '..\..\Sources\Functions\SDL\Date\uFuncDateFmt.pas',
  uFuncDateParse in '..\..\Sources\Functions\SDL\Date\uFuncDateParse.pas',
  uFuncFile in '..\..\Sources\Functions\SDL\File\uFuncFile.pas',
  uFuncFileCopy in '..\..\Sources\Functions\SDL\File\uFuncFileCopy.pas',
  uFuncFileGetPos in '..\..\Sources\Functions\SDL\File\uFuncFileGetPos.pas',
  uFuncFilePack in '..\..\Sources\Functions\SDL\File\uFuncFilePack.pas',
  uFuncFileReadFloat in '..\..\Sources\Functions\SDL\File\uFuncFileReadFloat.pas',
  uFuncFileReadInt in '..\..\Sources\Functions\SDL\File\uFuncFileReadInt.pas',
  uFuncFileReadString in '..\..\Sources\Functions\SDL\File\uFuncFileReadString.pas',
  uFuncFileSetPos in '..\..\Sources\Functions\SDL\File\uFuncFileSetPos.pas',
  uFuncFileSize in '..\..\Sources\Functions\SDL\File\uFuncFileSize.pas',
  uFuncFileTrunc in '..\..\Sources\Functions\SDL\File\uFuncFileTrunc.pas',
  uFuncFileUnpack in '..\..\Sources\Functions\SDL\File\uFuncFileUnpack.pas',
  uFuncFileWriteFloat in '..\..\Sources\Functions\SDL\File\uFuncFileWriteFloat.pas',
  uFuncFileWriteInt in '..\..\Sources\Functions\SDL\File\uFuncFileWriteInt.pas',
  uFuncFileWriteString in '..\..\Sources\Functions\SDL\File\uFuncFileWriteString.pas',
  uFuncFSAbsPath in '..\..\Sources\Functions\SDL\FS\uFuncFSAbsPath.pas',
  uFuncFSChDir in '..\..\Sources\Functions\SDL\FS\uFuncFSChDir.pas',
  uFuncFSClose in '..\..\Sources\Functions\SDL\FS\uFuncFSClose.pas',
  uFuncFSDelete in '..\..\Sources\Functions\SDL\FS\uFuncFSDelete.pas',
  uFuncFSMkDir in '..\..\Sources\Functions\SDL\FS\uFuncFSMkDir.pas',
  uFuncFSOpen in '..\..\Sources\Functions\SDL\FS\uFuncFSOpen.pas',
  uFuncFSPwd in '..\..\Sources\Functions\SDL\FS\uFuncFSPwd.pas',
  uFuncFSRmDir in '..\..\Sources\Functions\SDL\FS\uFuncFSRmDir.pas',
  uFuncFSScan in '..\..\Sources\Functions\SDL\FS\uFuncFSScan.pas',
  uFuncFSStat in '..\..\Sources\Functions\SDL\FS\uFuncFSStat.pas',
  uFuncGCClean in '..\..\Sources\Functions\SDL\GC\uFuncGCClean.pas',
  uFuncGCDisable in '..\..\Sources\Functions\SDL\GC\uFuncGCDisable.pas',
  uFuncGCEnable in '..\..\Sources\Functions\SDL\GC\uFuncGCEnable.pas',
  uFuncIO in '..\..\Sources\Functions\SDL\IO\uFuncIO.pas',
  uFuncIOClear in '..\..\Sources\Functions\SDL\IO\uFuncIOClear.pas',
  uFuncIOColor in '..\..\Sources\Functions\SDL\IO\uFuncIOColor.pas',
  uFuncIOCurPos in '..\..\Sources\Functions\SDL\IO\uFuncIOCurPos.pas',
  uFuncIOGet in '..\..\Sources\Functions\SDL\IO\uFuncIOGet.pas',
  uFuncIOGoto in '..\..\Sources\Functions\SDL\IO\uFuncIOGoto.pas',
  uFuncIOKey in '..\..\Sources\Functions\SDL\IO\uFuncIOKey.pas',
  uFuncIOPause in '..\..\Sources\Functions\SDL\IO\uFuncIOPause.pas',
  uFuncIOPut in '..\..\Sources\Functions\SDL\IO\uFuncIOPut.pas',
  uFuncIOSize in '..\..\Sources\Functions\SDL\IO\uFuncIOSize.pas',
  uFuncMathAbs in '..\..\Sources\Functions\SDL\Math\uFuncMathAbs.pas',
  uFuncMathArcCos in '..\..\Sources\Functions\SDL\Math\uFuncMathArcCos.pas',
  uFuncMathArcSin in '..\..\Sources\Functions\SDL\Math\uFuncMathArcSin.pas',
  uFuncMathArcTan in '..\..\Sources\Functions\SDL\Math\uFuncMathArcTan.pas',
  uFuncMathCos in '..\..\Sources\Functions\SDL\Math\uFuncMathCos.pas',
  uFuncMathFloor in '..\..\Sources\Functions\SDL\Math\uFuncMathFloor.pas',
  uFuncMathFrac in '..\..\Sources\Functions\SDL\Math\uFuncMathFrac.pas',
  uFuncMathLog in '..\..\Sources\Functions\SDL\Math\uFuncMathLog.pas',
  uFuncMathRand in '..\..\Sources\Functions\SDL\Math\uFuncMathRand.pas',
  uFuncMathSin in '..\..\Sources\Functions\SDL\Math\uFuncMathSin.pas',
  uFuncMathTan in '..\..\Sources\Functions\SDL\Math\uFuncMathTan.pas',
  uFuncOSEnvList in '..\..\Sources\Functions\SDL\OS\uFuncOSEnvList.pas',
  uFuncOSExec in '..\..\Sources\Functions\SDL\OS\uFuncOSExec.pas',
  uFuncOSGetEnv in '..\..\Sources\Functions\SDL\OS\uFuncOSGetEnv.pas',
  uFuncOSLsof in '..\..\Sources\Functions\SDL\OS\uFuncOSLsof.pas',
  uFuncOSLsos in '..\..\Sources\Functions\SDL\OS\uFuncOSLsos.pas',
  uFuncOSPid in '..\..\Sources\Functions\SDL\OS\uFuncOSPid.pas',
  uFuncOSSetEnv in '..\..\Sources\Functions\SDL\OS\uFuncOSSetEnv.pas',
  uFuncOSSleep in '..\..\Sources\Functions\SDL\OS\uFuncOSSleep.pas',
  uFuncOSThreadId in '..\..\Sources\Functions\SDL\OS\uFuncOSThreadId.pas',
  uFuncOSTime in '..\..\Sources\Functions\SDL\OS\uFuncOSTime.pas',
  uFuncProc in '..\..\Sources\Functions\SDL\Proc\uFuncProc.pas',
  uFuncProcClose in '..\..\Sources\Functions\SDL\Proc\uFuncProcClose.pas',
  uFuncProcOpen in '..\..\Sources\Functions\SDL\Proc\uFuncProcOpen.pas',
  uFuncProcRead in '..\..\Sources\Functions\SDL\Proc\uFuncProcRead.pas',
  uFuncProcStart in '..\..\Sources\Functions\SDL\Proc\uFuncProcStart.pas',
  uFuncProcWait in '..\..\Sources\Functions\SDL\Proc\uFuncProcWait.pas',
  uFuncProcWrite in '..\..\Sources\Functions\SDL\Proc\uFuncProcWrite.pas',
  uFuncRex in '..\..\Sources\Functions\SDL\Rex\uFuncRex.pas',
  uFuncRexFind in '..\..\Sources\Functions\SDL\Rex\uFuncRexFind.pas',
  uFuncRexMatch in '..\..\Sources\Functions\SDL\Rex\uFuncRexMatch.pas',
  uFuncRexReplace in '..\..\Sources\Functions\SDL\Rex\uFuncRexReplace.pas',
  uFuncRexSplit in '..\..\Sources\Functions\SDL\Rex\uFuncRexSplit.pas',
  uFuncStrAscii in '..\..\Sources\Functions\SDL\Str\uFuncStrAscii.pas',
  uFuncStrFmt in '..\..\Sources\Functions\SDL\Str\uFuncStrFmt.pas',
  uFuncStrLen in '..\..\Sources\Functions\SDL\Str\uFuncStrLen.pas',
  uFuncStrLower in '..\..\Sources\Functions\SDL\Str\uFuncStrLower.pas',
  uFuncStrParse in '..\..\Sources\Functions\SDL\Str\uFuncStrParse.pas',
  uFuncStrPos in '..\..\Sources\Functions\SDL\Str\uFuncStrPos.pas',
  uFuncStrSub in '..\..\Sources\Functions\SDL\Str\uFuncStrSub.pas',
  uFuncStrText in '..\..\Sources\Functions\SDL\Str\uFuncStrText.pas',
  uFuncStrUpper in '..\..\Sources\Functions\SDL\Str\uFuncStrUpper.pas',
  uFuncTCP in '..\..\Sources\Functions\SDL\TCP\uFuncTCP.pas',
  uFuncTCPAccept in '..\..\Sources\Functions\SDL\TCP\uFuncTCPAccept.pas',
  uFuncTCPClose in '..\..\Sources\Functions\SDL\TCP\uFuncTCPClose.pas',
  uFuncTCPConnect in '..\..\Sources\Functions\SDL\TCP\uFuncTCPConnect.pas',
  uFuncTCPListen in '..\..\Sources\Functions\SDL\TCP\uFuncTCPListen.pas',
  uFuncTCPReceive in '..\..\Sources\Functions\SDL\TCP\uFuncTCPReceive.pas',
  uFuncTCPRemote in '..\..\Sources\Functions\SDL\TCP\uFuncTCPRemote.pas',
  uFuncTCPSend in '..\..\Sources\Functions\SDL\TCP\uFuncTCPSend.pas',
  uFunction in '..\..\Sources\Functions\uFunction.pas',
  uFunctionLoader in '..\..\Sources\Machine\Compiler\uFunctionLoader.pas',
  uFuncUDP in '..\..\Sources\Functions\SDL\UDP\uFuncUDP.pas',
  uFuncUDPBind in '..\..\Sources\Functions\SDL\UDP\uFuncUDPBind.pas',
  uFuncUDPBroadcast in '..\..\Sources\Functions\SDL\UDP\uFuncUDPBroadcast.pas',
  uFuncUDPClose in '..\..\Sources\Functions\SDL\UDP\uFuncUDPClose.pas',
  uFuncUDPReceive in '..\..\Sources\Functions\SDL\UDP\uFuncUDPReceive.pas',
  uFuncUDPSend in '..\..\Sources\Functions\SDL\UDP\uFuncUDPSend.pas',
  uHashConverter in '..\..\Sources\Value\Converters\uHashConverter.pas',
  uIdentifierRegistry in '..\..\Sources\Builders\uIdentifierRegistry.pas',
  uInteger in '..\..\Sources\Value\Datas\Numeric\uInteger.pas',
  uIOProvider in '..\..\Sources\Machine\uIOProvider.pas',
  uMachine in '..\..\Sources\Machine\uMachine.pas',
  uMessageQueue in '..\..\Sources\Machine\uMessageQueue.pas',
  uNumeric in '..\..\Sources\Value\Datas\Numeric\uNumeric.pas',
  uOp in '..\..\Sources\Machine\Ops\uOp.pas',
  uOpBinary in '..\..\Sources\Machine\Ops\uOpBinary.pas',
  uOpCall in '..\..\Sources\Machine\Ops\uOpCall.pas',
  uOpClean in '..\..\Sources\Machine\Ops\uOpClean.pas',
  uOpCode in '..\..\Sources\Machine\Compiler\uOpCode.pas',
  uOpEmpty in '..\..\Sources\Machine\Ops\uOpEmpty.pas',
  uOperations in '..\..\Sources\Value\uOperations.pas',
  uOpField in '..\..\Sources\Machine\Ops\uOpField.pas',
  uOpItem in '..\..\Sources\Machine\Ops\uOpItem.pas',
  uOpJit in '..\..\Sources\Machine\Ops\uOpJit.pas',
  uOpJump in '..\..\Sources\Machine\Ops\uOpJump.pas',
  uOpLoad in '..\..\Sources\Machine\Ops\uOpLoad.pas',
  uOpLog in '..\..\Sources\Machine\Ops\uOpLog.pas',
  uOpMove in '..\..\Sources\Machine\Ops\uOpMove.pas',
  uOpNop in '..\..\Sources\Machine\Ops\uOpNop.pas',
  uOpPop in '..\..\Sources\Machine\Ops\uOpPop.pas',
  uOpRange in '..\..\Sources\Machine\Ops\uOpRange.pas',
  uOpReturn in '..\..\Sources\Machine\Ops\uOpReturn.pas',
  uOpRun in '..\..\Sources\Machine\Ops\uOpRun.pas',
  uOpSplit in '..\..\Sources\Machine\Ops\uOpSplit.pas',
  uOpStack in '..\..\Sources\Machine\Ops\uOpStack.pas',
  uOpSync in '..\..\Sources\Machine\Ops\uOpSync.pas',
  uOpTail in '..\..\Sources\Machine\Ops\uOpTail.pas',
  uOpThrow in '..\..\Sources\Machine\Ops\uOpThrow.pas',
  uOpUnary in '..\..\Sources\Machine\Ops\uOpUnary.pas',
  uParserLogic in '..\..\Sources\Parser\uParserLogic.pas',
  uProcessRepository in '..\..\Sources\Units\Repositories\uProcessRepository.pas',
  uQueue in '..\..\Sources\Common\uQueue.pas',
  uRepository in '..\..\Sources\Units\Repositories\uRepository.pas',
  uRepositoryStorage in '..\..\Sources\Units\Repositories\uRepositoryStorage.pas',
  uRuntime in '..\..\Sources\uRuntime.pas',
  uSocketListener in '..\..\Sources\Machine\Debugger\uSocketListener.pas',
  uSocketRepository in '..\..\Sources\Units\Repositories\uSocketRepository.pas',
  uSourceParser in '..\..\Sources\Parser\uSourceParser.pas',
  uStack in '..\..\Sources\Common\uStack.pas',
  uStandardFunc in '..\..\Sources\Functions\uStandardFunc.pas',
  uState in '..\..\Sources\Machine\uState.pas',
  uStreamConverter in '..\..\Sources\Value\Converters\uStreamConverter.pas',
  uStringConverter in '..\..\Sources\Value\Converters\uStringConverter.pas',
  uStringEscaping in '..\..\Sources\Parser\uStringEscaping.pas',
  uStringSet in '..\..\Sources\Common\uStringSet.pas',
  uTag in '..\..\Sources\Machine\uTag.pas',
  uThreadSafe in '..\..\Sources\Common\uThreadSafe.pas',
  uTypedStream in '..\..\Sources\Common\uTypedStream.pas',
  uWorker in '..\..\Sources\Machine\uWorker.pas',
  uWorkerList in '..\..\Sources\Machine\uWorkerList.pas';

procedure PrintGreeting;
begin
  Writeln('Jarog Runtime ' + APP_VERSION);
  Writeln('by Taras "Dr.Wolf" Supyk');
  Writeln;
end;

var
  Config: TConfig;
  Runtime: TRuntime;
  s: TStream;

begin
{$IFNDEF FPC}
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
{$ENDIF}
  FormatSettings.DecimalSeparator := '.';
  TDataPool.Initialize;

  Config := TConfig.Create([cpHelp, cpDebug]);
  if Config.Help then
  begin
    PrintGreeting;
    Config.PrintHelp;
  end
  else
    try
      Runtime := TRuntime.Create(Config, TIOProvider.Create(TConsole.Create, Config.DebugPort > 0));
      try
        s := TFileStream.Create(Config.Script, fmOpenReadWrite);
        Runtime.LoadFromStream(s);
        Runtime.Run;
      finally
        Runtime.Free;
        s.Free;
      end;
    except
      on e: ERuntimeException do
        Writeln('RUNTIME ERROR: ' + e.Message);
      on e: Exception do
        Writeln('UNEXPECTED ERROR: ' + e.Message);
    end;

  TDataPool.Finalize;
  Config.Free;

end.
