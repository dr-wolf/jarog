unit uStringEscaping;

interface

function StringEscape(const Text: string): string;
function StringUnescape(const Text: string): string;

implementation

uses SysUtils, uArray;

type
  TPatch = record
    Start: Integer;
    Size: Integer;
    Patch: string;
  end;

  TPatchList = record
    Patches: TArray<TPatch>;
    Size: Integer;
  end;

const
  ESCAPED: set of AnsiChar = [#$D, #$A, #$9, '\', '"'];

function Patch(Start, Size: Integer; const Patch: string): TPatch;
begin
  Result.Start := Start;
  Result.Size := Size;
  Result.Patch := Patch;
end;

function PatchList: TPatchList;
begin
  Result.Patches := nil;
  Result.Size := 0;
end;

procedure Append(P: TPatch; var List: TPatchList);
begin
  if List.Size >= Length(List.Patches) then
    SetLength(List.Patches, Length(List.Patches) + 128);
  List.Patches[List.Size] := P;
  Inc(List.Size);
end;

function BuildString(P: TPatchList; const Text: string): string;

  function PInk(var I: Integer; const D: Integer = 1): Integer; inline;
  begin
    Result := I;
    Inc(I, D);
  end;

var
  I, Size, n: Integer;
begin
  Size := 0;
  for I := 0 to P.Size - 1 do
    Inc(Size, P.Patches[I].Size);
  n := 1;
  SetLength(Result, Size);
  for I := 0 to P.Size - 1 do
    if P.Patches[I].Start > 0 then
      Move(Text[P.Patches[I].Start], Result[PInk(n, P.Patches[I].Size)], P.Patches[I].Size * SizeOf(Char))
    else
      Move(P.Patches[I].Patch[1], Result[PInk(n, P.Patches[I].Size)], P.Patches[I].Size * SizeOf(Char));
end;

function StringEscape(const Text: string): string;

  function Escape(c: Char; var Seq: string): Boolean;
  begin
    Result := (c in ESCAPED) or (c < #$20) or (c > #$7F);
    if Result then
      case c of
        #$D:
          Seq := '\r';
        #$A:
          Seq := '\n';
        #$9:
          Seq := '\t';
        '\', '"':
          Seq := '\' + c;
      else
        if (Ord(c) < $20) or (Ord(c) > $7F) then
        begin
          if Ord(c) <= $FF then
            Seq := '\x' + IntToHex(Ord(c), 2)
          else
            Seq := '\u' + IntToHex(Ord(c), 4);
        end
        else
          Seq := '\' + c;
      end;
  end;

var
  I, n: Integer;
  P: TPatchList;
  buff: string;

begin
  n := 1;
  P := PatchList;
  for I := 1 to Length(Text) do
    if (I >= n) and Escape(Text[I], buff) then
    begin
      if I > n then
        Append(Patch(n, I - n, ''), P);
      Append(Patch(-1, Length(buff), buff), P);
      n := I + 1;
    end;
  if n <= Length(Text) then
    Append(Patch(n, Length(Text) - n + 1, ''), P);

  Result := BuildString(P, Text);
end;

function StringUnescape(const Text: string): string;

  function Unescape(P: Integer; var Size: Integer; var c: Char): Boolean;
  begin
    Result := Text[P] = '\';
    if Result then
    begin
      if Text[P + 1] = 'u' then
        Size := 6
      else if Text[P + 1] = 'x' then
        Size := 4
      else
        Size := 2;
      case Text[P + 1] of
        'r':
          c := #$D;
        'n':
          c := #$A;
        't':
          c := #$9;
        'x':
          if P + 3 < Length(Text) then
            c := Chr(StrToInt('$' + Text[P + 2] + Text[P + 3]))
          else
            Result := False;
        'u':
          if P + 5 < Length(Text) then
            c := Chr(StrToInt('$' + Text[P + 2] + Text[P + 3] + Text[P + 4] + Text[P + 5]))
          else
            Result := False;
      else
        c := Text[P + 1];
      end;
    end;
  end;

var
  I, s, n: Integer;
  P: TPatchList;
  c: Char;
begin
  n := 2;
  P := PatchList;
  for I := 2 to Length(Text) - 1 do
    if (I >= n) and Unescape(I, s, c) then
    begin
      if I > n then
        Append(Patch(n, I - n, ''), P);
      Append(Patch(-1, 1, c), P);
      n := I + s;
    end;
  if n < Length(Text) then
    Append(Patch(n, Length(Text) - n, ''), P);

  Result := BuildString(P, Text);
end;

end.
