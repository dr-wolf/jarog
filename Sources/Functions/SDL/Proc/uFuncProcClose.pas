unit uFuncProcClose;

interface

uses
  uFuncProc, uFuncContext;

type
  TFuncProcClose = class(TFuncProc)
  public
    constructor Create;
  private const
    P_PROC = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Terminates and closes a process.
    @type func(socket: int)
    @in proc int # Process handle
    * }

implementation

uses uDataNumeric, uOpCode;

{ TFuncProcClose }

constructor TFuncProcClose.Create;
begin
  inherited Create([MakeInt]);
  FFuncUid := FID_PROC_CLOSE;
end;

procedure TFuncProcClose.Execute(Context: TFuncContext);
begin
  FCoreContext.Repos.Processes.Close(AsInt(Context.Data[P_PROC]));
end;

end.
