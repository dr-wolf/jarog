unit uTypeVoid;

interface

uses
  uType, uData;

type
  TTypeVoid = class(TType)
  public
    function Pipeable: Boolean; override;
    function Compatible(T: TType; StrictCompat: Boolean = False): Boolean; override;
    function Default: TData; override;
  end;

implementation

{ TTypeVoid }

function TTypeVoid.Compatible(T: TType; StrictCompat: Boolean = False): Boolean;
begin
  Result := T is TTypeVoid;
end;

function TTypeVoid.Default: TData;
begin
  Result := nil;
end;

function TTypeVoid.Pipeable: Boolean;
begin
  Result := False; // ^_^
end;

end.
