unit uFuncFSClose;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncFSClose = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_HANDLE = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Closes file handle.
    @type func(handle: int)
    @in handle int # File handle
    * }

implementation

uses uDataNumeric, uOpCode;

{ TFuncFSClose }

constructor TFuncFSClose.Create;
begin
  inherited Create([MakeInt]);
  FFuncUid := FID_FS_CLOSE;
end;

procedure TFuncFSClose.Execute(Context: TFuncContext);
begin
  FCoreContext.Repos.Files.Close(AsInt(Context.Data[P_HANDLE]));
end;

end.
