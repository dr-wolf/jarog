unit uDataArray;

interface

uses
  uArray, uOperations, uData, uDataPool, uExecFile, uTypedStream;

type
  TDataArray = class(TData)
  public
    constructor Create(Initial: TData); overload;
    constructor Create(Items: TArray<TData>; Struct: Boolean); overload;
    constructor Create(Items: array of TData); overload;
    destructor Destroy; override;
  private
    FItems: TArray<TData>;
    FSize: Integer;
    procedure Grow(Size: Integer);
  public
    function Apply(Op: TBinaryOp; const Data: TData): TData; override;
    function Apply(Op: TUnaryOp): TData; override;
    function IsEqual(const Data: TData): Boolean; override;
    function Dup: TData; override;
    function Count: Integer; override;
    function Item(I: Integer): TData; override;
    function Serialize(const ExecFile: TExecFile): TTypedStream; override;
    procedure Assign(const Data: TData); override;
    procedure Convert(C: TConverter); override;
    procedure SetGCLevel(Level: TGCLevel; Recursive: Boolean; Forced: Boolean = False); override;
  end;

function TranslateIndex(Index, ListSize: Integer): Integer;

implementation

uses uExceptions, uDataBool, uDataNumeric, uOpCode;

function TranslateIndex(Index, ListSize: Integer): Integer;
begin
  Result := Index;
  if Result < 0 then
    Result := Result + ListSize
  else
    Result := Result - 1;
end;

{ TDataArray }

constructor TDataArray.Create(Initial: TData);
begin
  inherited Create;
  Grow(0);
  FItems[0] := Initial;
end;

constructor TDataArray.Create(Items: TArray<TData>; Struct: Boolean);
var
  I: Integer;
begin
  inherited Create;
  if Struct then
  begin
    SetLength(FItems, Length(Items));
    FSize := -1;
  end
  else
    Grow(Length(Items));
  for I := 0 to High(Items) do
    FItems[I] := Items[I];
end;

constructor TDataArray.Create(Items: array of TData);
var
  I: Integer;
begin
  inherited Create;
  FSize := -1;
  SetLength(FItems, Length(Items));
  for I := 0 to High(Items) do
    FItems[I] := Items[I];
end;

destructor TDataArray.Destroy;
begin
  FItems := nil;
  inherited;
end;

function TDataArray.Apply(Op: TBinaryOp; const Data: TData): TData;
var
  I: Integer;
  R: TArray<TData>;
begin
  case Op of
    boAddition:
      begin
        SetLength(R, Count + Data.Count);
        for I := 0 to Count - 1 do
          R[I] := FItems[I].Dup;
        for I := 0 to Data.Count - 1 do
          R[Count + I] := Data.Item(I).Dup;
        Result := TDataArray.Create(R, False);
      end;
    boEqual:
      Result := TDataBool.Create(IsEqual(Data));
    boNotEqual:
      Result := TDataBool.Create(not IsEqual(Data));
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  end;
end;

function TDataArray.Apply(Op: TUnaryOp): TData;
begin
  if Op = uoLength then
    Result := TDataNumeric.Create(Count)
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

procedure TDataArray.Assign(const Data: TData);
var
  I: Integer;
begin
  if FSize = -1 then
  begin
    for I := 0 to High(FItems) do
      FItems[I].Assign(Data.Item(I).Unwrap);
  end else begin
    Grow(Data.Count);
    for I := 0 to Data.Count - 1 do
    begin
      if FItems[I] = nil then
        FItems[I] := FItems[0].Dup;
      FItems[I].Assign(Data.Item(I).Unwrap);
    end;
  end;
end;

function TDataArray.IsEqual(const Data: TData): Boolean;
var
  I: Integer;
begin
  Result := Count = Data.Count;
  if Result then
    for I := 0 to Count - 1 do
    begin
      Result := Result and FItems[I].IsEqual(Data.Item(I));
      if not Result then
        Break;
    end;
end;

function TDataArray.Item(I: Integer): TData;
begin
  if (I >= 0) and (I < Count) then
    Result := FItems[I]
  else
    raise ERuntimeException.Create(E_LIST_BOUNDS, nil);
end;

function TDataArray.Serialize(const ExecFile: TExecFile): TTypedStream;
var
  I: Integer;
begin
  Result := TTypedStream.Create;
  with Result do
  begin
    WriteByte(DID_DATA_ARRAY);
    if FSize = 0 then
    begin
      if FItems[0] <> nil then
      begin
        WriteByte(DID_DATA_ARRAY_EMPTY);
        WriteInteger(ExecFile.Save(FItems[0] as TData, (FItems[0] as TData).Serialize));
      end else begin
        WriteByte(DID_DATA_ARRAY_STUB);
        WriteInteger(-1);
      end;
    end else begin
      if FSize < 0 then
        WriteByte(DID_DATA_ARRAY_FIXED)
      else
        WriteByte(DID_DATA_ARRAY_VAR);
      WriteInteger(Count);
      for I := 0 to Count - 1 do
        WriteInteger(ExecFile.Save(FItems[I] as TData, (FItems[I] as TData).Serialize));
    end;
  end;
end;

procedure TDataArray.SetGCLevel(Level: TGCLevel; Recursive, Forced: Boolean);
var
  I: Integer;
begin
  inherited;
  if Recursive then
    for I := 0 to High(FItems) do
      if FItems[I] <> nil then
        FItems[I].SetGCLevel(Level, Recursive, Forced);
end;

procedure TDataArray.Convert(C: TConverter);
begin
  C.ConvertArray(Copy(FItems, 0, Count), FSize);
end;

function TDataArray.Count: Integer;
begin
  if FSize >= 0 then
    Result := FSize
  else
    Result := Length(FItems);
end;

function TDataArray.Dup: TData;
var
  I: Integer;
  Items: TArray<TData>;
begin
  if FSize = 0 then
  begin
    if FItems[0] <> nil then
      Result := TDataArray.Create(FItems[0].Dup)
    else
      Result := TDataArray.Create(nil);
  end else begin
    SetLength(Items, Count);
    for I := 0 to High(Items) do
      Items[I] := FItems[I].Dup;
    Result := TDataArray.Create(Items, FSize < 0);
  end;
end;

procedure TDataArray.Grow(Size: Integer);
const
  BLOCK_SIZE = 256;
var
  NewSize, I: Integer;
begin
  FSize := Size;
  NewSize := (Size div BLOCK_SIZE + 1) * BLOCK_SIZE;
  if NewSize = Length(FItems) then
    Exit;
  for I := NewSize to High(FItems) do
    if FItems[I] <> nil then
      FItems[I].Dispose();
  SetLength(FItems, NewSize);
end;

end.
