unit uUnitOS;

interface

uses
  uUnit;

type
  TUnitOS = class(TUnit)
  public
    constructor Create;
  end;

  { *
    @descr Interaction with some OS features
    * }

implementation

uses uTypeFactory, uFuncOSEnvList, uFuncOSSleep, uFuncOSExec,
  uFuncOSLsof, uFuncOSLsos, uFuncOSGetEnv, uFuncOSSetEnv, uFuncOSTime, uFuncOSPid, uFuncOSThreadId;

{ TUnitOS }

constructor TUnitOS.Create;
begin
  inherited Create('<os>');

  AddValue('envlist', MakeFunc(TFuncOSEnvList.Create, [], [], TypeFactory.GetList(TypeFactory.GetString)));
  AddValue('exec', MakeFunc(TFuncOSExec.Create, [TypeFactory.GetString, TypeFactory.GetBool], ['command', 'wait']));
  AddValue('lsof', MakeFunc(TFuncOSLsof.Create, [], [], TypeFactory.GetList(TypeFactory.GetInt)));
  AddValue('lsos', MakeFunc(TFuncOSLsos.Create, [TypeFactory.GetBool], ['tcp'],
    TypeFactory.GetList(TypeFactory.GetInt)));
  AddValue('getenv', MakeFunc(TFuncOSGetEnv.Create, [TypeFactory.GetString], ['name'], TypeFactory.GetString));
  AddValue('setenv', MakeFunc(TFuncOSSetEnv.Create, [TypeFactory.GetString, TypeFactory.GetString], ['name', 'value']));
  AddValue('sleep', MakeFunc(TFuncOSSleep.Create, [TypeFactory.GetInt], ['timeout']));
  AddValue('pid', MakeFunc(TFuncOSPid.Create, [], [], TypeFactory.GetInt));
  AddValue('threadid', MakeFunc(TFuncOSThreadId.Create, [], [], TypeFactory.GetInt));
  AddValue('time', MakeFunc(TFuncOSTime.Create, [], [], TypeFactory.GetInt));
end;

end.
