unit uFuncTCPRemote;

interface

uses
  uFuncTCP, uFuncContext, BlckSock;

type
  TFuncTCPRemote = class(TFuncTCP)
  public
    constructor Create;
  private const
    P_SOCKET = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Returns remote IP and port from a socket.
    @type func(socket: int -> struct(ip: string, port: int))
    @in socket int # Socket handle
    @out ip string # Remote IP
    @out port int # Remote port
    * }

implementation

uses uDataArray, uDataString, uDataNumeric, uOpCode;

{ TFuncTCPRemote }

constructor TFuncTCPRemote.Create;
begin
  inherited Create([MakeInt], MakeStruct([MakeString, MakeInt]));
  FFuncUid := FID_TCP_REMOTE;
end;

procedure TFuncTCPRemote.Execute(Context: TFuncContext);
var
  sock: TTCPBlockSocket;
begin
  sock := RetrieveSocket(P_SOCKET, Context);
  sock.GetSinRemote;
  Return(TDataArray.Create([TDataString.Create(sock.GetRemoteSinIP), TDataNumeric.Create(sock.GetRemoteSinPort)]));
end;

end.
