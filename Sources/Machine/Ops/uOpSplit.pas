unit uOpSplit;

interface

uses
  uOp;

type
  TOpSplit = class(TOp)
  public
    constructor Create;
  end;

implementation

uses uOpCode;

{ TOpSplit }

constructor TOpSplit.Create;
begin
  inherited Create(OID_SPLIT);
end;

end.
