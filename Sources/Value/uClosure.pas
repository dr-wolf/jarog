unit uClosure;

interface

uses
  uData;

type
  IClosure = interface(IInterface)
    function GetData: TData;
  end;

type
  TClosure = class(TInterfacedObject, IClosure)
  public
    constructor Create(Data: TData);
    destructor Destroy; override;
  protected
    FData: TData;
  public
    function GetData: TData;
  end;

implementation

{ TClosure }

constructor TClosure.Create(Data: TData);
begin
  FData := Data;
end;

destructor TClosure.Destroy;
begin
  FData.Dispose;
  inherited;
end;

function TClosure.GetData: TData;
begin
  Result := FData;
end;

end.
