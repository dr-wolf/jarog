unit uFuncFSScan;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncFSScan = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_PATH = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr List files and directories inside the specified path.
    @type func(path: string -> dirs: list(string), files: list(string))
    @in path string # Path to be scanned
    @out dirs list(string) # List of directories
    @out files list(string) # List of files
    * }

implementation

uses SysUtils, uArray, uExceptions, uData, uDataArray, uDataString, uOpCode;

{ TFuncFSScan }

constructor TFuncFSScan.Create;
begin
  inherited Create([MakeString], MakeStruct([MakeList(MakeString), MakeList(MakeString)]));
  FFuncUid := FID_FS_SCAN;
end;

procedure TFuncFSScan.Execute(Context: TFuncContext);

  procedure AppendA(var A: TArray<TData>; D: TData);
  begin
    SetLength(A, Length(A) + 1);
    A[High(A)] := D;
  end;

var
  Path: string;
  dirs, files: TArray<TData>;
  r: TSearchRec;
begin
  Path := AsString(Context.Data[P_PATH]);
  if not DirectoryExists(Path) then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Directory "' + Path + '" does not exists!'));
  SetLength(dirs, 0);
  SetLength(files, 0);
  if FindFirst(Path + '/*', faAnyFile, r) = 0 then
    repeat
      if r.Attr and faDirectory = faDirectory then
        AppendA(dirs, TDataString.Create(r.Name))
      else
        AppendA(files, TDataString.Create(r.Name));
    until FindNext(r) <> 0;
  FindClose(r);
  Return(TDataArray.Create([TDataArray.Create(dirs, False), TDataArray.Create(files, False)]));
end;

end.
