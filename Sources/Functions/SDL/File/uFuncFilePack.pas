unit uFuncFilePack;

interface

uses
  uFuncFile, uFuncContext;

type
  TFuncFilePack = class(TFuncFile)
  public
    constructor Create;
  private const
    P_HANDLE = 0;
    P_VALUE = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Packs any value to file stream.
    @type func(handle: int, value: any)
    @in handle int # Handle of a stream
    @in value any # Value to be packed
    * }

implementation

uses uData, uOpCode, uTypedStream, uStreamConverter;

{ TFuncFilePack }

constructor TFuncFilePack.Create;
begin
  inherited Create([MakeInt, MakeAny]);
  FFuncUid := FID_FILE_PACK;
end;

procedure TFuncFilePack.Execute(Context: TFuncContext);
var
  ts: TTypedStream;
  c: TConverter;
begin
  ts := TTypedStream.Create(RetrieveStream(P_HANDLE, Context), False);
  c := TStreamConverter.Create(ts);
  try
    Context.Data[P_VALUE].Convert(c);
  finally
    c.Free;
    ts.Free;
  end;
end;

end.
