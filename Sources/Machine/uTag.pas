unit uTag;

interface

type
  ITag = interface(IInterface)
    function GetAddress: Integer;
    procedure SetAddress(Address: Integer);
  end;

type
  TTag = class(TInterfacedObject, ITag)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FAddress: Integer;
  public
    function GetAddress: Integer;
    procedure SetAddress(Address: Integer);
  end;

implementation

{ TTag }

constructor TTag.Create;
begin
  FAddress := -1;
end;

destructor TTag.Destroy;
begin
  inherited;
end;

function TTag.GetAddress: Integer;
begin
  Result := FAddress;
end;

procedure TTag.SetAddress(Address: Integer);
begin
  FAddress := Address;
end;

end.
