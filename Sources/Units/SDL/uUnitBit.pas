unit uUnitBit;

interface

uses
  uUnit;

type
  TUnitBit = class(TUnit)
  public
    constructor Create;
  end;

  { *
    @descr Contains functions for bitwise operations on integer values.
    * }

implementation

uses uTypeFactory, uFuncBitAnd, uFuncBitNot, uFuncBitOr, uFuncBitShl, uFuncBitShr, uFuncBitXor;

{ TUnitInt }

constructor TUnitBit.Create;
begin
  inherited Create('<bit>');

  AddValue('and', MakeFunc(TFuncBitAnd.Create, [TypeFactory.GetInt, TypeFactory.GetInt], ['a', 'b'],
    TypeFactory.GetInt));
  AddValue('not', MakeFunc(TFuncBitNot.Create, [TypeFactory.GetInt], ['value'], TypeFactory.GetInt));
  AddValue('or', MakeFunc(TFuncBitOr.Create, [TypeFactory.GetInt, TypeFactory.GetInt], ['a', 'b'], TypeFactory.GetInt));
  AddValue('shl', MakeFunc(TFuncBitShl.Create, [TypeFactory.GetInt, TypeFactory.GetInt], ['value', 'bits'],
    TypeFactory.GetInt));
  AddValue('shr', MakeFunc(TFuncBitShr.Create, [TypeFactory.GetInt, TypeFactory.GetInt], ['value', 'bits'],
    TypeFactory.GetInt));
  AddValue('xor', MakeFunc(TFuncBitXor.Create, [TypeFactory.GetInt, TypeFactory.GetInt], ['a', 'b'],
    TypeFactory.GetInt));

end;

end.
