unit uUnitUDP;

interface

uses
  uUnit;

type
  TUnitUDP = class(TUnit)
  public
    constructor Create;
  end;

  { *
    @descr Network UDP blocking sockets
    * }

implementation

uses uTypeFactory, uFuncUDPBind, uFuncUDPClose, uFuncUDPReceive, uFuncUDPSend, uFuncUDPBroadcast;

{ TUnitUDP }

constructor TUnitUDP.Create;
begin
  inherited Create('<udp>');

  AddValue('bind', MakeFunc(TFuncUDPBind.Create, [TypeFactory.GetString, TypeFactory.GetInt], ['ip', 'port'],
    TypeFactory.GetInt));
  AddValue('broadcast', MakeFunc(TFuncUDPBroadcast.Create, [TypeFactory.GetInt, TypeFactory.GetInt], ['port', 'file']));
  AddValue('close', MakeFunc(TFuncUDPClose.Create, [TypeFactory.GetInt], ['socket']));
  AddValue('send', MakeFunc(TFuncUDPSend.Create, [TypeFactory.GetString, TypeFactory.GetInt, TypeFactory.GetInt],
    ['ip', 'port', 'file']));
  AddValue('receive', MakeFunc(TFuncUDPReceive.Create, [TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt],
    ['socket', 'file', 'timeout']));
end;

end.
