unit uFuncIOColor;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncIOColor = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_TEXT = 0;
    P_BACK = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Sets text and background color.
    @type func(text: int, back: int)
    @in text int # Color of the text
    @in back int # Color of the background
    * }

implementation

uses uDataNumeric, uOpCode;

{ TFuncIOColor }

constructor TFuncIOColor.Create;
begin
  inherited Create([MakeInt, MakeInt]);
  FFuncUid := FID_IO_COLOR;
end;

procedure TFuncIOColor.Execute(Context: TFuncContext);
var
  t, b: Integer;
begin
  t := AsInt(Context.Data[P_TEXT]);
  if t = 0 then
    t := 8;
  b := AsInt(Context.Data[P_BACK]);
  if b = 0 then
    b := 1;
  FCoreContext.IOProvider.SetColor(t - 1, b - 1);
end;

end.
