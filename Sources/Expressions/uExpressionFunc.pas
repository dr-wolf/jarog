unit uExpressionFunc;

interface

uses
  uArray, uType, uData, uExpression, uValue, uOp, uDataSection;

type
  TExpressionFunc = class(TExpression)
  public
    constructor Create(Data: TDataSection; Func: TData; FuncType: TType; Closures: TArray<Integer>);
    destructor Destroy; override;
  private
    FFunc: TData;
    FClosures: TArray<Integer>;
  public
    function HasNoArgs: Boolean;
    function Compile: TArray<TOp>; override;
    function Eval: IValue; override;
  end;

implementation

uses uOpInject, uOpLoad, uOpStack, uTypeFunc, uTypeStruct;

{ TExpressionFunc }

constructor TExpressionFunc.Create(Data: TDataSection; Func: TData; FuncType: TType; Closures: TArray<Integer>);
var
  i: Integer;
begin
  inherited Create(Data);
  FFunc := Func;
  if Length(Closures) = 0 then
    FGroup := egStatic
  else
    FGroup := egMixed;
  FResultType := FuncType;
  SetLength(FClosures, Length(Closures));
  for i := 0 to High(FClosures) do
    FClosures[i] := Closures[i];
end;

destructor TExpressionFunc.Destroy;
begin
  inherited;
end;

function TExpressionFunc.Compile: TArray<TOp>;
var
  i: Integer;
begin
  SetLength(Result, 0);
  AppendOp(Result, [TOpLoad.Create(FData.Keep(FFunc), False)]);
  for i := 0 to High(FClosures) do
    AppendOp(Result, [TOpLoad.Create(FClosures[i], True)]);
  AppendOp(Result, [TOpStack.Create(atStruct, Length(FClosures)), TOpInject.Create]);
end;

function TExpressionFunc.Eval: IValue;
begin
  Result := TValue.Create(FResultType, FFunc);
end;

function TExpressionFunc.HasNoArgs: Boolean;
begin
  Result := ((FResultType as TTypeFunc).Params as TTypeStruct).Size = 0;
end;

end.
