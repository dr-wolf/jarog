unit uExpressionPipeTail;

interface

uses
  uArray, uExpression, uOp, uDataSection;

type
  TExpressionPipeTail = class(TExpression)
  public
    constructor Create(Data: TDataSection; Pipe: TExpression);
    destructor Destroy; override;
  private
    FPipe: TExpression;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uExceptions, uType, uTypePipe, uOpTail;

{ TExpressionPipeTail }

constructor TExpressionPipeTail.Create(Data: TDataSection; Pipe: TExpression);
begin
  if not(Pipe.ResultType is TTypePipe) then
    raise ECodeException.Create(E_EXPECTED_PIPE);
  inherited Create(Data);
  FPipe := Pipe;
  FGroup := egVariable;
  FResultType := FPipe.ResultType.NestedType();
end;

destructor TExpressionPipeTail.Destroy;
begin
  FPipe.Free;
  inherited;
end;

function TExpressionPipeTail.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FPipe.Compile);
  AppendOp(Result, [TOpTail.Create]);
end;

end.
