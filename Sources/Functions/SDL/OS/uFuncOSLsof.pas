unit uFuncOSLsof;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncOSLsof = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Returns list of handles of files that are open in script.
    @type func(-> list(int))
    @out - list(int) # List of handles of open files
    * }

implementation

uses uArray, uData, uDataNumeric, uDataArray, uOpCode;

{ TFuncOSLsof }

constructor TFuncOSLsof.Create;
begin
  inherited Create([], MakeList(MakeInt));
  FFuncUid := FID_OS_LSOF;
end;

procedure TFuncOSLsof.Execute(Context: TFuncContext);
var
  a: TArray<TData>;
  h: TArray<Integer>;
  I: Integer;
begin
  h := FCoreContext.Repos.Files.GetAllHandles;
  SetLength(a, Length(h));
  for I := 0 to High(h) do
    a[I] := TDataNumeric.Create(h[I]);
  Return(TDataArray.Create(a, False));
end;

end.
