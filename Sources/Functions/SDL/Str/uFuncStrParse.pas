unit uFuncStrParse;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncStrParse = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_TEXT = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Parses float from string, throws exception if no float value is found.
    @type func(text: string -> float)
    @in text string # String representstion of float value
    @out - float # Parsed value
    * }

implementation

uses uDataNumeric, uDataString, uOpCode;

{ TFuncStrParse }

constructor TFuncStrParse.Create;
begin
  inherited Create([MakeString], MakeFloat);
  FFuncUid := FID_STR_PARSE;
end;

procedure TFuncStrParse.Execute(Context: TFuncContext);
begin
  Return(TDataNumeric.Create(AsString(Context.Data[P_TEXT])));
end;

end.
