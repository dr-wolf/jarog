unit uNumeric;

interface

uses
  mp_types, uTypedStream, uFormatTemplate;

type
  TBitOp = (boAnd, boOr, boXor);

type
  TNumeric = class(TObject)
  protected
    { Addition }
    function AddInt(const V: Integer): TNumeric; virtual; abstract;
    function AddBigInt(const V: mp_int): TNumeric; virtual; abstract;
    function AddBigFloat(const V: mp_float): TNumeric; virtual; abstract;
    { Subtraction }
    function SubInt(const V: Integer): TNumeric; virtual; abstract;
    function SubBigInt(const V: mp_int): TNumeric; virtual; abstract;
    function SubBigFloat(const V: mp_float): TNumeric; virtual; abstract;
    { Multiplication }
    function MulInt(const V: Integer): TNumeric; virtual; abstract;
    function MulBigInt(const V: mp_int): TNumeric; virtual; abstract;
    function MulBigFloat(const V: mp_float): TNumeric; virtual; abstract;
    { Division }
    function DivInt(const V: Integer): TNumeric; virtual; abstract;
    function DivBigInt(const V: mp_int): TNumeric; virtual; abstract;
    function DivBigFloat(const V: mp_float): TNumeric; virtual; abstract;
    { Modulo }
    function ModInt(const V: Integer): TNumeric; virtual; abstract;
    function ModBigInt(const V: mp_int): TNumeric; virtual; abstract;
    function ModBigFloat(const V: mp_float): TNumeric; virtual; abstract;
    { Power }
    function PowInt(const V: Integer): TNumeric; virtual; abstract;
    function PowBigInt(const V: mp_int): TNumeric; virtual; abstract;
    function PowBigFloat(const V: mp_float): TNumeric; virtual; abstract;
    { Bit }
    function BitInt(const BitOp: TBitOp; const V: Integer): TNumeric; virtual; abstract;
    function BitBigInt(const BitOp: TBitOp; const V: mp_int): TNumeric; virtual; abstract;
    function BitBigFloat(const BitOp: TBitOp; const V: mp_float): TNumeric; virtual; abstract;
    { Compare }
    function CmpInt(const V: Integer): Integer; virtual; abstract;
    function CmpBigInt(const V: mp_int): Integer; virtual; abstract;
    function CmpBigFloat(const V: mp_float): Integer; virtual; abstract;
  public
    function Add(Value: TNumeric): TNumeric;
    function Subtract(Value: TNumeric): TNumeric;
    function Multiply(Value: TNumeric): TNumeric;
    function Divide(Value: TNumeric): TNumeric;
    function Modulo(Value: TNumeric): TNumeric;
    function Power(Value: TNumeric): TNumeric;
    function Bit(const BitOp: TBitOp; const Value: TNumeric): TNumeric;
    function Compare(Value: TNumeric): Integer;

    function IsZero: Boolean; virtual; abstract;

    function Dup(TypeExpand: Boolean): TNumeric; virtual; abstract;
    function Floor: TNumeric; virtual; abstract;
    function Frac: TNumeric; virtual; abstract;
    function Invert: TNumeric; virtual; abstract;
    function Module: TNumeric; virtual; abstract;
    function Negative: TNumeric; virtual; abstract;
    function Shift(Bits: Integer): TNumeric; virtual; abstract;

    function ToInt64: Int64; virtual; abstract;
    function ToUInt64: UInt64; virtual; abstract;
    function ToFloat: mp_float; virtual; abstract;
    function ToDecimal: string; virtual; abstract;
    function ToText(Format: TFormatTemplate): string; virtual; abstract;
    procedure Serialize(Stream: TTypedStream); virtual; abstract;
  end;

function ToNumeric(var Value: mp_int): TNumeric;

implementation

uses
  mp_base, mp_real, uInteger, uBigInt, uBigFloat;

function ToNumeric(var Value: mp_int): TNumeric;
var
  V: Integer;
begin
  if mp_is_longint(Value, V) then
  begin
    mp_clear(Value);
    Result := TInteger.Create(V);
  end
  else
    Result := TBigInt.Create(Value);
end;

{ TNumeric }

function TNumeric.Add(Value: TNumeric): TNumeric;
begin
  Result := nil;
  if Value is TInteger then
    Result := AddInt((Value as TInteger).Value)
  else if Value is TBigInt then
    Result := AddBigInt((Value as TBigInt).Value)
  else if Value is TBigFloat then
    Result := AddBigFloat((Value as TBigFloat).Value);
end;

function TNumeric.Subtract(Value: TNumeric): TNumeric;
begin
  Result := nil;
  if Value is TInteger then
    Result := SubInt((Value as TInteger).Value)
  else if Value is TBigInt then
    Result := SubBigInt((Value as TBigInt).Value)
  else if Value is TBigFloat then
    Result := SubBigFloat((Value as TBigFloat).Value);
end;

function TNumeric.Multiply(Value: TNumeric): TNumeric;
begin
  Result := nil;
  if Value is TInteger then
    Result := MulInt((Value as TInteger).Value)
  else if Value is TBigInt then
    Result := MulBigInt((Value as TBigInt).Value)
  else if Value is TBigFloat then
    Result := MulBigFloat((Value as TBigFloat).Value);
end;

function TNumeric.Divide(Value: TNumeric): TNumeric;
begin
  Result := nil;
  if Value is TInteger then
    Result := DivInt((Value as TInteger).Value)
  else if Value is TBigInt then
    Result := DivBigInt((Value as TBigInt).Value)
  else if Value is TBigFloat then
    Result := DivBigFloat((Value as TBigFloat).Value);
end;

function TNumeric.Modulo(Value: TNumeric): TNumeric;
begin
  Result := nil;
  if Value is TInteger then
    Result := ModInt((Value as TInteger).Value)
  else if Value is TBigInt then
    Result := ModBigInt((Value as TBigInt).Value)
  else if Value is TBigFloat then
    Result := ModBigFloat((Value as TBigFloat).Value);
end;

function TNumeric.Power(Value: TNumeric): TNumeric;
begin
  Result := nil;
  if Value is TInteger then
    Result := PowInt((Value as TInteger).Value)
  else if Value is TBigInt then
    Result := PowBigInt((Value as TBigInt).Value)
  else if Value is TBigFloat then
    Result := PowBigFloat((Value as TBigFloat).Value);
end;

function TNumeric.Bit(const BitOp: TBitOp; const Value: TNumeric): TNumeric;
begin
  Result := nil;
  if Value is TInteger then
    Result := BitInt(BitOp, (Value as TInteger).Value)
  else if Value is TBigInt then
    Result := BitBigInt(BitOp, (Value as TBigInt).Value)
  else if Value is TBigFloat then
    Result := BitBigFloat(BitOp, (Value as TBigFloat).Value);
end;

function TNumeric.Compare(Value: TNumeric): Integer;
begin
  Result := 0;
  if Value is TInteger then
    Result := CmpInt((Value as TInteger).Value)
  else if Value is TBigInt then
    Result := CmpBigInt((Value as TBigInt).Value)
  else if Value is TBigFloat then
    Result := CmpBigFloat((Value as TBigFloat).Value);
end;

end.
