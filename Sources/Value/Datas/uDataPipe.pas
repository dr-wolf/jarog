unit uDataPipe;

interface

uses
  uQueue, uOperations, uData, uExecFile, uTypedStream;

type
  IDataQueue = IQueue<TData>;

type
  TDataPipe = class(TData)
  public
    constructor Create(Data: TData; Pipe: IDataQueue);
    destructor Destroy; override;
  private
    FData: TData;
    FPipe: IDataQueue;
  public
    function Apply(Op: TBinaryOp; const Data: TData): TData; override;
    function Apply(Op: TUnaryOp): TData; override;
    function Dup: TData; override;
    function IsEqual(const Data: TData): Boolean; override;
    function Serialize(const ExecFile: TExecFile): TTypedStream; override;
    function Tail: TData;
    procedure Assign(const Data: TData); override;
    procedure Convert(C: TConverter); override;
  end;

implementation

uses uExceptions, uDataBool, uOpCode;

{ TDataPipe }

constructor TDataPipe.Create(Data: TData; Pipe: IDataQueue);
begin
  inherited Create;
  FData := Data;
  if Pipe = nil then
    FPipe := TQueue<TData>.Create
  else
    FPipe := Pipe;
end;

destructor TDataPipe.Destroy;
begin
  FData := nil;
  FPipe := nil;
  inherited;
end;

function TDataPipe.Apply(Op: TBinaryOp; const Data: TData): TData;
begin
  case Op of
    boEqual:
      Result := TDataBool.Create(IsEqual(Data));
    boNotEqual:
      Result := TDataBool.Create(not IsEqual(Data));
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  end
end;

function TDataPipe.Apply(Op: TUnaryOp): TData;
begin
  FPipe.Lock;
  case Op of
    uoPop:
      begin
        Result := FPipe.Pop;
        Result.Dispose();
      end;
    uoLength:
      Result := TDataBool.Create(FPipe.Count > 0);
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  end;
  FPipe.Unlock;
end;

procedure TDataPipe.Assign(const Data: TData);
begin
  FData.Assign((Data as TDataPipe).FData);
  FPipe := (Data as TDataPipe).FPipe;
end;

procedure TDataPipe.Convert(C: TConverter);
begin
  C.ConvertPipe(FData);
end;

function TDataPipe.Dup: TData;
begin
  Result := TDataPipe.Create(FData.Dup, FPipe);
end;

function TDataPipe.IsEqual(const Data: TData): Boolean;
begin
  Result := FPipe = (Data as TDataPipe).FPipe;
end;

function TDataPipe.Serialize(const ExecFile: TExecFile): TTypedStream;
begin
  Result := TTypedStream.Create;
  with Result do
  begin
    WriteByte(DID_DATA_PIPE);
    WriteInteger(ExecFile.Save(FData as TData, (FData as TData).Serialize));
  end;
end;

function TDataPipe.Tail: TData;
begin
  Result := FData.Dup;
  FPipe.Lock;
  FPipe.Push(Result);
  FPipe.Unlock;
end;

end.
