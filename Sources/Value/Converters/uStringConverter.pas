unit uStringConverter;

interface

uses
  uArray, uData, uNumeric;

type
  TStringConverter = class(TConverter)
  public
    constructor Create;
  private
    FLog: string;
    procedure AppendLog(const Text: string);
    procedure AppendArray(Items: array of TData; const StartTerm, EndTerm: string);
  public
    property Log: string read FLog;
    procedure ConvertAny(Value: TData); override;
    procedure ConvertArray(Items: array of TData; Size: Integer); override;
    procedure ConvertBool(Value: Boolean); override;
    procedure ConvertFunc(Func: TObject; Closures: TArray<TData>); override;
    procedure ConvertNumeric(Value: TNumeric); override;
    procedure ConvertPipe(Data: TData); override;
    procedure ConvertSet(Items: TArray<UInt64>); override;
    procedure ConvertString(Value: string); override;
    procedure ConvertStub; override;
  end;

function LogData(Data: TData): string;

implementation

uses SysUtils, uIdentifierRegistry, uStringEscaping, uFunction, mp_real;

function LogData(Data: TData): string;
var
  c: TStringConverter;
begin
  c := TStringConverter.Create;
  try
    Data.Convert(c);
    Result := c.Log;
  finally
    c.Free;
  end;
end;

{ TStringConverter }

constructor TStringConverter.Create;
begin
  FLog := '';
end;

procedure TStringConverter.AppendArray(Items: array of TData; const StartTerm, EndTerm: string);
var
  I: Integer;
begin
  if Length(Items) = 0 then
    AppendLog(StartTerm + EndTerm)
  else
  begin
    AppendLog(StartTerm);
    Items[0].Convert(Self);
    for I := 1 to High(Items) do
    begin
      AppendLog(', ');
      Items[I].Convert(Self);
    end;
    AppendLog(EndTerm);
  end;
end;

procedure TStringConverter.AppendLog(const Text: string);
begin
  FLog := FLog + Text;
end;

procedure TStringConverter.ConvertAny(Value: TData);
begin
  Value.Convert(Self);
end;

procedure TStringConverter.ConvertArray(Items: array of TData; Size: Integer);
begin
  if Size < 0 then
    AppendArray(Items, '{', '}')
  else
    AppendArray(Items, '[', ']');
end;

procedure TStringConverter.ConvertBool(Value: Boolean);
begin
  if Value then
    AppendLog(RI_TRUE)
  else
    AppendLog(RI_FALSE);
end;

procedure TStringConverter.ConvertFunc(Func: TObject; Closures: TArray<TData>);
var
  I: Integer;
begin
  AppendLog('func ' + (Func as TFunction).Log);
  for I := 0 to High(Closures) do
  begin
    if I > 0 then
      AppendLog(', ');
    Closures[I].Convert(Self);
  end;
end;

procedure TStringConverter.ConvertNumeric(Value: TNumeric);
begin
  AppendLog(Value.ToDecimal);
end;

procedure TStringConverter.ConvertPipe(Data: TData);
begin
  inherited;
  AppendLog('<pipe ');
  Data.Convert(Self);
  AppendLog('>');
end;

procedure TStringConverter.ConvertSet(Items: TArray<UInt64>);
begin
  AppendLog('[:' + IntToStr(Length(Items)) + ':]');
end;

procedure TStringConverter.ConvertString(Value: string);
begin
  AppendLog('"' + StringEscape(Value) + '"');
end;

procedure TStringConverter.ConvertStub;
begin
  AppendLog('?');
end;

end.
