unit ChildProcess;

interface

uses
  SysUtils, {$IFDEF UNIX} Process, {$ELSE}Windows, ProcessPipes, {$ENDIF} Classes;

type
  TChildProcess = class(TObject)
  public
    constructor Create(Executable: string);
    destructor Destroy; override;
  private
    FExecutable: string;
    FDirectory: string;
    FArguments: TStrings;
{$IFDEF UNIX}
    FProcess: TProcess;
{$ELSE}
    FProcInfo: TProcessInformation;
    FStdinReadHandle: THandle;
    FStdoutWriteHandle: THandle;
    FStdinPipe: TInputPipeStream;
    FStdoutPipe: TOutputPipeStream;
{$ENDIF}
    function BuildExec: string;
  public
    property Executable: string read FExecutable;
    property Directory: string read FDirectory;
    function WaitFor(Timeout: Integer): Integer;
    procedure AddArgument(Argument: string);
    procedure Execute;
    procedure SendData(Stream: TStream; Size: Cardinal);
    procedure ReceiveData(Stream: TStream; var Size: Cardinal);
    procedure Terminate;
  end;

implementation

{ TChildProcess }

procedure TChildProcess.AddArgument(Argument: string);
begin
  FArguments.Add(Argument);
end;

function TChildProcess.BuildExec: string;
var
  i: Integer;
begin
  Result := '"' + FExecutable + '"';
  for i := 0 to FArguments.Count - 1 do
    Result := Result + ' ' + FArguments[i];
end;

constructor TChildProcess.Create(Executable: string);
{$IFDEF UNIX}
{$ELSE}
var
  SA: TSecurityAttributes;
  stdinw, stdoutr, hr, hw, ph: THandle;
{$ENDIF}
begin
{$IFDEF UNIX}
  FProcess := TProcess.Create(nil);
  FProcess.Options := [poUsePipes];
{$ELSE}
  ZeroMemory(@SA, SizeOf(TSecurityAttributes));
  SA.nLength := SizeOf(TSecurityAttributes);
  SA.bInheritHandle := True;
  SA.lpSecurityDescriptor := nil;

  if not CreatePipe(FStdinReadHandle, stdinw, @SA, 0) then
    raise EPipeCreation.Create('Can not create STDIN pipe');
  if not CreatePipe(stdoutr, FStdoutWriteHandle, @SA, 0) then
    raise EPipeCreation.Create('Can not create STDOUT pipe');

  ph := GetCurrentProcess();
  if not DuplicateHandle(ph, stdinw, ph, @hw, 0, False, DUPLICATE_SAME_ACCESS) Then
    raise EPipeCreation.Create('Can not duplicate STDIN pipe');
  if not DuplicateHandle(ph, stdoutr, ph, @hr, 0, False, DUPLICATE_SAME_ACCESS) Then
    raise EPipeCreation.Create('Can not duplicate STDOUT pipe');

  CloseHandle(stdinw);
  CloseHandle(stdoutr);

  FStdinPipe := TInputPipeStream.Create(hw);
  FStdoutPipe := TOutputPipeStream.Create(hr);

{$ENDIF}
  FExecutable := Executable;
  FDirectory := ExtractFilePath(Executable);

  FArguments := TStringList.Create;
end;

destructor TChildProcess.Destroy;
begin
  FArguments.Free;
{$IFDEF UNIX}
  FProcess.Free;
{$ELSE}
  FStdinPipe.Free;
  FStdoutPipe.Free;
  CloseHandle(FStdinReadHandle);
  CloseHandle(FStdoutWriteHandle);
{$ENDIF}
  inherited;
end;

procedure TChildProcess.Execute;
{$IFDEF UNIX}
{$ELSE}
var
  StartInfo: TStartupInfo;
{$ENDIF}
begin
{$IFDEF UNIX}
  FProcess.Executable := BuildExec;
  FProcess.CurrentDirectory := FDirectory;
  FProcess.Execute;
{$ELSE}
  ZeroMemory(@StartInfo, SizeOf(TStartupInfo));
  StartInfo.cb := SizeOf(TStartupInfo);
  StartInfo.hStdInput := FStdinReadHandle;
  StartInfo.hStdOutput := FStdoutWriteHandle;
  StartInfo.dwFlags := STARTF_USESTDHANDLES or STARTF_USESHOWWINDOW;
  StartInfo.wShowWindow := SW_NORMAL;
  if not CreateProcess(nil, PChar(BuildExec), nil, nil, True, 0, nil, PChar(FDirectory), StartInfo, FProcInfo) then
    raise Exception.Create('Can not create process ' + FExecutable);
{$ENDIF}
end;

procedure TChildProcess.ReceiveData(Stream: TStream; var Size: Cardinal);
begin
{$IFDEF UNIX}
  Size := FProcess.Output.NumBytesAvailable;
  if Size > 0 then
    Stream.CopyFrom(FProcess.Output, Size);
{$ELSE}
  Size := FStdoutPipe.NumBytesAvailable;
  if Size > 0 then
    Stream.CopyFrom(FStdoutPipe, Size);
{$ENDIF}
end;

procedure TChildProcess.SendData(Stream: TStream; Size: Cardinal);
begin
{$IFDEF UNIX}
  FProcess.Input.CopyFrom(Stream, Size);
{$ELSE}
  FStdinPipe.CopyFrom(Stream, Size);
{$ENDIF}
end;

procedure TChildProcess.Terminate;
begin
{$IFDEF UNIX}
  FProcess.Terminate(0);
{$ELSE}
  TerminateProcess(FProcInfo.hProcess, 0);
  WaitForSingleObject(FProcInfo.hProcess, 1000);
  CloseHandle(FProcInfo.hProcess);
  CloseHandle(FProcInfo.hThread);
{$ENDIF}
end;

function TChildProcess.WaitFor(Timeout: Integer): Integer;
var
{$IFDEF UNIX}
  Tick: Cardinal;
{$ELSE}
  r: Cardinal;
{$ENDIF}
begin
{$IFDEF UNIX}
  if Timeout > -1 then begin
    Tick := GetTickCount;
    while FProcess.Running and (GetTickCount - Tick < Timeout) do
      Sleep(10);
  end else
    FProcess.WaitOnExit();
  if FProcess.Running then
    Result := -1
  else
    Result := FProcess.ExitCode;
{$ELSE}
  if Timeout > -1 then
    WaitForSingleObject(FProcInfo.hProcess, Timeout)
  else
    WaitForSingleObject(FProcInfo.hProcess, INFINITE);
  GetExitCodeProcess(FProcInfo.hProcess, r);
  if r = STILL_ACTIVE then
    Result := -1
  else
    Result := r;
{$ENDIF}
end;

end.
