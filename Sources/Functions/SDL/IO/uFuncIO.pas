unit uFuncIO;

interface

function ParseCursorPosResponse(Text: string; var X, Y: Integer): Boolean;

implementation

uses SysUtils;

function ParseCursorPosResponse(Text: string; var X, Y: Integer): Boolean;
var
  buff: string;
  p: Integer;
begin
  Result := False;
  if (Text[1] <> #$1B) or (Text[2] <> '[') or (Text[Length(Text)] <> 'R') then
    Exit;
  buff := Copy(Text, 3, Length(Text) - 3);
  p := Pos(';', buff);
  X := StrToInt(Copy(buff, 1, p - 1));
  Y := StrToInt(Copy(buff, p + 1));
  Result := True;
end;

end.
