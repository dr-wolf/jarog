unit uFuncFileWriteString;

interface

uses
  uFuncFile, uFuncContext;

type
  TFuncFileWriteString = class(TFuncFile)
  public
    constructor Create;
  private const
    P_HANDLE = 0;
    P_VALUE = 1;
    P_ENCODING = 2;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Writes string value to file stream.
    @type func(handle: int, value: string, encoding: int)
    @in handle int # Handle of a stream
    @in value string # Value to be written
    @in encoding int # Text codepage, see enc constant
    * }

implementation

uses SysUtils, Classes, uDataNumeric, uDataString, uOpCode;

{ TFuncFileWriteString }

constructor TFuncFileWriteString.Create;
begin
  inherited Create([MakeInt, MakeString, MakeInt]);
  FFuncUid := FID_FILE_WRITESTRING;
end;

procedure TFuncFileWriteString.Execute(Context: TFuncContext);
var
  s: TStream;
  cp: TEncoding;
  str: string;
  bytes: TBytes;
begin
  s := RetrieveStream(P_HANDLE, Context);
  cp := TEncoding.GetEncoding(AsInt(Context.Data[P_ENCODING]));
  str := AsString(Context.Data[P_VALUE]);
  bytes := TEncoding.Convert(TEncoding.UTF8, cp, BytesOf(str));
  cp.Free;
  s.WriteBuffer(Pointer(bytes)^, Length(bytes));
end;

end.
