unit uOpDup;

interface

uses
  uOp;

type
  TOpDup = class(TOp)
  public
    constructor Create;
  end;

implementation

uses uOpCode;

{ TOpDup }

constructor TOpDup.Create;
begin
  inherited Create(OID_DUP);
end;

end.
