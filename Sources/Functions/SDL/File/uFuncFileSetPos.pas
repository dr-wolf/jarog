unit uFuncFileSetPos;

interface

uses
  uFuncFile, uFuncContext;

type
  TFuncFileSetPos = class(TFuncFile)
  public
    constructor Create;
  private const
    P_HANDLE = 0;
    P_POSITION = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Set position in file stream.
    @type func(handle: int, position: int)
    @in handle int # Handle of a stream
    @in postition int # Position to be set from beginning of the file
    * }

implementation

uses Classes, uDataNumeric, uOpCode;

{ TFuncFileSetPos }

constructor TFuncFileSetPos.Create;
begin
  inherited Create([MakeInt, MakeInt]);
  FFuncUid := FID_FILE_SETPOS;
end;

procedure TFuncFileSetPos.Execute(Context: TFuncContext);
var
  s: TStream;
begin
  s := RetrieveStream(P_HANDLE, Context);
  s.Position := AsInt(Context.Data[P_POSITION]);
end;

end.
