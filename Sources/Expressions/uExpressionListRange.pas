unit uExpressionListRange;

interface

uses
  uExpression, uArray, uValue, uOp, uDataSection;

type
  TExpressionListRange = class(TExpression)
  public
    constructor Create(Data: TDataSection; List, ListIndex: TExpression);
    destructor Destroy; override;
  private
    FList: TExpression;
    FListIndex: TExpression;
  public
    function Compile: TArray<TOp>; override;
    function Eval: IValue; override;
  end;

implementation

uses uExceptions, uTypeInt, uTypeList, uData, uDataArray, uDataNumeric, uOpRange;

{ TExpressionListRange }

constructor TExpressionListRange.Create(Data: TDataSection; List, ListIndex: TExpression);
begin
  if not(ListIndex.ResultType is TTypeList) or not(ListIndex.ResultType.NestedType() is TTypeInt) then
    raise ECodeException.Create(E_EXPECTED_ITERABLE);
  if not(List.ResultType is TTypeList) then
    raise ECodeException.Create(E_EXPECTED_LIST);
  inherited Create(Data);
  FList := List;
  FListIndex := ListIndex;
  if FList.Group = egVariable then
    FGroup := egVariable
  else if FList.Group = FListIndex.Group then
    FGroup := FList.Group
  else
    FGroup := egMixed;
  FResultType := FList.ResultType;
end;

destructor TExpressionListRange.Destroy;
begin
  FList.Free;
  FListIndex.Free;
  inherited;
end;

function TExpressionListRange.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FList.Compile);
  AppendOp(Result, FListIndex.Compile);
  AppendOp(Result, [TOpRange.Create]);
end;

function TExpressionListRange.Eval: IValue;
var
  Source, Index: TData;
  Items: TArray<TData>;
  i: Integer;
begin
  Source := FList.Eval.Data;
  Index := FListIndex.Eval.Data;
  SetLength(Items, Index.Count);
  for i := 0 to High(Items) do
    Items[i] := Source.Item(TranslateIndex(AsInt(Index.Item(i)), Source.Count));
  Result := TValue.Create(FResultType, TDataArray.Create(Items, False));
end;

end.
