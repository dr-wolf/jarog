unit uInstructionWhile;

interface

uses
  uArray, uInstruction, uExpression, uOp;

type
  TInstructionWhile = class(TInstruction)
  public
    constructor Create(Condition: TExpression; Instruction: TInstruction);
    destructor Destroy; override;
  private
    FCondition: TExpression;
    FInstruction: TInstruction;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uExceptions, uDataStack, uTypeBool, uOpPush, uOpJump, uOpJit, uOpPop;

{ TInstructionWhile }

constructor TInstructionWhile.Create(Condition: TExpression; Instruction: TInstruction);
begin
  if not(Condition.ResultType is TTypeBool) then
    raise ECodeException.Create(E_EXPECTED_BOOL);
  inherited Create;
  FCondition := Condition;
  FInstruction := Instruction;
  FInstruction.SetParent(Self);

  InitTag(TAG_LOOP_BEGIN);
  InitTag(TAG_LOOP_END);
end;

destructor TInstructionWhile.Destroy;
begin
  FCondition.Free;
  FInstruction.Free;
  inherited;
end;

function TInstructionWhile.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, [TOpPush.Create(ftLoop, -1)]);
  AppendOp(Result, AttachTag(FCondition.Compile, GetTag(TAG_LOOP_BEGIN), tpAtFirst));
  AppendOp(Result, [TOpJit.Create(True, GetTag(TAG_LOOP_END))]);
  AppendOp(Result, FInstruction.Compile);
  AppendOp(Result, [TOpJump.Create(0, GetTag(TAG_LOOP_BEGIN)), TOpPop.Create(ftLoop).WithTag(GetTag(TAG_LOOP_END))]);
end;

end.
