unit uFunctionLoader;

interface

uses
  uExecFile, uTypedStream;

procedure LoadFunction(Stream: TTypedStream; ExecFile: TExecFile; var Result: TObject);

implementation

uses SysUtils, uArray, uData, uFuncContext, uCustomFunc, uOpCode,
  uFuncBitAnd, uFuncBitNot, uFuncBitOr, uFuncBitShl, uFuncBitShr, uFuncBitXor,
  uFuncDateDecode, uFuncDateEncode, uFuncDateFmt, uFuncDateParse,
  uFuncFileCopy, uFuncFileGetPos, uFuncFilePack, uFuncFileSetPos, uFuncFileSize, uFuncFileTrunc, uFuncFileReadInt,
  uFuncFileReadFloat, uFuncFileReadString, uFuncFileUnpack, uFuncFileWriteInt, uFuncFileWriteFloat,
  uFuncFileWriteString, uFuncFSPwd, uFuncFSDelete, uFuncFSChDir, uFuncFSMkDir, uFuncFSRmDir, uFuncFSStat,
  uFuncFSAbsPath, uFuncFSScan, uFuncFSOpen, uFuncFSClose,
  uFuncGCClean, uFuncGCDisable, uFuncGCEnable,
  uFuncIOClear, uFuncIOColor, uFuncIOCurPos, uFuncIOGet, uFuncIOGoto, uFuncIOKey, uFuncIOPause, uFuncIOPut, uFuncIOSize,
  uFuncMathAbs, uFuncMathCos, uFuncMathFrac, uFuncMathFloor, uFuncMathLog, uFuncMathRand, uFuncMathSin, uFuncMathArcTan,
  uFuncMathTan, uFuncMathArcSin, uFuncMathArcCos,
  uFuncOSEnvList, uFuncOSSleep, uFuncOSExec, uFuncOSLsof, uFuncOSLsos, uFuncOSGetEnv, uFuncOSSetEnv, uFuncOSTime,
  uFuncOSPid, uFuncOSThreadId,
  uFuncProcOpen, uFuncProcClose, uFuncProcRead, uFuncProcStart, uFuncProcWrite, uFuncProcWait,
  uFuncRexMatch, uFuncRexFind, uFuncRexReplace, uFuncRexSplit,
  uFuncStrFmt, uFuncStrLen, uFuncStrSub, uFuncStrPos, uFuncStrParse, uFuncStrAscii, uFuncStrText,
  uFuncStrLower, uFuncStrUpper,
  uFuncTCPConnect, uFuncTCPClose, uFuncTCPSend, uFuncTCPReceive, uFuncTCPListen, uFuncTCPAccept, uFuncTCPRemote,
  uFuncUDPBind, uFuncUDPClose, uFuncUDPReceive, uFuncUDPSend, uFuncUDPBroadcast;

procedure LoadFunction(Stream: TTypedStream; ExecFile: TExecFile; var Result: TObject);

  procedure LoadCustom(Stream: TTypedStream; ExecFile: TExecFile; var Result: TObject);

    function ReadArray(Stream: TTypedStream): TArray<Integer>;
    var
      i: Integer;
    begin
      SetLength(Result, Stream.ReadInteger);
      for i := 0 to High(Result) do
        Result[i] := Stream.ReadInteger;
    end;

  var
    cx: TFuncContext;
    p, c: TArray<Integer>;
    a: Integer;
  begin
    a := Stream.ReadInteger;
    p := ReadArray(Stream);
    c := ReadArray(Stream);
    cx := TFuncContext.Create;
    Result := TCustomFunc.Create(cx, c, p, ExecFile.Load(Stream.ReadInteger) as TData);
    (Result as TCustomFunc).SetAddress(a);
    cx.Load(Stream, ExecFile);
  end;

begin
  Stream.ReadByte;
  case Stream.ReadByte of
    FID_CUSTOM:
      LoadCustom(Stream, ExecFile, Result);

    FID_BIT_AND:
      Result := TFuncBitAnd.Create;
    FID_BIT_NOT:
      Result := TFuncBitNot.Create;
    FID_BIT_OR:
      Result := TFuncBitOr.Create;
    FID_BIT_SHL:
      Result := TFuncBitShl.Create;
    FID_BIT_SHR:
      Result := TFuncBitShr.Create;
    FID_BIT_XOR:
      Result := TFuncBitXor.Create;

    FID_DATE_DECODE:
      Result := TFuncDateDecode.Create;
    FID_DATE_ENCODE:
      Result := TFuncDateEncode.Create;
    FID_DATE_FMT:
      Result := TFuncDateFmt.Create;
    FID_DATE_PARSE:
      Result := TFuncDateParse.Create;

    FID_FILE_COPY:
      Result := TFuncFileCopy.Create;
    FID_FILE_GETPOS:
      Result := TFuncFileGetPos.Create;
    FID_FILE_PACK:
      Result := TFuncFilePack.Create;
    FID_FILE_READFLOAT:
      Result := TFuncFileReadFloat.Create;
    FID_FILE_READINT:
      Result := TFuncFileReadInt.Create;
    FID_FILE_READSTRING:
      Result := TFuncFileReadString.Create;
    FID_FILE_SETPOS:
      Result := TFuncFileSetPos.Create;
    FID_FILE_SIZE:
      Result := TFuncFileSize.Create;
    FID_FILE_TRUNC:
      Result := TFuncFileTrunc.Create;
    FID_FILE_UNPACK:
      Result := TFuncFileUnpack.Create;
    FID_FILE_WRITEFLOAT:
      Result := TFuncFileWriteFloat.Create;
    FID_FILE_WRITEINT:
      Result := TFuncFileWriteInt.Create;
    FID_FILE_WRITESTRING:
      Result := TFuncFileWriteString.Create;

    FID_FS_ABSPATH:
      Result := TFuncFSAbsPath.Create;
    FID_FS_CHDIR:
      Result := TFuncFSChDir.Create;
    FID_FS_CLOSE:
      Result := TFuncFSClose.Create;
    FID_FS_DELETE:
      Result := TFuncFSDelete.Create;
    FID_FS_MKDIR:
      Result := TFuncFSMkDir.Create;
    FID_FS_OPEN:
      Result := TFuncFSOpen.Create;
    FID_FS_PWD:
      Result := TFuncFSPwd.Create;
    FID_FS_RMDIR:
      Result := TFuncFSRmDir.Create;
    FID_FS_SCAN:
      Result := TFuncFSScan.Create;
    FID_FS_STAT:
      Result := TFuncFSStat.Create;

    FID_GC_CLEAN:
      Result := TFuncGCClean.Create;
    FID_GC_DISABLE:
      Result := TFuncGCDisable.Create;
    FID_GC_ENABLE:
      Result := TFuncGCEnable.Create;

    FID_IO_CLEAR:
      Result := TFuncIOClear.Create;
    FID_IO_COLOR:
      Result := TFuncIOColor.Create;
    FID_IO_CURPOS:
      Result := TFuncIOCurPos.Create;
    FID_IO_GET:
      Result := TFuncIOGet.Create;
    FID_IO_GOTO:
      Result := TFuncIOGoto.Create;
    FID_IO_KEY:
      Result := TFuncIOKey.Create;
    FID_IO_PAUSE:
      Result := TFuncIOPause.Create;
    FID_IO_PUT:
      Result := TFuncIOPut.Create;
    FID_IO_SIZE:
      Result := TFuncIOSize.Create;

    FID_MATH_ABS:
      Result := TFuncMathAbs.Create;
    FID_MATH_ARCCOS:
      Result := TFuncMathArcCos.Create;
    FID_MATH_ARCSIN:
      Result := TFuncMathArcSin.Create;
    FID_MATH_ARCTAN:
      Result := TFuncMathArcTan.Create;
    FID_MATH_COS:
      Result := TFuncMathCos.Create;
    FID_MATH_FLOOR:
      Result := TFuncMathFloor.Create;
    FID_MATH_FRAC:
      Result := TFuncMathFrac.Create;
    FID_MATH_LOG:
      Result := TFuncMathLog.Create;
    FID_MATH_RAND:
      Result := TFuncMathRand.Create;
    FID_MATH_SIN:
      Result := TFuncMathSin.Create;
    FID_MATH_TAN:
      Result := TFuncMathTan.Create;

    FID_OS_ENVLIST:
      Result := TFuncOSEnvList.Create;
    FID_OS_EXEC:
      Result := TFuncOSExec.Create;
    FID_OS_GETENV:
      Result := TFuncOSGetEnv.Create;
    FID_OS_LSOF:
      Result := TFuncOSLsof.Create;
    FID_OS_LSOS:
      Result := TFuncOSLsos.Create;
    FID_OS_PID:
      Result := TFuncOSPid.Create;
    FID_OS_SETENV:
      Result := TFuncOSSetEnv.Create;
    FID_OS_SLEEP:
      Result := TFuncOSSleep.Create;
    FID_OS_THREADID:
      Result := TFuncOSThreadId.Create;
    FID_OS_TIME:
      Result := TFuncOSTime.Create;

    FID_PROC_CLOSE:
      Result := TFuncProcClose.Create;
    FID_PROC_OPEN:
      Result := TFuncProcOpen.Create;
    FID_PROC_READ:
      Result := TFuncProcRead.Create;
    FID_PROC_START:
      Result := TFuncProcStart.Create;
    FID_PROC_WAIT:
      Result := TFuncProcWait.Create;
    FID_PROC_WRITE:
      Result := TFuncProcWrite.Create;

    FID_REX_FIND:
      Result := TFuncRexFind.Create;
    FID_REX_MATCH:
      Result := TFuncRexMatch.Create;
    FID_REX_REPLACE:
      Result := TFuncRexReplace.Create;
    FID_REX_SPLIT:
      Result := TFuncRexSplit.Create;

    FID_STR_ASCII:
      Result := TFuncStrAscii.Create;
    FID_STR_FMT:
      Result := TFuncStrFmt.Create;
    FID_STR_LEN:
      Result := TFuncStrLen.Create;
    FID_STR_LOWER:
      Result := TFuncStrLower.Create;
    FID_STR_PARSE:
      Result := TFuncStrParse.Create;
    FID_STR_POS:
      Result := TFuncStrPos.Create;
    FID_STR_SUB:
      Result := TFuncStrSub.Create;
    FID_STR_TEXT:
      Result := TFuncStrText.Create;
    FID_STR_UPPER:
      Result := TFuncStrUpper.Create;

    FID_TCP_ACCEPT:
      Result := TFuncTCPAccept.Create;
    FID_TCP_CLOSE:
      Result := TFuncTCPClose.Create;
    FID_TCP_CONNECT:
      Result := TFuncTCPConnect.Create;
    FID_TCP_LISTEN:
      Result := TFuncTCPListen.Create;
    FID_TCP_RECEIVE:
      Result := TFuncTCPReceive.Create;
    FID_TCP_REMOTE:
      Result := TFuncTCPRemote.Create;
    FID_TCP_SEND:
      Result := TFuncTCPSend.Create;

    FID_UDP_BIND:
      Result := TFuncUDPBind.Create;
    FID_UDP_BROADCAST:
      Result := TFuncUDPBroadcast.Create;
    FID_UDP_CLOSE:
      Result := TFuncUDPClose.Create;
    FID_UDP_RECEIVE:
      Result := TFuncUDPReceive.Create;
    FID_UDP_SEND:
      Result := TFuncUDPSend.Create;
  else
    raise Exception.Create('Unknown FID');
  end;
end;

end.
