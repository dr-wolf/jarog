unit uOpRun;

interface

uses
  uOp;

type
  TOpRun = class(TOp)
  public
    constructor Create;
  end;

implementation

uses uOpCode;

{ TOpRun }

constructor TOpRun.Create;
begin
  inherited Create(OID_RUN);
end;

end.
