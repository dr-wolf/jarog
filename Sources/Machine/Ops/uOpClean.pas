unit uOpClean;

interface

uses
  uOp;

type
  TOpClean = class(TOp)
  public
    constructor Create;
  end;

implementation

uses uOpCode;

{ TOpClean }

constructor TOpClean.Create;
begin
  inherited Create(OID_CLEAN);
end;

end.
