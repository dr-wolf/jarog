unit uConsoleInterface;

interface

type
  IConsole = interface(IInterface)
    function Key(WaitForInput: Boolean): Byte;
    function Read: string;
    procedure Clear;
    procedure GetCursor(var X, Y: Integer);
    procedure GetSize(var X, Y: Integer);
    procedure SetCursor(X, Y: Integer);
    procedure SetColor(Text, Background: Byte);
    procedure Print(const Text: string);
  end;

implementation

end.
