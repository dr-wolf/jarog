unit uFuncStrHex;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncStrHex = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Converts int into hexadecimal string representation.
    @type func(value: int, width: int -> string)
    @in value int # Initial value
    @in width int # Hex digit width
    @out - string # Hexadecimal representation
    * }

implementation

uses SysUtils, uTypeFactory, uDataNumeric, uDataString, uOpCode;

{ TFuncStrHex }

constructor TFuncStrHex.Create;
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt], ['value', 'width'], TypeFactory.GetString);
  FFuncUid := FID_STR_HEX;
end;

procedure TFuncStrHex.Execute(Context: TFuncContext);
begin
  Return(TDataString.Create(IntToHex(AsInt(Context.Data[P_value]), AsInt(Context.Data[P_width]))));
end;

end.
