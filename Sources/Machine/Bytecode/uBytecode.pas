unit uBytecode;

interface

uses
  uOpCode, uTypedStream;

type
  TBytecode = record
    Op: TOID;
    Break: Boolean;
    FlagA: Boolean;
    FlagB: Boolean;
    SParam: Byte;
    LParam: Integer;
  end;

procedure ZeroCode(var Code: TBytecode);
procedure WriteCodeToStream(const Code: TBytecode; Stream: TTypedStream);
procedure ReadCodeFromStream(var Code: TBytecode; Stream: TTypedStream);

implementation

procedure ZeroCode(var Code: TBytecode);
begin
  Code.Op := 0;
  Code.SParam := 0;
  Code.LParam := 0;
end;

procedure WriteCodeToStream(const Code: TBytecode; Stream: TTypedStream);
begin
  Stream.WriteByte(Code.Op);
  if (Code.Op and $1F) in [OID_BINARY, OID_POP, OID_PUSH, OID_STACK, OID_UNARY] then
    Stream.WriteByte(Code.SParam);
  if (Code.Op and $1F) in [OID_FIELD, OID_JIT, OID_JMP, OID_LOAD, OID_LOG, OID_PUSH, OID_STACK, OID_SYNC] then
    Stream.WriteInteger(Code.LParam);
end;

procedure ReadCodeFromStream(var Code: TBytecode; Stream: TTypedStream);
var
  b: Byte;
begin
  b := Stream.ReadByte;
  Code.Op := b and $1F;
  Code.FlagA := b and $20 <> 0;
  Code.FlagB := b and $40 <> 0;
  Code.Break := b and $80 <> 0;
  if Code.Op in [OID_BINARY, OID_POP, OID_PUSH, OID_STACK, OID_UNARY] then
    Code.SParam := Stream.ReadByte;
  if Code.Op in [OID_FIELD, OID_JIT, OID_JMP, OID_LOAD, OID_LOG, OID_PUSH, OID_STACK, OID_SYNC] then
    Code.LParam := Stream.ReadInteger;
end;

end.
