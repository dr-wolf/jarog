unit uFuncIOGet;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncIOGet = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Reads text from console.
    @type func(-> string)
    @out - string # Text read from console
    * }

implementation

uses uDataString, uOpCode;

{ TFuncIOGet }

constructor TFuncIOGet.Create;
begin
  inherited Create([], MakeString);
  FFuncUid := FID_IO_GET;
end;

procedure TFuncIOGet.Execute(Context: TFuncContext);
var
  s: string;
begin
  FCoreContext.IOProvider.Read(s);
  Return(TDataString.Create(s));
end;

end.
