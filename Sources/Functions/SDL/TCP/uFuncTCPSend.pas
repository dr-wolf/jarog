unit uFuncTCPSend;

interface

uses
  uFuncTCP, uFuncContext, BlckSock;

type
  TFuncTCPSend = class(TFuncTCP)
  public
    constructor Create;
  private const
    P_SOCKET = 0;
    P_FILE = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Read bytes from file and sends them to socket.
    @type func(socket: int, file: int)
    @in socket int # Socket handle
    @in file int # File handle
    * }

implementation

uses Classes, uDataString, uExceptions, uOpCode;

{ TFuncTCPSend }

constructor TFuncTCPSend.Create;
begin
  inherited Create([MakeInt, MakeInt]);
  FFuncUid := FID_TCP_SEND;
end;

procedure TFuncTCPSend.Execute(Context: TFuncContext);
var
  sock: TTCPBlockSocket;
  stream: TStream;
begin
  sock := RetrieveSocket(P_SOCKET, Context);
  stream := RetrieveStream(P_FILE, Context);
  stream.Seek(0, soFromBeginning);
  sock.SendStreamRaw(stream);
  if sock.LastError <> 0 then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create(sock.LastErrorDesc));
end;

end.
