unit uConfig;

interface

uses
  uArray;

const
{$I build.inc};

  type TConfigParam = (cpLibPath, cpDebug, cpOutput, cpHelp, cpLog, pErr);
  TConfigParamSet = set of TConfigParam;

type
  TConfig = class(TObject)
  public
    constructor Create(AcceptedParams: TConfigParamSet);
  private
    FAcceptedParams: TConfigParamSet;
    FLibPathList: TArray<string>;
    FCliArgs: TArray<string>;
    FScript: string;
    FOutput: string;
    FBasePath: string;
    FDebugPort: Integer;
    FHelp: Boolean;
    FLog: Boolean;
    function GetLibPathCount: Integer;
    function GetLibPath(I: Integer): string;
    function GetCliArgCount: Integer;
    function GetCliArg(I: Integer): string;
    function ParsePathList(Text: string): TArray<string>;
    procedure ParseParamStr(const Param: string);
  public
    property Script: string read FScript;
    property Output: string read FOutput;
    property DebugPort: Integer read FDebugPort;
    property Help: Boolean read FHelp;
    property Log: Boolean read FLog;

    property LibPath[I: Integer]: string read GetLibPath;
    property LibPathCount: Integer read GetLibPathCount;
    property CliArg[I: Integer]: string read GetCliArg;
    property CliArgCount: Integer read GetCliArgCount;

    procedure PrintHelp;
  end;

implementation

uses SysUtils;

function IsRelativePath(const Path: string): Boolean;
var
  L: Integer;
begin
  L := Length(Path);
  Result := (L > 0) and (Path[1] <> PathDelim)
{$IFDEF MSWINDOWS} and (L > 1) and (Path[2] <> ':'){$ENDIF MSWINDOWS};
end;

{ TConfig }

constructor TConfig.Create(AcceptedParams: TConfigParamSet);
var
  I, n: Integer;
  s: string;
begin
  FAcceptedParams := AcceptedParams;

  FLibPathList := nil;
  FCliArgs := nil;

  FOutput := '';
  FHelp := False;
  FLog := False;
  FBasePath := ExtractFilePath(ParamStr(0));
  FDebugPort := 0;

  I := 1;
  while I <= ParamCount() do
  begin
    s := ParamStr(I);
    if (Length(s) > 2) and (s[1] + s[2] = '--') then
    begin
      ParseParamStr(Copy(s, 3));
      Inc(I);
    end
    else
      Break;
  end;

  if I <= ParamCount() then
  begin
    FScript := ParamStr(I);
    SetLength(FCliArgs, ParamCount - I + 1);
    for n := I to ParamCount() do
      FCliArgs[n - I] := ParamStr(n);
  end
  else
    FHelp := True;
end;

function TConfig.GetCliArg(I: Integer): string;
begin
  Result := FCliArgs[I];
end;

function TConfig.GetCliArgCount: Integer;
begin
  Result := Length(FCliArgs);
end;

function TConfig.GetLibPath(I: Integer): string;
begin
  Result := FLibPathList[I];
end;

function TConfig.GetLibPathCount: Integer;
begin
  Result := Length(FLibPathList);
end;

procedure TConfig.ParseParamStr(const Param: string);

  function DetectParam(const ParamName: string): TConfigParam;
  const
    PARAM_KWORDS: array [TConfigParam] of string = ('libpath', 'debug', 'out', 'help', 'log', '');
  begin
    Result := Low(TConfigParam);
    while (Result < High(TConfigParam)) and (PARAM_KWORDS[Result] <> ParamName) do
      Result := Succ(Result);
  end;

var
  pname: string;
  p: TConfigParam;
  I: Integer;
begin
  pname := Copy(Param, 1, Pos('=', Param + '=') - 1);
  p := DetectParam(pname);
  if p in FAcceptedParams then
    case p of
      cpHelp:
        FHelp := True;
      cpLog:
        FLog := True;
      cpLibPath:
        begin
          FLibPathList := ParsePathList(Copy(Param, Pos('=', Param + '=') + 1));
          for I := 0 to High(FLibPathList) do
            if IsRelativePath(FLibPathList[I]) then
              FLibPathList[I] := IncludeTrailingPathDelimiter(ExpandFileName(FBasePath + FLibPathList[I]));
        end;
      cpOutput:
        FOutput := Copy(Param, Pos('=', Param + '=') + 1);
      cpDebug:
        FDebugPort := StrToIntDef(Copy(Param, Pos('=', Param + '=') + 1), 0);
    else
      FHelp := True;
    end
  else
    FHelp := True;
end;

function TConfig.ParsePathList(Text: string): TArray<string>;
const
  PATH_SEP = {$IFDEF UNIX}':'{$ELSE}';'{$ENDIF};
var
  p: Integer;
begin
  SetLength(Result, 0);
  if Text <> '' then
    repeat
      p := Pos(PATH_SEP, Text + PATH_SEP);
      SetLength(Result, Length(Result) + 1);
      Result[High(Result)] := Copy(Text, 1, p - 1);
      Delete(Text, 1, p);
    until (Text = '');
end;

procedure TConfig.PrintHelp;
const
  MAN: array [TConfigParam] of string =
    ('--libpath=<path>: comma or semicolon separated list of pathes with units for include',
    '--debug=<port>: UPD port for debugger',
    '--out=<path>: output filename, script will be executed if no output specified', '--help: print help',
    '--log: print bytecode listing to filename.log', '');
var
  p: TConfigParam;
begin
  Writeln('Usage: ' + ExtractFileName(ParamStr(0)) + ' [--param=value] <script>');
  for p := Low(p) to High(p) do
    if p in FAcceptedParams then
      Writeln('  ' + MAN[p]);
end;

end.
