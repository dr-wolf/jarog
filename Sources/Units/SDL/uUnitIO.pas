unit uUnitIO;

interface

uses
  uUnit;

type
  TUnitIO = class(TUnit)
  public
    constructor Create;
  end;

  { *
    @descr This unit designed for console input and output.
    @const kb struct # Keyboard scancodes string
    @const color struct # Console colors
    @const eol string # Contains \r\n string
    * }

implementation

uses SysUtils, uArray, uValue, uTypeFactory, uType, uData, uDataString, uDataArray, uDataNumeric, uFuncIOClear,
  uFuncIOColor,
  uFuncIOCurPos, uFuncIOGet, uFuncIOGoto, uFuncIOKey, uFuncIOPause, uFuncIOPut, uFuncIOSize;

{ TUnitIO }

function CreateStruct(Keys: array of string; Vals: array of Integer): IValue;
var
  i: Integer;
  a: TArray<TData>;
  t: TArray<TType>;
begin
  if Length(Keys) <> Length(Vals) then
    raise Exception.Create('Keys must match values');
  SetLength(a, Length(Vals));
  SetLength(t, Length(Keys));
  for i := 0 to High(a) do
  begin
    a[i] := TDataNumeric.Create(Vals[i]);
    t[i] := TypeFactory.GetInt;
  end;
  Result := TValue.Create(TypeFactory.GetStruct(t, Keys), TDataArray.Create(a), True);
end;

constructor TUnitIO.Create;
begin
  inherited Create('<io>');
  AddValue('eol', TValue.Create(TypeFactory.GetString, TDataString.Create(#$D#$A)));
  AddValue('kb', CreateStruct(['backspace', 'tab', 'return', 'pause', 'escape', 'space', 'pageup', 'pagedn', 'last',
    'home', 'left', 'up', 'right', 'down', 'insert', 'delete', 'win', 'np0', 'np1', 'np2', 'np3', 'np4', 'np5', 'np6',
    'np7', 'np8', 'np9', 'npmul', 'npplus', 'npminus', 'npdot', 'f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9',
    'f10', 'f11', 'f12'], [8, 9, 13, 19, 27, 32, 33, 34, 35, 36, 37, 38, 39, 40, 45, 46, 92, 96, 97, 98, 99, 100, 101,
    102, 103, 104, 105, 106, 107, 109, 110, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123]));
  AddValue('clr', CreateStruct(['black', 'navy', 'green', 'teal', 'maroon', 'purple', 'orange', 'silver', 'gray',
    'blue', 'lime', 'cyan', 'red', 'magenta', 'yellow', 'white'], [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
    15, 16]));

  AddValue('clear', MakeFunc(TFuncIOClear.Create, [], []));
  AddValue('color', MakeFunc(TFuncIOColor.Create, [TypeFactory.GetInt, TypeFactory.GetInt], ['text', 'back']));
  AddValue('curpos', MakeFunc(TFuncIOCurPos.Create, [], [], TypeFactory.GetStruct([TypeFactory.GetInt,
    TypeFactory.GetInt], ['x', 'y'])));
  AddValue('get', MakeFunc(TFuncIOGet.Create, [], [], TypeFactory.GetString));
  AddValue('goto', MakeFunc(TFuncIOGoto.Create, [TypeFactory.GetInt, TypeFactory.GetInt], ['x', 'y']));
  AddValue('key', MakeFunc(TFuncIOKey.Create, [TypeFactory.GetBool], ['wait'], TypeFactory.GetInt));
  AddValue('pause', MakeFunc(TFuncIOPause.Create, [], []));
  AddValue('put', MakeFunc(TFuncIOPut.Create, [TypeFactory.GetString], ['text']));
  AddValue('size', MakeFunc(TFuncIOSize.Create, [], [], TypeFactory.GetStruct([TypeFactory.GetInt, TypeFactory.GetInt],
    ['width', 'height'])));
end;

end.
