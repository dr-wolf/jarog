unit uDataNumeric;

interface

uses
  uOperations, uData, uExecFile, uTypedStream, uNumeric;

type
  TDataNumeric = class(TData)
  public
    constructor Create(Value: Integer); overload;
    constructor Create(Value: TNumeric); overload;
    constructor Create(const Value: string); overload;
    destructor Destroy; override;
  private
    FValue: TNumeric;
  public
    function Apply(Op: TBinaryOp; const Data: TData): TData; override;
    function Apply(Op: TUnaryOp): TData; override;
    function Dup: TData; override;
    function IsEqual(const Data: TData): Boolean; override;
    function Serialize(const ExecFile: TExecFile): TTypedStream; override;
    procedure Assign(const Data: TData); override;
    procedure Convert(C: TConverter); override;
  end;

  TDataNumericE = class(TDataNumeric)
  public
    constructor Create;
  end;

  TDataNumericPi = class(TDataNumeric)
  public
    constructor Create;
  end;

function AsInt(const Data: TData): Integer;
function AsNumeric(const Data: TData): TNumeric;

implementation

uses uArray, uExceptions, uDataBool, uDataArray, uOpCode, uInteger, uBigFloat, mp_types, mp_base, mp_real;

function AsInt(const Data: TData): Integer;
var
  d: TData;
begin
  d := Data.Unwrap;
  if d is TDataNumeric then
  begin
    if (d as TDataNumeric).FValue is TInteger then
      Result := ((d as TDataNumeric).FValue as TInteger).Value
    else
      raise ERuntimeException.Create(E_INT_OVERFLOW, nil);
  end
  else
    raise ERuntimeException.Create(E_TYPE_UNCOMPATIBLE, nil);
end;

function AsNumeric(const Data: TData): TNumeric;
begin
  if Data.Unwrap is TDataNumeric then
    Result := (Data.Unwrap as TDataNumeric).FValue
  else
    raise ERuntimeException.Create(E_TYPE_UNCOMPATIBLE, nil);
end;

{ TDataNumeric }

constructor TDataNumeric.Create(Value: Integer);
begin
  inherited Create;
  FValue := TInteger.Create(Value);
end;

constructor TDataNumeric.Create(Value: TNumeric);
begin
  FValue := Value;
end;

constructor TDataNumeric.Create(const Value: string);

  function IsIntDecimal(const s: string): Boolean;
  var
    i: Integer;
  begin
    Result := True;
    i := 1;
    repeat
      Result := Result and (s[i] in ['0' .. '9']);
      Inc(i);
    until not Result or (i > Length(s));
  end;

var
  i: mp_int;
  f: mp_float;
  si: string;
begin
  inherited Create;
  try
    if Copy(Value, 1, 2) = '0x' then
    begin
      si := Copy(Value, 3);
      mp_init(i);
      mp_read_radix(i, PAnsiChar(AnsiString(si)), 16);
      FValue := ToNumeric(i);
    end else if IsIntDecimal(Value) then
    begin
      mp_init(i);
      mp_read_radix(i, PAnsiChar(AnsiString(Value)), 10);
      FValue := ToNumeric(i);
    end else begin
      mpf_init(f);
      mpf_read_decimal(f, PAnsiChar(AnsiString(Value)));
      FValue := TBigFloat.Create(f);
    end;
  except
    raise ECodeException.Create(E_PARSE_ERROR, Value);
  end;
end;

destructor TDataNumeric.Destroy;
begin
  FValue.Free;
  inherited;
end;

function TDataNumeric.Apply(Op: TBinaryOp; const Data: TData): TData;

  function Range(RangeFrom, RangeTo: Integer): TArray<TData>;
  var
    i: Integer;
  begin
    SetLength(Result, Abs(RangeFrom - RangeTo) + 1);
    if RangeFrom <= RangeTo then
    begin
      i := RangeFrom;
      while i <= RangeTo do
      begin
        Result[i - RangeFrom] := TDataNumeric.Create(i);
        Inc(i);
      end;
    end else begin
      i := RangeFrom;
      while i >= RangeTo do
      begin
        Result[-i + RangeFrom] := TDataNumeric.Create(i);
        Dec(i);
      end;
    end;
  end;

var
  d: TDataNumeric;
begin
  if not(Data is TDataNumeric) then
    raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, 'int, float');
  d := Data as TDataNumeric;
  case Op of
    boAddition:
      Result := TDataNumeric.Create(FValue.Add(d.FValue));
    boSubtraction:
      Result := TDataNumeric.Create(FValue.Subtract(d.FValue));
    boMultiplication:
      Result := TDataNumeric.Create(FValue.Multiply(d.FValue));
    boDivision:
      if not d.FValue.IsZero then
        Result := TDataNumeric.Create(FValue.Divide(d.FValue))
      else
        raise ERuntimeException.Create(E_DIVIZION_ZERO, nil);
    boModulo:
      if not d.FValue.IsZero then
        Result := TDataNumeric.Create(FValue.Modulo(d.FValue))
      else
        raise ERuntimeException.Create(E_DIVIZION_ZERO, nil);
    boRange:
      Result := TDataArray.Create(Range(AsInt(Self), AsInt(d)), False);
    boPower:
      Result := TDataNumeric.Create(FValue.Power(d.FValue));
    boEqual:
      Result := TDataBool.Create(IsEqual(d));
    boNotEqual:
      Result := TDataBool.Create(not IsEqual(d));
    boLess:
      Result := TDataBool.Create(FValue.Compare(d.FValue) < 0);
    boGreater:
      Result := TDataBool.Create(FValue.Compare(d.FValue) > 0);
    boLessEqual:
      Result := TDataBool.Create(FValue.Compare(d.FValue) <= 0);
    boGreaterEqual:
      Result := TDataBool.Create(FValue.Compare(d.FValue) >= 0);
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  end;
end;

function TDataNumeric.Apply(Op: TUnaryOp): TData;
begin
  case Op of
    uoPositive:
      Result := Self;
    uoNegative:
      Result := TDataNumeric.Create(FValue.Negative);
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  end;
end;

procedure TDataNumeric.Assign(const Data: TData);
var
  e: Boolean;
begin
  e := FValue is TBigFloat;
  if Data is TDataNumeric then
  begin
    FValue.Free;
    FValue := TDataNumeric(Data).FValue.Dup(e);
  end
  else
    raise ERuntimeException.Create('Data must be numeric');
end;

function TDataNumeric.Dup: TData;
begin
  Result := TDataNumeric.Create(FValue.Dup(False));
end;

procedure TDataNumeric.Convert(C: TConverter);
begin
  C.ConvertNumeric(FValue);
end;

function TDataNumeric.IsEqual(const Data: TData): Boolean;
begin
  Result := FValue.Compare((Data as TDataNumeric).FValue) = 0;
end;

function TDataNumeric.Serialize(const ExecFile: TExecFile): TTypedStream;
begin
  Result := TTypedStream.Create;
  Result.WriteByte(DID_DATA_NUMERIC);
  FValue.Serialize(Result);
end;

{ TDataNumericE }

constructor TDataNumericE.Create;
var
  V: mp_float;
begin
  mpf_init(V);
  mpf_set_exp1(V);
  inherited Create(TBigFloat.Create(V));
end;

{ TDataNumericPi }

constructor TDataNumericPi.Create;
var
  V: mp_float;
begin
  mpf_init(V);
  mpf_set_pi(V);
  inherited Create(TBigFloat.Create(V));
end;

end.
