unit uInstructionWith;

interface

uses
  uArray, uInstruction, uExpression, uOp;

type
  TInstructionWith = class(TInstruction)
  public
    constructor Create(Variable, Value: TExpression; Instruction: TInstruction);
    destructor Destroy; override;
  private
    FAssign: TInstruction;
    FInstruction: TInstruction;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uInstructionAssign;

{ TInstructionWith }

constructor TInstructionWith.Create(Variable, Value: TExpression; Instruction: TInstruction);
begin
  inherited Create;
  FAssign := TInstructionAssign.Create(Variable, Value);
  FInstruction := Instruction;
  FInstruction.SetParent(Self);
end;

destructor TInstructionWith.Destroy;
begin
  FAssign.Free;
  FInstruction.Free;
  inherited;
end;

function TInstructionWith.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FAssign.Compile);
  AppendOp(Result, FInstruction.Compile);
end;

end.
