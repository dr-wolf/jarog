unit uFuncFSRmDir;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncFSRmDir = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_PATH = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Deletes empty directory.
    @type func(path: string)
    @in path string # Path of directory to be deleted
    * }

implementation

uses SysUtils, uDataString, uOpCode;

{ TFuncFSRmDir }

constructor TFuncFSRmDir.Create;
begin
  inherited Create([MakeString]);
  FFuncUid := FID_FS_RMDIR;
end;

procedure TFuncFSRmDir.Execute(Context: TFuncContext);
begin
  if not RemoveDir(ExpandFileName(AsString(Context.Data[P_PATH]))) then
    RaiseLastOSError;
end;

end.
