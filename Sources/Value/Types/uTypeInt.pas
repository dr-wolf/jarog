unit uTypeInt;

interface

uses
  uOperations, uType, uTypeFloat, uData;

type
  TTypeInt = class(TType)
  public
    function Apply(Op: TBinaryOp; T: TType): TType; override;
    function Apply(Op: TUnaryOp): TType; override;
    function Compatible(T: TType; StrictCompat: Boolean = False): Boolean; override;
    function Default: TData; override;
  end;

implementation

uses uExceptions, uTypeFactory, uDataNumeric;

{ TTypeInt }

function TTypeInt.Apply(Op: TBinaryOp; T: TType): TType;
begin
  if not((T is TTypeInt) or (T is TTypeFloat)) then
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, FHash);
  case Op of
    boAddition .. boDivision, boPower:
      if T is TTypeInt then
        Result := TypeFactory.GetInt
      else
        Result := TypeFactory.GetFloat;
    boModulo:
      if T is TTypeInt then
        Result := TypeFactory.GetInt
      else
        raise ECodeException.Create(E_UNSUPPORTED_OPERATION, FHash);
    boRange:
      if T is TTypeInt then
        Result := TypeFactory.GetList(TypeFactory.GetInt)
      else
        raise ECodeException.Create(E_UNSUPPORTED_OPERATION, FHash);
    boEqual .. boGreaterEqual:
      Result := TypeFactory.GetBool;
  else
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, FHash);
  end;
end;

function TTypeInt.Apply(Op: TUnaryOp): TType;
begin
  if Op in [uoPositive, uoNegative] then
    Result := TypeFactory.GetInt
  else
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, 'int');
end;

function TTypeInt.Compatible(T: TType; StrictCompat: Boolean = False): Boolean;
begin
  if StrictCompat then
    Result := T is TTypeInt
  else
    Result := (T is TTypeFloat) or (T is TTypeInt);
end;

function TTypeInt.Default: TData;
begin
  Result := TDataNumeric.Create(0);
end;

end.
