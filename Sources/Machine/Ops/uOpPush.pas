unit uOpPush;

interface

uses
  uBytecode, uOp, uTag, uDataStack;

type
  TOpPush = class(TOp)
  public
    constructor Create(Frame: TFrameType; Address: Integer); overload;
    constructor Create(Frame: TFrameType; Tag: ITag = nil); overload;
  private
    FFrame: TFrameType;
    FFrameTag: ITag;
    FReturnAddress: Integer;
  public
    procedure Serialize(var Code: TBytecode); override;
  end;

implementation

uses uOpCode;

{ TOpPush }

constructor TOpPush.Create(Frame: TFrameType; Address: Integer);
begin
  inherited Create(OID_PUSH);
  FFrame := Frame;
  FFrameTag := nil;
  FReturnAddress := Address;
end;

constructor TOpPush.Create(Frame: TFrameType; Tag: ITag = nil);
begin
  inherited Create(OID_PUSH);
  FFrame := Frame;
  FFrameTag := Tag;
  FReturnAddress := -1;
end;

procedure TOpPush.Serialize(var Code: TBytecode);
begin
  Code.Op := CompileOp();
  Code.SParam := Byte(FFrame);
  if FFrameTag <> nil then
  begin
    if FReturnAddress >= 0 then
      Code.LParam := FReturnAddress + FFrameTag.GetAddress
    else
      Code.LParam := FFrameTag.GetAddress
  end
  else
    Code.LParam := FReturnAddress;
end;

end.
