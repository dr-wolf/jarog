unit uOpField;

interface

uses
  uOp, uBytecode;

type
  TOpField = class(TOp)
  public
    constructor Create(Field: Integer);
  private
    FField: Integer;
  public
    procedure Serialize(var Code: TBytecode); override;
  end;

implementation

uses uOpCode;

{ TOpField }

constructor TOpField.Create(Field: Integer);
begin
  inherited Create(OID_FIELD);
  FField := Field;
end;

procedure TOpField.Serialize(var Code: TBytecode);
begin
  Code.Op := CompileOp();
  Code.LParam := FField;
end;

end.
