unit uDataAny;

interface

uses
  uData, uDataPool, uExecFile, uTypedStream;

type
  TDataAny = class(TData)
  public
    constructor Create; overload;
    constructor Create(const Data: TData); overload;
  private
    FData: TData;
  public
    function Dup: TData; override;
    function Unwrap: TData; override;
    function IsEqual(const Data: TData): Boolean; override;
    function Serialize(const ExecFile: TExecFile): TTypedStream; override;
    procedure Assign(const Data: TData); override;
    procedure Convert(C: TConverter); override;
    procedure SetGCLevel(Level: TGCLevel; Recursive: Boolean; Forced: Boolean = False); override;
  end;

implementation

uses uOpCode, uDataStub;

{ TDataAny }

constructor TDataAny.Create;
begin
  inherited Create;
  FData := TDataStub.Create;
end;

constructor TDataAny.Create(const Data: TData);
begin
  inherited Create;
  FData := Data;
end;

function TDataAny.Unwrap: TData;
begin
  Result := FData;
end;

function TDataAny.Dup: TData;
begin
  Result := TDataAny.Create(FData);
end;

procedure TDataAny.Convert(C: TConverter);
begin
  C.ConvertAny(FData);
end;

function TDataAny.IsEqual(const Data: TData): Boolean;
begin
  Result := FData.IsEqual(Data);
end;

function TDataAny.Serialize(const ExecFile: TExecFile): TTypedStream;
begin
  Result := TTypedStream.Create;
  with Result do
  begin
    WriteByte(DID_DATA_ANY);
    WriteInteger(ExecFile.Save(FData as TObject, (FData as TData).Serialize));
  end;
end;

procedure TDataAny.SetGCLevel(Level: TGCLevel; Recursive, Forced: Boolean);
begin
  inherited;
  if Recursive then
    FData.SetGCLevel(Level, Recursive, Forced);
end;

procedure TDataAny.Assign(const Data: TData);
begin
  FData := Data.Dup;
end;

end.
