unit uFuncIOPut;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncIOPut = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_TEXT = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Prints a text in console.
    @type func(value: string)
    @in value string # Text to be printed
    * }

implementation

uses uDataString, uOpCode;

// TODO: fix comment
{ TFuncPut }

constructor TFuncIOPut.Create;
begin
  inherited Create([MakeString]);
  FFuncUid := FID_IO_PUT;
end;

procedure TFuncIOPut.Execute(Context: TFuncContext);
begin
  FCoreContext.IOProvider.Write(AsString(Context.Data[P_TEXT]));
end;

end.
