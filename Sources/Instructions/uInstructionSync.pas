unit uInstructionSync;

interface

uses
  uArray, uInstruction, uOp;

type
  TInstructionSync = class(TInstruction)
  public
    constructor Create(SectionId: Integer; Instruction: TInstruction);
    destructor Destroy; override;
  private
    FSectionId: Integer;
    FInstruction: TInstruction;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uOpSync;

{ TInstructionSync }

constructor TInstructionSync.Create(SectionId: Integer; Instruction: TInstruction);
begin
  inherited Create;
  FSectionId := SectionId;
  FInstruction := Instruction;
  FInstruction.SetParent(Self);
end;

destructor TInstructionSync.Destroy;
begin
  FInstruction.Free;
  inherited;
end;

function TInstructionSync.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, [TOpSync.Create(FSectionId, True)]);
  AppendOp(Result, FInstruction.Compile);
  AppendOp(Result, [TOpSync.Create(FSectionId, False)]);
end;

end.
