unit uProcessRepository;

interface

uses
  uArray, uRepository, ChildProcess;

type
  TProcessRepository = class(TRepository)
  public
    constructor Create;
  private type
    TProc = record
      Handle: Integer;
      Proc: TChildProcess;
    end;
  protected
    FProcs: TArray<TProc>;
    function GetHandle(Id: Integer): Integer; override;
    function Size: Integer; override;
    procedure Release(Id: Integer); override;
    function GetProcess(Handle: Integer): TChildProcess;
  public
    property Process[Handle: Integer]: TChildProcess read GetProcess;
    function Add(Proc: TChildProcess): Integer;
  end;

implementation

{ TProcessRepository }

constructor TProcessRepository.Create;
begin
  SetLength(FProcs, 0);
end;

function TProcessRepository.GetHandle(Id: Integer): Integer;
begin
  Result := FProcs[Id].Handle;
end;

function TProcessRepository.GetProcess(Handle: Integer): TChildProcess;
var
  I: Integer;
begin
  I := Find(Handle);
  if I > -1 then
    Result := FProcs[I].Proc
  else
    Result := nil;
end;

procedure TProcessRepository.Release(Id: Integer);
begin
  with FProcs[Id] do
  begin
    Proc.Terminate;
    Proc.Free;
  end;
  if Id < High(FProcs) then
    FProcs[Id] := FProcs[High(FProcs)];
  SetLength(FProcs, Length(FProcs) - 1);
end;

function TProcessRepository.Size: Integer;
begin
  Result := Length(FProcs);
end;

function TProcessRepository.Add(Proc: TChildProcess): Integer;
var
  p: TProc;
begin
  p.Proc := Proc;
  p.Handle := Random(MaxInt);
  SetLength(FProcs, Length(FProcs) + 1);
  FProcs[High(FProcs)] := p;
  Result := p.Handle;
end;

end.
