unit uFuncDateFmt;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncDateFmt = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_UNIX = 0;
    P_FORMAT = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Formats unix timestamp, see date and time [format] in additions.
    @type func(unix: int, format: string -> string)
    @in unix int # Unix timestamp
    @in format string # Date and time format string
    @out - string # Output string
    * }

implementation

uses SysUtils, DateUtils, uDataNumeric, uDataString, uOpCode;

{ TFuncDateFmt }

constructor TFuncDateFmt.Create;
begin
  inherited Create([MakeInt, MakeString], MakeString);
  FFuncUid := FID_DATE_FMT;
end;

procedure TFuncDateFmt.Execute(Context: TFuncContext);
var
  fmt, res: string;
  i: Integer;
  d: TDateTime;
  wd, wdw, wm, wy, wh, wmi, ws, wss: Word;
begin
  d := (AsInt(Context.Data[P_UNIX]) / 86400) + 25569;
  DecodeDateFully(d, wy, wm, wd, wdw);
  DecodeTime(d, wh, wmi, ws, wss);
  fmt := AsString(Context.Data[P_FORMAT]);
  res := '';

  i := 1;
  while i < Length(fmt) do
    if fmt[i] = '%' then
    begin
      Inc(i);
      case fmt[i] of
        '%':
          res := res + '%';
        'a':
          res := res + FormatDateTime('ddd', d);
        'A':
          res := res + FormatDateTime('dddd', d);
        'b':
          res := res + FormatDateTime('mmm', d);
        'B':
          res := res + FormatDateTime('mmmmm', d);
        'c':
          res := res + DateToStr(d) + ' ' + TimeToStr(d);
        'd':
          res := res + Format('%d', [wd]);
        'D':
          res := res + Format('%.*d', [2, wd]);
        'h':
          res := res + Format('%d', [wh]);
        'H':
          res := res + Format('%.*d', [2, wh]);
        'i':
          res := res + Format('%d', [(wh + 11) mod 12 + 1]);
        'I':
          res := res + Format('%.*d', [2, (wh + 11) mod 12 + 1]);
        'j':
          res := res + Format('%d', [DayOfTheYear(d)]);
        'J':
          res := res + Format('%.*d', [3, DayOfTheYear(d)]);
        'm':
          res := res + Format('%d', [wmi]);
        'M':
          res := res + Format('%.*d', [2, wmi]);
        'n':
          res := res + Format('%d', [wm]);
        'N':
          res := res + Format('%.*d', [2, wm]);
        'p':
          if wh >= 12 then
            res := res + 'pm'
          else
            res := res + 'am';
        'P':
          if wh >= 12 then
            res := res + 'PM'
          else
            res := res + 'AM';
        's':
          res := res + Format('%d', [ws]);
        'S':
          res := res + Format('%.*d', [2, ws]);
        'u':
          res := res + IntToStr(WeekOfTheYear(d));
        'w':
          res := res + Format('%d', [wdw - 1]);
        'X':
          res := res + TimeToStr(d);
        'x':
          res := res + DateToStr(d);
        'y':
          res := res + Format('%.*d', [2, wy mod 100]);
        'Y':
          res := res + Format('%.*d', [4, wy]);
      else
        res := res + fmt[i];
      end;
      Inc(i)
    end else begin
      res := res + fmt[i];
      Inc(i);
    end;

  if i = Length(fmt) then
    res := res + fmt[i];

  Return(TDataString.Create(StringOf(TEncoding.Convert(TEncoding.ANSI, TEncoding.UTF8, BytesOf(res)))));
end;

end.
