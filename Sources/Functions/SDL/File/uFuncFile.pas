unit uFuncFile;

interface

uses
  Classes, uStandardFunc, uFuncContext;

type
  TByteSet = set of Byte;

type
  TFuncFile = class(TStandardFunc)
  protected
    function RetrieveStream(const VarId: Integer; Context: TFuncContext): TStream;
    function RetrievePackSize(const VarId: Integer; AcceptedSizes: TByteSet; Context: TFuncContext): Byte;
  end;

implementation

uses uExceptions, uDataNumeric, uDataString;

{ TFuncFile }

function TFuncFile.RetrievePackSize(const VarId: Integer; AcceptedSizes: TByteSet; Context: TFuncContext): Byte;
begin
  Result := AsInt(Context.Data[VarId]);
  if not(Result in AcceptedSizes) then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Invalid packet size!'));
end;

function TFuncFile.RetrieveStream(const VarId: Integer; Context: TFuncContext): TStream;
begin
  Result := FCoreContext.Repos.Files.Stream[AsInt(Context.Data[VarId])];
  if Result = nil then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Invalid file handle!'));
end;

end.
