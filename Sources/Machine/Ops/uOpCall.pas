unit uOpCall;

interface

uses
  uOp;

type
  TOpCall = class(TOp)
  public
    constructor Create;
  end;

implementation

uses uOpCode;

{ TOpCall }

constructor TOpCall.Create;
begin
  inherited Create(OID_CALL);
end;

end.
