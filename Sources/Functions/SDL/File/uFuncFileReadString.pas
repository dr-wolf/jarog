unit uFuncFileReadString;

interface

uses
  uFuncFile, uFuncContext;

type
  TFuncFileReadString = class(TFuncFile)
  public
    constructor Create;
  private const
    P_HANDLE = 0;
    P_SIZE = 1;
    P_ENCODING = 2;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Reads string value from file stream.
    @type func(handle: int, size: int, encoding: int -> string)
    @in handle int # Handle of a stream
    @in size int # Maximun bytes amount to read
    @in encoding int # Text codepage, see enc constant
    @out - string # Returned value
    * }

implementation

uses SysUtils, Classes, uDataNumeric, uDataString, uOpCode;

{ TFuncFileReadString }

constructor TFuncFileReadString.Create;
begin
  inherited Create([MakeInt, MakeInt, MakeInt], MakeString);
  FFuncUid := FID_FILE_READSTRING;
end;

procedure TFuncFileReadString.Execute(Context: TFuncContext);
var
  bytes: TBytes;
  l, cp: Integer;
  s: TStream;
  e: TEncoding;
begin
  s := RetrieveStream(P_HANDLE, Context);
  cp := AsInt(Context.Data[P_ENCODING]);
  l := AsInt(Context.Data[P_SIZE]);
  if l > s.Size - s.Position then
    l := s.Size - s.Position;

  if l > 0 then
    try
      e := TEncoding.GetEncoding(cp);
      SetLength(bytes, l);
      s.ReadBuffer(Pointer(bytes)^, l);
      Return(TDataString.Create(StringOf(TEncoding.Convert(e, TEncoding.UTF8, bytes))));
    finally
      e.Free;
    end
  else
    Return(TDataString.Create(''));
end;

end.
