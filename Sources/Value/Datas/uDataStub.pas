unit uDataStub;

interface

uses
  uOperations, uData, uExecFile, uTypedStream;

type
  TDataStub = class(TData)
  public
    function Apply(Op: TBinaryOp; const Data: TData): TData; override;
    function Dup: TData; override;
    function IsEqual(const Data: TData): Boolean; override;
    function Serialize(const ExecFile: TExecFile): TTypedStream; override;
    procedure Assign(const Data: TData); override;
    procedure Convert(C: TConverter); override;
  end;

implementation

uses uExceptions, uOpCode;

{ TDataStub }

procedure TDataStub.Assign(const Data: TData);
begin

end;

function TDataStub.Apply(Op: TBinaryOp; const Data: TData): TData;
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

function TDataStub.Dup: TData;
begin
  Result := TDataStub.Create;
end;

procedure TDataStub.Convert(C: TConverter);
begin
  C.ConvertStub();
end;

function TDataStub.IsEqual(const Data: TData): Boolean;
begin
  Result := Data is TDataStub;
end;

function TDataStub.Serialize(const ExecFile: TExecFile): TTypedStream;
begin
  Result := TTypedStream.Create;
  Result.WriteByte(DID_DATA_STUB);
end;

end.
