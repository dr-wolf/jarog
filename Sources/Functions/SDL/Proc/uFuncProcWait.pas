unit uFuncProcWait;

interface

uses
  uFuncProc, uFuncContext;

type
  TFuncProcWait = class(TFuncProc)
  public
    constructor Create;
  private const
    P_PROC = 0;
    P_TIMEOUT = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Waits till process end or timeout.
    @type func(proc: int, timeout: int -> int)
    @in proc int # Proc handle
    @in timeout int # Timeout in ms
    @out - int # 0 if process is terminated or 1 if not
    * }

implementation

uses ChildProcess, uDataNumeric, uOpCode;

{ TFuncProcWait }

constructor TFuncProcWait.Create;
begin
  inherited Create([MakeInt, MakeInt], MakeInt);
  FFuncUid := FID_PROC_WAIT;
end;

procedure TFuncProcWait.Execute(Context: TFuncContext);
var
  p: TChildProcess;
begin
  p := RetrieveProcess(P_PROC, Context);
  Return(TDataNumeric.Create(p.WaitFor(AsInt(Context.Data[P_TIMEOUT]))));
end;

end.
