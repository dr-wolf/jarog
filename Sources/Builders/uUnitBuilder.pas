unit uUnitBuilder;

interface

uses
  uBuilder, uValue, uUnit, uUnitLibrary, uSourceParser, uResolver, uAppMold;

type
  TUnitBuilder = class(TBuilder)
  public
    constructor Create(const FileName: string; UnitLibrary: TUnitLibrary; AppMold: TAppMold);
    destructor Destroy; override;
  private
    FTokenizer: TTokenizer;
    FUnitLibrary: TUnitLibrary;
    FUnit: TUnit;
    FLinesCount: Integer;
    procedure ProcessUnit(Tokens: TTokenStream);
    procedure ProcessType(Tokens: TTokenStream);
  protected
    function MakeResolver: IResolver; override;
    function SaveValue(Name: string; Value: IValue; Section: TValueSection): Integer; override;
  public
    property Application: TUnit read FUnit;
    property LinesCount: Integer read FLinesCount;
  end;

implementation

uses SysUtils, Classes, uExceptions, uParserLogic, uTypeBuilder;

{ TUnitBuilder }

constructor TUnitBuilder.Create(const FileName: string; UnitLibrary: TUnitLibrary; AppMold: TAppMold);
var
  src: TStrings;
  Tokens: TTokenStream;
  t: TToken;
begin
  inherited Create(AppMold);
  FTokenizer := TTokenizer.Create;
  FillRuleset(FTokenizer);
  FUnitLibrary := UnitLibrary;
  FUnit := TUnit.Create(FileName);

  try
    Tokens := TTokenStream.Create;
    try
      src := TStringList.Create;
      with src do
      begin
        LoadFromFile(FileName);
        FLinesCount := Count;
        FTokenizer.Process(src, Tokens);
        Free;
      end;

      { -- parsing starts here -- }
      while not Tokens.Empty do
      begin
        t := Tokens.Token();
        if t.Kind = tkKeyword then
          case ToKword(t.Value) of
            kwLoad:
              ProcessUnit(Tokens);
            kwType:
              ProcessType(Tokens);
            kwConst:
              ProcessConst(Tokens);
            kwVar:
              ProcessVariable(Tokens);
          else
            raise ECodeException.Create(E_UNEXPECTED_KEYWORD, t.Value, t.Index);
          end
        else
          raise ECodeException.Create(E_SYNTAX_ERROR, t.Index);
      end;
    finally
      Tokens.Free;
    end;
  except
    on e: ECodeException do
      raise ECodeException.Create(e, FileName, 0);
  end;
end;

destructor TUnitBuilder.Destroy;
begin
  FTokenizer.Free;
  inherited;
end;

function TUnitBuilder.MakeResolver: IResolver;
begin
  Result := TResolver.Create(FUnit.GetType, FUnit.GetValue, nil);
end;

procedure TUnitBuilder.ProcessType(Tokens: TTokenStream);
var
  Index: Integer;
  identifier: TToken;
begin
  Tokens.Test(kwType, E_SYNTAX_ERROR); // Guard, never fails
  identifier := Tokens.Fetch(tkIdentifier);
  Tokens.Test(sColon, E_SYNTAX_ERROR);
  index := Tokens.Token().Index;
  if FRegistry.Add(identifier.Value) then
    try
      FUnit.AddType(identifier.Value, BuildType(Tokens, TResolver.Create(FUnit.GetType, FUnit.GetValue, nil)));
    except
      on e: ECodeException do
        raise ECodeException.Create(e, '', index);
    end
  else
    raise ECodeException.Create(E_IDENTIFIER_REDECLARED, identifier.Value, identifier.Index);
  Tokens.Test(sSemicolon, E_MISSING_SEMICOLON);
end;

procedure TUnitBuilder.ProcessUnit(Tokens: TTokenStream);
var
  script: string;
  identifier: TToken;
begin
  Tokens.Test(kwLoad, E_SYNTAX_ERROR); // Guard, never fails
  script := Tokens.Fetch(tkString).Value;
  Tokens.Test(kwAs, E_SYNTAX_ERROR);
  identifier := Tokens.Fetch(tkIdentifier);
  Tokens.Test(sSemicolon, E_MISSING_SEMICOLON);
  if FRegistry.Add(identifier.Value) then
  begin
    try
      FUnit.AddUnit(identifier.Value, FUnitLibrary.Load(script + '.jr', FAppMold, ExtractFilePath(FUnit.FileName)));
    except
      on e: ECodeException do
        raise ECodeException.Create(e, '', identifier.Index);
    end;
  end
  else
    raise ECodeException.Create(E_IDENTIFIER_REDECLARED, identifier.Value, identifier.Index);
end;

function TUnitBuilder.SaveValue(Name: string; Value: IValue; Section: TValueSection): Integer;
begin
  FUnit.AddValue(Name, Value);
  Result := -1;
end;

end.
