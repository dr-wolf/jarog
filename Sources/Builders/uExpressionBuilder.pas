unit uExpressionBuilder;

interface

uses
  uResolver, uFuncContext, uExpression, uSourceParser, uAppMold;

function BuildExpression(Tokens: TTokenStream; Resolver: IResolver; AppMold: TAppMold; Context: TFuncContext)
  : TExpression;

implementation

uses uArray, uStringSet, uIdentifierRegistry, uTypeFactory, uTypeBuilder, uType, uTypeInt, uOperations,
  uExceptions, uFuncBuilder, uExpressionFunc, uExpressionList, uExpressionListItem, uExpressionListRange,
  uExpressionUnary, uExpressionCall, uExpressionCallNamed, uExpressionCast, uExpressionSet, uExpressionStruct,
  uExpressionStructField, uExpressionTest, uExpressionPipeTail, uExpressionValue, uExpressionVariable, uDataBool,
  uDataNumeric, uDataSet, uDataString, uDataArray, uValue, uTreeAssembler;

function BuildExpression(Tokens: TTokenStream; Resolver: IResolver; AppMold: TAppMold; Context: TFuncContext)
  : TExpression;

function BuildTree(Tokens: TTokenStream): TExpression; forward;

  procedure ParseCollection(Tokens: TTokenStream; var Names: TStringSet; var Expressions: TArray<TExpression>);
  var
    t: TToken;
  begin
    SetLength(Expressions, 0);
    repeat
      if (Tokens.Token.Kind = tkIdentifier) and Eq(Tokens.Token(1), sColon) then
      begin
        t := Tokens.Fetch(tkIdentifier);
        if Names.IndexOf(t.Value) >= 0 then
          raise ECodeException.Create(E_IDENTIFIER_REDECLARED, t.Value, t.Index);
        Names.Add(t.Value);
        Tokens.Test(sColon, E_SYNTAX_ERROR);
      end;
      SetLength(Expressions, Length(Expressions) + 1);
      Expressions[High(Expressions)] := BuildTree(Tokens);
      if not Eq(Tokens.Token, sComma) then
        Break;
      Tokens.Skip;
    until False;
  end;

  function BuildValue(Tokens: TTokenStream): TExpression;
  var
    t: TToken;
    v: IValue;
    f: Boolean;
  begin
    t := Tokens.Pop;
    case t.Kind of
      tkIdentifier:
        if Eq(Tokens.Token, sNameSp) then
        begin
          Tokens.Skip;
          if Tokens.Token.Kind <> tkIdentifier then
            raise ECodeException.Create(E_EXPECTED_IDENTIFIER, Tokens.Token.Value, Tokens.Token.Index);
          v := Resolver.ResolveValue(f, Tokens.Pop.Value, t.Value);
        end else if (t.Value = RI_TRUE) or (t.Value = RI_FALSE) then
          v := TValue.Create(TypeFactory.GetBool, TDataBool.Create(t.Value))
        else
        begin
          v := Resolver.ResolveValue(f, t.Value);
          if (Context <> nil) and not v.Immutable then
          begin
            Result := TExpressionVariable.Create(AppMold.DataSection, Context.Keep(v.Data, f, t.Value = RI_SELF),
              v.TypeInfo);
            Exit;
          end;
        end;
      tkInteger:
        v := TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(t.Value));
      tkFloat:
        v := TValue.Create(TypeFactory.GetFloat, TDataNumeric.Create(t.Value));
      tkString:
        v := TValue.Create(TypeFactory.GetString, TDataString.Create(t.Value));
    else
      raise ECodeException.Create(E_SYNTAX_ERROR, t.Index);
    end;
    Result := TExpressionValue.Create(AppMold.DataSection, v);
  end;

  function BuildItems(Tokens: TTokenStream; var t: TType): TArray<TExpression>;
  var
    e: TExpression;
  begin
    SetLength(Result, 0);
    t := nil;
    repeat
      e := BuildTree(Tokens);
      if t = nil then
        t := e.ResultType
      else if not t.Compatible(e.ResultType, True) then
        try
          t := t.Apply(boAddition, e.ResultType)
        except
          t := TypeFactory.GetAny;
        end;
      SetLength(Result, Length(Result) + 1);
      Result[High(Result)] := e;
      if not Eq(Tokens.Token, sComma) then
        Break
      else
        Tokens.Skip;
    until False;
  end;

  function BuildList(Tokens: TTokenStream): TExpression;
  var
    e: TArray<TExpression>;
    t: TType;
  begin
    Tokens.Test(sListBgn, E_SYNTAX_ERROR);
    if Eq(Tokens.Token, sListEnd) then
      Result := TExpressionValue.Create(AppMold.DataSection, TValue.Create(TypeFactory.GetList, TDataArray.Create(nil)))
    else
    begin
      e := BuildItems(Tokens, t);
      Result := TExpressionList.Create(AppMold.DataSection, e, t);
    end;
    Tokens.Test(sListEnd, E_SYNTAX_ERROR);
  end;

  function BuildSet(Tokens: TTokenStream): TExpression;
  var
    e: TArray<TExpression>;
    t: TType;
  begin
    Tokens.Test(sSetBgn, E_SYNTAX_ERROR);
    if Eq(Tokens.Token, sSetEnd) then
      Result := TExpressionValue.Create(AppMold.DataSection, TValue.Create(TypeFactory.GetSet, TDataSet.Create))
    else
    begin
      e := BuildItems(Tokens, t);
      Result := TExpressionSet.Create(AppMold.DataSection, e, t);
    end;
    Tokens.Test(sSetEnd, E_SYNTAX_ERROR);
  end;

  function BuildFunc(Tokens: TTokenStream): TExpressionFunc;
  var
    p, i: Integer;
    c: TArray<Integer>;
  begin
    p := Tokens.Token.Index;
    with TFuncBuilder.Create(Resolver, AppMold, Tokens) do
    begin
      if (ClosureNames.Count > 0) and (Context = nil) then
        raise ECodeException.Create(E_EXPECTED_STATIC, p);
      SetLength(c, ClosureNames.Count);
      for i := 0 to High(c) do
        c[i] := Context.Keep(Closure[ClosureNames[i]], True);
      Result := TExpressionFunc.Create(AppMold.DataSection, Func, FuncType, c);
      Free;
    end;
  end;

  function BuildStruct(Tokens: TTokenStream): TExpressionStruct;
  var
    e: TArray<TExpression>;
    s: TStringSet;
  begin
    s := TStringSet.Create;
    try
      Tokens.Test(sStructBgn, E_SYNTAX_ERROR);
      ParseCollection(Tokens, s, e);
      Result := TExpressionStruct.Create(AppMold.DataSection, e, s.AsArray);
    finally
      s.Free;
    end;
    Tokens.Test(sStructEnd, E_SYNTAX_ERROR);
  end;

  function BuildNode(Tokens: TTokenStream): TExpression;

    function BuildListIndex(Expression: TExpression; Tokens: TTokenStream): TExpression;
    var
      f: TExpression;
    begin
      Tokens.Test(sListBgn, E_SYNTAX_ERROR);
      f := BuildExpression(Tokens, Resolver, AppMold, Context);
      if f.ResultType is TTypeInt then
        Result := TExpressionListItem.Create(AppMold.DataSection, Expression, f)
      else
        Result := TExpressionListRange.Create(AppMold.DataSection, Expression, f);
      Tokens.Test(sListEnd, E_SYNTAX_ERROR);
    end;

    function BuildFuncCall(Expression: TExpression; Tokens: TTokenStream): TExpression;
    var
      a: TArray<TExpression>;
      p: TExpression;
      n: TStringSet;
    begin
      Tokens.Test(sBrOp, E_SYNTAX_ERROR);
      if not Eq(Tokens.Token, sBrCl) then
      begin
        n := TStringSet.Create;
        try
          ParseCollection(Tokens, n, a);
          if n.Count = 0 then
          begin
            p := TExpressionStruct.Create(AppMold.DataSection, a, n.AsArray);
            if p.Group = egStatic then
            begin
              Result := TExpressionCall.Create(AppMold.DataSection, Expression,
                TExpressionValue.Create(AppMold.DataSection, p.Eval));
              p.Free;
            end
            else
              Result := TExpressionCall.Create(AppMold.DataSection, Expression, p);
          end
          else
            Result := TExpressionCallNamed.Create(AppMold.DataSection, Expression, a, n);
        finally
          n.Free;
        end;
      end else begin
        if (Expression is TExpressionFunc) and TExpressionFunc(Expression).HasNoArgs then
          Result := TExpressionCall.Create(AppMold.DataSection, Expression,
            TExpressionStruct.Create(AppMold.DataSection, [], []))
        else
          Result := TExpressionCallNamed.Create(AppMold.DataSection, Expression, [], nil);
      end;
      Tokens.Test(sBrCl, E_SYNTAX_ERROR);
    end;

  var
    o: TUnaryOp;
  begin
    if Tokens.Token.Kind = tkKeyword then
      raise ECodeException.Create(E_UNEXPECTED_KEYWORD, Tokens.Token.Value, Tokens.Token.Index);

    if Tokens.Token.Kind = tkSymbol then
      case ToSmbl(Tokens.Token.Value) of
        sMinus, sPlus, sExcl, sAt, sTilde:
          begin
            o := UnaryOp(ToSmbl(Tokens.Pop.Value));
            Result := TExpressionUnary.Create(AppMold.DataSection, BuildNode(Tokens), o);
          end;
        sListBgn:
          Result := BuildList(Tokens);
        sFunc:
          Result := BuildFunc(Tokens);
        sSetBgn:
          Result := BuildSet(Tokens);
        sStructBgn:
          Result := BuildStruct(Tokens);
        sBrOp:
          begin
            Tokens.Test(sBrOp, E_SYNTAX_ERROR);
            Result := BuildExpression(Tokens, Resolver, AppMold, Context);
            Tokens.Test(sBrCl, E_SYNTAX_ERROR);
          end;
      else
        raise ECodeException.Create(E_SYNTAX_ERROR, Tokens.Token.Index);
      end
    else
      Result := BuildValue(Tokens);

    while Tokens.Token.Kind = tkSymbol do
      case ToSmbl(Tokens.Token.Value) of
        sDot:
          begin
            Tokens.Test(sDot, E_SYNTAX_ERROR);
            Result := TExpressionStructField.Create(AppMold.DataSection, Result, Tokens.Fetch(tkIdentifier).Value);
          end;
        sListBgn:
          Result := BuildListIndex(Result, Tokens);
        sBrOp:
          Result := BuildFuncCall(Result, Tokens);
        sExcl:
          begin
            Tokens.Test(sExcl, E_SYNTAX_ERROR);
            Result := TExpressionCast.Create(AppMold.DataSection, Result, BuildType(Tokens, Resolver));
          end;
        sQuest:
          begin
            Tokens.Test(sQuest, E_SYNTAX_ERROR);
            Result := TExpressionTest.Create(AppMold.DataSection, Result, BuildType(Tokens, Resolver));
          end;
        sTilde:
          begin
            Tokens.Test(sTilde, E_SYNTAX_ERROR);
            Result := TExpressionPipeTail.Create(AppMold.DataSection, Result);
          end;
      else
        Break;
      end;
  end;

  function BuildTree(Tokens: TTokenStream): TExpression;
  var
    t: TToken;
    ta: ITreeAssembler;
  begin
    ta := MakeAssembler(BuildNode(Tokens));
    repeat
      t := Tokens.Token;
      if (t.Kind <> tkSymbol) or not(ToSmbl(t.Value) in OPERATIONS) then
        Break;
      ta := ta.Attach(BuildNode(Tokens.Skip), BinaryOp(ToSmbl(t.Value)));
    until False;
    Result := ta.Assemble(AppMold.DataSection);
  end;

var
  ind: Integer;
  tree: TExpression;

begin
  ind := Tokens.Token.Index;

  try
    tree := BuildTree(Tokens);
  except
    on e: ECodeException do
      raise ECodeException.Create(e, '', ind);
  end;

  if (tree.Group = egStatic) and not(tree is TExpressionValue) then
  begin
    Result := TExpressionValue.Create(AppMold.DataSection, tree.Eval);
    tree.Free;
  end
  else
    Result := tree;
end;

end.
