unit uFuncFSDelete;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncFSDelete = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_PATH = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Deletes file from filesystem.
    @type func(path: string)
    @in path string # Path to file to be deleted
    * }

implementation

uses SysUtils, uDataString, uOpCode;

{ TFuncFSDelete }

constructor TFuncFSDelete.Create;
begin
  inherited Create([MakeString]);
  FFuncUid := FID_FS_DELETE;
end;

procedure TFuncFSDelete.Execute(Context: TFuncContext);
begin
  if not DeleteFile(ExpandFileName(AsString(Context.Data[P_PATH]))) then
    RaiseLastOSError;
end;

end.
