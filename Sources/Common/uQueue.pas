unit uQueue;

interface

uses
  uThreadSafe;

type
  IQueue<T> = interface(IInterface)
    function Count: Integer;
    function Pop: T;
    procedure Push(Value: T);
    procedure Clear;
    procedure Lock;
    procedure Unlock;
  end;

type
  TQueue<T> = class(TThreadInterfacedObject, IQueue<T>)
  public
    constructor Create;
    destructor Destroy; override;
  private type
    PQueueItem = ^TQueueItem;

    TQueueItem = record
      Item: T;
      Next: PQueueItem;
    end;
  private
    FHead: PQueueItem;
    FTail: PQueueItem;
    FLength: Integer;
  public
    function Count: Integer;
    function Pop: T;
    procedure Push(Value: T);
    procedure Clear;
  end;

implementation

uses SysUtils;

{ TQueue<T> }

constructor TQueue<T>.Create;
begin
  inherited Create;
  FHead := nil;
  FTail := nil;
end;

destructor TQueue<T>.Destroy;
begin
  Clear;
  inherited;
end;

function TQueue<T>.Count: Integer;
begin
  Result := FLength;
end;

procedure TQueue<T>.Clear;
begin
  while Count > 0 do
    Pop;
end;

function TQueue<T>.Pop: T;
var
  tmp: PQueueItem;
begin
  if FHead = nil then
    raise Exception.Create('Queue is empty');
  Result := FHead.Item;
  tmp := FHead.Next;
  FHead.Item := Default (T);
  FreeMem(FHead);
  FHead := tmp;
  if FHead = nil then
    FTail := nil;
  Dec(FLength);
end;

procedure TQueue<T>.Push(Value: T);
var
  tmp: PQueueItem;
begin
  tmp := AllocMem(SizeOf(TQueueItem));
  tmp.Item := Value;
  tmp.Next := nil;
  if FHead = nil then
    FHead := tmp;
  if FTail <> nil then
    FTail.Next := tmp;
  FTail := tmp;
  Inc(FLength);
end;

end.
