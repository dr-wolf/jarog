unit uUnitMath;

interface

uses
  uUnit;

type
  TUnitMath = class(TUnit)
  public
    constructor Create;
  end;

  { *
    @descr Math fuctions and constants
    @const pi float # Pi value
    @const e float # Exponent
    * }

implementation

uses uValue, uTypeFactory, uDataNumeric, uFuncMathAbs, uFuncMathArcCos, uFuncMathArcSin, uFuncMathArcTan, uFuncMathCos,
  uFuncMathFrac, uFuncMathFloor, uFuncMathLog, uFuncMathRand, uFuncMathSin, uFuncMathTan;

{ TUnitMath }

constructor TUnitMath.Create;
begin
  inherited Create('<math>');

  AddValue('pi', TValue.Create(TypeFactory.GetFloat, TDataNumericPi.Create));
  AddValue('e', TValue.Create(TypeFactory.GetFloat, TDataNumericE.Create));

  AddValue('abs', MakeFunc(TFuncMathAbs.Create, [TypeFactory.GetFloat], ['value'], TypeFactory.GetFloat));
  AddValue('arccos', MakeFunc(TFuncMathArcCos.Create, [TypeFactory.GetFloat], ['value'], TypeFactory.GetFloat));
  AddValue('arcsin', MakeFunc(TFuncMathArcSin.Create, [TypeFactory.GetFloat], ['value'], TypeFactory.GetFloat));
  AddValue('arctan', MakeFunc(TFuncMathArcTan.Create, [TypeFactory.GetFloat], ['value'], TypeFactory.GetFloat));
  AddValue('cos', MakeFunc(TFuncMathCos.Create, [TypeFactory.GetFloat], ['value'], TypeFactory.GetFloat));
  AddValue('frac', MakeFunc(TFuncMathFrac.Create, [TypeFactory.GetFloat], ['value'], TypeFactory.GetFloat));
  AddValue('floor', MakeFunc(TFuncMathFloor.Create, [TypeFactory.GetFloat], ['value'], TypeFactory.GetFloat));
  AddValue('log', MakeFunc(TFuncMathLog.Create, [TypeFactory.GetFloat, TypeFactory.GetFloat], ['base', 'value'],
    TypeFactory.GetFloat));
  AddValue('rand', MakeFunc(TFuncMathRand.Create, [], [], TypeFactory.GetFloat));
  AddValue('sin', MakeFunc(TFuncMathSin.Create, [TypeFactory.GetFloat], ['value'], TypeFactory.GetFloat));
  AddValue('tan', MakeFunc(TFuncMathTan.Create, [TypeFactory.GetFloat], ['value'], TypeFactory.GetFloat));
end;

end.
