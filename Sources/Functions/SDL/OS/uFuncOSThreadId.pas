unit uFuncOSThreadId;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncOSThreadId = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Returns current thread ID.
    @type func(-> int)
    @out - int # Thread ID
    * }

implementation

uses uDataNumeric, uOpCode;

{ TFuncOSThreadId }

constructor TFuncOSThreadId.Create;
begin
  inherited Create([], MakeInt);
  FFuncUid := FID_OS_THREADID;
end;

procedure TFuncOSThreadId.Execute(Context: TFuncContext);
begin
  Return(TDataNumeric.Create(Context.ThreadId));
end;

end.
