unit uArray;

interface

type
  TArray<T> = array of T;

  TArrayType = (atStruct, atArray, atSet);

implementation

end.
