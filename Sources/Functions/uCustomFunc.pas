unit uCustomFunc;

interface

uses
  uArray, uFunction, uCoreContext, uFuncContext, uClosure, uExecFile, uTypedStream, uState, uData;

type
  TCustomFunc = class(TFunction)
  public
    constructor Create(Context: TFuncContext; Closures, Params: array of Integer; Result: TData);
    destructor Destroy; override;
  private
    FOpAddress: Integer;
    FResult: TData;
  public
    property Context: TFuncContext read FContext;
    function Log: string; override;
    function Serialize(const ExecFile: TExecFile): TTypedStream; override;
    procedure Call(State: TState; Caller: TData; Closures: TArray<IClosure>); override;
    procedure AttachContext(CoreContext: TCoreContext); override;
    procedure SetAddress(OpAddress: Integer);
  end;

implementation

uses SysUtils, uDataStack, uOpCode, uExceptions;

{ TCustomFunc }

constructor TCustomFunc.Create(Context: TFuncContext; Closures, Params: array of Integer; Result: TData);
begin
  inherited Create(Context, Closures, Params);
  FFuncUid := FID_CUSTOM;
  FOpAddress := -1;
  FResult := Result;
end;

destructor TCustomFunc.Destroy;
begin
  if FResult <> nil then
    FResult.Dispose();
  inherited;
end;

function TCustomFunc.Log: string;
begin
  Result := 'at <0x' + IntToHex(FOpAddress, 8) + '>';
end;

procedure TCustomFunc.AttachContext(CoreContext: TCoreContext);
begin
  // no core context in custom functions
end;

procedure TCustomFunc.Call(State: TState; Caller: TData; Closures: TArray<IClosure>);
var
  c: TFuncContext;
  i: Integer;
begin
  c := FContext.Recreate(Caller);
  LoadParams(c, State);
  if Length(Closures) <> Length(FClosures) then
    raise ERuntimeException.Create(E_UNEXPECTED, nil); // guard, never fails
  for i := 0 to High(Closures) do
    c.Replace(FClosures[i], Closures[i].GetData);

  State.Call(FOpAddress, c);

  if FOpAddress < 0 then
  begin
    State.Return;
    if FResult <> nil then
      State.Stack.Push(FResult.Dup.Dispose);
  end;
end;

function TCustomFunc.Serialize(const ExecFile: TExecFile): TTypedStream;

  procedure WriteArray(Stream: TTypedStream; a: TArray<Integer>);
  var
    i: Integer;
  begin
    Stream.WriteInteger(Length(a));
    for i := 0 to High(a) do
      Stream.WriteInteger(a[i]);
  end;

begin
  Result := inherited Serialize(ExecFile);
  with Result do
  begin
    WriteInteger(FOpAddress);
    WriteArray(Result, FParameters);
    WriteArray(Result, FClosures);
    if FResult <> nil then
      WriteInteger(ExecFile.Save(FResult as TObject, FResult.Serialize))
    else
      WriteInteger(-1);
    FContext.Save(Result, ExecFile);
  end;
end;

procedure TCustomFunc.SetAddress(OpAddress: Integer);
begin
  FOpAddress := OpAddress;
end;

end.
