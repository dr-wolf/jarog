unit uFuncOSTime;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncOSTime = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

    { *
    @descr Returns current unix timestamp.
    @type func(-> int)
    @out - int # Unix timestamp
    * }

implementation

uses SysUtils, uDataNumeric, uOpCode;

{ TFuncOSTime }

constructor TFuncOSTime.Create;
begin
  inherited Create([], MakeInt);
  FFuncUid := FID_OS_TIME;
end;

procedure TFuncOSTime.Execute(Context: TFuncContext);
begin
  Return(TDataNumeric.Create(Trunc((Now - 25569) * 86400)));
end;

end.
