unit uFuncFileReadFloat;

interface

uses
  uFuncFile, uFuncContext;

type
  TFuncFileReadFloat = class(TFuncFile)
  public
    constructor Create;
  private const
    P_HANDLE = 0;
    P_SIZE = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Reads float value from file stream.
    @type func(handle: int, size: int -> float)
    @in handle int # Handle of a stream
    @in size int # Precision, can be 4 or 8 bytes, see sz_float and sz_double constants
    @out - float # Returned value
    * }

implementation

uses Classes, uDataNumeric, uBigFloat, uOpCode;

{ TFuncFileReadFloat }

constructor TFuncFileReadFloat.Create;
begin
  inherited Create([MakeInt, MakeInt], MakeFloat);
  FFuncUid := FID_FILE_READFLOAT;
end;

procedure TFuncFileReadFloat.Execute(Context: TFuncContext);
var
  s: TStream;
  l: Byte;
  vs: Single;
  vd: Double;
begin
  s := RetrieveStream(P_HANDLE, Context);
  l := RetrievePackSize(P_SIZE, [4, 8], Context);
  if l = 4 then
  begin
    vs := 0;
    s.ReadBuffer(vs, SizeOf(vs));
    Return(TDataNumeric.Create(TBigFloat.Create(vs)));
  end else begin
    vd := 0;
    s.ReadBuffer(vd, SizeOf(vd));
    Return(TDataNumeric.Create(TBigFloat.Create(vd)));
  end;
end;

end.
