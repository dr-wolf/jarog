unit uDebuggerConst;

interface

const
  DBG_GET_CODE = $11;
  DBG_GET_STATE = $12;
  DBG_GET_LOGS = $13;

  DBG_TOGGLE_BP = $21;

  DBG_RUN = $31;
  DBG_STEP = $32;
  DBG_STEPOVER = $33;

  DBG_DETACH = 0;



implementation

end.
