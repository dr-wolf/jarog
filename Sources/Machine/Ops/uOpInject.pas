unit uOpInject;

interface

uses
  uOp;

type
  TOpInject = class(TOp)
  public
    constructor Create;
  end;

implementation

uses uOpCode;

{ TOpInject }

constructor TOpInject.Create;
begin
  inherited Create(OID_INJECT);
end;

end.
