unit uFuncMathCos;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncMathCos = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_VALUE = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Retruns cosinus of the angle.
    @type func(value: float -> float)
    @in value float # Angle given in radians
    @out - float # Cosinus
    * }

implementation

uses uDataNumeric, uOpCode, uBigFloat, mp_types, mp_real;

{ TFuncMathCos }

constructor TFuncMathCos.Create;
begin
  inherited Create([MakeFloat], MakeFloat);
  FFuncUid := FID_MATH_COS;
end;

procedure TFuncMathCos.Execute(Context: TFuncContext);
var
  v, r: mp_float;
begin
  mpf_init(r);
  v := AsNumeric(Context.Data[P_VALUE]).ToFloat;
  mpf_cos(v, r);
  mpf_clear(v);
  Return(TDataNumeric.Create(TBigFloat.Create(r)));
end;

end.
