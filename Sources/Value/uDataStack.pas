unit uDataStack;

interface

uses
  uData, uStack, uTypedStream;

type
  TFrameType = (ftFunction, ftCatch, ftFinally, ftFinallyReturn, ftLoop, ftException);

type
  TDataStack = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private type
    TFrame = record
      Address: Integer;
      Depth: Integer;
      Frame: TFrameType;
    end;
  private
    FStack: TStack<TData>;
    FFrames: TStack<TFrame>;
    FCallDepth: Integer;
    function GetFrame: TFrameType;
    function GetAddress: Integer;
  public
    property Frame: TFrameType read GetFrame;
    property Address: Integer read GetAddress;
    property CallDepth: Integer read FCallDepth;
    function IsEmpty: Boolean;
    function Pop: TData;
    procedure Push(Data: TData);
    procedure DropFrame;
    procedure PushFrame(Frame: TFrameType; Addres: Integer = -1);
    procedure Dump(Stream: TTypedStream);
  end;

function FrameName(Frame: TFrameType): string;

implementation

uses SysUtils, uArray, uStringConverter, uDataPool;

{ TDataStack }

function FrameName(Frame: TFrameType): string;
const
  FRAMES: array [TFrameType] of string = ('f', 'c', 'fi', 'fr', 'l', 'ex');
begin
  Result := FRAMES[Frame];
end;

constructor TDataStack.Create;
begin
  FStack := TStack<TData>.Create;
  FFrames := TStack<TFrame>.Create;
end;

destructor TDataStack.Destroy;
begin
  FFrames.Free;
  FStack.Free;
  inherited;
end;

procedure TDataStack.DropFrame;
var
  i: Integer;
  f: TFrame;
  Items: TArray<TData>;
begin
  f := FFrames.Pop;
  Items := FStack.Take(FStack.Depth - f.Depth);
  for i := 0 to High(Items) do
    Items[i].SetGCLevel(lDisposable, True);
  if f.Frame = ftFunction then
    Dec(FCallDepth);
end;

function TDataStack.GetAddress: Integer;
begin
  if FFrames.Depth = 0 then
    Result := -1
  else
    Result := FFrames.Peek.Address;
end;

function TDataStack.GetFrame: TFrameType;
begin
  Result := FFrames.Peek.Frame;
end;

function TDataStack.IsEmpty: Boolean;
begin
  if FFrames.Depth > 0 then
    Result := FStack.Depth <= FFrames.Peek.Depth
  else
    Result := FStack.Depth = 0;
end;

function TDataStack.Pop: TData;
begin
  // if IsEmpty then
  // raise Exception.Create('Stack frame is empty!');
  Result := FStack.Pop;
  Result.SetGCLevel(lDisposable, True);
end;

procedure TDataStack.Push(Data: TData);
begin
  Data.SetGCLevel(lPreDisposable, True);
  FStack.Push(Data);
end;

procedure TDataStack.PushFrame(Frame: TFrameType; Addres: Integer = -1);
var
  f: TFrame;
begin
  f.Address := Addres;
  f.Frame := Frame;
  f.Depth := FStack.Depth;
  FFrames.Push(f);
  if f.Frame = ftFunction then
    Inc(FCallDepth);
end;

procedure TDataStack.Dump(Stream: TTypedStream);
var
  i, d: Integer;
begin
  d := 0;
  Stream.WriteInteger(FStack.Depth + FFrames.Depth);
  for i := 0 to FStack.Depth - 1 do
  begin
    if (FFrames.Depth > 0) and (FStack.Depth - i = FFrames.Peek(d).Depth) then
    begin
      Stream.WriteString('> ' + FrameName(FFrames.Peek(d).Frame) + ': 0x' + IntToHex(FFrames.Peek(d).Address, 8));
      Inc(d);
    end;
    Stream.WriteString(LogData(FStack.Peek(i)));
  end;
  for i := d to FFrames.Depth - 1 do
    Stream.WriteString('> ' + FrameName(FFrames.Peek(i).Frame) + ': 0x' + IntToHex(FFrames.Peek(i).Address, 8));
end;

end.
