unit uFuncOSSleep;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncOSSleep = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_TIMEOUT = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Stops program execution for given timeout.
    @type func(timeout: int)
    @in timeout int # Milliseconds to wait
    * }

implementation

uses SyncObjs, SysUtils, uDataNumeric, uOpCode;

{ TFuncOSSleep }

constructor TFuncOSSleep.Create;
begin
  inherited Create([MakeInt]);
  FFuncUid := FID_OS_SLEEP;
end;

procedure TFuncOSSleep.Execute(Context: TFuncContext);
var
  Timeout: Integer;
begin
  Timeout := AsInt(Context.Data[P_TIMEOUT]);
  Context.Unlock;
  Sleep(Timeout);
end;

end.
