unit uInstructionTryFinally;

interface

uses
  uArray, uInstruction, uOp;

type
  TInstructionTryFinally = class(TInstruction)
  public
    constructor Create(InstrTry, InstrFin: TInstruction);
    destructor Destroy; override;
  private
    FInstruction: TInstruction;
    FFinally: TInstruction;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uDataStack, uOpThrow, uOpReturn, uOpPush, uOpPop;

{ TInstructionTryFinally }

constructor TInstructionTryFinally.Create(InstrTry, InstrFin: TInstruction);
begin
  inherited Create;
  FInstruction := InstrTry;
  FInstruction.SetParent(Self);
  FFinally := InstrFin;
  FFinally.SetParent(Self);
  InitTag(TAG_FINALLY);
end;

destructor TInstructionTryFinally.Destroy;
begin
  FInstruction.Free;
  FFinally.Free;
  inherited;
end;

function TInstructionTryFinally.Compile: TArray<TOp>;
begin
  FFinally.SetParent(FParent);

  SetLength(Result, 0);
  AppendOp(Result, [TOpPush.Create(ftFinally, GetTag(TAG_FINALLY))]);
  AppendOp(Result, FInstruction.Compile);
  AppendOp(Result, [TOpPop.Create(ftFinally)]);
  AppendOp(Result, AttachTag(FFinally.Compile, GetTag(TAG_FINALLY), tpAtFirst));
  AppendOp(Result, [TOpThrow.Create, TOpReturn.Create(False, False)]);
end;

end.
