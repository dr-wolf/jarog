unit uFuncBuilder;

interface

uses
  uArray, uDict, uStringSet, uSourceParser, uCustomFunc, uFuncContext, uAppMold, uBuilder, uResolver, uValue, uData,
  uType,
  uInstruction;

type
  TFuncBuilder = class(TBuilder)
  public
    constructor Create(Resolver: IResolver; AppMold: TAppMold; Tokens: TTokenStream);
    destructor Destroy; override;
  private
    FValues: TDict<IValue>;
    FParamNames: TStringSet;
    FResultType: TType;
    FResolver: IResolver;
    FContext: TFuncContext;
    FFunc: TCustomFunc;
    FClosureNames: TStringSet;
    FClosures: TArray<Integer>;
    function BuildInstruction(Tokens: TTokenStream; AllowControl: Boolean): TInstruction;
    function GetValue(const Name, UnitName: string): IValue;
    function MakeInterface: TType;
    function GetSelfData: TData;
    procedure ProcessClosures(Tokens: TTokenStream);
    procedure BuildHeader(Tokens: TTokenStream);
    procedure UnregisterValue(const Name: string);
    function GetClosure(Name: string): TData;
  protected
    function MakeResolver: IResolver; override;
    function SaveValue(Name: string; Value: IValue; Section: TValueSection): Integer; override;
  public
    property Func: TData read GetSelfData;
    property FuncType: TType read MakeInterface;
    property ClosureNames: TStringSet read FClosureNames;
    property Closure[Name: string]: TData read GetClosure;
  end;

implementation

uses uExceptions, uDataFunc, uDataString, uExpressionBuilder, uExpression, uExpressionVariable,
  uTypeBuilder, uTypeFactory, uTypeString, uTypeVoid, uOp, uOpLoad, uOpReturn, uIdentifierRegistry,
  uInstructionAssign, uInstructionBlock, uInstructionControl, uInstructionDebug, uInstructionEach,
  uInstructionExecute, uInstructionIf, uInstructionLog, uInstructionReturn, uInstructionSync, uInstructionThrow,
  uInstructionTryCatch, uInstructionTryFinally, uInstructionWhile, uInstructionWith;

{ TFuncBuilder }

constructor TFuncBuilder.Create(Resolver: IResolver; AppMold: TAppMold; Tokens: TTokenStream);
var
  p: TArray<Integer>;
  i: Integer;
  a: TArray<TOp>;
  r: TData;
begin
  inherited Create(AppMold);
  FResolver := Resolver;

  FRegistry.Add(RI_SELF);

  FValues := TDict<IValue>.Create;
  FParamNames := TStringSet.Create;
  FClosureNames := TStringSet.Create;
  FResultType := TypeFactory.GetVoid;
  SetLength(FClosures, 0);

  FContext := TFuncContext.Create;

  BuildHeader(Tokens);

  SetLength(p, FParamNames.Count);
  for i := 0 to High(p) do
    p[i] := FContext.Keep(FValues[FParamNames[i]].Data, False);

  if not(FResultType is TTypeVoid) then
    r := FResultType.Default
  else
    r := nil;
  FFunc := TCustomFunc.Create(FContext, FClosures, p, r);

  SetLength(a, 0);
  with BuildInstruction(Tokens, False) do
  begin
    AppendOp(a, Compile);
    Free;
  end;
  if not(FResultType is TTypeVoid) then
    AppendOp(a, [TOpLoad.Create(FAppMold.DataSection.Keep(FResultType.Default), False)]);
  AppendOp(a, [TOpReturn.Create(True, not(FResultType is TTypeVoid))]);

  FFunc.SetAddress(FAppMold.Size);
  FAppMold.AddAll(a);
end;

destructor TFuncBuilder.Destroy;
begin
  FClosureNames.Free;
  FValues.Free;
  FParamNames.Free;
  inherited;
end;

function TFuncBuilder.GetValue(const Name, UnitName: string): IValue;
var
  V: IValue;
begin
  if FValues.Contains(Name) then
    Result := FValues[Name]
  else
  begin
    if Name = RI_SELF then
    begin
      V := TValue.Create(MakeInterface, TDataFunc.Create(FFunc), False);
      FValues.Put(RI_SELF, V);
      Result := V;
    end
    else
      Result := nil;
  end;
end;

function TFuncBuilder.BuildInstruction(Tokens: TTokenStream; AllowControl: Boolean): TInstruction;

  function BuildBlock(Tokens: TTokenStream; AllowControl: Boolean): TInstructionBlock;
  begin
    Result := TInstructionBlock.Create;
    repeat
      if Eq(Tokens.Token, kwEnd) then
      begin
        Tokens.Skip;
        Exit;
      end;
      Result.AddInstruction(BuildInstruction(Tokens, AllowControl));
      Tokens.Test(sSemicolon, E_MISSING_SEMICOLON);
    until False;
  end;

  function BuildEach(Tokens: TTokenStream; AllowControl: Boolean): TInstructionEach;
  var
    list, item: TExpression;
    identifier: TToken;
    T: TType;
    f: Boolean;
  begin
    Tokens.Test(sBrOp, E_SYNTAX_ERROR);
    list := BuildExpression(Tokens, FResolver, FAppMold, FContext);
    Tokens.Test(sArrow, E_SYNTAX_ERROR);
    f := (Tokens.Token().Kind = tkIdentifier) and Eq(Tokens.Token(1), sColon);
    if f then
    begin
      identifier := Tokens.Fetch(tkIdentifier);
      if FRegistry.Add(identifier.Value) then
      begin
        T := BuildType(Tokens.Skip, FResolver);
        item := TExpressionVariable.Create(FAppMold.DataSection,
          SaveValue(identifier.Value, TValue.Create(T, T.Default, False), vsLocal), T);
      end
      else
        raise ECodeException.Create(E_IDENTIFIER_REDECLARED, identifier.Value, identifier.Index);
    end
    else
      item := BuildExpression(Tokens, FResolver, FAppMold, FContext);
    Tokens.Test(sBrCl, E_SYNTAX_ERROR);
    Result := TInstructionEach.Create(list, item, BuildInstruction(Tokens, AllowControl));
    if f then
      UnregisterValue(identifier.Value);
  end;

  function BuildIf(Tokens: TTokenStream; AllowControl: Boolean): TInstructionIf;
  var
    cond: TExpression;
    ib, eb: TInstruction;
  begin
    Tokens.Test(sBrOp, E_SYNTAX_ERROR);
    cond := BuildExpression(Tokens, FResolver, FAppMold, FContext);
    Tokens.Test(sBrCl, E_SYNTAX_ERROR);
    ib := BuildInstruction(Tokens, AllowControl);
    if Eq(Tokens.Token(), kwElse) then
      eb := BuildInstruction(Tokens.Skip, AllowControl)
    else
      eb := TInstructionBlock.Create;
    Result := TInstructionIf.Create(cond, ib, eb);
  end;

  function BuildSync(Tokens: TTokenStream; AllowControl: Boolean): TInstructionSync;
  const
    ALPHA = '0123456789ABCDEF';
  var
    i: Integer;
    E: TExpression;
    Name: string;
  begin
    if Eq(Tokens.Token, sBrOp) then
    begin
      Tokens.Test(sBrOp, E_SYNTAX_ERROR);
      i := Tokens.Token.Index;
      E := BuildExpression(Tokens, FResolver, FAppMold, FContext);
      if not(E.ResultType is TTypeString) then
        raise ECodeException.Create(E_EXPECTED_STRING, i);
      if E.Group <> egStatic then
        raise ECodeException.Create(E_EXPECTED_STATIC, i);
      Tokens.Test(sBrCl, E_SYNTAX_ERROR);
      name := AsString(E.Eval.Data);
    end
    else
      name := TIdentifierRegistry.RandomIdentifier(ALPHA, 16);
    Result := TInstructionSync.Create(FAppMold.RegisterCriticalSection(name), BuildInstruction(Tokens, AllowControl));
  end;

  function BuildTry(Tokens: TTokenStream; AllowControl: Boolean): TInstruction;
  var
    Body: TInstruction;
    E: TExpression;
    T: TType;
    identifier: TToken;
  begin
    Body := BuildInstruction(Tokens, AllowControl);
    if Eq(Tokens.Token, kwFinally) then
      Result := TInstructionTryFinally.Create(Body, BuildInstruction(Tokens.Skip, AllowControl))
    else
    begin
      Result := TInstructionTryCatch.Create(Body);
      while True do
        if Eq(Tokens.Token, kwCatch) then
        begin
          Tokens.Skip.Test(sBrOp, E_SYNTAX_ERROR);
          identifier := Tokens.Fetch(tkIdentifier);
          Tokens.Test(sColon, E_SYNTAX_ERROR);
          if FRegistry.Add(identifier.Value) then
          begin
            T := BuildType(Tokens, FResolver);
            E := TExpressionVariable.Create(FAppMold.DataSection,
              SaveValue(identifier.Value, TValue.Create(T, T.Default, False), vsLocal), T);
          end
          else
            raise ECodeException.Create(E_IDENTIFIER_REDECLARED, identifier.Value, identifier.Index);
          Tokens.Test(sBrCl, E_SYNTAX_ERROR);
          if not(Result as TInstructionTryCatch).AddCatchCaluse(E, BuildInstruction(Tokens, AllowControl)) then
            raise ECodeException.Create(E_CATCH_REDECLARED, E.ResultType.Hash, identifier.Index);
          UnregisterValue(identifier.Value);
          Continue;
        end else begin
          Break;
        end;
    end;
  end;

  function BuildWhile(Tokens: TTokenStream; AllowControl: Boolean): TInstructionWhile;
  var
    cond: TExpression;
  begin
    Tokens.Test(sBrOp, E_SYNTAX_ERROR);
    cond := BuildExpression(Tokens, FResolver, FAppMold, FContext);
    Tokens.Test(sBrCl, E_SYNTAX_ERROR);
    Result := TInstructionWhile.Create(cond, BuildInstruction(Tokens, AllowControl));
  end;

  function BuildWith(Tokens: TTokenStream; AllowControl: Boolean): TInstructionWith;
  var
    Value, variable: TExpression;
    identifier: TToken;
    T: TType;
  begin
    Tokens.Test(sBrOp, E_SYNTAX_ERROR);
    Value := BuildExpression(Tokens, FResolver, FAppMold, FContext);
    Tokens.Test(sArrow, E_SYNTAX_ERROR);
    identifier := Tokens.Fetch(tkIdentifier);
    if FRegistry.Add(identifier.Value) then
    begin
      Tokens.Test(sColon, E_SYNTAX_ERROR);
      T := BuildType(Tokens, FResolver);
      variable := TExpressionVariable.Create(FAppMold.DataSection,
        SaveValue(identifier.Value, TValue.Create(T, T.Default, False), vsLocal), T);
    end
    else
      raise ECodeException.Create(E_IDENTIFIER_REDECLARED, identifier.Value, identifier.Index);
    Tokens.Test(sBrCl, E_SYNTAX_ERROR);
    Result := TInstructionWith.Create(variable, Value, BuildInstruction(Tokens, AllowControl));
    UnregisterValue(identifier.Value);
  end;

var
  T: TToken;
  E: TExpression;
begin
  T := Tokens.Token;
  try
    if T.Kind = tkKeyword then
      case ToKword(Tokens.Pop.Value) of
        kwDo:
          Result := BuildBlock(Tokens, AllowControl);
        kwEach:
          Result := BuildEach(Tokens, True);
        kwIf:
          Result := BuildIf(Tokens, AllowControl);
        kwWhile:
          Result := BuildWhile(Tokens, True);
        kwWith:
          Result := BuildWith(Tokens, AllowControl);
        kwSync:
          Result := BuildSync(Tokens, AllowControl);
        kwTry:
          Result := BuildTry(Tokens, AllowControl);
        kwDebug:
          Result := TInstructionDebug.Create;
        kwThrow:
          Result := TInstructionThrow.Create(BuildExpression(Tokens, FResolver, FAppMold, FContext));
        kwRun:
          Result := TInstructionExecute.Create(BuildExpression(Tokens, FResolver, FAppMold, FContext), True);
        kwLog:
          begin
            E := BuildExpression(Tokens, FResolver, FAppMold, FContext);
            Result := TInstructionLog.Create(E, FAppMold.DataSection.Keep(E.ResultType.Hash));
          end;
        kwReturn:
          begin
            if Eq(Tokens.Token, sSemicolon) then
              E := nil
            else
              E := BuildExpression(Tokens, FResolver, FAppMold, FContext);
            if (E <> nil) and not FResultType.Compatible(E.ResultType) then
              raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, E.ResultType.Hash, T.Index);
            Result := TInstructionReturn.Create(E, FResultType, FAppMold.DataSection);
          end;
        kwBreak, kwContinue:
          begin
            if not AllowControl then
              raise ECodeException.Create(E_UNEXPECTED_KEYWORD, T.Value, T.Index);
            Result := TInstructionControl.Create(ToKword(T.Value) = kwBreak);
          end
      else
        raise ECodeException.Create(E_UNEXPECTED_KEYWORD, T.Value, T.Index);
      end
    else begin
      E := BuildExpression(Tokens, FResolver, FAppMold, FContext);
      if Eq(Tokens.Token(), sSet) then
      begin
        Tokens.Skip;
        Result := TInstructionAssign.Create(E, BuildExpression(Tokens, FResolver, FAppMold, FContext));
      end
      else
        Result := TInstructionExecute.Create(E, False);
    end;
  except
    on E: ECodeException do
      raise ECodeException.Create(E, '', T.Index);
  end;
end;

procedure TFuncBuilder.BuildHeader(Tokens: TTokenStream);
var
  kw: TKeyword;
begin
  FResolver := FResolver.GoDown(GetValue);
  Tokens.Test(sFunc, E_SYNTAX_ERROR);
  Tokens.Test(sBrOp, E_SYNTAX_ERROR);
  while not Eq(Tokens.Token, sBrCl) do
  begin
    if Tokens.Token.Kind = tkKeyword then
      kw := ToKword(Tokens.Token.Value)
    else
      kw := kwError;
    case kw of
      kwConst:
        ProcessConst(Tokens);
      kwVar, kwArg:
        ProcessVariable(Tokens);
      kwUse:
        ProcessClosures(Tokens);
    else
      raise ECodeException.Create(E_SYNTAX_ERROR, Tokens.Token.Value, Tokens.Token.Index);
    end;
  end;
  Tokens.Test(sBrCl, E_SYNTAX_ERROR);
  if Eq(Tokens.Token, sColon) then
  begin
    Tokens.Test(sColon, E_SYNTAX_ERROR);
    FResultType := BuildType(Tokens, MakeResolver);
  end;
end;

function TFuncBuilder.MakeInterface: TType;
var
  i: Integer;
  a: TArray<TType>;
  p: TType;
begin
  SetLength(a, FParamNames.Count);
  for i := 0 to High(a) do
    a[i] := FValues[FParamNames[i]].TypeInfo;
  p := TypeFactory.GetStruct(a, FParamNames.AsArray);
  Result := TypeFactory.GetFunc(p, FResultType);
end;

function TFuncBuilder.MakeResolver: IResolver;
begin
  Result := FResolver;
end;

procedure TFuncBuilder.ProcessClosures(Tokens: TTokenStream);
var
  T: TToken;
  V: IValue;
begin
  Tokens.Test(kwUse, E_SYNTAX_ERROR); // Guard, never fails
  repeat
    T := Tokens.Fetch(tkIdentifier);
    try
      if FRegistry.Add(T.Value) then
      begin
        V := FResolver.ResolveClosure(T.Value);
        SaveValue(T.Value, V, vsClosure);
        if not V.Immutable then
        begin
          SetLength(FClosures, Length(FClosures) + 1);
          FClosures[High(FClosures)] := FContext.Keep(V.Data, False);
          FClosureNames.Add(T.Value);
        end;
      end
      else
        raise ECodeException.Create(E_IDENTIFIER_REDECLARED, T.Value);
    except
      on E: ECodeException do
        raise ECodeException.Create(E, '', T.Index);
    end;

    T := Tokens.Pop;
    if Eq(T, sComma) then
      Continue;
    if Eq(T, sSemicolon) then
      Break
    else
      raise ECodeException.Create(E_MISSING_SEMICOLON, T.Index);
  until False;
end;

function TFuncBuilder.SaveValue(Name: string; Value: IValue; Section: TValueSection): Integer;
begin
  FValues.Put(Name, Value);
  Result := FContext.Keep(Value.Data, Section = vsClosure);
  if Section = vsParam then
    FParamNames.Add(Name);
end;

procedure TFuncBuilder.UnregisterValue(const Name: string);
begin
  FRegistry.Unregister(Name);
  FValues.Delete(Name);
end;

function TFuncBuilder.GetClosure(Name: string): TData;
begin
  Result := FValues[Name].Data;
end;

function TFuncBuilder.GetSelfData: TData;
begin
  if FValues.Contains(RI_SELF) then
    Result := FValues[RI_SELF].Data
  else
    Result := TDataFunc.Create(FFunc);
end;

end.
