unit uFuncProcRead;

interface

uses
  uFuncProc, uFuncContext, ChildProcess;

type
  TFuncProcRead = class(TFuncProc)
  public
    constructor Create;
  private const
    P_PROC = 0;
    P_FILE = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Reads data from process stdout pipe.
    @type func(proc: int, file: int -> int)
    @in proc int # Process handle
    @in file int # File handle
    @out - int # NBumber of bytes read
    * }

implementation

uses Classes, uDataNumeric, uOpCode;

{ TFuncProcRead }

constructor TFuncProcRead.Create;
begin
  inherited Create([MakeInt, MakeInt], MakeInt);
  FFuncUid := FID_PROC_READ;
end;

procedure TFuncProcRead.Execute(Context: TFuncContext);
var
  p: TChildProcess;
  f: TStream;
  bytes: Cardinal;
begin
  p := RetrieveProcess(P_PROC, Context);
  f := RetrieveStream(P_FILE, Context);
  p.ReceiveData(f, bytes);
  Return(TDataNumeric.Create(bytes));
end;

end.
