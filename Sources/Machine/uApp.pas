unit uApp;

interface

uses
  Classes, uArray, uByteCode, uDataSection, uExecFile;

type
  TApp = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FData: TDataSection;
    FOps: TArray<TByteCode>;
    function GetSize: Integer;
    function GetOp(I: Integer): TByteCode;
  public
    property Data: TDataSection read FData;
    property Op[I: Integer]: TByteCode read GetOp; default;
    property Size: Integer read GetSize;
    procedure ClearBreaks;
    procedure Load(ExecFile: TExecFile);
    procedure Log(Stream: TStream);
    procedure ToggleBreak(I: Integer);
  end;

implementation

uses uTypedStream, uBytecodeLogger;

{ TApp }

constructor TApp.Create;
begin
  FData := TDataSection.Create;
  SetLength(FOps, 0);
end;

destructor TApp.Destroy;
begin
  FData.Free;
  FOps := nil;
  inherited;
end;

procedure TApp.ClearBreaks;
var
  I: Integer;
begin
  for I := 0 to High(FOps) do
    FOps[I].Break := False;
end;

function TApp.GetOp(I: Integer): TByteCode;
begin
  Result := FOps[I];
end;

function TApp.GetSize: Integer;
begin
  Result := Length(FOps);
end;

procedure TApp.Load(ExecFile: TExecFile);
var
  I: Integer;
begin
  FData.Load(ExecFile);
  FData.SetSectionSize(ExecFile.OpStream.ReadInteger);
  SetLength(FOps, ExecFile.OpStream.ReadInteger);
  for I := 0 to High(FOps) do
    ReadCodeFromStream(FOps[I], ExecFile.OpStream);
end;

procedure TApp.Log(Stream: TStream);
var
  I: Integer;
begin
  with TTypedStream.Create do
  begin
    WriteInteger(Length(FOps));
    for I := 0 to High(FOps) do
    begin
      WriteByte(Byte(FOps[I].Break));
      WriteString(LogBytecode(FOps[I], FData));
    end;
    SaveTo(Stream);
    Free;
  end;
end;

procedure TApp.ToggleBreak(I: Integer);
begin
  FOps[I].Break := not FOps[I].Break;
end;

end.
