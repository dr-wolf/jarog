unit uFuncFSAbsPath;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncFSAbsPath = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_PATH = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Returns canonicalized absolute pathname from given string.
    @type func(path: string -> string)
    @in path string # Initial path
    @out - string # Absolute path
    * }

implementation

uses SysUtils, uDataString, uOpCode;

{ TFuncFSAbsPath }

constructor TFuncFSAbsPath.Create;
begin
  inherited Create([MakeString], MakeString);
  FFuncUid := FID_FS_ABSPATH;
end;

procedure TFuncFSAbsPath.Execute(Context: TFuncContext);
begin
  Return(TDataString.Create(ExpandFileName(AsString(Context.Data[P_PATH]))));
end;

end.
