unit uTypeFunc;

interface

uses
  uOperations, uType, uData;

type
  TTypeFunc = class(TType)
  public
    constructor Create(const Hash: string; ParamTypes, ResultType: TType);
  private
    FParamTypes: TType;
    FResultType: TType;
  public
    property Params: TType read FParamTypes;
    function Apply(Op: TBinaryOp; T: TType): TType; override;
    function Call(T: TType): TType; override;
    function Pipeable: Boolean; override;
    function Compatible(T: TType; StrictCompat: Boolean = False): Boolean; override;
    function Default: TData; override;
  end;

implementation

uses uExceptions, uArray, uFuncContext, uTypeFactory, uTypeVoid, uTypeStruct, uDataFunc, uCustomFunc;

{ TTypeFunc }

constructor TTypeFunc.Create(const Hash: string; ParamTypes, ResultType: TType);
begin
  inherited Create(Hash);
  FParamTypes := ParamTypes;
  FResultType := ResultType;
end;

function TTypeFunc.Apply(Op: TBinaryOp; T: TType): TType;
begin
  inherited;
  if not(Op in [boEqual, boNotEqual]) then
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION);
  Result := TypeFactory.GetBool;
end;

function TTypeFunc.Call(T: TType): TType;
var
  f: TArray<string>;
  i: Integer;
begin
  if (T is TTypeStruct) and (not TTypeStruct(T).Nameless) or (TTypeStruct(T).Size = 0) then
  begin
    f := TTypeStruct(T).Fields;
    for i := 0 to High(f) do
      if not FParamTypes.NestedType(f[i]).Compatible(T.NestedType(f[i])) then
        raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, FParamTypes.NestedType(f[i]).Hash)
  end else if not FParamTypes.Compatible(T) then
    raise ECodeException.Create(E_ARG_MISMATCH);
  Result := FResultType;
end;

function TTypeFunc.Compatible(T: TType; StrictCompat: Boolean = False): Boolean;
begin
  Result := (T is TTypeFunc) and FParamTypes.Compatible(TTypeFunc(T).FParamTypes, True) and
    FResultType.Compatible(TTypeFunc(T).FResultType, True);
end;

function TTypeFunc.Default: TData;
var
  i: Integer;
  c: TFuncContext;
  p: TArray<Integer>;
  r: TData;
begin
  c := TFuncContext.Create;
  SetLength(p, TTypeStruct(FParamTypes).Size);
  for i := 0 to High(p) do
    p[i] := c.Keep(FParamTypes.NestedType(TTypeStruct(FParamTypes).Fields[i]).Default, False);
  if not(FResultType is TTypeVoid) then
    r := FResultType.Default
  else
    r := nil;
  Result := TDataFunc.Create(TCustomFunc.Create(c, [], p, r));
end;

function TTypeFunc.Pipeable: Boolean;
begin
  Result := False;
end;

end.
