unit uFuncRexFind;

interface

uses
  uFuncRex, uFuncContext;

type
  TFuncRexFind = class(TFuncRex)
  public
    constructor Create;
  private const
    P_REGEX = 0;
    P_TEXT = 1;
    P_MODIFIERS = 2;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Finds all substrings matching given regular expression.
    @type func(regex: string, text: string -> list(tmatch))
    @in regex string # Regular expression to apply
    @in text string # Text to be matched with regular expression
    @in modifiers int # Reqular expression modifiers
    @out - list(_match) # List of matches: string, position and length
    * }

implementation

uses uArray, uData, uDataNumeric, uDataString, uDataArray, uOpCode;

{ TFuncRexFind }

constructor TFuncRexFind.Create;
begin
  inherited Create([MakeString, MakeString, MakeInt], MakeList(MakeStruct([MakeString, MakeInt, MakeInt])));
  FFuncUid := FID_REX_FIND;
end;

procedure TFuncRexFind.Execute(Context: TFuncContext);
var
  s: string;
  a: TArray<TData>;
begin
  SetLength(a, 0);
  s := AsString(Context.Data[P_TEXT]);
  with InitRex(AsInt(Context.Data[P_MODIFIERS])) do
  begin
    Expression := AsString(Context.Data[P_REGEX]);
    if Exec(s) then
      repeat
        SetLength(a, Length(a) + 1);
        a[High(a)] := TDataArray.Create([TDataString.Create(Match[0]), TDataNumeric.Create(MatchPos[0]),
          TDataNumeric.Create(MatchLen[0])]);
      until not ExecNext;
    Free;
  end;
  Return(TDataArray.Create(a, False));
end;

end.
