unit uFuncBitNot;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncBitNot = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_VALUE = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Inverts all bits of the value.
    @type func(value: int -> int)
    @in value int # Value to be inverted
    @out - int # Result of inversion
    * }

implementation

uses uDataNumeric, uOpCode;

{ TFuncBitNot }

constructor TFuncBitNot.Create;
begin
  inherited Create([MakeInt], MakeInt);
  FFuncUid := FID_BIT_NOT;
end;

procedure TFuncBitNot.Execute(Context: TFuncContext);
begin
  Return(TDataNumeric.Create(AsNumeric(Context.Data[P_VALUE]).Invert));
end;

end.
