unit uUnitProc;

interface

uses
  uUnit;

type
  TUnitProc = class(TUnit)
  public
    constructor Create;
  end;

  { *
    @descr Process routines.
    * }

implementation

uses uTypeFactory, uFuncProcOpen, uFuncProcClose, uFuncProcRead, uFuncProcStart, uFuncProcWrite,
  uFuncProcWait;

{ TUnitProc }

constructor TUnitProc.Create;
begin
  inherited Create('<proc>');
  AddValue('close', MakeFunc(TFuncProcClose.Create, [TypeFactory.GetInt], ['proc']));
  AddValue('open', MakeFunc(TFuncProcOpen.Create, [TypeFactory.GetString, TypeFactory.GetList(TypeFactory.GetString)],
    ['command', 'args'], TypeFactory.GetInt));
  AddValue('read', MakeFunc(TFuncProcRead.Create, [TypeFactory.GetInt, TypeFactory.GetInt], ['proc', 'file'],
    TypeFactory.GetInt));
  AddValue('start', MakeFunc(TFuncProcStart.Create, [TypeFactory.GetInt], ['proc']));
  AddValue('wait', MakeFunc(TFuncProcWait.Create, [TypeFactory.GetInt, TypeFactory.GetInt], ['proc', 'timeout'],
    TypeFactory.GetInt));
  AddValue('write', MakeFunc(TFuncProcWrite.Create, [TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt],
    ['proc', 'file', 'size']));
end;

end.
