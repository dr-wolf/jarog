unit uFuncProcOpen;

interface

uses
  uFuncProc, uFuncContext, BlckSock;

type
  TFuncProcOpen = class(TFuncProc)
  public
    constructor Create;
  private const
    P_COMMAND = 0;
    P_ARGS = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Creates new process.
    @type func(command: string, args: list(string) -> int)
    @in command string # Command to execute
    @in args string # Arguments to pass to the command
    @out - int # Handle of process or 0
    * }

implementation

uses Classes, uData, uDataNumeric, uDataString, uOpCode, ChildProcess;

{ TFuncProcOpen }

constructor TFuncProcOpen.Create;
begin
  inherited Create([MakeString, MakeList(MakeString)], MakeInt);
  FFuncUid := FID_PROC_OPEN;
end;

procedure TFuncProcOpen.Execute(Context: TFuncContext);
var
  p: TChildProcess;
  r, i: Integer;
  d: TData;
begin
  p := TChildProcess.Create(AsString(Context.Data[P_COMMAND]));
  try
    d := Context.Data[P_ARGS];
    for i := 0 to d.Count - 1 do
      p.AddArgument(AsString(d.Item(i)));
    r := FCoreContext.Repos.Processes.Add(p);
  except
    p.Free;
    r := 0;
  end;
  Return(TDataNumeric.Create(r));
end;

end.
