unit uFuncOSPid;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncOSPid = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Returns current process ID.
    @type func(-> int)
    @out - int # Process ID
    * }

implementation

uses {$IFDEF UNIX} BaseUnix {$ELSE} Windows {$ENDIF}, uDataNumeric, uOpCode;

function GetPid: Cardinal;
begin
{$IFDEF UNIX}
  Result := FpGetpid;
{$ELSE}
  Result := GetCurrentProcessId;
{$ENDIF}
end;

{ TFuncOSPid }

constructor TFuncOSPid.Create;
begin
  inherited Create([], MakeInt);
  FFuncUid := FID_OS_PID;
end;

procedure TFuncOSPid.Execute(Context: TFuncContext);
begin
  Return(TDataNumeric.Create(GetPid));
end;

end.
