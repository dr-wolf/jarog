unit uFuncStrAscii;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncStrAscii = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_TEXT = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Explodes string into list of ascii char codes.
    @type func(text: string -> list(int))
    @in text string # Incoming string
    @out - list(int) # Returned list of ints
    * }

implementation

uses uArray, uData, uDataArray, uDataNumeric, uDataString, uOpCode;

{ TFuncStrAscii }

constructor TFuncStrAscii.Create;
begin
  inherited Create([MakeString], MakeList(MakeInt));
  FFuncUid := FID_STR_ASCII;
end;

procedure TFuncStrAscii.Execute(Context: TFuncContext);
var
  a: TArray<TData>;
  i: Integer;
  s: string;
begin
  s := AsString(Context.Data[P_TEXT]);
  SetLength(a, Length(s));
  for i := 1 to Length(s) do
    a[i - 1] := TDataNumeric.Create(Ord(s[i]));
  Return(TDataArray.Create(a, False));
end;

end.
