unit uExpressionSet;

interface

uses
  uArray, uType, uExpression, uValue, uOp, uDataSection;

type
  TExpressionSet = class(TExpression)
  public
    constructor Create(Data: TDataSection; Expressions: TArray<TExpression>; ItemType: TType);
    destructor Destroy; override;
  private
    FItemType: TType;
    FItems: TArray<TExpression>;
  public
    function Compile: TArray<TOp>; override;
    function Eval: IValue; override;
  end;

implementation

uses uExceptions, uTypeFactory, uDataSet, uOpStack;

{ TExpressionSet }

constructor TExpressionSet.Create(Data: TDataSection; Expressions: TArray<TExpression>; ItemType: TType);
var
  i: Integer;
begin
  inherited Create(Data);
  FResultType := TypeFactory.GetSet(ItemType);
  FItemType := ItemType;
  SetLength(FItems, Length(Expressions));
  FGroup := egStatic;
  for i := 0 to High(Expressions) do
  begin
    if not ItemType.Compatible(Expressions[i].ResultType) then
      raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, ItemType.Hash);
    FItems[i] := Expressions[i];
    if Expressions[i].Group <> egStatic then
      FGroup := egMixed;
  end;
end;

destructor TExpressionSet.Destroy;
var
  i: Integer;
begin
  for i := 0 to High(FItems) do
    FItems[i].Free;
  inherited;
end;

function TExpressionSet.Compile: TArray<TOp>;
var
  i: Integer;
begin
  SetLength(Result, 0);
  for i := High(FItems) downto 0 do
    AppendOp(Result, FItems[i].Compile);
  AppendOp(Result, [TOpStack.Create(atSet, Length(FItems))]);
end;

function TExpressionSet.Eval: IValue;
var
  i: Integer;
  d: TDataSet;
begin
  d := TDataSet.Create;
  for i := 0 to High(FItems) do
    d.AddItem(FItems[i].Eval.Data);
  Result := TValue.Create(FResultType, d);
end;

end.
