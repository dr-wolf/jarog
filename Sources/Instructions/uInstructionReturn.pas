unit uInstructionReturn;

interface

uses
  uArray, uType, uInstruction, uExpression, uOp, uDataSection;

type
  TInstructionReturn = class(TInstruction)
  public
    constructor Create(Expression: TExpression; Result: TType; Data: TDataSection);
    destructor Destroy; override;
  private
    FExpression: TExpression;
    FResult: TType;
    FData: TDataSection;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uTag, uTypeVoid, uDataStack, uOpMove, uOpPop, uOpPush, uOpJump, uOpReturn, uOpLoad;

{ TInstructionReturn }

constructor TInstructionReturn.Create(Expression: TExpression; Result: TType; Data: TDataSection);
begin
  inherited Create;
  FExpression := Expression;
  FResult := Result;
  FData := Data;
end;

destructor TInstructionReturn.Destroy;
begin
  if not(FResult is TTypeVoid) then
    FExpression.Free;
  inherited;
end;

function TInstructionReturn.Compile: TArray<TOp>;
type
  TTagPos = record
    Tag: ITag;
    Pos: Integer;
  end;
var
  p: TInstruction;
  t: TArray<TTagPos>;
  i: Integer;
begin
  SetLength(Result, 0);

  p := FParent;
  while (p <> nil) do
  begin
    if p.Tags[TAG_FINALLY] <> nil then
    begin
      SetLength(t, Length(t) + 1);
      t[High(t)].Tag := TTag.Create;
      AppendOp(Result, [TOpPop.Create(ftFinally), TOpPush.Create(ftFinallyReturn, t[High(t)].Tag),
        TOpJump.Create(0, p.Tags[TAG_FINALLY])]);
      t[High(t)].Pos := Length(Result);
    end;
    p := p.Parent;
  end;

  if not(FResult is TTypeVoid) then
  begin
    AppendOp(Result, FExpression.Compile);
    if not FExpression.ResultType.Compatible(FResult, True) then
      AppendOp(Result, [TOpLoad.Create(FData.Keep(FResult.Default), False), TOpMove.Create(False, True)]);
  end;
  AppendOp(Result, [TOpReturn.Create(True, not(FResult is TTypeVoid))]);

  for i := 0 to High(t) do
    Result[t[i].Pos].WithTag(t[i].Tag);
end;

end.
