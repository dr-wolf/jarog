unit uExpressionList;

interface

uses
  uArray, uType, uExpression, uValue, uOp, uDataSection;

type
  TExpressionList = class(TExpression)
  public
    constructor Create(Data: TDataSection; Expressions: TArray<TExpression>; ItemType: TType);
    destructor Destroy; override;
  private
    FItemType: TType;
    FItems: TArray<TExpression>;
  public
    function Compile: TArray<TOp>; override;
    function Eval: IValue; override;
    procedure AddItem(Expression: TExpression);
  end;

implementation

uses uExceptions, uTypeFactory, uTypeVoid, uData, uDataArray, uOpStack;

{ TExpressionList }

constructor TExpressionList.Create(Data: TDataSection; Expressions: TArray<TExpression>; ItemType: TType);
var
  i: Integer;
begin
  inherited Create(Data);
  FResultType := TypeFactory.GetList(ItemType);
  FItemType := ItemType;
  SetLength(FItems, Length(Expressions));
  if Length(Expressions) > 0 then
    FGroup := Expressions[0].Group
  else
    FGroup := egStatic;
  for i := 0 to High(Expressions) do
  begin
    if Expressions[i].ResultType is TTypeVoid then
      raise ECodeException.Create(E_NO_RESULT);
    if not ItemType.Compatible(Expressions[i].ResultType) then
      raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, ItemType.Hash);
    FItems[i] := Expressions[i];
    if FGroup <> Expressions[i].Group then
      FGroup := egMixed;
  end;
end;

destructor TExpressionList.Destroy;
var
  i: Integer;
begin
  for i := 0 to High(FItems) do
    FItems[i].Free;
  inherited;
end;

function TExpressionList.Eval: IValue;
var
  i: Integer;
  d: TArray<TData>;
begin
  SetLength(d, Length(FItems));
  for i := 0 to High(FItems) do
    d[i] := FItems[i].Eval.Data.Dup;
  Result := TValue.Create(FResultType, TDataArray.Create(d, False));
end;

function TExpressionList.Compile: TArray<TOp>;
var
  i: Integer;
begin
  SetLength(Result, 0);
  for i := High(FItems) downto 0 do
    AppendOp(Result, FItems[i].Compile);
  AppendOp(Result, [TOpStack.Create(atArray, Length(FItems))]);
end;

procedure TExpressionList.AddItem(Expression: TExpression);
begin
  SetLength(FItems, Length(FItems) + 1);
  if Length(FItems) = 1 then
  begin
    FResultType := TypeFactory.GetList(Expression.ResultType);
    FItems[0] := Expression;
  end else begin
    if not FItems[0].ResultType.Compatible(Expression.ResultType) then
    begin
      if FItems[0].ResultType.Compatible(Expression.ResultType, False) then
        raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, FItems[0].ResultType.Hash);
    end;
    FItems[High(FItems)] := Expression;
  end;
  if FGroup <> Expression.Group then
    FGroup := egMixed;
end;

end.
