unit uFuncFSMkDir;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncFSMkDir = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_PATH = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Creates directories by given path.
    @type func(path: string)
    @in path string # Path to create
    * }

implementation

uses SysUtils, uDataString, uOpCode;

{ TFuncFSMkDir }

constructor TFuncFSMkDir.Create;
begin
  inherited Create([MakeString]);
  FFuncUid := FID_FS_MKDIR;
end;

procedure TFuncFSMkDir.Execute(Context: TFuncContext);
begin
  ForceDirectories(ExpandFileName(AsString(Context.Data[P_PATH])));
end;

end.
