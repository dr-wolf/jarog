unit uFunction;

interface

uses
  uArray, uData, uCoreContext, uFuncContext, uClosure, uExecFile, uOpCode, uState, uTypedStream;

type
  IFunction = interface(IInterface)
    function Log: string;
    function Serialize(const ExecFile: TExecFile): TTypedStream;
    procedure Call(State: TState; Caller: TData; Closures: TArray<IClosure>);
    procedure AttachContext(CoreContext: TCoreContext);
  end;

type
  TFunction = class(TInterfacedObject, IFunction)
  public
    constructor Create(Context: TFuncContext; Closures, Params: array of Integer);
    destructor Destroy; override;
  protected
    FFuncUid: TFID;
    FContext: TFuncContext;
    FClosures: TArray<Integer>;
    FParameters: TArray<Integer>;
    procedure LoadParams(Context: TFuncContext; State: TState);
  public
    function Log: string; virtual; abstract;
    function Serialize(const ExecFile: TExecFile): TTypedStream; virtual;
    procedure Call(State: TState; Caller: TData; Closures: TArray<IClosure>); virtual; abstract;
    procedure AttachContext(CoreContext: TCoreContext); virtual; abstract;
  end;

implementation

uses uDataStub;

{ TFunction }

constructor TFunction.Create(Context: TFuncContext; Closures, Params: array of Integer);
var
  i: Integer;
begin
  FContext := Context;
  SetLength(FClosures, Length(Closures));
  for i := 0 to High(FClosures) do
    FClosures[i] := Closures[i];
  SetLength(FParameters, Length(Params));
  for i := 0 to High(FParameters) do
    FParameters[i] := Params[i];
end;

destructor TFunction.Destroy;
begin
  FClosures := nil;
  FParameters := nil;
  FContext.DisposeData;
  FContext.Free;
  inherited;
end;

procedure TFunction.LoadParams(Context: TFuncContext; State: TState);
var
  d, p: TData;
  i: Integer;
begin
  d := State.Stack.Pop;
  for i := 0 to d.Count - 1 do
  begin
    p := d.Item(i);
    if not(p is TDataStub) then
      Context.Data[FParameters[i]].Assign(p);
  end;
end;

function TFunction.Serialize(const ExecFile: TExecFile): TTypedStream;
begin
  Result := TTypedStream.Create;
  with Result do
  begin
    WriteByte(DID_FUNCTION);
    WriteByte(FFuncUid);
  end;
end;

end.
