unit uBigInt;

interface

uses
  uNumeric, uTypedStream, uFormatTemplate, mp_types;

type
  TBigInt = class(TNumeric)
  public
    constructor Create(Value: mp_int); overload;
    constructor Create(Value: UInt64); overload;
    constructor Create(Value: Int64); overload;
    destructor Destroy; override;
  protected
    FValue: mp_int;
    { Addition }
    function AddInt(const V: Integer): TNumeric; override;
    function AddBigInt(const V: mp_int): TNumeric; override;
    function AddBigFloat(const V: mp_float): TNumeric; override;
    { Subtraction }
    function SubInt(const V: Integer): TNumeric; override;
    function SubBigInt(const V: mp_int): TNumeric; override;
    function SubBigFloat(const V: mp_float): TNumeric; override;
    { Multiplication }
    function MulInt(const V: Integer): TNumeric; override;
    function MulBigInt(const V: mp_int): TNumeric; override;
    function MulBigFloat(const V: mp_float): TNumeric; override;
    { Division }
    function DivInt(const V: Integer): TNumeric; override;
    function DivBigInt(const V: mp_int): TNumeric; override;
    function DivBigFloat(const V: mp_float): TNumeric; override;
    { Modulo }
    function ModInt(const V: Integer): TNumeric; override;
    function ModBigInt(const V: mp_int): TNumeric; override;
    function ModBigFloat(const V: mp_float): TNumeric; override;
    { Power }
    function PowInt(const V: Integer): TNumeric; override;
    function PowBigInt(const V: mp_int): TNumeric; override;
    function PowBigFloat(const V: mp_float): TNumeric; override;
    { Compare }
    function CmpInt(const V: Integer): Integer; override;
    function CmpBigInt(const V: mp_int): Integer; override;
    function CmpBigFloat(const V: mp_float): Integer; override;
    { Bit }
    function BitInt(const BitOp: TBitOp; const V: Integer): TNumeric; override;
    function BitBigInt(const BitOp: TBitOp; const V: mp_int): TNumeric; override;
    function BitBigFloat(const BitOp: TBitOp; const V: mp_float): TNumeric; override;
  public
    property Value: mp_int read FValue;
    function IsZero: Boolean; override;

    function Dup(TypeExpand: Boolean): TNumeric; override;
    function Floor: TNumeric; override;
    function Frac: TNumeric; override;
    function Invert: TNumeric; override;
    function Module: TNumeric; override;
    function Negative: TNumeric; override;
    function Shift(Bits: Integer): TNumeric; override;

    function ToInt64: Int64; override;
    function ToUInt64: UInt64; override;
    function ToFloat: mp_float; override;
    function ToDecimal: string; override;
    function ToText(Format: TFormatTemplate): string; override;
    procedure Serialize(Stream: TTypedStream); override;
  end;

implementation

uses SysUtils, mp_base, mp_real, uInteger, uBigFloat, uOpCode, uExceptions;

procedure RevertBytes(var Bytes: UInt64);
var
  b: array [0 .. 7] of Byte absolute Bytes;
  i: Integer;
  t: Byte;
begin
  for i := 0 to 3 do
  begin
    t := b[i];
    b[i] := b[7 - i];
    b[7 - i] := t;
  end;
end;

{ TBigInt }

constructor TBigInt.Create(Value: mp_int);
begin
  FValue := Value;
end;

constructor TBigInt.Create(Value: UInt64);
begin
  mp_init(FValue);
  RevertBytes(Value);
  mp_read_unsigned_bin(FValue, Value, 8);
end;

constructor TBigInt.Create(Value: Int64);
begin
  Create(UInt64(Abs(Value)));
  if Value < 0 then
    mp_chs(FValue, FValue);
end;

destructor TBigInt.Destroy;
begin
  mp_clear(FValue);
  inherited;
end;

function TBigInt.AddInt(const V: Integer): TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  mp_add_int(FValue, V, r);
  Result := ToNumeric(r);
end;

function TBigInt.AddBigInt(const V: mp_int): TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  mp_add(FValue, V, r);
  Result := ToNumeric(r);
end;

function TBigInt.AddBigFloat(const V: mp_float): TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_add_mpi(V, FValue, r);
  Result := TBigFloat.Create(r);
end;

function TBigInt.SubInt(const V: Integer): TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  mp_sub_int(FValue, V, r);
  Result := ToNumeric(r);
end;

function TBigInt.SubBigInt(const V: mp_int): TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  mp_sub(FValue, V, r);
  Result := ToNumeric(r);
end;

function TBigInt.SubBigFloat(const V: mp_float): TNumeric;
var
  r: mp_float;
begin
  r := ToFloat;
  mpf_sub(r, V, r);
  Result := TBigFloat.Create(r);
end;

function TBigInt.MulInt(const V: Integer): TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  mp_mul_int(FValue, V, r);
  Result := TBigInt.Create(r);
end;

function TBigInt.MulBigInt(const V: mp_int): TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  mp_mul(FValue, V, r);
  Result := TBigInt.Create(r);
end;

function TBigInt.MulBigFloat(const V: mp_float): TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_mul_mpi(V, FValue, r);
  Result := TBigFloat.Create(r);
end;

function TBigInt.DivInt(const V: Integer): TNumeric;
var
  r: mp_int;
  d: Integer;
begin
  mp_init(r);
  mp_div_int(FValue, V, @r, d);
  Result := ToNumeric(r);
end;

function TBigInt.DivBigInt(const V: mp_int): TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  mp_div(FValue, V, r);
  Result := ToNumeric(r);
end;

function TBigInt.DivBigFloat(const V: mp_float): TNumeric;
var
  r: mp_float;
begin
  r := ToFloat;
  mpf_div(r, V, r);
  Result := TBigFloat.Create(r);
end;

function TBigInt.ModInt(const V: Integer): TNumeric;
var
  r: Integer;
begin
  mp_mod_int(FValue, V, r);
  Result := TInteger.Create(r);
end;

function TBigInt.ModBigInt(const V: mp_int): TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  mp_mod(FValue, V, r);
  Result := ToNumeric(r);
end;

function TBigInt.ModBigFloat(const V: mp_float): TNumeric;
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

function TBigInt.PowInt(const V: Integer): TNumeric;
var
  r: mp_int;
begin
  mp_init_set_int(r, V);
  mp_expt(FValue, r, r);
  Result := TBigInt.Create(r);
end;

function TBigInt.PowBigInt(const V: mp_int): TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  mp_expt(FValue, V, r);
  Result := TBigInt.Create(r);
end;

function TBigInt.PowBigFloat(const V: mp_float): TNumeric;
var
  s, r: mp_float;
begin
  mpf_init2(s, r);
  mpf_set_mpi(s, FValue);
  mpf_expt(s, V, r);
  mpf_clear(s);
  Result := TBigFloat.Create(r);
end;

function TBigInt.BitInt(const BitOp: TBitOp; const V: Integer): TNumeric;
var
  r, t: mp_int;
begin
  mp_init(r);
  mp_init_set_int(t, V);
  case BitOp of
    boAnd:
      mp_and(FValue, t, r);
    boOr:
      mp_or(FValue, t, r);
    boXor:
      mp_xor(FValue, t, r);
  end;
  mp_clear(t);
  Result := ToNumeric(r);
end;

function TBigInt.BitBigInt(const BitOp: TBitOp; const V: mp_int): TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  case BitOp of
    boAnd:
      mp_and(FValue, V, r);
    boOr:
      mp_or(FValue, V, r);
    boXor:
      mp_xor(FValue, V, r);
  end;
  Result := ToNumeric(r);
end;

function TBigInt.BitBigFloat(const BitOp: TBitOp; const V: mp_float): TNumeric;
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

function TBigInt.CmpInt(const V: Integer): Integer;
begin
  Result := mp_cmp_int(FValue, V);
end;

function TBigInt.CmpBigInt(const V: mp_int): Integer;
begin
  Result := mp_cmp(FValue, V);
end;

function TBigInt.CmpBigFloat(const V: mp_float): Integer;
var
  t: mp_float;
begin
  t := ToFloat;
  Result := mpf_cmp(t, V);
  mpf_clear(t);
end;

function TBigInt.IsZero: Boolean;
begin
  Result := mp_is0(FValue);
end;

function TBigInt.Dup(TypeExpand: Boolean): TNumeric;
var
  V: mp_int;
begin
  if TypeExpand then
    Result := TBigFloat.Create(ToFloat)
  else
  begin
    mp_init_copy(V, FValue);
    Result := TBigInt.Create(V);
  end;
end;

function TBigInt.Floor: TNumeric;
begin
  Result := TBigInt.Create(FValue);
end;

function TBigInt.Frac: TNumeric;
begin
  Result := TInteger.Create(0);
end;

function TBigInt.Invert: TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  mp_chs(FValue, r);
  mp_dec(r);
  Result := ToNumeric(r);
end;

function TBigInt.Module: TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  mp_abs(FValue, r);
  Result := TBigInt.Create(r);
end;

function TBigInt.Negative: TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  mp_chs(FValue, r);
  Result := TBigInt.Create(r);
end;

function TBigInt.Shift(Bits: Integer): TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  if Bits > 0 then
    mp_shl(FValue, Bits, r)
  else
    mp_shr(FValue, Abs(Bits), r);
  Result := ToNumeric(r);
end;

function TBigInt.ToUInt64: UInt64;
begin
  mp_to_unsigned_bin_n(FValue, Result, 8);
end;

function TBigInt.ToInt64: Int64;
begin
  Result := (ToUInt64 and $7FFFFFFFFFFFFFFF);
  if FValue.sign = MP_NEG then
    Result := -Result;
end;

function TBigInt.ToFloat: mp_float;
begin
  mpf_init(Result);
  mpf_set_mpi(Result, FValue);
end;

function TBigInt.ToDecimal: string;
begin
  Result := string(mp_adecimal(FValue));
end;

function TBigInt.ToText(Format: TFormatTemplate): string;
var
  r: PAnsiChar;
  w: Integer;
begin
  case Format.Format of
    ofDecimal, ofFixed:
      begin
        w := mp_radix_size(FValue, 10);
        GetMem(r, w);
        mp_toradix(FValue, r, 10);
      end;
    ofHexadecimal:
      begin
        w := mp_radix_size(FValue, 16);
        GetMem(r, w);
        mp_toradix(FValue, r, 16);
      end;
  else
    raise Exception.Create('Unsupported format for integer');
  end;
  Result := UpperCase(string(r));
  FreeMem(r);

  if (Format.Format = ofFixed) and (Format.Precision > 0) then
  begin
    Result := Result + '.';
    for w := 1 to Format.Precision do
      Result := Result + '0';
  end;

  if Format.Format = ofHexadecimal then
  begin
    if FValue.sign = MP_NEG then
      Result := '-0x' + Copy(Result, 2)
    else
      Result := '0x' + Result;
  end;

  while Length(Result) < Format.Width do
    if Format.RightPadded then
      Result := Result + ' '
    else
      Result := ' ' + Result;
end;

procedure TBigInt.Serialize(Stream: TTypedStream);
var
  b: packed array of Byte;
begin
  with Stream do
  begin
    WriteByte(DID_DATA_NUMERIC_BIGINT);
    SetLength(b, mp_unsigned_bin_size(FValue));
    mp_to_unsigned_bin_n(FValue, b[0], Length(b));
    WriteInteger(Length(b));
    Write(b[0], Length(b));
    WriteByte(Byte(FValue.sign = MP_NEG));
  end;
end;

end.
