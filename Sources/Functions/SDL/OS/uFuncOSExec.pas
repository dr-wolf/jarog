unit uFuncOSExec;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncOSExec = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_COMMAND = 0;
    P_WAIT = 1;
  private
    function EnvString(const Separator: string): string;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Executes external application. Throws string exception if application can not be executed.
    @type func(command: string, wait: bool)
    @in command string # Path to executable binary with arguments
    @in wait bool # Waits for application to be closed if true
    * }

implementation

uses SysUtils, {$IFDEF FPC} Process, Classes {$ELSE} Windows {$ENDIF}, uArray, uExceptions, uDataBool,
  uDataString, uOpCode;

function ShellExec(const Command, Env: string; WaitFor: Boolean): string;
{$IFDEF FPC}
var
  P: TProcess;
  s: TStringList;
  i: Integer;
begin
  Result := '';
  try
    P := TProcess.Create(nil);
    s := TStringList.Create;
    s.Delimiter := ' ';
    s.DelimitedText := Command;
    P.Executable := s[0];
    for i := 1 to s.Count - 1 do
      P.Parameters.Add(s[i]);
    s.Free;
    P.Environment.Text := Env;
    if WaitFor then
      P.Options := P.Options + [poWaitOnExit];
    P.Execute;
    P.Free;
  except
    on E: Exception do
      Result := E.Message;
  end
{$ELSE}

var
  si: TStartupInfo;
  pi: TProcessInformation;
begin
  Result := '';
  try
    ZeroMemory(@si, SizeOf(si));
    si.cb := SizeOf(si);
    si.dwFlags := STARTF_USESHOWWINDOW;
    if CreateProcessW(nil, PChar(Command), nil, nil, False, CREATE_UNICODE_ENVIRONMENT, PChar(Env), nil, si, pi) then
    begin
      if WaitFor then
        WaitForSingleObject(pi.hProcess, INFINITE);
      CloseHandle(pi.hThread);
      CloseHandle(pi.hProcess);
    end
    else
      Result := SysErrorMessage(GetLastError());
  except
    on E: Exception do
      Result := E.Message;
  end;
{$ENDIF}
end;

{ TFuncOSExec }

constructor TFuncOSExec.Create;
begin
  inherited Create([MakeString, MakeBool]);
  FFuncUid := FID_OS_EXEC;
end;

function TFuncOSExec.EnvString(const Separator: string): string;
var
  k: TArray<string>;
  i: Integer;
begin
  Result := '';
  k := FCoreContext.Env.Variables.Keys;
  for i := 0 to High(k) do
  begin
    if Result <> '' then
      Result := Result + Separator;
    Result := Result + k[i] + '=' + FCoreContext.Env.Variables[k[i]];
  end;
end;

procedure TFuncOSExec.Execute(Context: TFuncContext);
var
  envstr, emsg: string;
begin
  envstr := EnvString({$IFDEF FPC} #$10 {$ELSE} #0 {$ENDIF});
  emsg := ShellExec(AsString(Context.Data[P_COMMAND]), envstr, AsBool(Context.Data[P_WAIT]));
  if emsg <> '' then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create(emsg));
end;

end.
