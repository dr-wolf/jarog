unit uFuncIOPause;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncIOPause = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Waits till user strikes Enter key.
    @type func()
    * }

implementation

uses uOpCode;

{ TFuncIOPause }

constructor TFuncIOPause.Create;
begin
  inherited Create([]);
  FFuncUid := FID_IO_PAUSE;
end;

procedure TFuncIOPause.Execute(Context: TFuncContext);
var
  s: string;
begin
  FCoreContext.IOProvider.Read(s);
end;

end.
