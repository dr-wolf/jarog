unit uInstructionEach;

interface

uses
  uArray, uInstruction, uExpression, uOp;

type
  TInstructionEach = class(TInstruction)
  public
    constructor Create(List, Item: TExpression; Instruction: TInstruction);
    destructor Destroy; override;
  private
    FList: TExpression;
    FItem: TExpression;
    FInstruction: TInstruction;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uExceptions, uDataStack, uTypeList, uOpPush, uOpMove, uOpEmpty, uOpJit, uOpJump, uOpPop, uOpSplit;

{ TInstructionEach }

constructor TInstructionEach.Create(List, Item: TExpression; Instruction: TInstruction);
begin
  if not(List.ResultType is TTypeList) then
    raise ECodeException.Create(E_EXPECTED_ITERABLE);
  if Item.Group <> egVariable then
    raise ECodeException.Create(E_CONSTANT_ASSIGNMENT);
  if not(List.ResultType.NestedType().Compatible(Item.ResultType)) then
    raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, List.ResultType.NestedType().Hash);
  inherited Create;
  FList := List;
  FItem := Item;
  FInstruction := Instruction;
  FInstruction.SetParent(Self);

  InitTag(TAG_LOOP_BEGIN);
  InitTag(TAG_LOOP_END);
end;

destructor TInstructionEach.Destroy;
begin
  FList.Free;
  FItem.Free;
  FInstruction.Free;
  inherited;
end;

function TInstructionEach.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, [TOpPush.Create(ftLoop)]);
  AppendOp(Result, FList.Compile);
  AppendOp(Result, [TOpSplit.Create, TOpEmpty.Create.WithTag(GetTag(TAG_LOOP_BEGIN)),
    TOpJit.Create(False, GetTag(TAG_LOOP_END))]);
  AppendOp(Result, FItem.Compile);
  AppendOp(Result, [TOpMove.Create(False, False)]);
  AppendOp(Result, FInstruction.Compile);
  AppendOp(Result, [TOpJump.Create(0, GetTag(TAG_LOOP_BEGIN)),
    TOpPop.Create(ftLoop).WithTag(GetTag(TAG_LOOP_END))]);
end;

end.
