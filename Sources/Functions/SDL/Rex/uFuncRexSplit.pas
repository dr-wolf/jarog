unit uFuncRexSplit;

interface

uses
  uFuncRex, uFuncContext;

type
  TFuncRexSplit = class(TFuncRex)
  public
    constructor Create;
  private const
    P_REGEX = 0;
    P_TEXT = 1;
    P_MODIFIERS = 2;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Splits string into list of strings by regular expression.
    @type func(regex: string, text: string -> list(string))
    @in regex string # Regular expression
    @in text string # Source text
    @in modifiers int # Reqular expression modifiers
    @out - list(string) # Splitted text
    * }

implementation

uses Classes, uArray, uData, uDataArray, uDataNumeric, uDataString, uOpCode;

{ TFuncRexSplit }

constructor TFuncRexSplit.Create;
begin
  inherited Create([MakeString, MakeString, MakeInt], MakeList(MakeString));
  FFuncUid := FID_REX_SPLIT;
end;

procedure TFuncRexSplit.Execute(Context: TFuncContext);
var
  r: TArray<TData>;
  s: TStrings;
  i: Integer;
begin
  s := TStringList.Create;
  with InitRex(AsInt(Context.Data[P_MODIFIERS])) do
  begin
    Expression := AsString(Context.Data[P_REGEX]);
    Split(AsString(Context.Data[P_TEXT]), s);
    SetLength(r, s.Count);
    for i := 0 to s.Count - 1 do
      r[i] := TDataString.Create(s[i]);
    Return(TDataArray.Create(r, False));
    Free;
  end;
  s.Free;
end;

end.
