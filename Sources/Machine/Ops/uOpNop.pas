unit uOpNop;

interface

uses
  uOp;

type
  TOpNop = class(TOp)
  public
    constructor Create;
  public
  end;

implementation

uses uOpCode;

{ TOpNop }

constructor TOpNop.Create;
begin
  inherited Create(OID_NOP);
  FBreak := True;
end;

end.
