unit uFuncStrPos;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncStrPos = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_SUBTEXT = 0;
    P_TEXT = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Finds position oof subtext in given text, returns -1 if no subtext found.
    @type func(subtext: string, text: string -> int)
    @in subtext string # Subtext to find in text
    @in text string # Initial text
    @out - int # Position of subtext
    * }

implementation

uses uDataNumeric, uDataString, uOpCode;

{ TFuncStrPos }

constructor TFuncStrPos.Create;
begin
  inherited Create([MakeString, MakeString], MakeInt);
  FFuncUid := FID_STR_POS;
end;

procedure TFuncStrPos.Execute(Context: TFuncContext);
begin
  Return(TDataNumeric.Create(Pos(AsString(Context.Data[P_SUBTEXT]), AsString(Context.Data[P_TEXT])) - 1));
end;

end.
