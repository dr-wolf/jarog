unit uFuncRexReplace;

interface

uses
  uFuncRex, uFuncContext;

type
  TFuncRexReplace = class(TFuncRex)
  public
    constructor Create;
  private const
    P_REGEX = 0;
    P_TEXT = 1;
    P_REPLACEMENT = 2;
    P_MODIFIERS = 3;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Replaces all matched substring with given one.
    @type func(regex: string, text: string, replacement: string -> string)
    @in regex string # Regular expression to test
    @in text string # Source text
    @in replacement string # Replacement
    @in modifiers int # Reqular expression modifiers
    @out - string # Text after replace
    * }

implementation

uses uDataString, uDataNumeric, uOpCode;

{ TFuncRexReplace }

constructor TFuncRexReplace.Create;
begin
  inherited Create([MakeString, MakeString, MakeString, MakeInt], MakeString);
  FFuncUid := FID_REX_REPLACE;
end;

procedure TFuncRexReplace.Execute(Context: TFuncContext);
begin
  with InitRex(AsInt(Context.Data[P_MODIFIERS])) do
  begin
    Expression := AsString(Context.Data[P_REGEX]);
    Return(TDataString.Create(Replace(AsString(Context.Data[P_TEXT]), AsString(Context.Data[P_REPLACEMENT]), False)));
    Free;
  end;
end;

end.
