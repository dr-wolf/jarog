unit uFuncTCPAccept;

interface

uses
  uFuncTCP, uFuncContext, BlckSock;

type
  TFuncTCPAccept = class(TFuncTCP)
  public
    constructor Create;
  private const
    P_SOCKET = 0;
    P_TIMEOUT = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Accepts incoming connection on listening socket.
    @type func(socket: int, timeout: int -> int)
    @in socket int # Listening socket handle
    @in timeout int # Time waiting for incoming connection
    @out - int # Handle of socket with connected client or 0
    * }

implementation

uses uDataNumeric, uDataString, uExceptions, SynSock, uOpCode;

{ TFuncTCPAccept }

constructor TFuncTCPAccept.Create;
begin
  inherited Create([MakeInt, MakeInt], MakeInt);
  FFuncUid := FID_TCP_ACCEPT;
end;

procedure TFuncTCPAccept.Execute(Context: TFuncContext);
var
  sock, client: TTCPBlockSocket;
  cs: TSocket;
  r: Integer;
begin
  sock := RetrieveSocket(P_SOCKET, Context);
  if sock.CanRead(AsInt(Context.Data[P_TIMEOUT])) then
  begin
    cs := sock.Accept;
    if sock.LastError = 0 then
    begin
      client := TTCPBlockSocket.Create;
      client.socket := cs;
      client.GetSins;
      r := FCoreContext.Repos.TCPSocks.Add(client);
    end
    else
      raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create(sock.LastErrorDesc));
  end
  else
    r := 0;
  Return(TDataNumeric.Create(r));
end;

end.
