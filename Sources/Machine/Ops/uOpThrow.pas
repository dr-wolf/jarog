unit uOpThrow;

interface

uses
  uOp;

type
  TOpThrow = class(TOp)
  public
    constructor Create;
  end;

implementation

uses uOpCode;

{ TOpThrow }

constructor TOpThrow.Create;
begin
  inherited Create(OID_THROW);
end;

end.
