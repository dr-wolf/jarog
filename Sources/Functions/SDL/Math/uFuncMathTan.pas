unit uFuncMathTan;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncMathTan = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_VALUE = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Retruns tangent of the angle.
    @type func(value: float -> float)
    @in value float # Angle given in radians
    @out - float # Tangent
    * }

implementation

uses uDataNumeric, uOpCode, uBigFloat, mp_types, mp_real;

{ TFuncMathTan }

constructor TFuncMathTan.Create;
begin
  inherited Create([MakeFloat], MakeFloat);
  FFuncUid := FID_MATH_TAN;
end;

procedure TFuncMathTan.Execute(Context: TFuncContext);
var
  v, r: mp_float;
begin
  mpf_init(r);
  v := AsNumeric(Context.Data[P_VALUE]).ToFloat;
  mpf_tan(v, r);
  mpf_clear(v);
  Return(TDataNumeric.Create(TBigFloat.Create(r)));
end;

end.
