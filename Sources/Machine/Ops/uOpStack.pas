unit uOpStack;

interface

uses
  uArray, uOp, uBytecode;

type
  TOpStack = class(TOp)
  public
    constructor Create(ArrayType: TArrayType; Size: Integer);
  private
    FType: TArrayType;
    FSize: Integer;
  public
    procedure Serialize(var Code: TBytecode); override;
  end;

implementation

uses uOpCode;

{ TOpStack }

constructor TOpStack.Create(ArrayType: TArrayType; Size: Integer);
begin
  inherited Create(OID_STACK);
  FType := ArrayType;
  FSize := Size;
end;

procedure TOpStack.Serialize(var Code: TBytecode);
begin
  Code.Op := CompileOp();
  Code.SParam := Byte(FType);
  Code.LParam := FSize;
end;

end.
