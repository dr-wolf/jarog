unit uDataSet;

interface

uses
  uArray, uOperations, uData, uExecFile, uTypedStream;

type
  TDataSet = class(TData)
  public
    constructor Create; overload;
    constructor Create(const Items: array of UInt64); overload;
    destructor Destroy; override;
  private
    FItems: TArray<UInt64>;
    function Find(Data: UInt64): Integer;
  public
    function Apply(Op: TBinaryOp; const Data: TData): TData; override;
    function Apply(Op: TUnaryOp): TData; override;
    function IsEqual(const Data: TData): Boolean; override;
    function Dup: TData; override;
    function Count: Integer; override;
    function Serialize(const ExecFile: TExecFile): TTypedStream; override;
    procedure AddItem(const Item: TData);
    procedure Assign(const Data: TData); override;
    procedure Convert(C: TConverter); override;
  end;

implementation

uses uExceptions, uDataNumeric, uDataBool, uOpCode, uHashConverter;

type
  TAssoc = (aLeft, aMiddle, aRight);
  TAssocSet = set of TAssoc;
  TIntersection = (iNone, iPartial, iFull);

function Merge(Left, Right: TArray<UInt64>; Assoc: TAssocSet): TArray<UInt64>;

  function Pink(var N: Integer; A: Integer = 1): Integer;
  begin
    Result := N;
    Inc(N, A);
  end;

var
  L, R, N: Integer;
begin
  L := 0;
  R := 0;
  N := 0;
  SetLength(Result, Length(Left) + Length(Right));
  while (L <= High(Left)) and (R <= High(Right)) do
    if Left[L] = Right[R] then
    begin
      if aMiddle in Assoc then
        Result[Pink(N)] := Left[L];
      Inc(L);
      Inc(R);
    end else if Left[L] < Right[R] then
    begin
      if aLeft in Assoc then
        Result[Pink(N)] := Left[L];
      Inc(L);
    end else begin
      if aRight in Assoc then
        Result[Pink(N)] := Right[R];
      Inc(R);
    end;
  if (aLeft in Assoc) and (Length(Left) > L) then
    Move(Left[L], Result[Pink(N, Length(Left) - L)], (Length(Left) - L) * SizeOf(UInt64));
  if (aRight in Assoc) and (Length(Right) > R) then
    Move(Right[R], Result[Pink(N, Length(Right) - R)], (Length(Right) - R) * SizeOf(UInt64));
  SetLength(Result, N);
end;

function Intersect(Left, Right: TArray<UInt64>): TIntersection;
var
  L, R: Integer;
  none, full: Boolean;
begin
  L := 0;
  R := 0;
  none := True;
  full := True;
  while (L <= High(Left)) and (R <= High(Right)) do
  begin
    if Left[L] = Right[R] then
    begin
      Inc(L);
      Inc(R);
      none := False;
    end else if Left[L] < Right[R] then
    begin
      Inc(L);
      full := False;
    end
    else
      Inc(R);
  end;
  full := full and (L > High(Left));
  if full then
    Result := iFull
  else if none then
    Result := iNone
  else
    Result := iPartial;
end;

{ TDataSet }

constructor TDataSet.Create;
begin
  inherited Create;
  SetLength(FItems, 0);
end;

constructor TDataSet.Create(const Items: array of UInt64);
var
  I: Integer;
begin
  SetLength(FItems, Length(Items));
  for I := 0 to High(Items) do
    FItems[I] := Items[I];
end;

destructor TDataSet.Destroy;
begin
  FItems := nil;
  inherited;
end;

procedure TDataSet.AddItem(const Item: TData);
var
  I, N: Integer;
  C: THashConverter;
begin
  C := THashConverter.Create;
  try
    Item.Convert(C);
    N := Find(C.CRC64);
    if (N < Length(FItems)) and (FItems[N] = C.CRC64) then
      Exit;
    SetLength(FItems, Length(FItems) + 1);
    for I := High(FItems) downto N + 1 do
      FItems[I] := FItems[I - 1];
    FItems[N] := C.CRC64;
  finally
    C.Free;
  end;
end;

function TDataSet.Apply(Op: TBinaryOp; const Data: TData): TData;
begin
  case Op of
    boAddition:
      begin
        Result := TDataSet.Create;
        (Result as TDataSet).FItems := Merge(FItems, (Data as TDataSet).FItems, [aLeft, aMiddle, aRight]);
      end;
    boMultiplication:
      begin
        Result := TDataSet.Create;
        (Result as TDataSet).FItems := Merge(FItems, (Data as TDataSet).FItems, [aMiddle]);
      end;
    boDivision:
      begin
        Result := TDataSet.Create;
        (Result as TDataSet).FItems := Merge(FItems, (Data as TDataSet).FItems, [aLeft]);
      end;
    boLess:
      Result := TDataBool.Create(Intersect(FItems, (Data as TDataSet).FItems) = iFull);
    boGreater:
      Result := TDataBool.Create(Intersect((Data as TDataSet).FItems, FItems) = iFull);
    boEqual:
      Result := TDataBool.Create(IsEqual(Data));
    boNotEqual:
      Result := TDataBool.Create(Intersect(FItems, (Data as TDataSet).FItems) = iNone);
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  end;
end;

function TDataSet.Apply(Op: TUnaryOp): TData;
begin
  if Op = uoLength then
    Result := TDataNumeric.Create(Length(FItems))
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

procedure TDataSet.Assign(const Data: TData);
var
  I: Integer;
begin
  SetLength(FItems, Length((Data as TDataSet).FItems));
  for I := 0 to High(FItems) do
    FItems[I] := (Data as TDataSet).FItems[I];
end;

function TDataSet.Dup: TData;
var
  I: Integer;
begin
  Result := TDataSet.Create();
  SetLength((Result as TDataSet).FItems, Length(FItems));
  for I := 0 to High(FItems) do
    (Result as TDataSet).FItems[I] := FItems[I];
end;

procedure TDataSet.Convert(C: TConverter);
begin
  C.ConvertSet(FItems);
end;

function TDataSet.Count: Integer;
begin
  Result := Length(FItems);
end;

function TDataSet.Find(Data: UInt64): Integer;
var
  s, f, m: Integer;
begin
  s := 0;
  f := High(FItems);
  while s <= f do
  begin
    m := s + (f - s) div 2;
    if FItems[m] = Data then
    begin
      Result := m;
      Exit;
    end else if FItems[m] > Data then
      f := m - 1
    else
      s := m + 1;
  end;
  Result := s;
end;

function TDataSet.IsEqual(const Data: TData): Boolean;
var
  I: Integer;
begin
  Result := Count = Data.Count;
  if Result then
    for I := 0 to High(FItems) do
    begin
      Result := Result and (FItems[I] = (Data as TDataSet).FItems[I]);
      if not Result then
        Exit;
    end;
end;

function TDataSet.Serialize(const ExecFile: TExecFile): TTypedStream;
var
  I: Integer;
begin
  Result := TTypedStream.Create;
  with Result do
  begin
    WriteByte(DID_DATA_SET);
    WriteInteger(Length(FItems));
    for I := 0 to High(FItems) do
      Write(FItems[I], 8);
  end;
end;

end.
