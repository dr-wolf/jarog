unit uDataFunc;

interface

uses
  uArray, uOperations, uData, uFunction, uState, uClosure, uExecFile, uTypedStream;

type
  TDataFunc = class(TData)
  public
    constructor Create; overload;
    constructor Create(Func: IFunction); overload;
    destructor Destroy; override;
  private
    FFunc: IFunction;
    FClosures: TArray<IClosure>;
  public
    function Apply(Op: TBinaryOp; const Data: TData): TData; override;
    function Dup: TData; override;
    function IsEqual(const Data: TData): Boolean; override;
    function Serialize(const ExecFile: TExecFile): TTypedStream; override;
    procedure Assign(const Data: TData); override;
    procedure AddClosures(const Closures: TArray<TData>);
    procedure Call(const State: TState);
    procedure SetFunc(Func: IFunction);
    procedure Convert(C: TConverter); override;
  end;

implementation

uses uExceptions, uDataBool, uOpCode;

{ TDataFunc }

constructor TDataFunc.Create;
begin
  inherited Create;
  FFunc := nil;
  SetLength(FClosures, 0);
end;

constructor TDataFunc.Create(Func: IFunction);
begin
  inherited Create;
  FFunc := Func;
  SetLength(FClosures, 0);
end;

destructor TDataFunc.Destroy;
begin
  FFunc := nil;
  FClosures := nil;
  inherited;
end;

procedure TDataFunc.AddClosures(const Closures: TArray<TData>);
var
  i, p: Integer;
begin
  p := High(FClosures) + 1;
  SetLength(FClosures, Length(FClosures) + Length(Closures));
  for i := 0 to High(Closures) do
    FClosures[p + i] := TClosure.Create(Closures[i]);
end;

function TDataFunc.Apply(Op: TBinaryOp; const Data: TData): TData;
begin
  case Op of
    boEqual:
      Result := TDataBool.Create(IsEqual(Data));
    boNotEqual:
      Result := TDataBool.Create(not IsEqual(Data));
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  end
end;

procedure TDataFunc.Assign(const Data: TData);
var
  i: Integer;
begin
  FFunc := (Data as TDataFunc).FFunc;
  SetLength(FClosures, Length((Data as TDataFunc).FClosures));
  for i := 0 to High(FClosures) do
    FClosures[i] := (Data as TDataFunc).FClosures[i];
end;

procedure TDataFunc.Call(const State: TState);
begin
  FFunc.Call(State, Self, FClosures);
end;

function TDataFunc.Dup: TData;
var
  R: TDataFunc;
  i: Integer;
begin
  R := TDataFunc.Create(FFunc);
  SetLength(R.FClosures, Length(FClosures));
  for i := 0 to High(FClosures) do
    R.FClosures[i] := FClosures[i];
  Result := R;
end;

procedure TDataFunc.Convert(C: TConverter);
var
  i: Integer;
  Closures: TArray<TData>;
begin
  SetLength(Closures, Length(FClosures));
  for i := 0 to High(FClosures) do
    Closures[i] := FClosures[i].GetData;
  C.ConvertFunc(TObject(FFunc), Closures);
end;

function TDataFunc.IsEqual(const Data: TData): Boolean;
var
  i: Integer;
begin
  Result := FFunc = (Data as TDataFunc).FFunc;
  for i := 0 to High(FClosures) do
  begin
    Result := Result and FClosures[i].GetData.IsEqual((Data as TDataFunc).FClosures[i].GetData);
    if not Result then
      Break;
  end;
end;

function TDataFunc.Serialize(const ExecFile: TExecFile): TTypedStream;
var
  i: Integer;
begin
  Result := TTypedStream.Create;
  with Result do
  begin
    WriteByte(DID_DATA_FUNC);
    WriteInteger(ExecFile.Save(FFunc as TFunction, (FFunc as TFunction).Serialize));
    WriteInteger(Length(FClosures));
    for i := 0 to High(FClosures) do
      WriteInteger(ExecFile.Save((FClosures[i] as TData), (FClosures[i] as TData).Serialize));
  end;
end;

procedure TDataFunc.SetFunc(Func: IFunction);
begin
  FFunc := Func;
end;

end.
