unit uFuncUDPClose;

interface

uses
  uFuncUDP, uFuncContext, BlckSock;

type
  TFuncUDPClose = class(TFuncUDP)
  public
    constructor Create;
  private const
    P_SOCKET = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Unbinds and closes a socket.
    @type func(socket: int)
    @in socket int # Socket handle
    * }

implementation

uses uDataNumeric, uOpCode;

{ TFuncUDPClose }

constructor TFuncUDPClose.Create;
begin
  inherited Create([MakeInt]);
  FFuncUid := FID_UDP_CLOSE;
end;

procedure TFuncUDPClose.Execute(Context: TFuncContext);
begin
  FCoreContext.Repos.UDPSocks.Close(AsInt(Context.Data[P_SOCKET]));
end;

end.
