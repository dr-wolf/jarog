unit uOpTail;

interface

uses
  uOp;

type
  TOpTail = class(TOp)
  public
    constructor Create;
  end;

implementation

uses uOpCode;

{ TOpTail }

constructor TOpTail.Create;
begin
  inherited Create(OID_TAIL);
end;

end.
