unit uExceptions;

interface

uses
  SysUtils, uData;

const
  E_MISSING_UNIT = 1001;
  E_UNKNOWN_UNIT = 1002;
  E_CIRCULAR_IMPORT = 1003;

  E_SYNTAX_ERROR = 2001;
  E_UNEXPECTED_CHARACTER = 2002;
  E_UNEXPECTED_KEYWORD = 2003;
  E_UNTERMINATED_STRING = 2004;
  E_MISSING_SEMICOLON = 2005;
  E_UNEXPECTED_END = 2006;

  E_EXPECTED_LIST = 2011;
  E_EXPECTED_ITERABLE = 2012;
  E_EXPECTED_INTEGER = 2013;
  E_EXPECTED_STRING = 2014;
  E_EXPECTED_BOOL = 2015;
  E_EXPECTED_PIPE = 2019;
  E_EXPECTED_STATIC = 2016;
  E_EXPECTED_IDENTIFIER = 2017;
  E_EXPECTED_STATEMENT = 2018;

  E_UNSUPPORTED_OPERATION = 2021;
  E_ATOMIC_VALUE = 2022;
  E_CONSTANT_ASSIGNMENT = 2023;
  E_NO_RESULT = 2024;

  E_TYPE_UNDECLARED = 3001;
  E_TYPE_ATOMIC = 3002;
  E_TYPE_UNCOMPATIBLE = 3003;
  E_TYPE_CAST = 3004;

  E_IDENTIFIER_UNDECLARED = 4001;
  E_IDENTIFIER_REDECLARED = 4002;

  E_FIELD_UNDECLARED = 4011;
  E_FIELD_REDECLARED = 4012;
  E_STRUCT_MIXED = 4013;

  E_LIST_BOUNDS = 5001;

  E_GLOBAL_CLOSURE = 6001;
  E_NOT_CALLABLE = 6002;
  E_ARG_MISMATCH = 6003;

  E_CATCH_REDECLARED = 7001;

  E_DIVIZION_ZERO = 8001;
  E_PARSE_ERROR = 8002;
  E_INT_OVERFLOW = 8003;

  E_RUNTIME_ERROR = 9001;

type
  ECodeException = class(Exception)
  public
    constructor Create(Code: Integer; const Fragment: string = ''; Line: Integer = 0); overload;
    constructor Create(Code, Line: Integer); overload;
    constructor Create(E: ECodeException; const FileName: string; Line: Integer); overload;
  private
    FCode: Integer;
    FFragment: string;
    FFileName: string;
    FLine: Integer;
  end;

type
  ERuntimeException = class(Exception)
  public
    constructor Create(Code: Integer; Data: TData); overload;
    constructor Create(const Error: string); overload;
  private
    FCode: Integer;
    FData: TData;
  public
    property Code: Integer read FCode;
    property Data: TData read FData;
  end;

implementation

uses uDataString, uStringConverter;

function DescribeError(Code: Integer): string;
begin
  case Code of
    E_MISSING_UNIT:
      Result := 'Can not load unit "%s"';
    E_UNKNOWN_UNIT:
      Result := 'Unit "%s" is not loaded';
    E_CIRCULAR_IMPORT:
      Result := 'Circular import';
    E_SYNTAX_ERROR:
      Result := 'Syntax error';
    E_UNEXPECTED_CHARACTER:
      Result := 'Unexpected character "%s"';
    E_UNEXPECTED_KEYWORD:
      Result := 'Unexpected keyword "%s"';
    E_UNTERMINATED_STRING:
      Result := 'Unterminated string';
    E_MISSING_SEMICOLON:
      Result := 'Missing semicolon';
    E_UNEXPECTED_END:
      Result := 'Unexpected end of file';
    E_EXPECTED_LIST:
      Result := 'List value is expected';
    E_EXPECTED_ITERABLE:
      Result := 'Iterable value is expected';
    E_EXPECTED_INTEGER:
      Result := 'Integer value is expected';
    E_EXPECTED_STATIC:
      Result := 'Static value is expected';
    E_EXPECTED_IDENTIFIER:
      Result := 'Identifier is expected';
    E_EXPECTED_STATEMENT:
      Result := 'Statement expected';
    E_EXPECTED_BOOL:
      Result := 'Boolean value expected';
    E_EXPECTED_PIPE:
      Result := 'Pipe expected';
    E_UNSUPPORTED_OPERATION:
      Result := 'Unsupported operation for %s';
    E_ATOMIC_VALUE:
      Result := 'Value has no fields or items';
    E_CONSTANT_ASSIGNMENT:
      Result := 'Can not assign value to constant';
    E_NO_RESULT:
      Result := 'Expression returns nothing';
    E_TYPE_UNDECLARED:
      Result := 'Type "%s" is not declared';
    E_TYPE_ATOMIC:
      Result := 'Atomic type';
    E_TYPE_UNCOMPATIBLE:
      Result := 'Uncompatible type with "%s"';
    E_TYPE_CAST:
      Result := 'Type cast error';
    E_IDENTIFIER_UNDECLARED:
      Result := 'Identifier "%s" is not declared';
    E_IDENTIFIER_REDECLARED:
      Result := 'Identifier "%s" is already declared';
    E_FIELD_UNDECLARED:
      Result := 'Property "%s" is not declared';
    E_FIELD_REDECLARED:
      Result := 'Property "%s" is already declared';
    E_STRUCT_MIXED:
      Result := 'Struct is mixed';
    E_LIST_BOUNDS:
      Result := 'List item does not exists';
    E_GLOBAL_CLOSURE:
      Result := 'Global value can not be scoped';
    E_NOT_CALLABLE:
      Result := 'Value can not be called';
    E_ARG_MISMATCH:
      Result := 'Arguments type mismatch';
    E_CATCH_REDECLARED:
      Result := 'Catch clause "%s" redeclared';
    E_DIVIZION_ZERO:
      Result := 'Division by zero';
    E_PARSE_ERROR:
      Result := 'String can not be parsed into numeric';
    E_INT_OVERFLOW:
      Result := 'Integer type overflow';
    E_RUNTIME_ERROR:
      Result := 'Runtime Error';
  else
    Result := 'Unknown error';
  end;
end;

{ ECodeExcetion }

constructor ECodeException.Create(Code: Integer; const Fragment: string = ''; Line: Integer = 0);
begin
  FCode := Code;
  FFragment := Fragment;
  FFileName := '';
  FLine := Line;
  inherited Create(Format('E%.4d: %s at #%d', [FCode, Format(DescribeError(FCode), [FFragment]), FLine]));
end;

constructor ECodeException.Create(Code, Line: Integer);
begin
  FCode := Code;
  FFragment := '';
  FFileName := '';
  FLine := Line;
  inherited Create(Format('E%.4d: %s at %s#%d', [FCode, Format(DescribeError(FCode), ['']), '', FLine]));
end;

constructor ECodeException.Create(E: ECodeException; const FileName: string; Line: Integer);
begin
  FCode := E.FCode;
  FFragment := E.FFragment;
  if E.FFileName = '' then
    FFileName := FileName
  else
    FFileName := E.FFileName;
  if E.FLine = 0 then
    FLine := Line
  else
    FLine := E.FLine;
  inherited Create(Format('E%.4d: %s at #%d', [FCode, Format(DescribeError(FCode), [FFragment]), FLine]));
end;

{ ERuntimeException }

constructor ERuntimeException.Create(Code: Integer; Data: TData);
begin
  if Data <> nil then
    inherited Create(DescribeError(Code) + ': ' + LogData(Data))
  else
    inherited Create(DescribeError(Code));
  FCode := Code;
  if Data <> nil then
    FData := Data
  else
    FData := TDataString.Create(DescribeError(Code));
end;

constructor ERuntimeException.Create(const Error: string);
begin
  inherited Create('RUNTIME ERROR: ' + Error);
  FCode := E_RUNTIME_ERROR;
  FData := TDataString.Create(Error);
end;

end.
