rmdir %TEMP%\jarog /S /Q
rmdir target /S /Q
xcopy Examples\demo target\demos /s /e /i
xcopy  Examples\libs target\units /s /e /i
mkdir %TEMP%\jarog
cd Binaries\Jacog
fpc -Mdelphi -Twin32 -Ur -O3 -FE..\..\target -FU%TEMP%\jarog jacog.dpr
cd ..\Jarog
fpc -Mdelphi -Twin32 -Ur -O3 -FE..\..\target -FU%TEMP%\jarog jarog.dpr

pause