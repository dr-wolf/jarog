unit uFuncIOKey;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncIOKey = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_WAIT = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Retruns first pressed key on keyboard.
    @type func(wait: bool -> int)
    @out - int # Key code
    * }

implementation

uses uDataBool, uDataNumeric, uOpCode;

{ TFuncIOKey }

constructor TFuncIOKey.Create;
begin
  inherited Create([MakeBool], MakeInt);
  FFuncUid := FID_IO_KEY;
end;

procedure TFuncIOKey.Execute(Context: TFuncContext);
begin
  Return(TDataNumeric.Create(FCoreContext.IOProvider.Key(AsBool(Context.Data[P_WAIT]))));
end;

end.
