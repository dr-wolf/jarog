unit uInstructionAssign;

interface

uses
  uArray, uInstruction, uExpression, uOp;

type
  TInstructionAssign = class(TInstruction)
  public
    constructor Create(Variable: TExpression; Value: TExpression);
    destructor Destroy; override;
  private
    FVariable: TExpression;
    FValue: TExpression;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uExceptions, uExpressionStruct, uTypeVoid, uOpMove, uOpClean;

{ TInstructionAssign }

constructor TInstructionAssign.Create(Variable: TExpression; Value: TExpression);
begin
  if (Variable.ResultType is TTypeVoid) or (Value.ResultType is TTypeVoid) then
    raise ECodeException.Create(E_NO_RESULT);
  if not Variable.ResultType.Compatible(Value.ResultType) then
    raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, FVariable.ResultType.Hash);
  if (Variable is TExpressionStruct) and not TExpressionStruct(Variable).Nameless then
    raise ECodeException.Create(E_CONSTANT_ASSIGNMENT);
  if Variable.Group <> egVariable then
    raise ECodeException.Create(E_CONSTANT_ASSIGNMENT);
  inherited Create;
  FVariable := Variable;
  FValue := Value;
end;

function TInstructionAssign.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FValue.Compile);
  AppendOp(Result, FVariable.Compile);
  AppendOp(Result, [TOpMove.Create(False, False), TOpClean.Create]);
end;

destructor TInstructionAssign.Destroy;
begin
  FVariable.Free;
  FValue.Free;
  inherited;
end;

end.
