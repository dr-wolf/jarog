unit uFuncDateDecode;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncDateDecode = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_UNIX = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Decodes unix date to its components.
    @type func(unix: int -> _date)
    @in unix int # Unix timestamp
    @out - _date # Struct with date and time components
    * }

implementation

uses SysUtils, uDataArray, uDataNumeric, uOpCode;

{ TFuncDateDecode }

constructor TFuncDateDecode.Create;
begin
  inherited Create([MakeInt], MakeStruct([MakeInt, MakeInt, MakeInt, MakeInt, MakeInt, MakeInt]));
  FFuncUid := FID_DATE_DECODE;
end;

procedure TFuncDateDecode.Execute(Context: TFuncContext);
var
  ms: Word;
  c: array [0 .. 5] of Word;
  d: TDateTime;
begin
  d := (AsInt(Context.Data[P_UNIX]) / 86400) + 25569;
  DecodeDate(d, c[0], c[1], c[2]);
  DecodeTime(d, c[3], c[4], c[5], ms);
  Return(TDataArray.Create([TDataNumeric.Create(c[0]), TDataNumeric.Create(c[1]), TDataNumeric.Create(c[2]),
    TDataNumeric.Create(c[3]), TDataNumeric.Create(c[4]), TDataNumeric.Create(c[5])]));
end;

end.
