unit uBuilder;

interface

uses
  uValue, uSourceParser, uResolver, uIdentifierRegistry, uAppMold;

type
  TValueSection = (vsParam, vsClosure, vsLocal);

type
  TBuilder = class(TObject)
  public
    constructor Create(AppMold: TAppMold);
    destructor Destroy; override;
  protected
    FRegistry: TIdentifierRegistry;
    FAppMold: TAppMold;
    function MakeResolver: IResolver; virtual; abstract;
    function SaveValue(Name: string; Value: IValue; Section: TValueSection): Integer; virtual; abstract;
    procedure ProcessConst(Tokens: TTokenStream);
    procedure ProcessVariable(Tokens: TTokenStream);
  end;

implementation

uses uArray, uType, uExceptions, uExpression, uExpressionBuilder, uTypeBuilder;

{ TBuilder }

constructor TBuilder.Create(AppMold: TAppMold);
begin
  FRegistry := TIdentifierRegistry.Create;
  FAppMold := AppMold;
end;

destructor TBuilder.Destroy;
begin
  FRegistry.Free;
  inherited;
end;

procedure TBuilder.ProcessConst(Tokens: TTokenStream);
var
  identifier: TToken;
  e: TExpression;
begin
  Tokens.Test(kwConst, E_SYNTAX_ERROR); // Guard, never fails
  identifier := Tokens.Fetch(tkIdentifier);
  Tokens.Test(sSet, E_SYNTAX_ERROR);
  e := BuildExpression(Tokens, MakeResolver, FAppMold, nil);
  try
    if e.Group <> egStatic then
      raise ECodeException.Create(E_EXPECTED_STATIC, identifier.Value, identifier.Index);
    if FRegistry.Add(identifier.Value) then
      SaveValue(identifier.Value, e.Eval, vsLocal)
    else
      raise ECodeException.Create(E_IDENTIFIER_REDECLARED, identifier.Value, identifier.Index);
  finally
    e.Free;
  end;
  Tokens.Test(sSemicolon, E_MISSING_SEMICOLON);
end;

procedure TBuilder.ProcessVariable(Tokens: TTokenStream);
var
  identifier: TToken;
  i: Integer;
  t: TType;
  e: TExpression;
  n: TArray<string>;
  v: TArray<IValue>;
  s: TValueSection;
begin
  case ToKword(Tokens.Pop.Value) of
    kwVar:
      s := vsLocal;
    kwArg:
      s := vsParam;
  else
    raise ECodeException.Create(E_SYNTAX_ERROR, '');
  end;

  SetLength(n, 0);
  repeat
    identifier := Tokens.Fetch(tkIdentifier);
    if FRegistry.Add(identifier.Value) then
    begin
      SetLength(n, Length(n) + 1);
      n[High(n)] := identifier.Value;
    end
    else
      raise ECodeException.Create(E_IDENTIFIER_REDECLARED, identifier.Value, identifier.Index);
    if not Eq(Tokens.Token, sComma) then
      Break;
    Tokens.Skip;
  until False;

  Tokens.Test(sColon, E_SYNTAX_ERROR);
  t := BuildType(Tokens, MakeResolver);

  if Eq(Tokens.Token, sSet) then
  begin
    Tokens.Skip;
    SetLength(v, 0);
    repeat
      i := Tokens.Token.Index;
      e := BuildExpression(Tokens, MakeResolver, FAppMold, nil);
      try
        if e.Group <> egStatic then
          raise ECodeException.Create(E_EXPECTED_STATIC, i);
        if not t.Compatible(e.ResultType) then
          raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, t.Hash, i);
        SetLength(v, Length(v) + 1);
        v[High(v)] := TValue.Create(t, t.Default, False);
        v[High(v)].Data.Assign(e.Eval.Data);
      finally
        e.Free;
      end;
      if not Eq(Tokens.Token, sComma) then
        Break;
      Tokens.Skip;
    until False;
    if Length(n) <> Length(v) then
      raise ECodeException.Create(E_SYNTAX_ERROR, i);
    for i := 0 to High(n) do
      SaveValue(n[i], v[i], s);
  end
  else
    for i := 0 to High(n) do
      SaveValue(n[i], TValue.Create(t), s);

  Tokens.Test(sSemicolon, E_MISSING_SEMICOLON);
end;

end.
