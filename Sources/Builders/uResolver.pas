unit uResolver;

interface

uses
  uType, uValue;

type
  TValueResolver = function(const Name, UnitName: string): IValue of object;
  TTypeResolver = function(const Name, UnitName: string): TType of object;

type
  IResolver = interface(IInterface)
    function ResolveType(Name: string; UnitName: string = ''): TType;
    function ResolveValue(var IsGlobal: Boolean; const Name: string; const UnitName: string = ''): IValue;
    function ResolveClosure(const Name: string): IValue;
    function GoDown(LocalResolver: TValueResolver): IResolver;
  end;

type
  TResolver = class(TInterfacedObject, IResolver)
  public
    constructor Create(TypeResolver: TTypeResolver; GlobalResolver, LocalResolver: TValueResolver);
  private
    FTypeResolver: TTypeResolver;
    FValueGlobalResolver: TValueResolver;
    FValueClosureResolver: TValueResolver;
    FValueLocalResolver: TValueResolver;
  public
    function ResolveType(Name: string; UnitName: string = ''): TType;
    function ResolveValue(var IsGlobal: Boolean; const Name: string; const UnitName: string = ''): IValue;
    function ResolveClosure(const Name: string): IValue;
    function GoDown(LocalResolver: TValueResolver): IResolver;
  end;

implementation

uses uExceptions;

{ TResolver }

constructor TResolver.Create(TypeResolver: TTypeResolver; GlobalResolver, LocalResolver: TValueResolver);
begin
  FTypeResolver := TypeResolver;
  FValueGlobalResolver := GlobalResolver;
  FValueLocalResolver := LocalResolver;
  FValueClosureResolver := nil;
end;

function TResolver.GoDown(LocalResolver: TValueResolver): IResolver;
var
  R: TResolver;
begin
  R := TResolver.Create(FTypeResolver, FValueGlobalResolver, LocalResolver);
  R.FValueClosureResolver := FValueLocalResolver;
  Result := R;
end;

function TResolver.ResolveClosure(const Name: string): IValue;
begin
  if Assigned(FValueClosureResolver) then
  begin
    Result := FValueClosureResolver(Name, '');
    if Result = nil then
      raise ECodeException.Create(E_IDENTIFIER_UNDECLARED, Name);
  end
  else
    raise ECodeException.Create(E_GLOBAL_CLOSURE);
end;

function TResolver.ResolveType(Name, UnitName: string): TType;
begin
  Result := FTypeResolver(Name, UnitName);
  if Result = nil then
  begin
    if UnitName <> '' then
      Name := UnitName + '::' + Name;
    raise ECodeException.Create(E_TYPE_UNDECLARED, Name);
  end;
end;

function TResolver.ResolveValue(var IsGlobal: Boolean; const Name, UnitName: string): IValue;
begin
  if UnitName <> '' then
  begin
    Result := FValueGlobalResolver(Name, UnitName);
    if Result <> nil then
    begin
      IsGlobal := True;
      Result := TValue.Create(Result.TypeInfo, Result.Data.Dup);
    end
    else
      raise ECodeException.Create(E_IDENTIFIER_UNDECLARED, UnitName + '::' + Name);
  end else begin
    Result := nil;
    IsGlobal := False;
    if Assigned(FValueLocalResolver) then
      Result := FValueLocalResolver(Name, '');
    if Result = nil then
    begin
      Result := FValueGlobalResolver(Name, '');
      IsGlobal := True;
    end;
    if Result = nil then
      raise ECodeException.Create(E_IDENTIFIER_UNDECLARED, Name);
  end;
end;

end.
