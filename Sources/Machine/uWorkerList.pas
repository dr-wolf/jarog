unit uWorkerList;

interface

uses
  uArray, uThreadSafe, uWorker;

type
  TWorkerList = class(TThreadObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FActiveWorkers: Integer;
    FWorkers: TArray<TWorker>;
    function GetWorker(I: Integer): TWorker;
  public
    property Worker[I: Integer]: TWorker read GetWorker; default;
    function Count: Integer;
    function AllWorksStopped: Boolean;
    procedure RegisterWorker(Worker: TWorker);
    procedure FireWorker(Worker: TWorker);
  end;

implementation

{ TWorkerList }

constructor TWorkerList.Create;
begin
  inherited Create;
  SetLength(FWorkers, 0);
  FActiveWorkers := 0;
end;

destructor TWorkerList.Destroy;
var
  I: Integer;
begin
  for I := 0 to High(FWorkers) do
    FWorkers[I].Free;
  inherited;
end;

function TWorkerList.AllWorksStopped: Boolean;
begin
  Result := (Length(FWorkers) > 0) and (FActiveWorkers = 0);
end;

function TWorkerList.Count: Integer;
begin
  Result := Length(FWorkers);
end;

procedure TWorkerList.FireWorker(Worker: TWorker);
var
  I: Integer;
begin
  for I := 0 to High(FWorkers) do
    if FWorkers[I] = Worker then
    begin
      FWorkers[I].Free;
      FWorkers[I] := nil;
      Dec(FActiveWorkers);
      Break;
    end;
end;

function TWorkerList.GetWorker(I: Integer): TWorker;
begin
  Result := FWorkers[I];
end;

procedure TWorkerList.RegisterWorker(Worker: TWorker);
var
  I: Integer;
begin
  I := 0;
  while (I <= High(FWorkers)) and (FWorkers[I] <> nil) do
    Inc(I);
  if I > High(FWorkers) then
    SetLength(FWorkers, Length(FWorkers) + 1);
  FWorkers[I] := Worker;
  Inc(FActiveWorkers);
end;

end.
