unit uExecFile;

interface

uses
  Classes, uArray, uCoreContext, uTypedStream;

type
  TFileHeader = array [0 .. 3] of Byte;

const
  HEADER: TFileHeader = ($0, $0, $0, $0);

type
  TExecFile = class(TObject)
  public
    constructor Create(CoreContext: TCoreContext);
    destructor Destroy; override;
  private type
    TClass = record
      Instance: TObject;
      Stream: TTypedStream;
    end;

    TSerializer = function(const ExecFile: TExecFile): TTypedStream of object;
  private
    FTable: TArray<TClass>;
    FOpStream: TTypedStream;
    FCoreContext: TCoreContext;
    function FindId(Instance: TObject): Integer;
    function GetDataSize: Int64;
  public
    property OpStream: TTypedStream read FOpStream;
    property DataSize: Int64 read GetDataSize;
    property CoreContext: TCoreContext read FCoreContext;
    function Load(Id: Integer): TObject;
    function Save(Instance: TObject; Serialize: TSerializer): Integer;
    procedure SaveToStream(Stream: TStream);
    procedure LoadFromStream(Stream: TStream);
  end;

implementation

uses SysUtils, uFunction, uDataLoader, uFunctionLoader, uOpCode;

function HeaderCompare(const H1, H2: TFileHeader): Boolean;
var
  I: Integer;
begin
  Result := True;
  for I := 0 to High(H1) do
  begin
    Result := Result and (H1[I] = H2[I]);
    if not Result then
      Exit;
  end;
end;

{ TExecFile }

constructor TExecFile.Create(CoreContext: TCoreContext);
begin
  SetLength(FTable, 0);
  FOpStream := TTypedStream.Create;
  FCoreContext := CoreContext;
end;

destructor TExecFile.Destroy;
var
  I: Integer;
begin
  for I := 0 to High(FTable) do
    FTable[I].Stream.Free;
  FTable := nil;
  FOpStream.Free;
  inherited;
end;

function TExecFile.FindId(Instance: TObject): Integer;
begin
  Result := High(FTable);
  while (Result >= 0) and (FTable[Result].Instance <> Instance) do
    Dec(Result);
end;

function TExecFile.GetDataSize: Int64;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to High(FTable) do
    Inc(Result, FTable[I].Stream.Size);
end;

procedure TExecFile.LoadFromStream(Stream: TStream);
var
  Table: TTypedStream;
  Sizes: TArray<Int64>;
  h: TFileHeader;
  l: Int64;
  I: Integer;
begin
  Stream.ReadBuffer(h, SizeOf(h));

  if not HeaderCompare(h, HEADER) then
    raise Exception.Create('This file is not Jarog executable file');

  Stream.ReadBuffer(l, SizeOf(l));
  FOpStream.Free;
  FOpStream := TTypedStream.Create(Stream, l);

  Stream.ReadBuffer(l, SizeOf(l));
  Table := TTypedStream.Create(Stream, l);
  SetLength(Sizes, Table.ReadInteger);
  for I := 0 to High(Sizes) do
    Sizes[I] := Table.ReadInteger;
  Table.Free;
  SetLength(FTable, Length(Sizes));
  for I := 0 to High(FTable) do
  begin
    FTable[I].Instance := nil;
    FTable[I].Stream := TTypedStream.Create(Stream, Sizes[I]);
  end;
end;

function TExecFile.Save(Instance: TObject; Serialize: TSerializer): Integer;
begin
  Result := FindId(Instance);
  if Result < 0 then
  begin
    SetLength(FTable, Length(FTable) + 1);
    Result := High(FTable);
    FTable[Result].Instance := Instance;
    FTable[Result].Stream := Serialize(Self);
  end;
end;

procedure TExecFile.SaveToStream(Stream: TStream);
var
  Table: TTypedStream;
  l: Int64;
  I: Integer;
begin
  Stream.WriteBuffer(HEADER, Length(HEADER));

  l := FOpStream.Size;
  Stream.WriteBuffer(l, SizeOf(l));
  FOpStream.SaveTo(Stream);

  Table := TTypedStream.Create;
  Table.WriteInteger(Length(FTable));
  for I := 0 to High(FTable) do
    Table.WriteInteger(FTable[I].Stream.Size);

  l := Table.Size;
  Stream.WriteBuffer(l, SizeOf(l));
  Table.SaveTo(Stream);

  Table.Free;

  for I := 0 to High(FTable) do
    FTable[I].Stream.SaveTo(Stream);

  Stream.Size := Stream.Position;
end;

function TExecFile.Load(Id: Integer): TObject;
begin
  if Id < 0 then
  begin
    Result := nil;
    Exit;
  end;
  if FTable[Id].Instance = nil then
  begin
    case FTable[Id].Stream.PeekByte and $F0 of
      DID_DATA:
        LoadData(FTable[Id].Stream, Self, FTable[Id].Instance);
      DID_FUNCTION:
        begin
          LoadFunction(FTable[Id].Stream, Self, FTable[Id].Instance);
          (FTable[Id].Instance as TFunction).AttachContext(FCoreContext);
        end
    else
      raise Exception.Create('Error Message');
    end;
  end;
  Result := FTable[Id].Instance;
end;

end.
