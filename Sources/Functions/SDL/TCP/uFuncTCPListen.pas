unit uFuncTCPListen;

interface

uses
  uFuncTCP, uFuncContext, BlckSock;

type
  TFuncTCPListen = class(TFuncTCP)
  public
    constructor Create;
  private const
    P_IP = 0;
    P_PORT = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Opens a listening socket on given port and interface.
    @type func(ip: string, port: int -> int)
    @in ip string # Interface to listen to
    @in port int # Listening port
    @out - int # Handle of a socket
    * }

implementation

uses SysUtils, uDataNumeric, uDataString, uExceptions, uOpCode;

{ TFuncTCPListen }

constructor TFuncTCPListen.Create;
begin
  inherited Create([MakeString, MakeInt], MakeInt);
  FFuncUid := FID_TCP_LISTEN;
end;

procedure TFuncTCPListen.Execute(Context: TFuncContext);
var
  ip: string;
  port: Integer;
  sock: TTCPBlockSocket;
begin
  ip := RetrieveIP(P_IP, Context);
  port := RetrievePort(P_PORT, Context);
  sock := TTCPBlockSocket.Create;
  with sock do
  begin
    CreateSocket;
    SetLinger(true, 10000);
    Bind(ip, IntToStr(port));
    Listen;
  end;
  if sock.LastError <> 0 then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create(sock.LastErrorDesc));

  Return(TDataNumeric.Create(FCoreContext.Repos.TCPSocks.Add(sock)));
end;

end.
