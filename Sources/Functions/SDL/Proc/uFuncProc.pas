unit uFuncProc;

interface

uses
  uStandardFunc, uFuncContext, ChildProcess, Classes;

type
  TFuncProc = class(TStandardFunc)
  protected
    function RetrieveProcess(const VarId: Integer; Context: TFuncContext): TChildProcess;
    function RetrieveStream(const VarId: Integer; Context: TFuncContext): TStream;
  end;

implementation

uses uDataNumeric, uDataString, uExceptions;

{ TFuncProc }

function TFuncProc.RetrieveProcess(const VarId: Integer; Context: TFuncContext): TChildProcess;
begin
  Result := FCoreContext.Repos.Processes.Process[AsInt(Context.Data[VarId])];
  if Result = nil then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Invalid process handle!'));
end;

function TFuncProc.RetrieveStream(const VarId: Integer; Context: TFuncContext): TStream;
begin
  Result := FCoreContext.Repos.Files.Stream[AsInt(Context.Data[VarId])];
  if Result = nil then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Invalid file handle!'));
end;

end.
