unit uFuncFileSize;

interface

uses
  uFuncFile, uFuncContext;

type
  TFuncFileSize = class(TFuncFile)
  public
    constructor Create;
  private const
    P_HANDLE = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Returns total file stream length.
    @type func(handle: int -> int)
    @in handle int # Handle of a stream
    @out - int # Length of file
    * }

implementation

uses Classes, uDataNumeric, uOpCode;

{ TFuncFileSize }

constructor TFuncFileSize.Create;
begin
  inherited Create([MakeInt], MakeInt);
  FFuncUid := FID_FILE_SIZE;
end;

procedure TFuncFileSize.Execute(Context: TFuncContext);
var
  s: TStream;
begin
  s := RetrieveStream(P_HANDLE, Context);
  Return(TDataNumeric.Create(s.Size));
end;

end.
