unit uTypeSet;

interface

uses
  uOperations, uType, uData;

type
  TTypeSet = class(TType)
  public
    constructor Create(const Hash: string; ItemType: TType);
  private
    FItemType: TType;
  public
    function Apply(Op: TBinaryOp; T: TType): TType; override;
    function Apply(Op: TUnaryOp): TType; override;
    function Pipeable: Boolean; override;
    function Compatible(T: TType; StrictCompat: Boolean = False): Boolean; override;
    function Default: TData; override;
  end;

implementation

uses uExceptions, uTypeFactory, uDataSet;

{ TTypeSet }

constructor TTypeSet.Create(const Hash: string; ItemType: TType);
begin
  if not((ItemType = nil) or ItemType.Pipeable) then
    raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, ItemType.Hash);
  inherited Create(Hash);
  FItemType := ItemType;
end;

function TTypeSet.Apply(Op: TBinaryOp; T: TType): TType;
begin
  if not Compatible(T, False) then
    raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, FHash);
  case Op of
    boAddition:
      if FItemType = nil then
        Result := T
      else if (T as TTypeSet).FItemType = nil then
        Result := Self
      else
        Result := TypeFactory.GetSet(FItemType.Apply(Op, (T as TTypeSet).FItemType));
    boMultiplication, boDivision:
      Result := Self;
    boEqual .. boGreater:
      Result := TypeFactory.GetBool;
  else
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, FHash);
  end;
end;

function TTypeSet.Apply(Op: TUnaryOp): TType;
begin
  if Op = uoLength then
    Result := TypeFactory.GetInt
  else
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, 'set');
end;

function TTypeSet.Compatible(T: TType; StrictCompat: Boolean = False): Boolean;
begin
  Result := (T is TTypeSet) and ((FItemType = nil) or (TTypeSet(T).FItemType = nil) or
    FItemType.Compatible(TTypeSet(T).FItemType, StrictCompat));
end;

function TTypeSet.Default: TData;
begin
  Result := TDataSet.Create;
end;

function TTypeSet.Pipeable: Boolean;
begin
  Result := FItemType.Pipeable;
end;

end.
