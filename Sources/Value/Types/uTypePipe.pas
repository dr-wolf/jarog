unit uTypePipe;

interface

uses
  uOperations, uType, uData;

type
  TTypePipe = class(TType)
  public
    constructor Create(const Hash: string; ItemType: TType);
  protected
    FItemType: TType;
  public
    function Apply(Op: TBinaryOp; T: TType): TType; override;
    function Apply(Op: TUnaryOp): TType; override;
    function Pipeable: Boolean; override;
    function Compatible(T: TType; StrictCompat: Boolean = False): Boolean; override;
    function Default: TData; override;
    function NestedType(Field: string = ''): TType; override;
  end;

implementation

uses uExceptions, uTypeFactory, uTypeString, uDataPipe;

{ TTypePipe }

constructor TTypePipe.Create(const Hash: string; ItemType: TType);
begin
  if not ItemType.Pipeable then
    raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, ItemType.Hash);
  inherited Create(Hash);
  FItemType := ItemType;
end;

function TTypePipe.Apply(Op: TUnaryOp): TType;
begin
  case Op of
    uoPop:
      Result := FItemType;
    uoLength:
      Result := TypeFactory.GetBool
  else
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, 'pipe');
  end;
end;

function TTypePipe.Compatible(T: TType; StrictCompat: Boolean = False): Boolean;
begin
  Result := (T is TTypePipe) and FItemType.Compatible((T as TTypePipe).FItemType, True);
  if not StrictCompat then
    Result := Result or (T is TTypeString);
end;

function TTypePipe.Apply(Op: TBinaryOp; T: TType): TType;
begin
  inherited;
  if Op in [boEqual, boNotEqual] then
    Result := TypeFactory.GetBool
  else
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, FHash);
end;

function TTypePipe.Default: TData;
begin
  Result := TDataPipe.Create(FItemType.Default, nil);
end;

function TTypePipe.NestedType(Field: string): TType;
begin
  if Field <> '' then
    raise ECodeException.Create(E_SYNTAX_ERROR, '');
  Result := FItemType;
end;

function TTypePipe.Pipeable: Boolean;
begin
  Result := False;
end;

end.
