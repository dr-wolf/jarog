unit uStack;

interface

uses
  uArray;

type
  TStack<T> = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  protected
    FItems: TArray<T>;
    FIndex: Integer;
    FAllocSize: Integer;
    function GetDepth: Integer;
  public
    property Depth: Integer read GetDepth;
    function Peek(Number: Integer = 0): T;
    function Pop: T;
    function Take(Count: Integer): TArray<T>;
    procedure Push(Value: T);
  end;

implementation

{ TStack<T> }

constructor TStack<T>.Create;
begin
  FIndex := -1;
  SetLength(FItems, 512);
  FAllocSize := 512;
end;

destructor TStack<T>.Destroy;
begin
  FItems := nil;
  inherited;
end;

function TStack<T>.GetDepth: Integer;
begin
  Result := FIndex + 1;
end;

function TStack<T>.Peek(Number: Integer = 0): T;
begin
  Result := FItems[FIndex - Number];
end;

function TStack<T>.Pop: T;
begin
  if FIndex >= 0 then
  begin
    Result := FItems[FIndex];
    FItems[FIndex] := Default (T);
    Dec(FIndex);
    if (FIndex < FAllocSize - 1024) then
    begin
      Dec(FAllocSize, 1024);
      SetLength(FItems, FAllocSize);
    end;
  end
  else
    Result := Default (T);
end;

procedure TStack<T>.Push(Value: T);
begin
  Inc(FIndex);
  if FIndex >= FAllocSize then
  begin
    Inc(FAllocSize, 1024);
    SetLength(FItems, FAllocSize);
  end;
  FItems[FIndex] := Value;
end;

function TStack<T>.Take(Count: Integer): TArray<T>;
var
  I: Integer;
begin
  SetLength(Result, Count);
  for I := 0 to Count - 1 do
    if FIndex >= 0 then
      Result[I] := Pop
    else
    begin
      SetLength(Result, I);
      Break;
    end;
end;

end.
