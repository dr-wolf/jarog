unit uUnitTCP;

interface

uses
  uUnit;

type
  TUnitTCP = class(TUnit)
  public
    constructor Create;
  end;

  { *
    @descr Network TCP blocking sockets
    * }

implementation

uses uTypeFactory, uFuncTCPConnect, uFuncTCPClose, uFuncTCPSend, uFuncTCPReceive, uFuncTCPListen, uFuncTCPAccept,
  uFuncTCPRemote;

{ TUnitTCP }

constructor TUnitTCP.Create;
begin
  inherited Create('<tcp>');

  AddValue('connect', MakeFunc(TFuncTCPConnect.Create, [TypeFactory.GetString, TypeFactory.GetInt], ['ip', 'port'],
    TypeFactory.GetInt));
  AddValue('close', MakeFunc(TFuncTCPClose.Create, [TypeFactory.GetInt], ['socket']));
  AddValue('receive', MakeFunc(TFuncTCPReceive.Create, [TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt],
    ['socket', 'file', 'timeout']));
  AddValue('send', MakeFunc(TFuncTCPSend.Create, [TypeFactory.GetInt, TypeFactory.GetInt], ['socket', 'file']));
  AddValue('listen', MakeFunc(TFuncTCPListen.Create, [TypeFactory.GetString, TypeFactory.GetInt], ['ip', 'port'],
    TypeFactory.GetInt));
  AddValue('accept', MakeFunc(TFuncTCPAccept.Create, [TypeFactory.GetInt, TypeFactory.GetInt], ['socket', 'timeout'],
    TypeFactory.GetInt));
  AddValue('remote', MakeFunc(TFuncTCPRemote.Create, [TypeFactory.GetInt], ['socket'],
    TypeFactory.GetStruct([TypeFactory.GetString, TypeFactory.GetInt], ['ip', 'port'])));
end;

end.
