unit uFuncTCPClose;

interface

uses
  uFuncTCP, uFuncContext, BlckSock;

type
  TFuncTCPClose = class(TFuncTCP)
  public
    constructor Create;
  private const
    P_SOCKET = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Disconnects and closes a socket.
    @type func(socket: int)
    @in socket int # Socket handle
    * }

implementation

uses uDataNumeric, uOpCode;

{ TFuncTCPClose }

constructor TFuncTCPClose.Create;
begin
  inherited Create([MakeInt]);
  FFuncUid := FID_TCP_CLOSE;
end;

procedure TFuncTCPClose.Execute(Context: TFuncContext);
begin
  FCoreContext.Repos.TCPSocks.Close(AsInt(Context.Data[P_SOCKET]));
end;

end.
