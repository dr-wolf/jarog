#!/bin/sh
rm -rf target
rm -rf /tmp/jarog
mkdir target
cp -r Examples/demo target/demos
cp -r Examples/libs target/units
mkdir /tmp/jarog
cd Binaries/Jacog
fpc -Mdelphi -Ur -FE../../target -FU/tmp/jarog jacog.dpr
cd ../Jarog
fpc -Mdelphi -Ur -FE../../target -FU/tmp/jarog jarog.dpr