unit uOpRange;

interface

uses
  uOp;

type
  TOpRange = class(TOp)
  public
    constructor Create;
  end;

implementation

uses uOpCode;

{ TOpRange }

constructor TOpRange.Create;
begin
  inherited Create(OID_RANGE);
end;

end.
