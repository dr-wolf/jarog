unit uFuncUDPSend;

interface

uses
  uFuncUDP, uFuncContext;

type
  TFuncUDPSend = class(TFuncUDP)
  public
    constructor Create;
  private const
    P_IP = 0;
    P_PORT = 1;
    P_FILE = 2;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Read bytes from file and sends them to remote IP and port.
    @type func(ip: string, port: int, file: int)
    @in ip string # Remote IP
    @in port int # Remote port
    @in file int # File handle
    * }

implementation

uses SysUtils, Classes, BlckSock, uDataNumeric, uExceptions, uOpCode;

{ TFuncUDPSend }

constructor TFuncUDPSend.Create;
begin
  inherited Create([MakeString, MakeInt, MakeInt]);
  FFuncUid := FID_UDP_SEND;
end;

procedure TFuncUDPSend.Execute(Context: TFuncContext);
var
  ip: string;
  port: Integer;
  sock: TUDPBlockSocket;
  stream: TStream;
begin
  ip := RetrieveIP(P_IP, Context);
  port := RetrievePort(P_PORT, Context);
  stream := RetrieveStream(P_FILE, Context);
  stream.Seek(0, soFromBeginning);
  sock := TUDPBlockSocket.Create;
  with sock do
  begin
    CreateSocket;
    Connect(ip, IntToStr(port));
    SendStreamRaw(stream);
    if LastError <> 0 then
      raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataNumeric.Create(sock.LastError));
    CloseSocket;
    Free;
  end;
end;

end.
