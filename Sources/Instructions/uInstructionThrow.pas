unit uInstructionThrow;

interface

uses
  uArray, uInstruction, uExpression, uOp;

type
  TInstructionThrow = class(TInstruction)
  public
    constructor Create(Exception: TExpression);
    destructor Destroy; override;
  private
    FException: TExpression;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uExceptions, uDataStack, uOpThrow, uOpPush, uTypeVoid;

{ TInstructionThrow }

constructor TInstructionThrow.Create(Exception: TExpression);
begin
  if Exception.ResultType is TTypeVoid then
    raise ECodeException.Create(E_NO_RESULT);
  inherited Create;
  FException := Exception;
end;

function TInstructionThrow.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, [TOpPush.Create(ftException)]);
  AppendOp(Result, FException.Compile);
  AppendOp(Result, [TOpThrow.Create]);
end;

destructor TInstructionThrow.Destroy;
begin
  FException.Free;
  inherited;
end;

end.
