unit uRepository;

interface

uses
  uArray;

type
  TRepository = class(TObject)
  public
    destructor Destroy; override;
  protected
    function GetHandle(Id: Integer): Integer; virtual; abstract;
    function Size: Integer; virtual; abstract;
    function Find(Handle: Integer): Integer;
    procedure Release(Id: Integer); virtual; abstract;
  public
    function GetAllHandles: TArray<Integer>;
    procedure Close(Handle: Integer);
  end;

implementation

{ TRepository }

procedure TRepository.Close(Handle: Integer);
var
  I: Integer;
begin
  I := Find(Handle);
  if I > -1 then
    Release(I);
end;

destructor TRepository.Destroy;
var
  I: Integer;
begin
  for I := 0 to Size - 1 do
    Release(I);
  inherited;
end;

function TRepository.Find(Handle: Integer): Integer;
begin
  Result := Size - 1;
  while (Result > -1) and (GetHandle(Result) <> Handle) do
    Dec(Result);
end;

function TRepository.GetAllHandles: TArray<Integer>;
var
  I: Integer;
begin
  SetLength(Result, Size);
  for I := 0 to High(Result) do
    Result[I] := GetHandle(I);
end;

end.
