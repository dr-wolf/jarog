unit uFuncBitAnd;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncBitAnd = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_A = 0;
    P_B = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Performs the logical AND operation on each pair of the corresponding bits, by multiplying them.
    @type func(a: int, b: int -> int)
    @in a int # Left value
    @in b int # Rignt value
    @out - int # Result of disjunction
    * }

implementation

uses uDataNumeric, uNumeric, uOpCode;

{ TFuncBitAnd }

constructor TFuncBitAnd.Create;
begin
  inherited Create([MakeInt, MakeInt], MakeInt);
  FFuncUid := FID_BIT_AND;
end;

procedure TFuncBitAnd.Execute(Context: TFuncContext);
begin
  Return(TDataNumeric.Create(AsNumeric(Context.Data[P_A]).Bit(boAnd, AsNumeric(Context.Data[P_B]))));
end;

end.
