unit uFuncIOSize;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncIOSize = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Returns console width and height.
    @type func(-> struct(width: int, height:int))
    @out - struct(width: int, height:int) # Console width and height
    * }

implementation

uses uDataArray, uDataNumeric, uOpCode;

{ TFuncIOSize }

constructor TFuncIOSize.Create;
begin
  inherited Create([], MakeStruct([MakeInt, MakeInt]));
  FFuncUid := FID_IO_SIZE;
end;

procedure TFuncIOSize.Execute(Context: TFuncContext);
var
  X, Y: Integer;
begin
  FCoreContext.IOProvider.GetScreenSize(X, Y);
  Return(TDataArray.Create([TDataNumeric.Create(X), TDataNumeric.Create(Y)]));
end;

end.
