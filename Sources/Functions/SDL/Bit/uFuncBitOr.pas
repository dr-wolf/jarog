unit uFuncBitOr;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncBitOr = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_A = 0;
    P_B = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Performs the logical inclusive OR operation on each pair of corresponding bits.
    @type func(a: int, b: int -> int)
    @in a int # Left value
    @in b int # Rignt value
    @out - int # Result of inclusive conjuction
    * }

implementation

uses uDataNumeric, uNumeric, uOpCode;

{ TFuncBitOr }

constructor TFuncBitOr.Create;
begin
  inherited Create([MakeInt, MakeInt], MakeInt);
  FFuncUid := FID_BIT_OR;
end;

procedure TFuncBitOr.Execute(Context: TFuncContext);
begin
  Return(TDataNumeric.Create(AsNumeric(Context.Data[P_A]).Bit(boOr, AsNumeric(Context.Data[P_B]))));
end;

end.
