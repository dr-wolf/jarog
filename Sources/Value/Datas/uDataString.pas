unit uDataString;

interface

uses
  uOperations, uData, uExecFile, uTypedStream;

type
  TDataString = class(TData)
  public
    constructor Create; overload;
    constructor Create(const Value: string); overload;
  private
    FValue: string;
  public
    function Apply(Op: TBinaryOp; const Data: TData): TData; override;
    function Dup: TData; override;
    function IsEqual(const Data: TData): Boolean; override;
    function Serialize(const ExecFile: TExecFile): TTypedStream; override;
    procedure Assign(const Data: TData); override;
    procedure Convert(C: TConverter); override;
  end;

function AsString(const Data: TData): string;

implementation

uses uExceptions, uDataBool, uOpCode;

function AsString(const Data: TData): string;
begin
  if Data.Unwrap is TDataString then
    Result := (Data.Unwrap as TDataString).FValue
  else
    raise ERuntimeException.Create(E_EXPECTED_STRING, nil);
end;

{ TDataString }

constructor TDataString.Create;
begin
  inherited Create;
  FValue := '';
end;

constructor TDataString.Create(const Value: string);
begin
  inherited Create;
  FValue := Value;
end;

procedure TDataString.Assign(const Data: TData);
begin
  FValue := AsString(Data);
end;

function TDataString.Apply(Op: TBinaryOp; const Data: TData): TData;
begin
  case Op of
    boAddition:
      Result := TDataString.Create(FValue + AsString(Data));
    boEqual:
      Result := TDataBool.Create(IsEqual(Data));
    boNotEqual:
      Result := TDataBool.Create(not IsEqual(Data));
    boLess:
      Result := TDataBool.Create(FValue < AsString(Data));
    boGreater:
      Result := TDataBool.Create(FValue > AsString(Data));
    boLessEqual:
      Result := TDataBool.Create(FValue <= AsString(Data));
    boGreaterEqual:
      Result := TDataBool.Create(FValue >= AsString(Data));
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  end;
end;

function TDataString.Dup: TData;
begin
  Result := TDataString.Create(FValue);
end;

procedure TDataString.Convert(C: TConverter);
begin
  C.ConvertString(FValue);
end;

function TDataString.IsEqual(const Data: TData): Boolean;
begin
  Result := FValue = AsString(Data);
end;

function TDataString.Serialize(const ExecFile: TExecFile): TTypedStream;
begin
  Result := TTypedStream.Create;
  with Result do
  begin
    WriteByte(DID_DATA_STRING);
    WriteString(FValue);
  end;
end;

end.
