unit uFuncOSSetEnv;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncOSSetEnv = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_NAME = 0;
    P_VALUE = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Sets environment variable for current program and child processes.
    @type func(name: string, value: string)
    @in name string # Name of variable
    @in value string # New value of variable
    * }

implementation

uses uDataString, uOpCode;

{ TFuncOSSetEnv }

constructor TFuncOSSetEnv.Create;
begin
  inherited Create([MakeString, MakeString]);
  FFuncUid := FID_OS_SETENV;
end;

procedure TFuncOSSetEnv.Execute(Context: TFuncContext);
begin
  FCoreContext.Env.SetValue(AsString(Context.Data[P_NAME]), AsString(Context.Data[P_VALUE]));
end;

end.
