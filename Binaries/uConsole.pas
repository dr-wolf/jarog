unit uConsole;

interface

uses
{$IFNDEF UNIX}Windows, {$ENDIF} uConsoleInterface;

type
  TConsole = class(TInterfacedObject, IConsole)
  public
    constructor Create;
    destructor Destroy; override;
  private
{$IFNDEF UNIX}
    FInputHabdle: THandle;
    FOutputHandle: THandle;
{$ENDIF}
  public
    function Key(WaitForInput: Boolean): Byte;
    function Read: string;
    procedure Clear;
    procedure GetCursor(var X, Y: Integer);
    procedure GetSize(var X, Y: Integer);
    procedure SetCursor(X, Y: Integer);
    procedure SetColor(Text, Background: Byte);
    procedure Print(const Text: string);
  end;

implementation

uses {$IFDEF UNIX} Crt, {$ENDIF} SysUtils;

{$IFDEF UNIX}
{$ELSE}

function ApplyColors(Text, Back: Byte; var Attrib: Word): Word;

  function DecodeColor(Color: Byte): Byte;
  const
    BRIGHT = 8;
    RED = 4;
    GREEN = 2;
    BLUE = 1;
  begin
    case Color of
      1, 9:
        Result := BLUE;
      2, 10:
        Result := GREEN;
      3, 11:
        Result := GREEN or BLUE;
      4, 12:
        Result := RED;
      5, 13:
        Result := RED or BLUE;
      6, 14:
        Result := RED or GREEN;
      7, 15:
        Result := RED or GREEN or BLUE;
    else
      Result := 0;
    end;
    if Color > 7 then
      Result := Result or BRIGHT;
  end;

begin
  Result := Attrib and $FF00 or (DecodeColor(Back) shl 4) or DecodeColor(Text);
end;

procedure CheckErr(RetVal: BOOL);
begin
  if not RetVal then
    RaiseLastOSError;
end;

{$ENDIF}
{ TConsole }

constructor TConsole.Create;
begin
{$IFNDEF UNIX}
  FInputHabdle := GetStdHandle(STD_INPUT_HANDLE);
  FOutputHandle := GetStdHandle(STD_OUTPUT_HANDLE);
{$ENDIF}
end;

destructor TConsole.Destroy;
begin
  // CloseHandle(FInputHabdle);
  // CloseHandle(FOutputHandle);
  inherited;
end;

procedure TConsole.Clear;
{$IFNDEF UNIX}
var
  cbInfo: TConsoleScreenBufferInfo;
  cs, nw: dword;
  c: TCoord;
{$ENDIF}
begin
{$IFDEF UNIX}
  ClrScr;
{$ELSE}
  CheckErr(GetConsoleScreenBufferInfo(FOutputHandle, cbInfo));
  cs := cbInfo.dwSize.X * cbInfo.dwSize.Y;
  c.X := 0;
  c.Y := 0;
  CheckErr(FillConsoleOutputCharacter(FOutputHandle, ' ', cs, c, nw));
  CheckErr(FillConsoleOutputAttribute(FOutputHandle, cbInfo.wAttributes, cs, c, nw));
  CheckErr(SetConsoleCursorPosition(FOutputHandle, c));
{$ENDIF}
end;

procedure TConsole.GetCursor(var X, Y: Integer);
{$IFNDEF UNIX}
var
  cbInfo: TConsoleScreenBufferInfo;
{$ENDIF}
begin
{$IFDEF UNIX}
  X := WhereX;
  Y := WhereY;
{$ELSE}
  CheckErr(GetConsoleScreenBufferInfo(FOutputHandle, cbInfo));
  X := cbInfo.dwCursorPosition.X;
  Y := cbInfo.dwCursorPosition.Y;
{$ENDIF}
end;

procedure TConsole.GetSize(var X, Y: Integer);
{$IFNDEF UNIX}
var
  cbInfo: TConsoleScreenBufferInfo;
{$ENDIF}
begin
{$IFDEF UNIX}
  X := ScreenWidth;
  Y := ScreenHeight;
{$ELSE}
  CheckErr(GetConsoleScreenBufferInfo(FOutputHandle, cbInfo));
  X := cbInfo.srWindow.Right - cbInfo.srWindow.Left + 1;
  Y := cbInfo.srWindow.Bottom - cbInfo.srWindow.Top + 1;
{$ENDIF}
end;

function TConsole.Key(WaitForInput: Boolean): Byte;
{$IFNDEF UNIX}
  function KeyPressed: Boolean;
  var
    i: Integer;
    ir: array of TInputRecord;
    c, noe: Cardinal;
  begin
    CheckErr(GetNumberOfConsoleInputEvents(FInputHabdle, noe));
    if noe > 0 then
    begin
      SetLength(ir, noe);
      CheckErr(PeekConsoleInput(FInputHabdle, TInputRecord(ir[0]), noe, c));
      for i := 0 to c - 1 do
        if (ir[i].EventType = KEY_EVENT) and (ir[i].Event.KeyEvent.bKeyDown) then
        begin
          Result := True;
          Exit;
        end;
      Result := False;
    end
    else
      Result := False;
  end;

  function ReadKey: Char;
  var
    ir: TInputRecord;
    ic: dword;
  begin
    repeat
      ReadConsoleInput(FInputHabdle, ir, 1, ic);
    until (ir.EventType = KEY_EVENT) and ir.Event.KeyEvent.bKeyDown;
    Result := Char(ir.Event.KeyEvent.wVirtualKeyCode);
  end;

{$ENDIF}

begin
  if WaitForInput then
    while not KeyPressed do
      Sleep(25);
  if KeyPressed then
    Result := Ord(ReadKey)
  else
    Result := 0;
end;

function TConsole.Read: string;
begin
  Readln(Result);
end;

procedure TConsole.SetColor(Text, Background: Byte);
{$IFNDEF UNIX}
var
  info: TConsoleScreenBufferInfo;
{$ENDIF}
begin
{$IFDEF UNIX}
  TextColor(Text);
  TextBackground(Background);
{$ELSE}
  FillChar(info, SizeOf(info), #0);
  CheckErr(GetConsoleScreenBufferInfo(FOutputHandle, info));
  CheckErr(SetConsoleTextAttribute(FOutputHandle, ApplyColors(Text, Background, info.wAttributes)));
{$ENDIF}
end;

procedure TConsole.SetCursor(X, Y: Integer);
{$IFNDEF UNIX}
var
  c: TCoord;
{$ENDIF}
begin
{$IFDEF UNIX}
  GotoXY(X, Y);
{$ELSE}
  c.X := X;
  c.Y := Y;
  CheckErr(SetConsoleCursorPosition(FOutputHandle, c));
{$ENDIF}
end;

procedure TConsole.Print(const Text: string);
begin
  Write(Text);
end;

end.
