unit uFuncStrUpper;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncStrUpper = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_TEXT = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Converts string to upper case.
    @type func(text: string -> string)
    @in text string # Initial text
    @out - string # Text in upper case
    * }

implementation

uses Character, uDataString, uOpCode;

{ TFuncStrUpper }

constructor TFuncStrUpper.Create;
begin
  inherited Create([MakeString], MakeString);
  FFuncUid := FID_STR_UPPER;
end;

procedure TFuncStrUpper.Execute(Context: TFuncContext);
begin
  Return(TDataString.Create(ToUpper(AsString(Context.Data[P_TEXT]))));
end;

end.
