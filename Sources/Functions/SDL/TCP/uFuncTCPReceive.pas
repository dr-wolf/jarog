unit uFuncTCPReceive;

interface

uses
  uFuncTCP, uFuncContext, BlckSock;

type
  TFuncTCPReceive = class(TFuncTCP)
  public
    constructor Create;
  private const
    P_SOCKET = 0;
    P_FILE = 1;
    P_TIMEOUT = 2;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Receives a stream from a socket adn saves it ot file.
    @type func(socket: int, file: int, timeout: int)
    @in socket int # Socket handle
    @in file int # File handle
    @in timeout int # Read timeout
    * }

implementation

uses Classes, uDataNumeric, uDataString, uExceptions, uOpCode;

{ TFuncTCPReceive }

constructor TFuncTCPReceive.Create;
begin
  inherited Create([MakeInt, MakeInt, MakeInt]);
  FFuncUid := FID_TCP_RECEIVE;
end;

procedure TFuncTCPReceive.Execute(Context: TFuncContext);
var
  sock: TTCPBlockSocket;
  stream: TStream;
  oldp: Int64;
begin
  sock := RetrieveSocket(P_SOCKET, Context);
  stream := RetrieveStream(P_FILE, Context);
  oldp := stream.Position;
  sock.RecvStreamRaw(stream, AsInt(Context.Data[P_TIMEOUT]));
  if (sock.LastError <> 0) and (oldp = stream.Position) then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create(sock.LastErrorDesc));
end;

end.
