unit uTypedStream;

interface

uses
  Classes;

type
  TTypedStream = class(TObject)
  public
    constructor Create; overload;
    constructor Create(Stream: TStream; Size: Int64); overload;
    constructor Create(Stream: TStream; Autorelease: Boolean = True); overload;
    destructor Destroy; override;
  private
    FStream: TStream;
    FStreamAutorelease: Boolean;
    function GetSize: Int64;
  public
    property Size: Int64 read GetSize;
    function PeekByte: Byte;
    function ReadByte: Byte;
    function ReadInteger: Int64;
    function ReadDouble: Double;
    function ReadString: string;
    procedure WriteByte(const B: Byte);
    procedure WriteInteger(const I: Int64);
    procedure WriteDouble(const E: Double);
    procedure WriteString(const S: string);
    procedure Read(var Buff; Count: Integer);
    procedure Write(const Buff; Count: Integer);
    procedure SaveTo(Stream: TStream);
    procedure Reset;
  end;

implementation

uses SysUtils;

{ TTypedStream }

constructor TTypedStream.Create;
begin
  FStream := TMemoryStream.Create;
  FStreamAutorelease := True;
end;

constructor TTypedStream.Create(Stream: TStream; Size: Int64);
begin
  FStream := TMemoryStream.Create;
  FStream.CopyFrom(Stream, Size);
  FStream.Position := 0;
  FStreamAutorelease := True;
end;

constructor TTypedStream.Create(Stream: TStream; Autorelease: Boolean = True);
begin
  FStream := Stream;
  FStreamAutorelease := Autorelease;
end;

destructor TTypedStream.Destroy;
begin
  if FStreamAutorelease then
    FStream.Free;
  inherited;
end;

function TTypedStream.GetSize: Int64;
begin
  Result := FStream.Size;
end;

function TTypedStream.PeekByte: Byte;
begin
  FStream.ReadBuffer(Result, SizeOf(Result));
  FStream.Seek(-SizeOf(Result), soFromCurrent);
end;

procedure TTypedStream.Read(var Buff; Count: Integer);
begin
  FStream.ReadBuffer(Buff, Count);
end;

function TTypedStream.ReadByte: Byte;
begin
  FStream.ReadBuffer(Result, 1);
end;

function TTypedStream.ReadDouble: Double;
begin
  FStream.ReadBuffer(Result, SizeOf(Result));
end;

function TTypedStream.ReadInteger: Int64;
var
  h, l: Byte;
begin
  // TODO: better multibyte integer
  FStream.ReadBuffer(h, SizeOf(h));
  l := h and $F0 shr 4;
  Result := 0;
  FStream.ReadBuffer(Result, l);
  if h and $0F <> 0 then
    Result := -Result;
end;

function TTypedStream.ReadString: string;
var
  bytes: TBytes;
  l: Integer;
begin
  l := ReadInteger;
  SetLength(bytes, l);
  FStream.ReadBuffer(Pointer(bytes)^, l);
  Result := TEncoding.UTF8.GetString(bytes);
end;

procedure TTypedStream.Reset;
begin
  FStream.Seek(0, soFromBeginning);
end;

procedure TTypedStream.SaveTo(Stream: TStream);
begin
  Stream.CopyFrom(FStream, 0);
end;

procedure TTypedStream.Write(const Buff; Count: Integer);
begin
  FStream.WriteBuffer(Buff, Count);
end;

procedure TTypedStream.WriteByte(const B: Byte);
begin
  FStream.WriteBuffer(B, 1);
end;

procedure TTypedStream.WriteDouble(const E: Double);
begin
  FStream.WriteBuffer(E, SizeOf(E));
end;

procedure TTypedStream.WriteInteger(const I: Int64);

  function SigBytes(V: Int64): Byte;
  begin
    if V > 0 then
    begin
      Result := 8;
      while V and $FF00000000000000 = 0 do
      begin
        V := V shl 8;
        Dec(Result);
      end;
    end
    else
      Result := 0;
  end;

var
  h, l: Byte;
  ia: Int64;
begin
  // TODO: better multibyte integer
  l := SigBytes(Abs(I));
  h := (Byte(I < 0) and $0F) or (l and $0F shl 4);
  ia := Abs(I);
  FStream.WriteBuffer(h, SizeOf(h));
  FStream.WriteBuffer(ia, l);
end;

procedure TTypedStream.WriteString(const S: string);
var
  bytes: TBytes;
begin
  bytes := BytesOf(UTF8Encode(S));
  WriteInteger(Length(bytes));
  FStream.WriteBuffer(Pointer(bytes)^, Length(bytes));
end;

end.
