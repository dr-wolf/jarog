unit uFuncIOGoto;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncIOGoto = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_X = 0;
    P_Y = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Moves cursor to a given coords.
    @type func(x: int, y: int)
    @in x int # Desired column
    @in y int # Desired row
    * }

implementation

uses uDataNumeric, uOpCode;

{ TFuncIOGoto }

constructor TFuncIOGoto.Create;
begin
  inherited Create([MakeInt, MakeInt]);
  FFuncUid := FID_IO_GOTO;
end;

procedure TFuncIOGoto.Execute(Context: TFuncContext);
var
  x, y: Integer;
begin
  x := AsInt(Context.Data[P_X]);
  y := AsInt(Context.Data[P_Y]);
  FCoreContext.IOProvider.SetCursorPos(x, y);
end;

end.
