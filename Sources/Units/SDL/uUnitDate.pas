unit uUnitDate;

interface

uses
  uUnit;

type
  TUnitDate = class(TUnit)
  public
    constructor Create;
  end;

  { *
    @descr Contains functions for decoding and encoding date and time.
    @types _date: (year: int, month: int, day: int, hour: int, minute: int, second: int)
    * }

implementation

uses uTypeFactory, uType, uFuncDateDecode, uFuncDateEncode, uFuncDateFmt, uFuncDateParse;

{ TUnitDate }

constructor TUnitDate.Create;
var
  t: TType;
begin
  inherited Create('<date>');

  t := TypeFactory.GetPredefined(pdtDate);

  AddType('_date', t);

  AddValue('decode', MakeFunc(TFuncDateDecode.Create, [TypeFactory.GetInt], ['unix'], t));
  AddValue('encode', MakeFunc(TFuncDateEncode.Create, [t], ['date'], TypeFactory.GetInt));
  AddValue('fmt', MakeFunc(TFuncDateFmt.Create, [TypeFactory.GetInt, TypeFactory.GetString], ['unix', 'format'],
    TypeFactory.GetString));
  AddValue('parse', MakeFunc(TFuncDateParse.Create, [TypeFactory.GetString], ['text'], TypeFactory.GetInt));
end;

end.
