unit uInstructionTryCatch;

interface

uses
  uArray, uInstruction, uExpression, uType, uOp;

type
  TInstructionTryCatch = class(TInstruction)
  public
    constructor Create(Instruction: TInstruction);
    destructor Destroy; override;
  private type
    TCatchClause = record
      Exception: TExpression;
      Instruction: TInstruction;
    end;
  private
    FInstruction: TInstruction;
    FCatchClauses: TArray<TCatchClause>;
    function FindClause(ExceptionType: TType): Integer;
  public
    function Compile: TArray<TOp>; override;
    function AddCatchCaluse(Exception: TExpression; Instruction: TInstruction): Boolean;
  end;

implementation

uses SysUtils, uDataStack, uOpDup, uOpThrow, uOpPush, uOpPop, uOpJit, uOpJump, uTag, uOpMove;

{ TInstructionTryCatch }

constructor TInstructionTryCatch.Create(Instruction: TInstruction);
begin
  inherited Create;
  FInstruction := Instruction;
  FInstruction.SetParent(Self);
  SetLength(FCatchClauses, 0);

  InitTag(TAG_CATCH);
  InitTag(TAG_CATCH_END);
end;

destructor TInstructionTryCatch.Destroy;
var
  i: Integer;
begin
  FInstruction.Free;
  for i := 0 to High(FCatchClauses) do
  begin
    FCatchClauses[i].Exception.Free;
    FCatchClauses[i].Instruction.Free;
  end;
  FCatchClauses := nil;
  inherited;
end;

function TInstructionTryCatch.Compile: TArray<TOp>;
var
  i: Integer;
  t: TArray<ITag>;
begin
  SetLength(Result, 0);
  if Length(FCatchClauses) > 0 then
    AppendOp(Result, [TOpPush.Create(ftCatch, GetTag(TAG_CATCH))])
  else
    AppendOp(Result, [TOpPush.Create(ftCatch, GetTag(TAG_CATCH_END))]);
  AppendOp(Result, FInstruction.Compile);
  AppendOp(Result, [TOpPop.Create(ftCatch)]);

  if Length(FCatchClauses) > 0 then
  begin
    AppendOp(Result, [TOpJump.Create(0, GetTag(TAG_CATCH_END))]);

    SetLength(t, Length(FCatchClauses));

    for i := 0 to High(FCatchClauses) do
    begin
      t[i] := TTag.Create;
      if i = 0 then
        AppendOp(Result, [TOpDup.Create.WithTag(GetTag(TAG_CATCH))])
      else
        AppendOp(Result, [TOpDup.Create]);
      AppendOp(Result, FCatchClauses[i].Exception.Compile);
      AppendOp(Result, [TOpMove.Create(True, True), TOpJit.Create(False, t[i])]);
    end;
    AppendOp(Result, [TOpThrow.Create]);

    for i := 0 to High(FCatchClauses) do
    begin
      AppendOp(Result, AttachTag(FCatchClauses[i].Instruction.Compile, t[i], tpAtFirst));
      if i < High(FCatchClauses) then
        AppendOp(Result, [TOpJump.Create(0, GetTag(TAG_CATCH_END))]);
    end;
  end;

  AppendOp(Result, [TOpPop.Create(ftException).WithTag(GetTag(TAG_CATCH_END))]);
end;

function TInstructionTryCatch.AddCatchCaluse(Exception: TExpression; Instruction: TInstruction): Boolean;
begin
  Result := FindClause(Exception.ResultType) < 0;
  if Result then
  begin
    SetLength(FCatchClauses, Length(FCatchClauses) + 1);
    FCatchClauses[High(FCatchClauses)].Exception := Exception;
    FCatchClauses[High(FCatchClauses)].Instruction := Instruction;
    FCatchClauses[High(FCatchClauses)].Instruction.SetParent(Self);
  end;
end;

function TInstructionTryCatch.FindClause(ExceptionType: TType): Integer;
begin
  Result := High(FCatchClauses);
  while Result >= 0 do
  begin
    if FCatchClauses[Result].Exception.ResultType.Compatible(ExceptionType) then
      Break;
    Dec(Result);
  end;
end;

end.
