unit uFuncMathFrac;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncMathFrac = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_VALUE = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Retrieves the decimal part of a float number.
    @type func(value: float -> float)
    @in value float # Initial number
    @out - float # Decimal part of a number
    * }

implementation

uses uDataNumeric, uOpCode;

{ TFuncMathFrac }

constructor TFuncMathFrac.Create;
begin
  inherited Create([MakeFloat], MakeFloat);
  FFuncUid := FID_MATH_FRAC;
end;

procedure TFuncMathFrac.Execute(Context: TFuncContext);
begin
  Return(TDataNumeric.Create(AsNumeric(Context.Data[P_VALUE]).Frac));
end;

end.
