unit uUnit;

interface

uses
  uArray, uDict, uType, uValue, uFunction, uAppMold;

type
  TUnit = class(TObject)
  public
    constructor Create(const Filename: string); overload;
    destructor Destroy; override;
  protected
    FFilename: string;
    FUnits: TDict<TUnit>;
    FTypes: TDict<TType>;
    FValues: TDict<IValue>;
    function MakeFunc(Func: IFunction; Types: array of TType; Names: array of string; ResultType: TType = nil): IValue;
  public
    property Filename: string read FFilename write FFilename;
    function GetType(const Name, UnitName: string): TType;
    function GetValue(const Name, UnitName: string): IValue;
    procedure AddUnit(const Name: string; U: TUnit);
    procedure AddType(const Name: string; T: TType);
    procedure AddValue(const Name: string; V: IValue);
    procedure Initialize(AppMold: TAppMold);
  end;

implementation

uses uExceptions, uTypeFactory, uDataFunc, uOpLoad;

{ TUnit }

constructor TUnit.Create(const Filename: string);
begin
  FFilename := Filename;
  FUnits := TDict<TUnit>.Create;
  FTypes := TDict<TType>.Create;
  FValues := TDict<IValue>.Create;
end;

destructor TUnit.Destroy;
var
  k: TArray<string>;
  i: Integer;
begin
  k := FUnits.Keys;
  for i := 0 to High(k) do
    FUnits[k[i]].Free;
  FUnits.Free;
  FTypes.Free;
  FValues.Free;
  inherited;
end;

procedure TUnit.AddType(const Name: string; T: TType);
begin
  FTypes.Put(Name, T);
end;

procedure TUnit.AddUnit(const Name: string; U: TUnit);
begin
  FUnits.Put(Name, U);
end;

procedure TUnit.AddValue(const Name: string; V: IValue);
begin
  FValues.Put(Name, V);
end;

function TUnit.GetType(const Name, UnitName: string): TType;
begin
  Result := nil;
  if UnitName <> '' then
  begin
    if FUnits.Contains(UnitName) then
      Result := FUnits[UnitName].GetType(Name, '')
    else
      raise ECodeException.Create(E_UNKNOWN_UNIT, UnitName);
  end else if FTypes.Contains(Name) then
    Result := FTypes[Name];
end;

function TUnit.GetValue(const Name, UnitName: string): IValue;
begin
  Result := nil;
  if UnitName <> '' then
  begin
    if FUnits.Contains(UnitName) then
      Result := FUnits[UnitName].GetValue(Name, '')
    else
      raise ECodeException.Create(E_UNKNOWN_UNIT, UnitName);
  end else if FValues.Contains(Name) then
    Result := FValues[Name];
end;

procedure TUnit.Initialize(AppMold: TAppMold);
var
  T: TType;
begin
  if FValues.Contains('main') then
  begin
    T := TypeFactory.GetFunc(TypeFactory.GetStruct([TypeFactory.GetList(TypeFactory.GetString)], ['args']),
      TypeFactory.GetVoid);
    if not T.Compatible(FValues['main'].TypeInfo, True) then
      raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, T.Hash);
    AppMold.SetStartOp(TOpLoad.Create(AppMold.DataSection.Keep(FValues['main'].Data), False));
  end
  else
    raise ECodeException.Create(E_IDENTIFIER_UNDECLARED, 'main');
end;

function TUnit.MakeFunc(Func: IFunction; Types: array of TType; Names: array of string;
  ResultType: TType = nil): IValue;
begin
  if ResultType = nil then
    ResultType := TypeFactory.GetVoid;
  Result := TValue.Create(TypeFactory.GetFunc(TypeFactory.GetStruct(Types, Names), ResultType), TDataFunc.Create(Func));
end;

end.
