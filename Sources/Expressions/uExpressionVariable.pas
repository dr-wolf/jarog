unit uExpressionVariable;

interface

uses
  uExpression, uArray, uType, uOp, uDataSection;

type
  TExpressionVariable = class(TExpression)
  public
    constructor Create(Data: TDataSection; VariableId: Integer; TypeInfo: TType);
  private
    FVariableId: Integer;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uOpLoad;

{ TExpressionVariable }

constructor TExpressionVariable.Create(Data: TDataSection; VariableId: Integer; TypeInfo: TType);
begin
  inherited Create(Data);
  FVariableId := VariableId;
  FResultType := TypeInfo;
  FGroup := egVariable;
end;

function TExpressionVariable.Compile: TArray<TOp>;
begin
  SetLength(Result, 1);
  Result[0] := TOpLoad.Create(FVariableId, True);
end;

end.
