unit uDataLoader;

interface

uses
  uExecFile, uTypedStream;

procedure LoadData(Stream: TTypedStream; ExecFile: TExecFile; var Result: TObject);
function UnpackData(Stream: TTypedStream): TObject;

implementation

uses
  SysUtils, uArray, uData, uDataAny, uDataArray, uDataBool, uDataFunc, uDataNumeric, uDataPipe, uDataSet, uDataString,
  uDataStub, mp_types, mp_base, mp_real, uNumeric, uInteger, uBigInt, uBigFloat, uFunction, uOpCode;

function UnpackNumeric(Stream: TTypedStream): TDataNumeric;
var
  b: packed array of Byte;
  i: mp_int;
  f: mp_float;
  n: TNumeric;
begin
  case Stream.ReadByte of
    DID_DATA_NUMERIC_INT:
      n := TInteger.Create(Stream.ReadInteger);
    DID_DATA_NUMERIC_BIGINT:
      begin
        mp_init(i);
        SetLength(b, Stream.ReadInteger);
        Stream.Read(b[0], Length(b));
        mp_read_unsigned_bin(i, b[0], Length(b));
        if Boolean(Stream.ReadByte) then
          i.sign := MP_NEG
        else
          i.sign := MP_ZPOS;
        n := TBigInt.Create(i);
      end;
    DID_DATA_NUMERIC_BIGFLT:
      begin
        mpf_init(f);
        f.exponent := Stream.ReadInteger;
        f.bitprec := Stream.ReadInteger;
        SetLength(b, Stream.ReadInteger);
        Stream.Read(b[0], Length(b));
        mp_read_unsigned_bin(f.mantissa, b[0], Length(b));
        if Boolean(Stream.ReadByte) then
          f.mantissa.sign := MP_NEG
        else
          f.mantissa.sign := MP_ZPOS;
        n := TBigFloat.Create(f);
      end;
  else
    raise Exception.Create('Undefined numeric type');
  end;
  Result := TDataNumeric.Create(n);
end;

function UnpackSet(Stream: TTypedStream): TDataSet;
var
  i: Integer;
  a: TArray<UInt64>;
begin
  SetLength(a, Stream.ReadInteger);
  for i := 0 to High(a) do
    Stream.Read(a[i], 8);
  Result := TDataSet.Create(a);
end;

procedure LoadData(Stream: TTypedStream; ExecFile: TExecFile; var Result: TObject);

  function LoadArray(Stream: TTypedStream; ExecFile: TExecFile): TDataArray;
  var
    t: Byte;
    i: Integer;
    a: TArray<TData>;
  begin
    t := Stream.ReadByte;
    case t of
      DID_DATA_ARRAY_EMPTY, DID_DATA_ARRAY_STUB:
        Result := TDataArray.Create(ExecFile.Load(Stream.ReadInteger) as TData);
      DID_DATA_ARRAY_VAR, DID_DATA_ARRAY_FIXED:
        begin
          SetLength(a, Stream.ReadInteger);
          for i := 0 to High(a) do
            a[i] := ExecFile.Load(Stream.ReadInteger) as TData;
          Result := TDataArray.Create(a, t = DID_DATA_ARRAY_FIXED);
        end;
    else
      raise Exception.Create('Unknown array format');
    end;
  end;

  procedure LoadFunc(Stream: TTypedStream; ExecFile: TExecFile; var Result: TObject);
  var
    i: Integer;
    a: TArray<TData>;
  begin
    Result := TDataFunc.Create;
    (Result as TDataFunc).SetFunc(ExecFile.Load(Stream.ReadInteger) as TFunction);
    SetLength(a, Stream.ReadInteger);
    for i := 0 to High(a) do
      a[i] := ExecFile.Load(Stream.ReadInteger) as TData;
    (Result as TDataFunc).AddClosures(a);
  end;

begin
  case Stream.ReadByte of
    DID_DATA_ANY:
      Result := TDataAny.Create(ExecFile.Load(Stream.ReadInteger) as TData);
    DID_DATA_ARRAY:
      Result := LoadArray(Stream, ExecFile);
    DID_DATA_BOOL:
      Result := TDataBool.Create(Boolean(Stream.ReadByte));
    DID_DATA_FUNC:
      LoadFunc(Stream, ExecFile, Result);
    DID_DATA_NUMERIC:
      Result := UnpackNumeric(Stream);
    DID_DATA_PIPE:
      Result := TDataPipe.Create(ExecFile.Load(Stream.ReadInteger) as TData, nil);
    DID_DATA_SET:
      Result := UnpackSet(Stream);
    DID_DATA_STRING:
      Result := TDataString.Create(Stream.ReadString);
    DID_DATA_STUB:
      Result := TDataStub.Create;
  else
    raise Exception.Create('Stream read error');
  end;
end;

function UnpackData(Stream: TTypedStream): TObject;

  function UnpackArray(Stream: TTypedStream): TDataArray;
  var
    t: Byte;
    i: Integer;
    a: TArray<TData>;
  begin
    t := Stream.ReadByte;
    case t of
      DID_DATA_ARRAY_EMPTY:
        Result := TDataArray.Create(UnpackData(Stream) as TData);
      DID_DATA_ARRAY_STUB:
        Result := TDataArray.Create(nil);
      DID_DATA_ARRAY_VAR, DID_DATA_ARRAY_FIXED:
        begin
          SetLength(a, Stream.ReadInteger);
          for i := 0 to High(a) do
            a[i] := UnpackData(Stream) as TData;
          Result := TDataArray.Create(a, t = DID_DATA_ARRAY_FIXED);
        end;
    else
      raise Exception.Create('Unknown array format');
    end;
  end;

var
  b: Byte;

begin
  b := Stream.ReadByte;
  case b of
    DID_DATA_ANY:
      Result := TDataAny.Create(UnpackData(Stream) as TData);
    DID_DATA_ARRAY:
      Result := UnpackArray(Stream);
    DID_DATA_BOOL:
      Result := TDataBool.Create(Boolean(Stream.ReadByte));
    DID_DATA_NUMERIC:
      Result := UnpackNumeric(Stream);
    DID_DATA_SET:
      Result := UnpackSet(Stream);
    DID_DATA_STRING:
      Result := TDataString.Create(Stream.ReadString);
  else
    raise Exception.Create('Undefined data identifier ' + IntToStr(b));
  end;
end;

end.
