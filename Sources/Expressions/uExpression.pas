unit uExpression;

interface

uses
  uArray, uType, uValue, uDataSection, uOp;

type
  TExpression = class(TObject)
  public
    constructor Create(Data: TDataSection);
  private type
    TExpressionGroup = (egVariable, egMixed, egStatic);
  protected
    FData: TDataSection;
    FGroup: TExpressionGroup;
    FResultType: TType;
  public
    property Group: TExpressionGroup read FGroup;
    property ResultType: TType read FResultType;
    function Compile: TArray<TOp>; virtual; abstract;
    function Eval: IValue; virtual;
  end;

implementation

uses uExceptions;

{ TExpression }

constructor TExpression.Create(Data: TDataSection);
begin
  FData := Data;
end;

function TExpression.Eval: IValue;
begin
  raise ECodeException.Create(E_EXPECTED_STATIC);
end;

end.
