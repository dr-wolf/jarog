unit uFuncDateEncode;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncDateEncode = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_DATE = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Assembles date and time components into unix timestamp.
    @type func(date: _date -> int)
    @in date _date # Struct with date and time components
    @out - int # Unix timestamp
    * }

implementation

uses SysUtils, uExceptions, uDataString, uDataNumeric, uOpCode;

{ TFuncDateEncode }

constructor TFuncDateEncode.Create;
begin
  inherited Create([MakeStruct([MakeInt, MakeInt, MakeInt, MakeInt, MakeInt, MakeInt])], MakeInt);
  FFuncUid := FID_DATE_ENCODE;
end;

procedure TFuncDateEncode.Execute(Context: TFuncContext);

  function EncodeDateTime(const Year, Month, Day, Hour, Minute, Second: Word; var Value: TDateTime): Boolean;
  var
    LTime: TDateTime;
  begin
    Result := TryEncodeDate(Year, Month, Day, Value);
    if Result then
    begin
      Result := TryEncodeTime(Hour, Minute, Second, 0, LTime);
      if Result then
        if Value >= 0 then
          Value := Value + LTime
        else
          Value := Value - LTime;
    end;
  end;

var
  i: Integer;
  c: array [0 .. 5] of Word;
  d: TDateTime;

begin
  for i := 0 to 5 do
    c[i] := AsInt(Context.Data[P_DATE].Item(i));
  if EncodeDateTime(c[0], c[1], c[2], c[3], c[4], c[5], d) then
    Return(TDataNumeric.Create(Round((d - 25569) * 86400)))
  else
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Can not construct date!'));
end;

end.
