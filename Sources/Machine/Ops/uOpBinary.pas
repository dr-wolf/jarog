unit uOpBinary;

interface

uses
  uOperations, uOp, uBytecode;

type
  TOpBinary = class(TOp)
  public
    constructor Create(Operation: TBinaryOp);
  private
    FOperation: TBinaryOp;
  public
    procedure Serialize(var Code: TBytecode); override;
  end;

implementation

uses uOpCode;

{ TOpBinary }

constructor TOpBinary.Create(Operation: TBinaryOp);
begin
  inherited Create(OID_BINARY);
  FOperation := Operation;
end;

procedure TOpBinary.Serialize(var Code: TBytecode);
begin
  Code.Op := CompileOp();
  Code.SParam := Byte(FOperation);
end;

end.
