unit uFuncProcStart;

interface

uses
  uFuncProc, uFuncContext, BlckSock;

type
  TFuncProcStart = class(TFuncProc)
  public
    constructor Create;
  private const
    P_PROC = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Starts process execution.
    @type func(proc: int)
    @in proc int # Process handle
    * }

implementation

uses uOpCode;

{ TFuncProcStart }

constructor TFuncProcStart.Create;
begin
  inherited Create([MakeInt]);
  FFuncUid := FID_PROC_START;
end;

procedure TFuncProcStart.Execute(Context: TFuncContext);
begin
  RetrieveProcess(P_PROC, Context).Execute;
end;

end.
