unit uDict;

interface

uses
  uThreadSafe, uArray, uStringSet;

type
  TDict<T> = class(TThreadObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FItems: TArray<T>;
    FKeys: TStringSet;
    function GetItem(I: string): T;
    function GetCount: Integer;
  public
    property Item[Key: string]: T read GetItem; default;
    property Count: Integer read GetCount;
    function Contains(const Key: string): Boolean;
    function Keys: TArray<string>;
    procedure Put(const Key: string; Value: T);
    procedure Delete(const Key: string);
    procedure Clear;
  end;

implementation

{ TDict<T> }

constructor TDict<T>.Create;
begin
  FKeys := TStringSet.Create;
  Clear;
end;

destructor TDict<T>.Destroy;
begin
  FItems := nil;
  FKeys.Free;
  inherited;
end;

procedure TDict<T>.Clear;
begin
  FKeys.Clear;
  SetLength(FItems, 0);
end;

function TDict<T>.Contains(const Key: string): Boolean;
begin
  Result := FKeys.IndexOf(Key) >= 0;
end;

procedure TDict<T>.Delete(const Key: string);
var
  n: Integer;
begin
  n := FKeys.IndexOf(Key);
  if n < 0 then
    Exit;
  while n < High(FItems) do
  begin
    FItems[n] := FItems[n + 1];
    Inc(n);
  end;
  SetLength(FItems, Length(FItems) - 1);
  FKeys.Delete(Key);
end;

function TDict<T>.GetCount: Integer;
begin
  Result := FKeys.Count;
end;

function TDict<T>.GetItem(I: string): T;
begin
  Result := FItems[FKeys.IndexOf(I)];
end;

function TDict<T>.Keys: TArray<string>;
begin
  Result := FKeys.AsArray;
end;

procedure TDict<T>.Put(const Key: string; Value: T);
var
  p, n: Integer;
begin
  p := FKeys.IndexOf(Key);
  if p < 0 then
  begin
    p := FKeys.Add(Key);
    SetLength(FItems, Length(FItems) + 1);
    for n := High(FItems) downto p + 1 do
      FItems[n] := FItems[n - 1];
  end;
  FItems[p] := Value;
end;

end.
