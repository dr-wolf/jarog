unit uOpJit;

interface

uses
  uOp, uBytecode, uTag;

type
  TOpJit = class(TOp)
  public
    constructor Create(Invert: Boolean; Delta: Integer); overload;
    constructor Create(Invert: Boolean; Tag: ITag; Delta: Integer = 0); overload;
  private
    FInvert: Boolean;
    FJumpTag: ITag;
    FDelta: Integer;
  public
    procedure Serialize(var Code: TBytecode); override;
  end;

implementation

uses uOpCode;

{ TOpJit }

constructor TOpJit.Create(Invert: Boolean; Delta: Integer);
begin
  inherited Create(OID_JIT);
  FInvert := Invert;
  FDelta := Delta;
  FJumpTag := nil;
end;

constructor TOpJit.Create(Invert: Boolean; Tag: ITag; Delta: Integer = 0);
begin
  inherited Create(OID_JIT);
  FInvert := Invert;
  FJumpTag := Tag;
  FDelta := Delta;
end;

procedure TOpJit.Serialize(var Code: TBytecode);
begin
  Code.Op := CompileOp(FInvert);
  if FJumpTag <> nil then
    Code.LParam := FJumpTag.GetAddress - FIndex + FDelta
  else
    Code.LParam := FDelta;
end;

end.
