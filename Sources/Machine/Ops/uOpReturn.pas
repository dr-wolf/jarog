unit uOpReturn;

interface

uses
  uOp, uBytecode;

type
  TOpReturn = class(TOp)
  public
    constructor Create(FromFunc, KeepResult: Boolean);
  private
    FFromFunc: Boolean;
    FKeepResult: Boolean;
  public
    procedure Serialize(var Code: TBytecode); override;
  end;

implementation

uses uOpCode;

{ TOpReturn }

constructor TOpReturn.Create(FromFunc, KeepResult: Boolean);
begin
  inherited Create(OID_RETURN);
  FFromFunc := FromFunc;
  FKeepResult := KeepResult;
end;

procedure TOpReturn.Serialize(var Code: TBytecode);
begin
  Code.Op := CompileOp(FFromFunc, FKeepResult);
end;

end.
