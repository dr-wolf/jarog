unit uMachine;

interface

uses
  uApp, uCoreContext, uWorkerList, uData;

type
  TMachine = class(TObject)
  public
    constructor Create(App: TApp; CoreContext: TCoreContext);
    destructor Destroy; override;
  private
    FApp: TApp;
    FCoreContext: TCoreContext;
    FWorkers: TWorkerList;
    function ConstructCliArgs: TData;
  public
    procedure Run;
  end;

implementation

uses Classes, SyncObjs, uArray, uDataArray, uDataString, uMessageQueue, uState, uWorker, uDebugger;

{ TMachine }

constructor TMachine.Create(App: TApp; CoreContext: TCoreContext);
begin
  FApp := App;
  FCoreContext := CoreContext;
  FWorkers := TWorkerList.Create;
end;

destructor TMachine.Destroy;
begin
  FWorkers.Free;
  inherited;
end;

function TMachine.ConstructCliArgs: TData;
var
  a: TArray<TData>;
  i: Integer;
begin
  SetLength(a, FCoreContext.Config.CliArgCount);
  for i := 0 to FCoreContext.Config.CliArgCount - 1 do
    a[i] := TDataString.Create(FCoreContext.Config.CliArg[i]);
  Result := TDataArray.Create(a, False);
end;

procedure TMachine.Run;
var
  m: TMessage;
  S: TState;
  d: TDebugger;
begin
  if FCoreContext.Config.DebugPort = 0 then
    FApp.ClearBreaks;

  S := TState.Create(0, FApp.Data);
  S.Stack.Push(TDataArray.Create([ConstructCliArgs]));
  FWorkers.RegisterWorker(TWorker.Create(FApp, S, FCoreContext, FCoreContext.Config.DebugPort > 0));

  if FCoreContext.Config.DebugPort > 0 then
    d := TDebugger.Create(FCoreContext.Config.DebugPort, FCoreContext.IOProvider, FApp, FWorkers)
  else
    d := nil;

  with FCoreContext.MessageQueue do
    while True do
    begin
      MailboxFlag.WaitFor(INF);
      Messages.Lock;
      FWorkers.Lock;
      try
        while Messages.Count > 0 do
        begin
          m := Messages.Pop;
          case m.Subject of
            msCreatreThread:
              begin
                S := TState.Create(1, FApp.Data); //TODO: make a copy here
                S.Stack.Push((m.Params[1] as TData).Dup);
                S.Stack.Push((m.Params[0] as TData).Dup);
                FWorkers.RegisterWorker(TWorker.Create(FApp, S, FCoreContext, False));
                (m.Params[2] as TEvent).SetEvent;
              end;
            msShutdown:
              FWorkers.FireWorker(m.Params[0] as TWorker);
          end;
        end;
        if FWorkers.AllWorksStopped then
          Break;
      finally
        MailboxFlag.ResetEvent;
        Messages.Unlock;
        FWorkers.Unlock;
      end;
    end;

  if Assigned(d) then
    d.Terminate;
end;

end.
