unit uDataSection;

interface

uses
  SyncObjs, uArray, uExecFile, uData;

type
  TDataSection = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FData: TArray<TData>;
    FStrings: TArray<string>;
    FSections: TArray<TCriticalSection>;
    function GetData(I: Integer): TData;
    function GetText(I: Integer): string;
    function GetSection(I: Integer): TCriticalSection;
    procedure FreeSections;
  public
    property Data[I: Integer]: TData read GetData;
    property Text[I: Integer]: string read GetText;
    property Section[I: Integer]: TCriticalSection read GetSection;
    function Keep(Data: TData): Integer; overload;
    function Keep(Text: string): Integer; overload;
    procedure SetSectionSize(Size: Integer);
    procedure Load(ExecFile: TExecFile);
    procedure Save(ExecFile: TExecFile);
  end;

implementation

{ TDataStorage }

constructor TDataSection.Create;
begin
  SetLength(FData, 0);
  SetLength(FStrings, 0);
  SetLength(FSections, 0);
end;

destructor TDataSection.Destroy;
begin
  FreeSections;
  inherited;
end;

procedure TDataSection.FreeSections;
var
  I: Integer;
begin
  for I := 0 to High(FSections) do
    if FSections[I] <> nil then
      FSections[I].Free;
end;

function TDataSection.GetData(I: Integer): TData;
begin
  Result := FData[I];
end;

function TDataSection.GetSection(I: Integer): TCriticalSection;
begin
  if FSections[I] = nil then
    FSections[I] := TCriticalSection.Create;
  Result := FSections[I];
end;

function TDataSection.GetText(I: Integer): string;
begin
  Result := FStrings[I];
end;

function TDataSection.Keep(Text: string): Integer;
var
  I: Integer;
begin
  for I := 0 to High(FStrings) do
    if FStrings[I] = Text then
    begin
      Result := I;
      Exit;
    end;
  SetLength(FStrings, Length(FStrings) + 1);
  FStrings[High(FStrings)] := Text;
  Result := High(FStrings);
end;

function TDataSection.Keep(Data: TData): Integer;
begin
  SetLength(FData, Length(FData) + 1);
  FData[High(FData)] := Data;
  Result := High(FData);
end;

procedure TDataSection.Load(ExecFile: TExecFile);
var
  I: Integer;
begin
  SetLength(FData, ExecFile.OpStream.ReadInteger);
  for I := 0 to High(FData) do
    FData[I] := ExecFile.Load(ExecFile.OpStream.ReadInteger) as TData;

  SetLength(FStrings, ExecFile.OpStream.ReadInteger);
  for I := 0 to High(FStrings) do
    FStrings[I] := ExecFile.OpStream.ReadString;
end;

procedure TDataSection.Save(ExecFile: TExecFile);
var
  I: Integer;
begin
  ExecFile.OpStream.WriteInteger(Length(FData));
  for I := 0 to High(FData) do
    ExecFile.OpStream.WriteInteger(ExecFile.Save(FData[I], FData[I].Serialize));

  ExecFile.OpStream.WriteInteger(Length(FStrings));
  for I := 0 to High(FStrings) do
    ExecFile.OpStream.WriteString(FStrings[I]);
end;

procedure TDataSection.SetSectionSize(Size: Integer);
begin
  FreeSections;
  SetLength(FSections, Size);
end;

end.
