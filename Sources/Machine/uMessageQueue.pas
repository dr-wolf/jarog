unit uMessageQueue;

interface

uses
  SyncObjs, uQueue;

const
  INF = $FFFFFFFF;

type
  TMessageSubject = (msCreatreThread, msShutdown);

type
  TMessage = packed record
    Subject: TMessageSubject;
    Params: array of TObject;
  end;

type
  TMessageQueue = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FMailboxFlag: TEvent;
    FMessages: TQueue<TMessage>;
  public
    property MailboxFlag: TEvent read FMailboxFlag;
    property Messages: TQueue<TMessage> read FMessages;
    procedure PostMessage(Subj: TMessageSubject; Params: array of TObject);
  end;

function MakeMessage(Subj: TMessageSubject; Params: array of TObject): TMessage;

implementation

function MakeMessage(Subj: TMessageSubject; Params: array of TObject): TMessage;
var
  i: Integer;
begin
  Result.Subject := Subj;
  SetLength(Result.Params, Length(Params));
  for i := 0 to High(Params) do
    Result.Params[i] := Params[i];
end;

{ TMessageQueue }

constructor TMessageQueue.Create;
begin
  FMailboxFlag := TEvent.Create(nil, True, False, 'mailbox-flag');
  FMessages := TQueue<TMessage>.Create;
end;

destructor TMessageQueue.Destroy;
begin
  FMessages.Free;
  FMailboxFlag.Free;
  inherited;
end;

procedure TMessageQueue.PostMessage(Subj: TMessageSubject; Params: array of TObject);
begin
  FMessages.Lock;
  FMessages.Push(MakeMessage(Subj, Params));
  FMailboxFlag.SetEvent;
  FMessages.Unlock;
end;

end.
