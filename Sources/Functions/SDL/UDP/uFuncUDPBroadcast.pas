unit uFuncUDPBroadcast;

interface

uses
  uFuncUDP, uFuncContext;

type
  TFuncUDPBroadcast = class(TFuncUDP)
  public
    constructor Create;
  private const
    P_PORT = 0;
    P_FILE = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Read bytes from file and sends them as a broadcast message.
    @type func(ip: string, port: int, file: int)
    @in port int # Remote port
    @in file int # File handle
    * }

implementation

uses SysUtils, Classes, BlckSock, uDataNumeric, uExceptions, uOpCode;

{ TFuncUDPBroadcast }

constructor TFuncUDPBroadcast.Create;
begin
  inherited Create([MakeInt, MakeInt]);
  FFuncUid := FID_UDP_BROADCAST;
end;

procedure TFuncUDPBroadcast.Execute(Context: TFuncContext);
var
  port: Int64;
  sock: TUDPBlockSocket;
  stream: TStream;
begin
  port := RetrievePort(P_PORT, Context);
  stream := RetrieveStream(P_FILE, Context);
  stream.Seek(0, soFromBeginning);
  sock := TUDPBlockSocket.Create;
  with sock do
  begin
    CreateSocket;
    EnableBroadcast(True);
    Connect(cBroadcast, IntToStr(port));
    SendStreamRaw(stream);
    if LastError <> 0 then
      raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataNumeric.Create(sock.LastError));
    CloseSocket;
    Free;
  end;
end;

end.
