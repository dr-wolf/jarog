unit uFuncStrSub;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncStrSub = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_TEXT = 0;
    P_POS = 1;
    P_LEN = 2;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Returns a substring from a string by given position and length.
    @type func(text: string, pos: int, len: int -> string)
    @in text string # Initial text
    @in pos int # Position of a substring
    @in len int # Length of a substring
    @out - string # Substring
    * }

implementation

uses uDataString, uDataNumeric, uOpCode;

{ TFuncStrSub }

constructor TFuncStrSub.Create;
begin
  inherited Create([MakeString, MakeInt, MakeInt], MakeString);
  FFuncUid := FID_STR_SUB;
end;

procedure TFuncStrSub.Execute(Context: TFuncContext);
begin
  Return(TDataString.Create(Copy(AsString(Context.Data[P_TEXT]), AsInt(Context.Data[P_POS]) + 1,
    AsInt(Context.Data[P_LEN]))));
end;

end.
