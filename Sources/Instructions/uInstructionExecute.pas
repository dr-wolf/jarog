unit uInstructionExecute;

interface

uses
  uArray, uInstruction, uExpression, uOp;

type
  TInstructionExecute = class(TInstruction)
  public
    constructor Create(Expression: TExpression; Async: Boolean);
    destructor Destroy; override;
  private
    FExpression: TExpression;
    FAsync: Boolean;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uExceptions, uTypeVoid, uOpRun, uOpClean;

{ TInstructionExecute }

constructor TInstructionExecute.Create(Expression: TExpression; Async: Boolean);
begin
  if not(Expression.ResultType is TTypeVoid) then
    raise ECodeException.Create(E_EXPECTED_STATEMENT);
  inherited Create;
  FExpression := Expression;
  FAsync := Async;
end;

function TInstructionExecute.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FExpression.Compile);
  if FAsync then
  begin
    Result[High(Result)].Free;
    Result[High(Result)] := TOpRun.Create;
  end;
  AppendOp(Result, [TOpClean.Create]);
end;

destructor TInstructionExecute.Destroy;
begin
  FExpression.Free;
  inherited;
end;

end.
