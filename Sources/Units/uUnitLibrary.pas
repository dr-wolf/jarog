unit uUnitLibrary;

interface

uses
  uDict, uConfig, uUnit, uAppMold;

type
  TUnitLibrary = class(TObject)
  public
    constructor Create(Config: TConfig);
    destructor Destroy; override;
  private
    FUnits: TDict<TUnit>;
    FConfig: TConfig;
    FLinesProcessed: Integer;
    function LocateFile(const BasePath, Path: string): string;
  public
    property LinesProcessed: Integer read FLinesProcessed;
    function Load(Path: string; AppMold: TAppMold; const BasePath: string = ''): TUnit;
  end;

implementation

uses SysUtils, uArray, uExceptions, uUnitBuilder, uUnitBit, uUnitDate, uUnitFile, uUnitGC, uUnitFS, uUnitIO, uUnitOS,
  uUnitProc, uUnitRex, uUnitStr, uUnitMath, uUnitTCP, uUnitUDP;

function IsRelativePath(const Path: string): Boolean;
var
  L: Integer;
begin
  L := Length(Path);
  Result := (L > 0) and (Path[1] <> PathDelim)
{$IFDEF MSWINDOWS} and (L > 1) and (Path[2] <> ':'){$ENDIF MSWINDOWS};
end;

{ TUnitLibrary }

constructor TUnitLibrary.Create(Config: TConfig);
begin
  FUnits := TDict<TUnit>.Create;
  FConfig := Config;
  FLinesProcessed := 0;
end;

destructor TUnitLibrary.Destroy;
var
  k: TArray<string>;
  i: Integer;
begin
  k := FUnits.Keys;
  for i := 0 to High(k) do
    FUnits[k[i]].Free;
  FUnits.Free;
  inherited;
end;

function TUnitLibrary.Load(Path: string; AppMold: TAppMold; const BasePath: string = ''): TUnit;

type
  TUID = (uiBit, uiDate, uiFile, uiGC, uiFS, uiIO, uiMath, uiOS, uiProc, uiRex, uiStr, uiTCP, uiUDP, uiErr);

  function UnitID(const Script: string): TUID;
  const
    UNITS: array [TUID] of string = ('<bit>.jr', '<date>.jr', '<file>.jr', '<gc>.jr', '<fs>.jr', '<io>.jr', '<math>.jr',
      '<os>.jr', '<proc>.jr', '<rex>.jr', '<str>.jr', '<tcp>.jr', '<udp>.jr', '');
  begin
    Result := Low(TUID);
    while (Result < High(TUID)) and (UNITS[Result] <> Script) do
      Result := Succ(Result);
  end;

  function CreateBuiltIn(U: TUID): TUnit;
  begin
    case U of
      uiBit:
        Result := TUnitBit.Create;
      uiDate:
        Result := TUnitDate.Create;
      uiFile:
        Result := TUnitFile.Create;
      uiGC:
        Result := TUnitGC.Create;
      uiFS:
        Result := TUnitFS.Create;
      uiIO:
        Result := TUnitIO.Create;
      uiMath:
        Result := TUnitMath.Create;
      uiOS:
        Result := TUnitOS.Create;
      uiProc:
        Result := TUnitProc.Create;
      uiRex:
        Result := TUnitRex.Create;
      uiStr:
        Result := TUnitStr.Create;
      uiTCP:
        Result := TUnitTCP.Create;
      uiUDP:
        Result := TUnitUDP.Create;
    else
      Result := nil;
    end;
  end;

var
  uid: TUID;

begin
  uid := UnitID(Path);
  if uid = uiErr then
    Path := LocateFile(BasePath, Path);
  if FUnits.Contains(Path) then
  begin
    Result := FUnits[Path];
    if Result = nil then
      raise ECodeException.Create(E_CIRCULAR_IMPORT);
  end else begin
    if uid <> uiErr then
      Result := CreateBuiltIn(uid)
    else
    begin
      FUnits.Put(Path, nil);
      with TUnitBuilder.Create(Path, Self, AppMold) do
      begin
        FUnits.Put(Path, Application);
        Result := Application;
        Inc(FLinesProcessed, LinesCount);
        Free;
      end;
      Writeln('Loaded ' + Path);
    end;
  end;
end;

function TUnitLibrary.LocateFile(const BasePath, Path: string): string;
var
  i: Integer;
begin
  if not IsRelativePath(Path) then
    Result := Path
  else if FileExists(BasePath + Path) then
    Result := ExpandFileName(BasePath + Path)
  else
  begin
    for i := 0 to FConfig.LibPathCount - 1 do
      if FileExists(FConfig.LibPath[i] + Path) then
      begin
        Result := ExpandFileName(FConfig.LibPath[i] + Path);
        Exit;
      end;
    raise ECodeException.Create(E_MISSING_UNIT, Path);
  end;
end;

end.
