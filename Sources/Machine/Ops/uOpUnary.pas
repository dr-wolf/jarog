unit uOpUnary;

interface

uses
  uOperations, uOp, uBytecode;

type
  TOpUnary = class(TOp)
  public
    constructor Create(Operation: TUnaryOp);
  private
    FOperation: TUnaryOp;
  public
    procedure Serialize(var Code: TBytecode); override;
  end;

implementation

uses uOpCode;

{ TOpUnary }

constructor TOpUnary.Create(Operation: TUnaryOp);
begin
  inherited Create(OID_UNARY);
  FOperation := Operation;
end;

procedure TOpUnary.Serialize(var Code: TBytecode);
begin
  Code.Op := CompileOp();
  Code.SParam := Byte(FOperation);
end;

end.
