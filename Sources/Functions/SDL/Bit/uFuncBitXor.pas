unit uFuncBitXor;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncBitXor = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_A = 0;
    P_B = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Performs the logical exclusive OR operation on each pair of corresponding bits.
    @type func(a: int, b: int -> int)
    @in a int # Left value
    @in b int # Rignt value
    @out - int # Result of exclusive conjuction
    * }

implementation

uses uDataNumeric, uNumeric, uOpCode;

{ TFuncBitXor }

constructor TFuncBitXor.Create;
begin
  inherited Create([MakeInt, MakeInt], MakeInt);
  FFuncUid := FID_BIT_XOR;
end;

procedure TFuncBitXor.Execute(Context: TFuncContext);
begin
  Return(TDataNumeric.Create(AsNumeric(Context.Data[P_A]).Bit(boXor, AsNumeric(Context.Data[P_B]))));
end;

end.
