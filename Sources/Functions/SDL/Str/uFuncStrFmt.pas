unit uFuncStrFmt;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncStrFmt = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_FORMAT = 0;
    P_VALUES = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Converts float value into string using given string template.
    @type func(format: string, values: list(any) -> string)
    @in format string # Format template
    @in values list(any) # List of values
    @out - string # Formatted value
    * }

implementation

uses SysUtils, uDataNumeric, uDataString, uOpCode, uFormatTemplate;

{ TFuncStrFmt }

constructor TFuncStrFmt.Create;
begin
  inherited Create([MakeString, MakeList(MakeAny)], MakeString);
  FFuncUid := FID_STR_FMT;
end;

procedure TFuncStrFmt.Execute(Context: TFuncContext);

  function Chop(p: Integer; var s: string): string;
  begin
    Result := Copy(s, 1, p);
    Delete(s, 1, p);
  end;

  function ChopStr(var s: string): string;
  var
    p: Integer;
  begin
    p := Pos('%', s + '%');
    Result := Chop(p - 1, s);
    Delete(s, 1, 1);
  end;

  function ParseFormat(var s: string; var tf: TFormatTemplate): Boolean;

    function ChopNmb(var s: string): string;
    var
      p: Integer;
    begin
      p := 1;
      while (p <= Length(s)) and (s[p] in ['0' .. '9']) do
        Inc(p);
      Result := Chop(p - 1, s);
    end;

    function ChopSmb(var s: string; c: Char): Boolean;
    begin
      Result := (s <> '') and (s[1] = c);
      if Result then
        Delete(s, 1, 1);
    end;

  var
    bckp, p: string;
  begin
    bckp := s;
    p := ChopNmb(s);
    if ChopSmb(s, ':') then
    begin
      tf.ItemIndex := StrToInt(p);
      p := '';
    end;

    tf.RightPadded := ChopSmb(s, '-');

    if p = '' then
      p := ChopNmb(s);
    tf.Width := StrToIntDef(p, 0);

    if ChopSmb(s, '.') then
      tf.Precision := StrToIntDef(ChopNmb(s), 0);

    Result := (s <> '') and (s[1] in FORMAT_SYMBOLS_SET);
    if Result then
    begin
      tf.Format := DetectFormat(s[1]);
      Delete(s, 1, 1);
    end
    else
      s := bckp;
  end;

var
  i: Integer;
  f, r: string;
  ft: TFormatTemplate;
begin
  f := Copy(AsString(Context.Data[P_FORMAT]), 1);
  r := '';
  i := 0;
  while f <> '' do
  begin
    r := r + ChopStr(f);
    if f = '' then
      Break;
    if f[1] = '%' then
    begin
      r := r + '%';
      Delete(f, 1, 1);
      Continue;
    end;
    ft.ItemIndex := i;
    Inc(i);
    if ParseFormat(f, ft) then
      case ft.Format of
        ofDecimal, ofScientific, ofFixed, ofHexadecimal:
          r := r + AsNumeric(Context.Data[P_VALUES].Item(ft.ItemIndex)).ToText(ft);
        ofString:
          r := r + Format(GenericFormat(ft), [AsString(Context.Data[P_VALUES].Item(ft.ItemIndex))]);
      end
    else
      r := r + '%';
  end;
  Return(TDataString.Create(r));
end;

end.
