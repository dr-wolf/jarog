unit uInstructionControl;

interface

uses
  uArray, uInstruction, uOp;

type
  TInstructionControl = class(TInstruction)
  public
    constructor Create(DoBreak: Boolean);
    destructor Destroy; override;
  private
    FDoBreak: Boolean;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uTag, uDataStack, uOpJump, uOpPop, uOpPush;

{ TInstructionControl }

constructor TInstructionControl.Create(DoBreak: Boolean);
begin
  inherited Create;
  FDoBreak := DoBreak;
end;

destructor TInstructionControl.Destroy;
begin
  inherited;
end;

function TInstructionControl.Compile: TArray<TOp>;
type
  TTagPos = record
    Tag: ITag;
    Pos: Integer;
  end;
var
  b: string;
  p: TInstruction;
  t: TArray<TTagPos>;
  i: Integer;
begin
  if FDoBreak then
    b := TAG_LOOP_END
  else
    b := TAG_LOOP_BEGIN;

  SetLength(Result, 0);

  p := FParent;
  while (p.Tags[b] = nil) do
  begin
    if p.Tags[TAG_CATCH] <> nil then
      AppendOp(Result, [TOpPop.Create(ftException), TOpPop.Create(ftCatch)])
    else if p.Tags[TAG_FINALLY] <> nil then
    begin
      SetLength(t, Length(t) + 1);
      t[High(t)].Tag := TTag.Create;
      AppendOp(Result, [TOpPop.Create(ftFinally), TOpPush.Create(ftFinallyReturn, t[High(t)].Tag),
        TOpJump.Create(0, p.Tags[TAG_FINALLY])]);
      t[High(t)].Pos := Length(Result);
    end;
    p := p.Parent;
  end;

  AppendOp(Result, [TOpJump.Create(0, p.Tags[b])]);

  for i := 0 to High(t) do
    Result[t[i].Pos].WithTag(t[i].Tag);
end;

end.
