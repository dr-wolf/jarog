unit uSocketRepository;

interface

uses
  BlckSock, uArray, uRepository;

type
  TSocketRepository<T: TSocksBlockSocket> = class(TRepository)
  public
    constructor Create;
  private type
    TSock = record
      Handle: Integer;
      Sock: T;
    end;
  protected
    FSocks: TArray<TSock>;
    function GetHandle(Id: Integer): Integer; override;
    function Size: Integer; override;
    procedure Release(Id: Integer); override;
    function GetSock(Handle: Integer): T;
  public
    property Sock[Handle: Integer]: T read GetSock;
    function Add(Socket: T): Integer;
  end;

implementation

{ TSocketRepository }

constructor TSocketRepository<T>.Create;
begin
  SetLength(FSocks, 0);
end;

function TSocketRepository<T>.GetHandle(Id: Integer): Integer;
begin
  Result := FSocks[Id].Handle;
end;

function TSocketRepository<T>.GetSock(Handle: Integer): T;
var
  I: Integer;
begin
  I := Find(Handle);
  if I > -1 then
    Result := FSocks[I].Sock
  else
    Result := default (T);
end;

procedure TSocketRepository<T>.Release(Id: Integer);
begin
  with FSocks[Id] do
  begin
    Sock.CloseSocket;
    Sock.Free;
  end;
  if Id < High(FSocks) then
    FSocks[Id] := FSocks[High(FSocks)];
  SetLength(FSocks, Length(FSocks) - 1);
end;

function TSocketRepository<T>.Size: Integer;
begin
  Result := Length(FSocks);
end;

function TSocketRepository<T>.Add(Socket: T): Integer;
var
  s: TSock;
begin
  s.Sock := Socket;
  s.Handle := Random(MaxInt);
  SetLength(FSocks, Length(FSocks) + 1);
  FSocks[High(FSocks)] := s;
  Result := s.Handle;
end;

end.
