unit uOp;

interface

uses
  uArray, uTag, uOpCode, uBytecode;

type
  TOp = class(TObject)
  public
    constructor Create(OpUid: TOID);
  private
    FOpUid: TOID;
    FTag: ITag;
  protected
    FIndex: Integer;
    FBreak: Boolean;
    function CompileOp(FlagA: Boolean = False; FlagB: Boolean = False): Byte;
  public
    property Break: Boolean read FBreak write FBreak;
    function WithTag(Tag: ITag): TOp;
    procedure SetIndex(Index: Integer);
    procedure Serialize(var Code: TBytecode); virtual;
  end;

type
  TTagPos = (tpAtFirst, tpAtLast);

function Int(I: Integer): string;
function DataAddress(I: Integer): string;
function CodeAddress(I: Integer): string;
function SignedInt(I: Integer): string;
function AttachTag(const A: TArray<TOp>; Tag: ITag; Pos: TTagPos): TArray<TOp>;
procedure AppendOp(var A: TArray<TOp>; B: array of TOp);

implementation

uses SysUtils;

function Int(I: Integer): string;
begin
  Result := IntToStr(I);
end;

function DataAddress(I: Integer): string;
begin
  Result := '[0x' + IntToHex(I, 4) + ']';
end;

function CodeAddress(I: Integer): string;
begin
  Result := '<0x' + IntToHex(I, 8) + '>';
end;

function SignedInt(I: Integer): string;
begin
  if I > 0 then
    Result := '+' + IntToStr(I)
  else
    Result := IntToStr(I);
end;

function AttachTag(const A: TArray<TOp>; Tag: ITag; Pos: TTagPos): TArray<TOp>;
begin
  if Length(A) > 0 then
    case Pos of
      tpAtFirst:
        A[0].WithTag(Tag);
      tpAtLast:
        A[High(A)].WithTag(Tag);
    end;
  Result := A;
end;

procedure AppendOp(var A: TArray<TOp>; B: array of TOp);
var
  S, I: Integer;
begin
  S := High(A) + 1;
  SetLength(A, Length(A) + Length(B));
  for I := 0 to High(B) do
    A[S + I] := B[I];
end;

{ TOp }

constructor TOp.Create(OpUid: TOID);
begin
  FOpUid := OpUid;
  FTag := nil;
  FBreak := False;
end;

function TOp.WithTag(Tag: ITag): TOp;
begin
  FTag := Tag;
  Result := Self;
end;

function TOp.CompileOp(FlagA: Boolean = False; FlagB: Boolean = False): Byte;
begin
  Result := FOpUid;
  if FlagA then
    Result := Result or $20;
  if FlagB then
    Result := Result or $40;
  if Break then
    Result := Result or $80;
end;

procedure TOp.Serialize(var Code: TBytecode);
begin
  Code.Op := CompileOp();
end;

procedure TOp.SetIndex(Index: Integer);
begin
  FIndex := Index;
  if FTag <> nil then
    FTag.SetAddress(Index);
end;

end.
