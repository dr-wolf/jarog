unit uInteger;

interface

uses
  uNumeric, uTypedStream, uFormatTemplate, mp_types;

type
  TInteger = class(TNumeric)
  public
    constructor Create(Value: Integer);
  protected
    FValue: Integer;
    { Addition }
    function AddInt(const V: Integer): TNumeric; override;
    function AddBigInt(const V: mp_int): TNumeric; override;
    function AddBigFloat(const V: mp_float): TNumeric; override;
    { Subtraction }
    function SubInt(const V: Integer): TNumeric; override;
    function SubBigInt(const V: mp_int): TNumeric; override;
    function SubBigFloat(const V: mp_float): TNumeric; override;
    { Multiplication }
    function MulInt(const V: Integer): TNumeric; override;
    function MulBigInt(const V: mp_int): TNumeric; override;
    function MulBigFloat(const V: mp_float): TNumeric; override;
    { Division }
    function DivInt(const V: Integer): TNumeric; override;
    function DivBigInt(const V: mp_int): TNumeric; override;
    function DivBigFloat(const V: mp_float): TNumeric; override;
    { Modulo }
    function ModInt(const V: Integer): TNumeric; override;
    function ModBigInt(const V: mp_int): TNumeric; override;
    function ModBigFloat(const V: mp_float): TNumeric; override;
    { Power }
    function PowInt(const V: Integer): TNumeric; override;
    function PowBigInt(const V: mp_int): TNumeric; override;
    function PowBigFloat(const V: mp_float): TNumeric; override;
    { Compare }
    function CmpInt(const V: Integer): Integer; override;
    function CmpBigInt(const V: mp_int): Integer; override;
    function CmpBigFloat(const V: mp_float): Integer; override;
    { Bit }
    function BitInt(const BitOp: TBitOp; const V: Integer): TNumeric; override;
    function BitBigInt(const BitOp: TBitOp; const V: mp_int): TNumeric; override;
    function BitBigFloat(const BitOp: TBitOp; const V: mp_float): TNumeric; override;
  public
    property Value: Integer read FValue;
    function IsZero: Boolean; override;

    function Dup(TypeExpand: Boolean): TNumeric; override;
    function Floor: TNumeric; override;
    function Frac: TNumeric; override;
    function Invert: TNumeric; override;
    function Module: TNumeric; override;
    function Negative: TNumeric; override;
    function Shift(Bits: Integer): TNumeric; override;

    function ToInt64: Int64; override;
    function ToUInt64: UInt64; override;
    function ToFloat: mp_float; override;
    function ToDecimal: string; override;
    function ToText(Format: TFormatTemplate): string; override;
    procedure Serialize(Stream: TTypedStream); override;
  end;

implementation

uses SysUtils, mp_base, mp_real, uBigInt, uBigFloat, uOpCode, uExceptions;

{ TInteger }

constructor TInteger.Create(Value: Integer);
begin
  FValue := Value;
end;

function TInteger.AddInt(const V: Integer): TNumeric;
var
  r: mp_int;
begin
  try
{$Q+}
    Result := TInteger.Create(FValue + V);
{$Q-}
  except
    mp_init_set_int(r, FValue);
    mp_add_int(r, V, r);
    Result := TBigInt.Create(r);
  end;
end;

function TInteger.AddBigInt(const V: mp_int): TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  mp_add_int(V, FValue, r);
  Result := ToNumeric(r);
end;

function TInteger.AddBigFloat(const V: mp_float): TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_add_int(V, FValue, r);
  Result := TBigFloat.Create(r);
end;

function TInteger.SubInt(const V: Integer): TNumeric;
var
  r: mp_int;
begin
  try
{$Q+}
    Result := TInteger.Create(FValue - V);
{$Q-}
  except
    mp_init_set_int(r, FValue);
    mp_sub_int(r, V, r);
    Result := TBigInt.Create(r);
  end;
end;

function TInteger.SubBigInt(const V: mp_int): TNumeric;
var
  r: mp_int;
begin
  mp_init_set_int(r, FValue);
  mp_sub(r, V, r);
  Result := ToNumeric(r);
end;

function TInteger.SubBigFloat(const V: mp_float): TNumeric;
var
  r: mp_float;
begin
  r := ToFloat;
  mpf_sub(r, V, r);
  Result := TBigFloat.Create(r);
end;

function TInteger.DivInt(const V: Integer): TNumeric;
begin
  Result := TInteger.Create(FValue div V);
end;

function TInteger.MulInt(const V: Integer): TNumeric;
var
  r: mp_int;
begin
  try
{$Q+}
    Result := TInteger.Create(FValue * V);
{$Q-}
  except
    mp_init(r);
    mp_set_int(r, FValue);
    mp_mul_int(r, V, r);
    Result := TBigInt.Create(r);
  end;
end;

function TInteger.MulBigInt(const V: mp_int): TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  mp_mul_int(V, FValue, r);
  Result := TBigInt.Create(r);
end;

function TInteger.MulBigFloat(const V: mp_float): TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_mul_int(V, FValue, r);
  Result := TBigFloat.Create(r);
end;

function TInteger.DivBigInt(const V: mp_int): TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  mp_set_int(r, FValue);
  mp_div(r, V, r);
  Result := ToNumeric(r);
end;

function TInteger.DivBigFloat(const V: mp_float): TNumeric;
var
  r: mp_float;
begin
  r := ToFloat;
  mpf_div(r, V, r);
  Result := TBigFloat.Create(r);
end;

function TInteger.ModInt(const V: Integer): TNumeric;
begin
  Result := TInteger.Create(FValue mod V);
end;

function TInteger.ModBigInt(const V: mp_int): TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  mp_set_int(r, FValue);
  mp_mod(r, V, r);
  Result := ToNumeric(r);
end;

function TInteger.ModBigFloat(const V: mp_float): TNumeric;
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

function TInteger.PowInt(const V: Integer): TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  mp_set_pow(r, FValue, V);
  Result := ToNumeric(r);
end;

function TInteger.PowBigInt(const V: mp_int): TNumeric;
begin
  raise ERuntimeException.Create(E_INT_OVERFLOW, nil);
end;

function TInteger.PowBigFloat(const V: mp_float): TNumeric;
var
  s, r: mp_float;
begin
  mpf_init2(s, r);
  mpf_set_int(s, FValue);
  mpf_expt(s, V, r);
  mpf_clear(s);
  Result := TBigFloat.Create(r);
end;

function TInteger.BitInt(const BitOp: TBitOp; const V: Integer): TNumeric;
begin
  case BitOp of
    boAnd:
      Result := TInteger.Create(FValue and V);
    boOr:
      Result := TInteger.Create(FValue or V);
    boXor:
      Result := TInteger.Create(FValue xor V);
  else
    raise Exception.Create('Unsupported operation');
  end;
end;

function TInteger.BitBigInt(const BitOp: TBitOp; const V: mp_int): TNumeric;
var
  r, t: mp_int;
begin
  mp_init(r);
  mp_init_set_int(t, FValue);
  case BitOp of
    boAnd:
      mp_and(t, V, r);
    boOr:
      mp_or(t, V, r);
    boXor:
      mp_xor(t, V, r);
  end;
  mp_clear(t);
  Result := ToNumeric(r);
end;

function TInteger.BitBigFloat(const BitOp: TBitOp; const V: mp_float): TNumeric;
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

function TInteger.CmpInt(const V: Integer): Integer;
begin
  if FValue < V then
    Result := -1
  else if FValue > V then
    Result := 1
  else
    Result := 0;
end;

function TInteger.CmpBigInt(const V: mp_int): Integer;
begin
  Result := -mp_cmp_int(V, FValue);
end;

function TInteger.CmpBigFloat(const V: mp_float): Integer;
begin
  Result := -mpf_cmp_dbl(V, FValue);
end;

function TInteger.IsZero: Boolean;
begin
  Result := FValue = 0;
end;

function TInteger.Dup(TypeExpand: Boolean): TNumeric;
begin
  if TypeExpand then
    Result := TBigFloat.Create(ToFloat)
  else
    Result := TInteger.Create(FValue);
end;

function TInteger.Floor: TNumeric;
begin
  Result := TInteger.Create(FValue);
end;

function TInteger.Frac: TNumeric;
begin
  Result := TInteger.Create(0);
end;

function TInteger.Invert: TNumeric;
begin
  Result := TInteger.Create(not FValue);
end;

function TInteger.Module: TNumeric;
begin
  Result := TInteger.Create(Abs(FValue));
end;

function TInteger.Negative: TNumeric;
begin
  Result := TInteger.Create(-FValue);
end;

function TInteger.Shift(Bits: Integer): TNumeric;
var
  r: mp_int;
begin
  mp_init_set_int(r, FValue);
  if Bits > 0 then
    mp_shl(r, Bits, r)
  else
    mp_shr(r, Abs(Bits), r);
  Result := ToNumeric(r);
end;

function TInteger.ToDecimal: string;
begin
  Result := IntToStr(FValue);
end;

function TInteger.ToFloat: mp_float;
begin
  mpf_init(Result);
  mpf_set_int(Result, FValue);
end;

function TInteger.ToUInt64: UInt64;
begin
  Result := Abs(FValue);
end;

function TInteger.ToInt64: Int64;
begin
  Result := FValue;
end;

function TInteger.ToText(Format: TFormatTemplate): string;
var
  i: Integer;
begin
  case Format.Format of
    ofDecimal, ofFixed:
      Result := IntToStr(FValue);
    ofHexadecimal:
      begin
        Result := IntToHex(Abs(FValue), 8);
        while (Length(Result) > 1) and (Result[1] = '0') do
          Delete(Result, 1, 1);
        Result := '0x' + Result;
        if FValue < 0 then
          Result := '-' + Result;
      end;
  else
    raise Exception.Create('Unsuppoerted format for integer');
  end;

  if (Format.Format = ofFixed) and (Format.Precision > 0) then
  begin
    Result := Result + '.';
    for i := 1 to Format.Precision do
      Result := Result + '0';
  end;

  while Length(Result) < Format.Width do
    if Format.RightPadded then
      Result := Result + ' '
    else
      Result := ' ' + Result;
end;

procedure TInteger.Serialize(Stream: TTypedStream);
begin
  with Stream do
  begin
    WriteByte(DID_DATA_NUMERIC_INT);
    WriteInteger(FValue);
  end;
end;

end.
