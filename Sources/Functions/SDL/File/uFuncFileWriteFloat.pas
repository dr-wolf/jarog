unit uFuncFileWriteFloat;

interface

uses
  uFuncFile, uFuncContext;

type
  TFuncFileWriteFloat = class(TFuncFile)
  public
    constructor Create;
  private const
    P_HANDLE = 0;
    P_VALUE = 1;
    P_SIZE = 2;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Writes float value to file stream.
    @type func(handle: int, value: float, size: int)
    @in handle int # Handle of a stream
    @in value float # Value to be written
    @in size int # Precision, can be 4 or 8 bytes, see sz_float and sz_double constants
    * }

implementation

uses Classes, uDataNumeric, uOpCode, mp_types, mp_real;

{ TFuncFileWriteFloat }

constructor TFuncFileWriteFloat.Create;
begin
  inherited Create([MakeInt, MakeFloat, MakeInt]);
  FFuncUid := FID_FILE_WRITEFLOAT;
end;

procedure TFuncFileWriteFloat.Execute(Context: TFuncContext);
var
  s: TStream;
  l: Byte;
  f: mp_float;
  d: Double;
begin
  s := RetrieveStream(P_HANDLE, Context);
  l := RetrievePackSize(P_SIZE, [4, 8], Context);
  f := AsNumeric(Context.Data[P_VALUE]).ToFloat;
  d := mpf_todouble(f);
  s.WriteBuffer(d, l);
  mpf_clear(f);
end;

end.
