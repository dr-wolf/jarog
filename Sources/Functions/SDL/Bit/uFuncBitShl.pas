unit uFuncBitShl;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncBitShl = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_VALUE = 0;
    P_BITS = 1;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Shifts each bit of its first parameter to the left. The second parameter decides the number of bits the value is shifted.
    @type func(value: int, bits: int -> int)
    @in value int # Value
    @in bits int # Number of bits
    @out - int # Result value
    * }

implementation

uses SysUtils, uDataNumeric, uOpCode;

{ TFuncBitShl }

constructor TFuncBitShl.Create;
begin
  inherited Create([MakeInt, MakeInt], MakeInt);
  FFuncUid := FID_BIT_SHL;
end;

procedure TFuncBitShl.Execute(Context: TFuncContext);
var
  bits: Integer;
begin
  bits := AsInt(Context.Data[P_BITS]);
  if bits <= 0 then
    raise Exception.Create('Shift must be positive value');
  Return(TDataNumeric.Create(AsNumeric(Context.Data[P_VALUE]).Shift(bits)));
end;

end.
