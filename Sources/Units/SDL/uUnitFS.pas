unit uUnitFS;

interface

uses
  uUnit;

type
  TUnitFS = class(TUnit)
  public
    constructor Create;
  end;

  { *
    @descr Works with filesytem.
    @types tfilestat: (size: int, attr: int)
    @const fa_invalid # -1
    @const fa_readonly # 1
    @const fa_hidden # 2
    @const fa_sysfile # 4
    @const fa_directory # 16
    @const fa_archive # 32
    * }

implementation

uses uTypeFactory, uType, uValue, uDataNumeric, uFuncFSPwd, uFuncFSDelete, uFuncFSChDir, uFuncFSMkDir,
  uFuncFSRmDir, uFuncFSStat, uFuncFSAbsPath, uFuncFSScan, uFuncFSOpen, uFuncFSClose;

{ TUnitFS }

constructor TUnitFS.Create;
var
  t: TType;
begin
  inherited Create('<fs>');

  t := TypeFactory.GetPredefined(pdtFileAttr);

  AddType('tfilestat', t);

  AddValue('fa_invalid', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(-1)));
  AddValue('fa_readonly', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(1)));
  AddValue('fa_hidden', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(2)));
  AddValue('fa_sysfile', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(4)));
  AddValue('fa_directory', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(16)));
  AddValue('fa_archive', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(32)));

  AddValue('abspath', MakeFunc(TFuncFSAbsPath.Create, [TypeFactory.GetString], ['path'], TypeFactory.GetString));
  AddValue('chdir', MakeFunc(TFuncFSChDir.Create, [TypeFactory.GetString], ['path']));
  AddValue('close', MakeFunc(TFuncFSClose.Create, [TypeFactory.GetInt], ['handle']));
  AddValue('delete', MakeFunc(TFuncFSDelete.Create, [TypeFactory.GetString], ['path']));
  AddValue('mkdir', MakeFunc(TFuncFSMkDir.Create, [TypeFactory.GetString], ['path']));
  AddValue('open', MakeFunc(TFuncFSOpen.Create, [TypeFactory.GetString], ['filename'], TypeFactory.GetInt));
  AddValue('pwd', MakeFunc(TFuncFSPwd.Create, [], [], TypeFactory.GetString));
  AddValue('rmdir', MakeFunc(TFuncFSRmDir.Create, [TypeFactory.GetString], ['path']));
  AddValue('scan', MakeFunc(TFuncFSScan.Create, [TypeFactory.GetString], ['path'],
    TypeFactory.GetStruct([TypeFactory.GetList(TypeFactory.GetString), TypeFactory.GetList(TypeFactory.GetString)],
    ['dirs', 'files'])));
  AddValue('stat', MakeFunc(TFuncFSStat.Create, [TypeFactory.GetString], ['path'], t));
end;

end.
