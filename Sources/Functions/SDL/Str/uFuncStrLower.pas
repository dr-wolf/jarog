unit uFuncStrLower;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncStrLower = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_TEXT = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Converts string to lower case.
    @type func(text: string -> string)
    @in text string # Initial text
    @out - string # Text in lower case
    * }

implementation

uses Character, uDataString, uOpCode;

{ TFuncStrLower }

constructor TFuncStrLower.Create;
begin
  inherited Create([MakeString], MakeString);
  FFuncUid := FID_STR_LOWER;
end;

procedure TFuncStrLower.Execute(Context: TFuncContext);
begin
  Return(TDataString.Create(ToLower(AsString(Context.Data[P_TEXT]))));
end;

end.
