unit uFuncFSStat;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncFSStat = class(TStandardFunc)
  public
    constructor Create;
  private const
    P_PATH = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Returns file attributes and size.
    @type func(path: string -> tfilestat)
    @in path string # Initial path
    @out - tfilestat # File size and attributes
    * }

implementation

uses {$IFNDEF UNIX} Windows, {$ENDIF} SysUtils, uDataArray, uDataNumeric, uDataString, uOpCode;

function FileSize(const Path: string): Int64;
var
  f: THandle;
begin
  f := FileOpen(Path, fmOpenRead);
  Result := FileSeek(f, Int64(0), 2);
  FileClose(f);
end;

{ TFuncFSStat }

constructor TFuncFSStat.Create;
begin
  inherited Create([MakeString], MakeStruct([MakeInt, MakeInt]));
  FFuncUid := FID_FS_STAT;
end;

procedure TFuncFSStat.Execute(Context: TFuncContext);
var
  Path: string;
begin
  Path := ExpandFileName(AsString(Context.Data[P_PATH]));
  Return(TDataArray.Create([TDataNumeric.Create(FileSize(Path)), TDataNumeric.Create(FileGetAttr(Path))]));
end;

end.
