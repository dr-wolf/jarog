unit uFuncMathRand;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncMathRand = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Return random float value greater or equal 0 and less than 1.
    @type func(-> float)
    @out - float # Random value
    * }

implementation

uses uDataNumeric, uBigFloat, uOpCode;

{ TFuncMathRand }

constructor TFuncMathRand.Create;
begin
  inherited Create([], MakeFloat);
  FFuncUid := FID_MATH_RAND;
end;

procedure TFuncMathRand.Execute(Context: TFuncContext);
begin
  Return(TDataNumeric.Create(TBigFloat.Create));
end;

initialization

Randomize;

end.
