unit uBigFloat;

interface

uses
  uNumeric, uTypedStream, uFormatTemplate, mp_types;

type
  TBigFloat = class(TNumeric)
  public
    constructor Create(Value: mp_float); overload;
    constructor Create(Value: Double); overload;
    constructor Create; overload;
    destructor Destroy; override;
  protected
    FValue: mp_float;
    { Addition }
    function AddInt(const V: Integer): TNumeric; override;
    function AddBigInt(const V: mp_int): TNumeric; override;
    function AddBigFloat(const V: mp_float): TNumeric; override;
    { Subtraction }
    function SubInt(const V: Integer): TNumeric; override;
    function SubBigInt(const V: mp_int): TNumeric; override;
    function SubBigFloat(const V: mp_float): TNumeric; override;
    { Multiplication }
    function MulInt(const V: Integer): TNumeric; override;
    function MulBigInt(const V: mp_int): TNumeric; override;
    function MulBigFloat(const V: mp_float): TNumeric; override;
    { Division }
    function DivInt(const V: Integer): TNumeric; override;
    function DivBigInt(const V: mp_int): TNumeric; override;
    function DivBigFloat(const V: mp_float): TNumeric; override;
    { Modulo }
    function ModInt(const V: Integer): TNumeric; override;
    function ModBigInt(const V: mp_int): TNumeric; override;
    function ModBigFloat(const V: mp_float): TNumeric; override;
    { Power }
    function PowInt(const V: Integer): TNumeric; override;
    function PowBigInt(const V: mp_int): TNumeric; override;
    function PowBigFloat(const V: mp_float): TNumeric; override;
    { Compare }
    function CmpInt(const V: Integer): Integer; override;
    function CmpBigInt(const V: mp_int): Integer; override;
    function CmpBigFloat(const V: mp_float): Integer; override;
    { Bit }
    function BitInt(const BitOp: TBitOp; const V: Integer): TNumeric; override;
    function BitBigInt(const BitOp: TBitOp; const V: mp_int): TNumeric; override;
    function BitBigFloat(const BitOp: TBitOp; const V: mp_float): TNumeric; override;
  public
    property Value: mp_float read FValue;
    function IsZero: Boolean; override;

    function Dup(TypeExpand: Boolean): TNumeric; override;
    function Floor: TNumeric; override;
    function Frac: TNumeric; override;
    function Invert: TNumeric; override;
    function Module: TNumeric; override;
    function Negative: TNumeric; override;
    function Shift(Bits: Integer): TNumeric; override;

    function ToInt64: Int64; override;
    function ToUInt64: UInt64; override;
    function ToFloat: mp_float; override;
    function ToDecimal: string; override;
    function ToText(Format: TFormatTemplate): string; override;
    procedure Serialize(Stream: TTypedStream); override;
  end;

implementation

uses
  SysUtils, mp_base, mp_real, uOpCode, uExceptions;

{ TBigFloat }

constructor TBigFloat.Create(Value: mp_float);
begin
  FValue := Value;
end;

constructor TBigFloat.Create(Value: Double);
begin
  mpf_init(FValue);
  mpf_set_dbl(FValue, Value);
end;

constructor TBigFloat.Create;
begin
  mpf_init(FValue);
  mpf_random(FValue);
end;

destructor TBigFloat.Destroy;
begin
  mpf_clear(FValue);
  inherited;
end;

function TBigFloat.AddInt(const V: Integer): TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_add_int(FValue, V, r);
  Result := TBigFloat.Create(r);
end;

function TBigFloat.AddBigInt(const V: mp_int): TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_add_mpi(FValue, V, r);
  Result := TBigFloat.Create(r);
end;

function TBigFloat.AddBigFloat(const V: mp_float): TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_add(FValue, V, r);
  Result := TBigFloat.Create(r);
end;

function TBigFloat.SubInt(const V: Integer): TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_sub_int(FValue, V, r);
  Result := TBigFloat.Create(r);
end;

function TBigFloat.SubBigInt(const V: mp_int): TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_sub_mpi(FValue, V, r);
  Result := TBigFloat.Create(r);
end;

function TBigFloat.SubBigFloat(const V: mp_float): TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_sub(FValue, V, r);
  Result := TBigFloat.Create(r);
end;

function TBigFloat.MulInt(const V: Integer): TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_mul_int(FValue, V, r);
  Result := TBigFloat.Create(r);
end;

function TBigFloat.MulBigInt(const V: mp_int): TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_mul_mpi(FValue, V, r);
  Result := TBigFloat.Create(r);
end;

function TBigFloat.MulBigFloat(const V: mp_float): TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_mul(FValue, V, r);
  Result := TBigFloat.Create(r);
end;

function TBigFloat.DivInt(const V: Integer): TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_div_int(FValue, V, r);
  Result := TBigFloat.Create(r);
end;

function TBigFloat.DivBigInt(const V: mp_int): TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_div_mpi(FValue, V, r);
  Result := TBigFloat.Create(r);
end;

function TBigFloat.DivBigFloat(const V: mp_float): TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_div(FValue, V, r);
  Result := TBigFloat.Create(r);
end;

function TBigFloat.ModInt(const V: Integer): TNumeric;
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

function TBigFloat.ModBigInt(const V: mp_int): TNumeric;
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

function TBigFloat.ModBigFloat(const V: mp_float): TNumeric;
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

function TBigFloat.PowInt(const V: Integer): TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_expt_int(FValue, V, r);
  Result := TBigFloat.Create(r);
end;

function TBigFloat.PowBigInt(const V: mp_int): TNumeric;
var
  s, r: mp_float;
begin
  mpf_init2(r, s);
  mpf_set_mpi(s, V);
  mpf_expt(FValue, s, r);
  mpf_clear(s);
  Result := TBigFloat.Create(r);
end;

function TBigFloat.PowBigFloat(const V: mp_float): TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_expt(FValue, V, r);
  Result := TBigFloat.Create(r);
end;

function TBigFloat.BitInt(const BitOp: TBitOp; const V: Integer): TNumeric;
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

function TBigFloat.BitBigInt(const BitOp: TBitOp; const V: mp_int): TNumeric;
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

function TBigFloat.BitBigFloat(const BitOp: TBitOp; const V: mp_float): TNumeric;
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

function TBigFloat.CmpInt(const V: Integer): Integer;
begin
  Result := mpf_cmp_dbl(FValue, V);
end;

function TBigFloat.CmpBigInt(const V: mp_int): Integer;
var
  t: mp_float;
begin
  mpf_init(t);
  mpf_set_mpi(t, V);
  Result := mpf_cmp(FValue, t);
  mpf_clear(t);
end;

function TBigFloat.CmpBigFloat(const V: mp_float): Integer;
begin
  Result := mpf_cmp(FValue, V);
end;

function TBigFloat.IsZero: Boolean;
begin
  Result := mpf_is0(FValue);
end;

function TBigFloat.Dup(TypeExpand: Boolean): TNumeric;
begin
  Result := TBigFloat.Create(ToFloat);
end;

function TBigFloat.Floor: TNumeric;
var
  r: mp_int;
begin
  mp_init(r);
  mpf_trunc(FValue, r);
  Result := ToNumeric(r);
end;

function TBigFloat.Frac: TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_frac(FValue, r);
  Result := TBigFloat.Create(r);
end;

function TBigFloat.Invert: TNumeric;
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

function TBigFloat.Module: TNumeric;
var
  V: mp_float;
begin
  mpf_init(V);
  mpf_abs(FValue, V);
  Result := TBigFloat.Create(V);
end;

function TBigFloat.Negative: TNumeric;
var
  r: mp_float;
begin
  mpf_init(r);
  mpf_chs(FValue, r);
  Result := TBigFloat.Create(r);
end;

function TBigFloat.Shift(Bits: Integer): TNumeric;
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

function TBigFloat.ToFloat: mp_float;
begin
  mpf_init(Result);
  mpf_copy(FValue, Result);
end;

function TBigFloat.ToUInt64: UInt64;
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

function TBigFloat.ToInt64: Int64;
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

function TBigFloat.ToDecimal: string;
begin
  Result := string(mpf_adecimal_alt(FValue, 64));
end;

function TBigFloat.ToText(Format: TFormatTemplate): string;
var
  w: Integer;
  i: mp_int;
  f: mp_float;
begin
  case Format.Format of
    ofScientific:
      begin
        w := Format.Precision + 1;
        if w < 2 then
          w := 2;
        Result := string(mpf_adecimal(FValue, w));
      end;
    ofFixed:
      begin
        mp_init(i);
        mpf_init(f);
        mpf_trunc(FValue, i);
        mpf_frac(FValue, f);

        if Format.Precision > 0 then
        begin
          w := Format.Precision + 1;
          Result := string(mpf_adecimal_alt(f, w));
          Delete(Result, 1, 2);
          while Length(Result) < Format.Precision do
            Result := Result + '0';
        end;

        if Result <> '' then
          Result := string(mp_adecimal(i)) + '.' + Result
        else
          Result := string(mp_adecimal(i));

        mp_clear(i);
        mpf_clear(f);
      end
  else
    raise Exception.Create('Unsuppoerted format for float');
  end;

  while Length(Result) < Format.Width do
    if Format.RightPadded then
      Result := Result + ' '
    else
      Result := ' ' + Result;
end;

procedure TBigFloat.Serialize(Stream: TTypedStream);
var
  b: packed array of Byte;
begin
  with Stream do
  begin
    WriteByte(DID_DATA_NUMERIC_BIGFLT);

    WriteInteger(FValue.exponent);
    WriteInteger(FValue.bitprec);

    SetLength(b, mp_unsigned_bin_size(FValue.mantissa));
    mp_to_unsigned_bin_n(FValue.mantissa, b[0], Length(b));

    WriteInteger(Length(b));
    Write(b[0], Length(b));
    WriteByte(Byte(FValue.mantissa.sign = MP_NEG));
  end;
end;

end.
