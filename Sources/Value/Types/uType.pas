unit uType;

interface

uses
  uOperations, uData;

type
  TType = class(TObject)
  public
    constructor Create(const Hash: string);
  protected
    FHash: string;
  public
    property Hash: string read FHash;
    function Apply(Op: TBinaryOp; T: TType): TType; overload; virtual;
    function Apply(Op: TUnaryOp): TType; overload; virtual;
    function Call(T: TType): TType; virtual;
    function Compatible(T: TType; StrictCompat: Boolean = False): Boolean; virtual; abstract;
    function Pipeable: Boolean; virtual;
    function Default: TData; virtual; abstract;
    function NestedType(Field: string = ''): TType; virtual;
  end;

implementation

uses uExceptions;

{ TType }

constructor TType.Create(const Hash: string);
begin
  FHash := Hash;
end;

function TType.Apply(Op: TBinaryOp; T: TType): TType;
begin
  if not Compatible(T) then
    raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, FHash);
end;

function TType.Apply(Op: TUnaryOp): TType;
begin
  raise ECodeException.Create(E_UNSUPPORTED_OPERATION, FHash);
end;

function TType.Call(T: TType): TType;
begin
  raise ECodeException.Create(E_NOT_CALLABLE);
end;

function TType.NestedType(Field: string): TType;
begin
  raise ECodeException.Create(E_TYPE_ATOMIC);
end;

function TType.Pipeable: Boolean;
begin
  Result := True;
end;

end.
