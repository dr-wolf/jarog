unit uFuncFSPwd;

interface

uses
  uStandardFunc, uFuncContext;

type
  TFuncFSPwd = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Returns current directory.
    @type func(-> string)
    @out - string # Current directory
    * }

implementation

uses SysUtils, uDataString, uOpCode;

{ TFuncFSPwd }

constructor TFuncFSPwd.Create;
begin
  inherited Create([], MakeString);
  FFuncUid := FID_FS_PWD;
end;

procedure TFuncFSPwd.Execute(Context: TFuncContext);
begin
  Return(TDataString.Create(GetCurrentDir));
end;

end.
