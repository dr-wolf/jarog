unit uCoreContext;

interface

uses
  uConfig, uThreadSafe, uRepositoryStorage, uEnvironment, uIOProvider, uMessageQueue;

type
  TCoreContext = class(TThreadObject)
  public
    constructor Create(Config: TConfig; IOProvider: TIOProvider);
    destructor Destroy; override;
  private
    FConfig: TConfig;
    FRepositoryStorage: TRepositoryStorage;
    FEnvironment: TEnvironment;
    FIOProvider: TIOProvider;
    FMessageQueue: TMessageQueue;
  public
    property Config: TConfig read FConfig;
    property Env: TEnvironment read FEnvironment;
    property Repos: TRepositoryStorage read FRepositoryStorage;
    property IOProvider: TIOProvider read FIOProvider;
    property MessageQueue: TMessageQueue read FMessageQueue;
  end;

implementation

{ TCoreContext }

constructor TCoreContext.Create(Config: TConfig; IOProvider: TIOProvider);
begin
  inherited Create;

  FConfig := Config;
  FRepositoryStorage := TRepositoryStorage.Create;
  FEnvironment := TEnvironment.Create;
  FMessageQueue := TMessageQueue.Create;

  FIOProvider := IOProvider;
end;

destructor TCoreContext.Destroy;
begin
  FEnvironment.Free;
  FRepositoryStorage.Free;
  FMessageQueue.Free;
  FIOProvider.Free;
  inherited;
end;

end.
