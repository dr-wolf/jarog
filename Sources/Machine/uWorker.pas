unit uWorker;

interface

{$IFDEF UNIX}
{$LINKLIB libc.so}
{$ENDIF}

uses
  Classes, uState, uApp, uCoreContext;

type
  TRunMode = Integer;

const
  RM_RUN: TRunMode = $01;
  RM_STEP: TRunMode = $02;
  RM_STEPOVER: TRunMode = $03;
  RM_SUSPEND: TRunMode = $04;
  RM_STOP: TRunMode = $04;

type
  TWorker = class(TThread)
  public
    constructor Create(App: TApp; State: TState; CoreContext: TCoreContext; Suspend: Boolean);
    destructor Destroy; override;
  private
    FMode: TRunMode;
    FCallStackDepth: Integer;
    FApp: TApp;
    FState: TState;
    FCoreContext: TCoreContext;
    FDebug: Boolean;
  protected
    procedure Execute; override;
  public
    function CheckMode(Mode: TRunMode): Boolean;
    procedure Run(Mode: TRunMode);
    procedure SerializeState(Stream: TStream);
  end;

implementation

uses SysUtils, SyncObjs, uArray, uThreadSafe, uExceptions, uTypedStream, uFuncContext, uData, uDataArray, uDataBool,
  uDataFunc, uOperations, uDataNumeric, uDataStack, uDataSet, uDataPool, uDataPipe, uDataSection, uDataString,
  uIOProvider, uBytecode, uBytecodeExecutor, uMessageQueue, uStringConverter;

{ TWorker }

constructor TWorker.Create(App: TApp; State: TState; CoreContext: TCoreContext; Suspend: Boolean);
begin
  FMode := RM_RUN;

  FApp := App;
  FState := State;
  FCoreContext := CoreContext;

  FDebug := FCoreContext.Config.DebugPort > 0;

  while (FState.Stack.CallDepth = 0) and not FState.Completed do
    ExecuteCode(FApp[FState.Address], FState, FCoreContext);
  if Suspend then
    FApp.ToggleBreak(FState.Address);

  FreeOnTerminate := False;
  inherited Create(False);
end;

destructor TWorker.Destroy;
begin
  FState.Free;
  TDataPool.GetInstance.CleanUp(True);
  inherited;
end;

procedure TWorker.Execute;
var
  a: Integer;
begin
  while not FState.Completed do
    try
      if FDebug then
      begin
        if SafeCmp(FMode, RM_STEP) or (SafeCmp(FMode, RM_STEPOVER) and (FCallStackDepth >= FState.Stack.CallDepth)) or
          FApp[FState.Address].Break then
        begin
          SafeSet(FMode, RM_SUSPEND);
          FState.Event.WaitFor(INF);
          if SafeCmp(FMode, RM_STEPOVER) then
            FCallStackDepth := FState.Stack.CallDepth;
        end;
      end;

      ExecuteCode(FApp[FState.Address], FState, FCoreContext);
    except
      on e: Exception do
      begin
        while True do
          case FState.Stack.Frame of
            ftFunction:
              begin
                FState.Return;
                if FState.Completed then
                begin
                  FCoreContext.IOProvider.Log(e.Message);
                  Break;
                end;
              end;
            ftCatch, ftFinally:
              begin
                a := FState.Stack.Address;
                FState.Stack.DropFrame;
                FState.Stack.PushFrame(ftException);
                if e is ERuntimeException then
                  FState.Stack.Push((e as ERuntimeException).Data)
                else
                  FState.Stack.Push(TDataString.Create(e.Message));
                FState.Call(a, nil);
                Break;
              end;
          else
            FState.Stack.DropFrame;
          end;
      end;
    end;

  SafeSet(FMode, RM_STOP);
  FCoreContext.MessageQueue.PostMessage(msShutdown, [Self]);
  TDataPool.GetInstance.CleanUp(True);
  TDataPool.FreezeInstance;
end;

function TWorker.CheckMode(Mode: TRunMode): Boolean;
begin
  Result := SafeCmp(FMode, Mode);
end;

procedure TWorker.Run(Mode: TRunMode);
begin
  if Mode in [RM_RUN, RM_STEP, RM_STEPOVER] then
  begin
    SafeSet(FMode, Mode);
    FState.Event.SetEvent;
  end;
end;

procedure TWorker.SerializeState(Stream: TStream);
var
  cs: TTypedStream;
begin
  cs := TTypedStream.Create;
  if CheckMode(RM_SUSPEND) then
    FState.Serialize(cs)
  else
    cs.WriteInteger(-1);
  cs.SaveTo(Stream);
  cs.Free;
end;

end.
