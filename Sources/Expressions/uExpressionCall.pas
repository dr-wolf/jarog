unit uExpressionCall;

interface

uses
  uArray, uExpression, uOp, uDataSection;

type
  TExpressionCall = class(TExpression)
  public
    constructor Create(Data: TDataSection; Func: TExpression; Params: TExpression);
    destructor Destroy; override;
  private
    FFunc: TExpression;
    FParams: TExpression;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uTypeStruct, uExpressionStruct, uOpCall;

{ TExpressionCall }

constructor TExpressionCall.Create(Data: TDataSection; Func: TExpression; Params: TExpression);
begin
  inherited Create(Data);
  FFunc := Func;
  if Params.ResultType is TTypeStruct then
    FParams := Params
  else
    FParams := TExpressionStruct.Create(Data, [Params], []);
  FGroup := egMixed;
  FResultType := FFunc.ResultType.Call(FParams.ResultType);
end;

destructor TExpressionCall.Destroy;
begin
  FFunc.Free;
  FParams.Free;
  inherited;
end;

function TExpressionCall.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FParams.Compile);
  AppendOp(Result, FFunc.Compile);
  AppendOp(Result, [TOpCall.Create]);
end;

end.
