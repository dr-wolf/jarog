unit uIOProvider;

interface

uses
  Classes, uConsoleInterface, uThreadSafe, uQueue;

type
  TIOProvider = class(TThreadObject)
  public
    constructor Create(Console: IConsole; Debug: Boolean);
    destructor Destroy; override;
  private
    FConsole: IConsole;
    FLogBuffer: TQueue<string>;
  public
    function Key(WaitForInput: Boolean): Byte;
    procedure ClearScreen;
    procedure FlushLogs(Stream: TStream);
    procedure GetCursorPos(var X, Y: Integer);
    procedure GetScreenSize(var X, Y: Integer);
    procedure Log(const Text: string);
    procedure Read(var Text: string);
    procedure SetCursorPos(X, Y: Integer);
    procedure SetColor(Text, Background: Byte);
    procedure Write(const Text: string);
  end;

implementation

uses SyncObjs, uTypedStream;

{ TIOProvider }

constructor TIOProvider.Create(Console: IConsole; Debug: Boolean);
begin
  inherited Create;
  FConsole := Console;
  if Debug then
    FLogBuffer := TQueue<string>.Create;
end;

destructor TIOProvider.Destroy;
begin
  if FLogBuffer <> nil then
    FLogBuffer.Free;
  inherited;
end;

procedure TIOProvider.ClearScreen;
begin
  Lock;
  FConsole.Clear;
  Unlock;
end;

procedure TIOProvider.FlushLogs(Stream: TStream);
begin
  Lock;
  try
    with TTypedStream.Create do
    begin
      if FLogBuffer = nil then
        WriteInteger(0)
      else
      begin
        WriteInteger(FLogBuffer.Count);
        while FLogBuffer.Count > 0 do
          WriteString(FLogBuffer.Pop);
      end;
      SaveTo(Stream);
      Free;
    end;
  finally
    Unlock;
  end;
end;

procedure TIOProvider.GetCursorPos(var X, Y: Integer);
begin
  Lock;
  FConsole.GetCursor(X, Y);
  Unlock;
end;

procedure TIOProvider.GetScreenSize(var X, Y: Integer);
begin
  Lock;
  FConsole.GetSize(X, Y);
  Unlock;
end;

function TIOProvider.Key(WaitForInput: Boolean): Byte;
begin
  Lock;
  Result := FConsole.Key(WaitForInput);
  Unlock;
end;

procedure TIOProvider.Log(const Text: string);
begin
  Lock;
  if FLogBuffer <> nil then
  begin
    FLogBuffer.Lock;
    FLogBuffer.Push(Text);
    FLogBuffer.Unlock;
  end
  else
    FConsole.Print(Text + #$A);
  Unlock;
end;

procedure TIOProvider.Read(var Text: string);
begin
  Lock;
  Text := FConsole.Read;
  Unlock;
end;

procedure TIOProvider.SetColor(Text, Background: Byte);
begin
  Lock;
  FConsole.SetColor(Text, Background);
  Unlock;
end;

procedure TIOProvider.SetCursorPos(X, Y: Integer);
begin
  Lock;
  FConsole.SetCursor(X, Y);
  Unlock;
end;

procedure TIOProvider.Write(const Text: string);
begin
  Lock;
  FConsole.Print(Text);
  Unlock;
end;

end.
