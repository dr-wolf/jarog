unit uFuncFileGetPos;

interface

uses
  uFuncFile, uFuncContext;

type
  TFuncFileGetPos = class(TFuncFile)
  public
    constructor Create;
  private const
    P_HANDLE = 0;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Returns current position in file stream.
    @type func(handle: int -> int)
    @in handle int # Handle of a stream
    @out - int # Current position
    * }

implementation

uses Classes, uStandardFunc, uDataNumeric, uOpCode;

{ TFuncFileGetPos }

constructor TFuncFileGetPos.Create;
begin
  inherited Create([MakeInt], MakeInt);
  FFuncUid := FID_FILE_GETPOS;
end;

procedure TFuncFileGetPos.Execute(Context: TFuncContext);
var
  s: TStream;
begin
  s := RetrieveStream(P_HANDLE, Context);
  Return(TDataNumeric.Create(s.Position));
end;

end.
