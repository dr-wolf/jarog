unit uOpLog;

interface

uses
  uOp, uBytecode;

type
  TOpLog = class(TOp)
  public
    constructor Create(TypeHintId: Integer);
  private
    FTypeHintId: Integer;
  public
    procedure Serialize(var Code: TBytecode); override;
  end;

implementation

uses uOpCode;

{ TOpLog }

constructor TOpLog.Create(TypeHintId: Integer);
begin
  inherited Create(OID_LOG);
  FTypeHintId := TypeHintId;
end;

procedure TOpLog.Serialize(var Code: TBytecode);
begin
  Code.Op := CompileOp();
  Code.LParam := FTypeHintId;
end;

end.
