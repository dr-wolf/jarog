unit uOpItem;

interface

uses
  uOp;

type
  TOpItem = class(TOp)
  public
    constructor Create;
  end;

implementation

uses uOpCode;

{ TOpItem }

constructor TOpItem.Create;
begin
  inherited Create(OID_ITEM);
end;

end.
