unit uInstructionDebug;

interface

uses
  uArray, uInstruction, uOp;

type
  TInstructionDebug = class(TInstruction)
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uOpNop;

{ TInstructionDebug }

function TInstructionDebug.Compile: TArray<TOp>;
begin
  SetLength(Result, 1);
  Result[0] := TOpNop.Create;
end;

end.
