unit uExpressionCallNamed;

interface

uses
  uArray, uStringSet, uExpression, uOp, uDataSection;

type
  TExpressionCallNamed = class(TExpression)
  public
    constructor Create(Data: TDataSection; Func: TExpression; Params: array of TExpression; Names: TStringSet);
    destructor Destroy; override;
  private
    FFunc: TExpression;
    FParams: TArray<TExpression>;
    FParamMap: TArray<Integer>;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uTypeFactory, uType, uTypeStruct, uTypeFunc, uOpStack, uOpCall, uOpLoad, uDataStub;

{ TExpressionCallNamed }

constructor TExpressionCallNamed.Create(Data: TDataSection; Func: TExpression; Params: array of TExpression;
  Names: TStringSet);
var
  i: Integer;
  t: TArray<TType>;
  n: TArray<string>;
begin
  inherited Create(Data);
  FFunc := Func;
  SetLength(FParams, Length(Params));
  for i := 0 to High(Params) do
    FParams[i] := Params[i];
  FGroup := egMixed;

  SetLength(t, Length(FParams));
  for i := 0 to High(t) do
    t[i] := FParams[i].ResultType;

  if Names <> nil then
    FResultType := FFunc.ResultType.Call(TypeFactory.GetStruct(t, Names.AsArray))
  else
    FResultType := FFunc.ResultType.Call(TypeFactory.GetStruct(t, []));

  n := ((Func.ResultType as TTypeFunc).Params as TTypeStruct).Fields;
  SetLength(FParamMap, Length(n));
  for i := 0 to High(FParamMap) do
    if Names <> nil then
      FParamMap[i] := Names.IndexOf(n[i])
    else
      FParamMap[i] := -1;
end;

destructor TExpressionCallNamed.Destroy;
var
  i: Integer;
begin
  FFunc.Free;
  for i := 0 to High(FParams) do
    FParams[i].Free;
  inherited;
end;

function TExpressionCallNamed.Compile: TArray<TOp>;
var
  i: Integer;
begin
  SetLength(Result, 0);
  for i := High(FParamMap) downto 0 do
    if FParamMap[i] < 0 then
      AppendOp(Result, [TOpLoad.Create(FData.Keep(TDataStub.Create), False)])
    else
      AppendOp(Result, FParams[FParamMap[i]].Compile);
  AppendOp(Result, [TOpStack.Create(atStruct, Length(FParamMap))]);
  AppendOp(Result, FFunc.Compile);
  AppendOp(Result, [TOpCall.Create]);
end;

end.
