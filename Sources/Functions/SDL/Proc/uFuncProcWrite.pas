unit uFuncProcWrite;

interface

uses
  uFuncProc, uFuncContext;

type
  TFuncProcWrite = class(TFuncProc)
  public
    constructor Create;
  private const
    P_PROC = 0;
    P_FILE = 1;
    P_SIZE = 2;
  protected
    procedure Execute(Context: TFuncContext); override;
  end;

  { *
    @descr Writes data to process stdin.
    @type func(proc: int, file: int, bytes: int)
    @in proc int # Process handle
    @in file int # File handle
    @in size int # Amount of bytes to be written
    * }

implementation

uses ChildProcess, Classes, uDataNumeric, uOpCode;

{ TFuncProcWrite }

constructor TFuncProcWrite.Create;
begin
  inherited Create([MakeInt, MakeInt, MakeInt]);
  FFuncUid := FID_PROC_WRITE;
end;

procedure TFuncProcWrite.Execute(Context: TFuncContext);
var
  p: TChildProcess;
  f: TStream;
  s: Integer;
begin
  p := RetrieveProcess(P_PROC, Context);
  f := RetrieveStream(P_FILE, Context);
  s := AsInt(Context.Data[P_SIZE]);
  if s > f.Size - f.Position then
    s := f.Size - f.Position;
  p.SendData(f, s);
end;

end.
