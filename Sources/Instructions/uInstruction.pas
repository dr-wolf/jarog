unit uInstruction;

interface

uses
  uArray, uOp, uDict, uTag;

const
  TAG_LOOP_BEGIN = 'LOOP_BEGIN';
  TAG_LOOP_END = 'LOOP_END';
  TAG_FINALLY = 'FINALLY';
  TAG_CATCH = 'CATCH';
  TAG_CATCH_END = 'CATCH_END';


type
  TInstruction = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FTags: TDict<ITag>;
  protected
    FParent: TInstruction;
    function GetTag(Name: string): ITag;
    procedure InitTag(Name: string);
  public
    property Tags[Name: string]: ITag read GetTag;
    property Parent: TInstruction read FParent;
    function Compile: TArray<TOp>; virtual; abstract;
    procedure SetParent(Instruction: TInstruction);
  end;

implementation

{ TInstruction }

constructor TInstruction.Create;
begin
  FTags := TDict<ITag>.Create;
end;

destructor TInstruction.Destroy;
begin
  FTags.Free;
  inherited;
end;

function TInstruction.GetTag(Name: string): ITag;
begin
  if FTags.Contains(Name) then
    Result := FTags[Name]
  else
    Result := nil;
end;

procedure TInstruction.InitTag(Name: string);
begin
  FTags.Put(Name, TTag.Create);
end;

procedure TInstruction.SetParent(Instruction: TInstruction);
begin
  FParent := Instruction;
end;

end.
