unit uOpEmpty;

interface

uses
  uOp;

type
  TOpEmpty = class(TOp)
  public
    constructor Create;
  end;

implementation

uses uOpCode;

{ TOpEmpty }

constructor TOpEmpty.Create;
begin
  inherited Create(OID_EMPTY);
end;

end.
