unit uTreeAssembler;

interface

uses
  uExpression, uOperations, uDataSection;

type
  ITreeAssembler = interface(IInterface)
    function Attach(Expression: TExpression; Operation: TBinaryOp): ITreeAssembler;
    function Assemble(Data: TDataSection): TExpression;
  end;

function MakeAssembler(Expression: TExpression): ITreeAssembler;

implementation

uses uExpressionBinary;

type
  TTreeNode = class(TInterfacedObject, ITreeAssembler)
  public
    constructor Create(Left, Right: ITreeAssembler; Operation: TBinaryOp);
  private
    FLeft: ITreeAssembler;
    FRight: ITreeAssembler;
    FOperation: TBinaryOp;
  public
    function Attach(Expression: TExpression; Operation: TBinaryOp): ITreeAssembler;
    function Assemble(Data: TDataSection): TExpression;
  end;

  TTreeLeaf = class(TInterfacedObject, ITreeAssembler)
  public
    constructor Create(Expression: TExpression);
  private
    FLeaf: TExpression;
  public
    function Attach(Expression: TExpression; Operation: TBinaryOp): ITreeAssembler;
    function Assemble(Data: TDataSection): TExpression;
  end;

function MakeAssembler(Expression: TExpression): ITreeAssembler;
begin
  Result := TTreeLeaf.Create(Expression);
end;

{ TTreeNode }

constructor TTreeNode.Create(Left, Right: ITreeAssembler; Operation: TBinaryOp);
begin
  FLeft := Left;
  FRight := Right;
  FOperation := Operation;
end;

function TTreeNode.Attach(Expression: TExpression; Operation: TBinaryOp): ITreeAssembler;
begin
  if OperationPriority(FOperation) >= OperationPriority(Operation) then
    Result := TTreeNode.Create(Self, TTreeLeaf.Create(Expression), Operation)
  else
  begin
    FRight := FRight.Attach(Expression, Operation);
    Result := Self;
  end;
end;

function TTreeNode.Assemble(Data: TDataSection): TExpression;
begin
  Result := TExpressionBinary.Create(Data, FLeft.Assemble(Data), FRight.Assemble(Data), FOperation);
end;

{ TTreeLeaf }

constructor TTreeLeaf.Create(Expression: TExpression);
begin
  FLeaf := Expression;
end;

function TTreeLeaf.Assemble(Data: TDataSection): TExpression;
begin
  Result := FLeaf;
end;

function TTreeLeaf.Attach(Expression: TExpression; Operation: TBinaryOp): ITreeAssembler;
begin
  Result := TTreeNode.Create(Self, TTreeLeaf.Create(Expression), Operation);
end;

end.
